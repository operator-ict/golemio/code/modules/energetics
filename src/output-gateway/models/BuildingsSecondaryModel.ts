import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISecondaryBuildings } from "./interfaces/ISecondaryBuildings";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { Point } from "@golemio/core/dist/shared/geojson";

export class BuildingsSecondaryModel extends Model<ISecondaryBuildings> implements ISecondaryBuildings {
    public static tableName = "consumption_energy_buildings_secondary";
    public static modelName = "BuildingsSecondaryModelData";

    declare id: number;
    declare building_address_code: string;
    declare building_label: string;
    declare current_note: string;
    declare method_of_protection: string;
    declare built_up_area: number;
    declare heated_bulding_volume: number;
    declare students_count: number;
    declare employees_count: number;
    declare classrooms_count: number;
    declare beds_count: number;
    declare eno_id: string;
    declare csu_code: string;
    declare ku_code: string;
    declare allotment_number: string;
    declare registration_unit: string;
    declare energetic_management: string;
    declare opening_hours: string;
    declare weekend_opening_hours: string;
    declare latitude: string;
    declare longitude: string;
    declare location: Point;
    declare address_street: string;
    declare address_house_number: string;
    declare address_city: string;
    declare address_country: string;
    declare address_mail: string;
    declare address_phone: string;
    declare address_web_address: string;
    declare penb_penbnumber: number;
    declare penb_issue_date: string;
    declare penb_total_building_envelope_area: number;
    declare penb_volume_factor_of_avshape: string;
    declare penb_total_energy_reference_area: number;
    declare penb_total_provided_energy: number;
    declare penb_total_provided_energy_category: string;
    declare penb_primary_non_renewable_energy: number;
    declare penb_primary_non_renewable_energy_category: string;
    declare penb_building_envelope: number;
    declare penb_building_envelope_category: string;
    declare penb_heating: number;
    declare penb_heating_category: string;
    declare penb_cooling: number;
    declare penb_cooling_category: string;
    declare penb_ventilation: number;
    declare penb_ventilation_category: string;
    declare penb_humidity_adjustment: number;
    declare penb_humidity_adjustment_category: string;
    declare penb_warm_water: number;
    declare penb_warm_water_category: string;
    declare penb_lighting: number;
    declare penb_lighting_category: string;
    declare energy_audits_energy_audit: string;
    declare energy_audits_earegistration_number: string;
    declare energy_audits_created_at: string;
    declare building_envelope_side_wall_prevailing_construction: string;
    declare building_envelope_side_wall_area: number;
    declare building_envelope_side_wall_heat_insulation: string;
    declare building_envelope_side_wall_year_of_adjustment: string;
    declare building_envelope_side_wall_technical_condition: string;
    declare building_envelope_filling_of_hole_construction: string;
    declare building_envelope_filling_of_hole_area: number;
    declare building_envelope_filling_of_hole_year_of_adjustment: string;
    declare building_envelope_filling_of_hole_technical_condition: string;
    declare building_envelope_roof_construction: string;
    declare building_envelope_roof_area: number;
    declare building_envelope_roof_thermal_insulation: string;
    declare building_envelope_roof_year_of_adjustment: string;
    declare building_envelope_roof_technical_condition: string;
    declare building_envelope_floor_of_the_lowest_heated_floor_construction: string;
    declare building_envelope_floor_of_the_lowest_heated_floor_area: number;
    declare floor_of_the_lowest_heated_floor_thermal_insulation: string;
    declare building_envelope_floor_of_the_lowest_heated_floor_year_of_adju: string;
    declare floor_of_the_lowest_heated_floor_technical_condition: string;
    declare fuel_and_energy_coal: boolean;
    declare fuel_and_energy_gas: boolean;
    declare fuel_and_energy_electricity: boolean;
    declare fuel_and_energy_czt: boolean;
    declare fuel_and_energy_oze: boolean;
    declare fuel_and_energy_other: boolean;
    declare technical_equipment_heating_main_source_of_heat: string;
    declare technical_equipment_heating_heat_percentage: number;
    declare technical_equipment_heating_secondary_source_of_heat: string;
    declare technical_equipment_heating_heating_system: string;
    declare technical_equipment_heating_year: string;
    declare technical_equipment_heating_technical_condition: string;
    declare technical_equipment_cooling_cooling_system: string;
    declare technical_equipment_cooling_cooling_area_percentage: number;
    declare technical_equipment_cooling_year: string;
    declare technical_equipment_cooling_technical_condition: string;
    declare technical_equipment_ventilation_ventilation: string;
    declare technical_equipment_ventilation_year: string;
    declare technical_equipment_ventilation_technical_condition: string;
    declare technical_equipment_humidity_adjustment_humidity_adjustment: string;
    declare technical_equipment_humidity_adjustment_year: string;
    declare technical_equipment_humidity_adjustment_technical_condition: string;
    declare technical_equipment_hot_water_predominant_way_of_heating_tv: string;
    declare technical_equipment_hot_water_hot_water_source: string;
    declare technical_equipment_hot_water_year: string;
    declare technical_equipment_hot_water_technical_condition: string;
    declare technical_equipment_lighting_lighting: string;
    declare technical_equipment_lighting_year: string;
    declare technical_equipment_lighting_technical_condition: string;
    declare technical_equipment_lighting_other_technological_elements: string;
    declare technical_equipment_lighting_measurement_method: string;
    declare oze_energy_production_solar_energy_photovoltaic: string;
    declare oze_energy_production_solar_energy_photothermal: string;
    declare oze_energy_production_integrated_turbines_wind_energy: string;
    declare oze_energy_production_heat_pump: string;
    declare waste_and_emissions_solid_waste_production: string;
    declare waste_and_emissions_tied_co2_emissions: string;
    declare waste_and_emissions_sox_emissions: string;
    declare waste_and_emissions_operating_co_2emissions: string;
    declare link: string;

    public static attributeModel: ModelAttributes<BuildingsSecondaryModel> = {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        building_address_code: DataTypes.STRING(100),
        building_label: DataTypes.STRING(100),
        current_note: DataTypes.STRING(100),
        method_of_protection: DataTypes.STRING(100),
        built_up_area: DataTypes.REAL,
        heated_bulding_volume: DataTypes.INTEGER,
        students_count: DataTypes.INTEGER,
        employees_count: DataTypes.INTEGER,
        classrooms_count: DataTypes.INTEGER,
        beds_count: DataTypes.INTEGER,
        eno_id: DataTypes.STRING(100),
        csu_code: DataTypes.STRING(100),
        ku_code: DataTypes.STRING(100),
        allotment_number: DataTypes.STRING(100),
        registration_unit: DataTypes.STRING(100),
        energetic_management: DataTypes.STRING(100),
        opening_hours: DataTypes.STRING(100),
        weekend_opening_hours: DataTypes.STRING(100),
        location: DataTypes.GEOMETRY,
        address_street: DataTypes.STRING(100),
        address_house_number: DataTypes.STRING(100),
        address_city: DataTypes.STRING(100),
        address_country: DataTypes.STRING(100),
        address_mail: DataTypes.STRING(100),
        address_phone: DataTypes.STRING(100),
        address_web_address: DataTypes.STRING(100),
        penb_penbnumber: DataTypes.INTEGER,
        penb_issue_date: DataTypes.STRING(100),
        penb_total_building_envelope_area: DataTypes.STRING(100),
        penb_volume_factor_of_avshape: DataTypes.STRING(100),
        penb_total_energy_reference_area: DataTypes.STRING(100),
        penb_total_provided_energy: DataTypes.REAL,
        penb_total_provided_energy_category: DataTypes.STRING(50),
        penb_primary_non_renewable_energy: DataTypes.REAL,
        penb_primary_non_renewable_energy_category: DataTypes.STRING(50),
        penb_building_envelope: DataTypes.REAL,
        penb_building_envelope_category: DataTypes.STRING(50),
        penb_heating: DataTypes.REAL,
        penb_heating_category: DataTypes.STRING(50),
        penb_cooling: DataTypes.REAL,
        penb_cooling_category: DataTypes.REAL,
        penb_ventilation: DataTypes.STRING(100),
        penb_ventilation_category: DataTypes.STRING(50),
        penb_humidity_adjustment: DataTypes.REAL,
        penb_humidity_adjustment_category: DataTypes.STRING(50),
        penb_warm_water: DataTypes.REAL,
        penb_warm_water_category: DataTypes.STRING(50),
        penb_lighting: DataTypes.REAL,
        penb_lighting_category: DataTypes.STRING(50),
        energy_audits_energy_audit: DataTypes.STRING(100),
        energy_audits_earegistration_number: DataTypes.STRING(100),
        energy_audits_created_at: DataTypes.STRING(100),
        building_envelope_side_wall_prevailing_construction: DataTypes.STRING(100),
        building_envelope_side_wall_area: DataTypes.REAL,
        building_envelope_side_wall_heat_insulation: DataTypes.STRING(100),
        building_envelope_side_wall_year_of_adjustment: DataTypes.STRING(100),
        building_envelope_side_wall_technical_condition: DataTypes.STRING(100),
        building_envelope_filling_of_hole_construction: DataTypes.STRING(100),
        building_envelope_filling_of_hole_area: DataTypes.REAL,
        building_envelope_filling_of_hole_year_of_adjustment: DataTypes.STRING(100),
        building_envelope_filling_of_hole_technical_condition: DataTypes.STRING(100),
        building_envelope_roof_construction: DataTypes.STRING(100),
        building_envelope_roof_area: DataTypes.REAL,
        building_envelope_roof_thermal_insulation: DataTypes.STRING(100),
        building_envelope_roof_year_of_adjustment: DataTypes.STRING(100),
        building_envelope_roof_technical_condition: DataTypes.STRING(100),
        building_envelope_floor_of_the_lowest_heated_floor_construction: DataTypes.STRING(100),
        building_envelope_floor_of_the_lowest_heated_floor_area: DataTypes.REAL,
        floor_of_the_lowest_heated_floor_thermal_insulation: DataTypes.STRING(100),
        building_envelope_floor_of_the_lowest_heated_floor_year_of_adjustment: DataTypes.STRING(100),
        floor_of_the_lowest_heated_floor_technical_condition: DataTypes.STRING(100),
        fuel_and_energy_coal: DataTypes.BOOLEAN,
        fuel_and_energy_gas: DataTypes.BOOLEAN,
        fuel_and_energy_electricity: DataTypes.BOOLEAN,
        fuel_and_energy_czt: DataTypes.BOOLEAN,
        fuel_and_energy_oze: DataTypes.BOOLEAN,
        fuel_and_energy_other: DataTypes.BOOLEAN,
        technical_equipment_heating_main_source_of_heat: DataTypes.STRING(100),
        technical_equipment_heating_heat_percentage: DataTypes.INTEGER,
        technical_equipment_heating_secondary_source_of_heat: DataTypes.STRING(100),
        technical_equipment_heating_heating_system: DataTypes.STRING(100),
        technical_equipment_heating_year: DataTypes.STRING(50),
        technical_equipment_heating_technical_condition: DataTypes.STRING(100),
        technical_equipment_cooling_cooling_system: DataTypes.STRING(100),
        technical_equipment_cooling_cooling_area_percentage: DataTypes.STRING(100),
        technical_equipment_cooling_year: DataTypes.STRING(50),
        technical_equipment_cooling_technical_condition: DataTypes.STRING(100),
        technical_equipment_ventilation_ventilation: DataTypes.STRING(100),
        technical_equipment_ventilation_year: DataTypes.STRING(50),
        technical_equipment_ventilation_technical_condition: DataTypes.STRING(100),
        technical_equipment_humidity_adjustment_humidity_adjustment: DataTypes.STRING(100),
        technical_equipment_humidity_adjustment_year: DataTypes.STRING(50),
        technical_equipment_humidity_adjustment_technical_condition: DataTypes.STRING(100),
        technical_equipment_hot_water_predominant_way_of_heating_tv: DataTypes.STRING(100),
        technical_equipment_hot_water_hot_water_source: DataTypes.STRING(100),
        technical_equipment_hot_water_year: DataTypes.STRING(50),
        technical_equipment_hot_water_technical_condition: DataTypes.STRING(100),
        technical_equipment_lighting_lighting: DataTypes.STRING(100),
        technical_equipment_lighting_year: DataTypes.STRING(50),
        technical_equipment_lighting_technical_condition: DataTypes.STRING(100),
        technical_equipment_lighting_other_technological_elements: DataTypes.STRING(100),
        technical_equipment_lighting_measurement_method: DataTypes.STRING(100),
        oze_energy_production_solar_energy_photovoltaic: DataTypes.STRING(100),
        oze_energy_production_solar_energy_photothermal: DataTypes.STRING(100),
        oze_energy_production_integrated_turbines_wind_energy: DataTypes.STRING(100),
        oze_energy_production_heat_pump: DataTypes.STRING(100),
        waste_and_emissions_solid_waste_production: DataTypes.STRING(100),
        waste_and_emissions_tied_co2_emissions: DataTypes.STRING(100),
        waste_and_emissions_sox_emissions: DataTypes.STRING(100),
        waste_and_emissions_operating_co_2emissions: DataTypes.STRING(100),
    };

    public static arrayJsonSchema: JSONSchemaType<ISecondaryBuildings[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "number" },
                building_address_code: { type: "string" },
                building_label: { type: "string" },
                current_note: { type: "string" },
                method_of_protection: { type: "string" },
                built_up_area: { type: "number" },
                heated_bulding_volume: { type: "number" },
                students_count: { type: "number" },
                employees_count: { type: "number" },
                classrooms_count: { type: "number" },
                beds_count: { type: "number" },
                eno_id: { type: "string" },
                csu_code: { type: "string" },
                ku_code: { type: "string" },
                allotment_number: { type: "string" },
                registration_unit: { type: "string" },
                energetic_management: { type: "string" },
                opening_hours: { type: "string" },
                weekend_opening_hours: { type: "string" },
                location: { $ref: "#/definitions/geometry" },
                address_street: { type: "string" },
                address_house_number: { type: "string" },
                address_country: { type: "string" },
                address_mail: { type: "string" },
                address_phone: { type: "string" },
                address_web_address: { type: "string" },
                penb_penbnumber: { type: "number" },
                penb_issue_date: { type: "string" },
                penb_total_building_envelope_area: { type: "number" },
                penb_volume_factor_of_avshape: { type: "string" },
                penb_total_energy_reference_area: { type: "number" },
                penb_total_provided_energy: { type: "number" },
                penb_total_provided_energy_category: { type: "string" },
                penb_primary_non_renewable_energy: { type: "number" },
                penb_primary_non_renewable_energy_category: { type: "string" },
                penb_building_envelope: { type: "number" },
                penb_building_envelope_category: { type: "string" },
                penb_heating: { type: "number" },
                penb_heating_category: { type: "string" },
                penb_cooling: { type: "number" },
                penb_cooling_category: { type: "string" },
                penb_ventilation: { type: "number" },
                penb_ventilation_category: { type: "string" },
                penb_humidity_adjustment: { type: "number" },
                penb_humidity_adjustment_category: { type: "string" },
                penb_warm_water: { type: "number" },
                penb_warm_water_category: { type: "string" },
                penb_lighting: { type: "number" },
                penb_lighting_category: { type: "string" },
                energy_audits_energy_audit: { type: "string" },
                energy_audits_earegistration_number: { type: "string" },
                energy_audits_created_at: { type: "string" },
                building_envelope_side_wall_prevailing_construction: { type: "string" },
                building_envelope_side_wall_area: { type: "number" },
                building_envelope_side_wall_heat_insulation: { type: "string" },
                building_envelope_side_wall_year_of_adjustment: { type: "string" },
                building_envelope_side_wall_technical_condition: { type: "string" },
                building_envelope_filling_of_hole_construction: { type: "string" },
                building_envelope_filling_of_hole_area: { type: "number" },
                building_envelope_filling_of_hole_year_of_adjustment: { type: "string" },
                building_envelope_filling_of_hole_technical_condition: { type: "string" },
                building_envelope_roof_construction: { type: "string" },
                building_envelope_roof_area: { type: "number" },
                building_envelope_roof_thermal_insulation: { type: "string" },
                building_envelope_roof_year_of_adjustment: { type: "string" },
                building_envelope_roof_technical_condition: { type: "string" },
                building_envelope_floor_of_the_lowest_heated_floor_construction: { type: "string" },
                building_envelope_floor_of_the_lowest_heated_floor_area: { type: "number" },
                floor_of_the_lowest_heated_floor_thermal_insulation: { type: "string" },
                building_envelope_floor_of_the_lowest_heated_floor_year_of_adju: { type: "string" },
                floor_of_the_lowest_heated_floor_technical_condition: { type: "string" },
                fuel_and_energy_coal: { type: "boolean" },
                fuel_and_energy_gas: { type: "boolean" },
                fuel_and_energy_electricity: { type: "boolean" },
                fuel_and_energy_czt: { type: "boolean" },
                fuel_and_energy_oze: { type: "boolean" },
                fuel_and_energy_other: { type: "boolean" },
                technical_equipment_heating_main_source_of_heat: { type: "string" },
                technical_equipment_heating_heat_percentage: { type: "number" },
                technical_equipment_heating_secondary_source_of_heat: { type: "string" },
                technical_equipment_heating_heating_system: { type: "string" },
                technical_equipment_heating_year: { type: "string" },
                technical_equipment_heating_technical_condition: { type: "string" },
                technical_equipment_cooling_cooling_system: { type: "string" },
                technical_equipment_cooling_cooling_area_percentage: { type: "number" },
                technical_equipment_cooling_year: { type: "string" },
                technical_equipment_cooling_technical_condition: { type: "string" },
                technical_equipment_ventilation_ventilation: { type: "string" },
                technical_equipment_ventilation_year: { type: "string" },
                technical_equipment_ventilation_technical_condition: { type: "string" },
                technical_equipment_humidity_adjustment_humidity_adjustment: { type: "string" },
                technical_equipment_humidity_adjustment_year: { type: "string" },
                technical_equipment_humidity_adjustment_technical_condition: { type: "string" },
                technical_equipment_hot_water_predominant_way_of_heating_tv: { type: "string" },
                technical_equipment_hot_water_hot_water_source: { type: "string" },
                technical_equipment_hot_water_year: { type: "string" },
                technical_equipment_hot_water_technical_condition: { type: "string" },
                technical_equipment_lighting_lighting: { type: "string" },
                technical_equipment_lighting_year: { type: "string" },
                technical_equipment_lighting_technical_condition: { type: "string" },
                technical_equipment_lighting_other_technological_elements: { type: "string" },
                technical_equipment_lighting_measurement_method: { type: "string" },
                oze_energy_production_solar_energy_photovoltaic: { type: "string" },
                oze_energy_production_solar_energy_photothermal: { type: "string" },
                oze_energy_production_integrated_turbines_wind_energy: { type: "string" },
                oze_energy_production_heat_pump: { type: "string" },
                waste_and_emissions_solid_waste_production: { type: "string" },
                waste_and_emissions_tied_co2_emissions: { type: "string" },
                waste_and_emissions_sox_emissions: { type: "string" },
                waste_and_emissions_operating_co_2emissions: { type: "string" },
            },
            required: [
                "id",
                "address_phone",
                "address_web_address",
                "penb_penbnumber",
                "penb_issue_date",
                "penb_total_building_envelope_area",
                "penb_volume_factor_of_avshape",
                "penb_total_energy_reference_area",
                "penb_total_provided_energy",
                "penb_total_provided_energy_category",
                "penb_primary_non_renewable_energy",
                "penb_primary_non_renewable_energy_category",
                "penb_building_envelope",
                "penb_building_envelope_category",
                "penb_heating",
                "penb_heating_category",
                "penb_cooling",
                "penb_cooling_category",
                "penb_ventilation",
                "penb_ventilation_category",
                "penb_humidity_adjustment",
                "penb_humidity_adjustment_category",
                "penb_warm_water",
                "penb_warm_water_category",
                "penb_lighting",
                "penb_lighting_category",
                "energy_audits_energy_audit",
                "energy_audits_earegistration_number",
                "energy_audits_created_at",
                "building_envelope_side_wall_prevailing_construction",
                "building_envelope_side_wall_area",
                "building_envelope_side_wall_heat_insulation",
                "building_envelope_side_wall_year_of_adjustment",
                "building_envelope_side_wall_technical_condition",
                "building_envelope_filling_of_hole_construction",
                "building_envelope_filling_of_hole_area",
                "building_envelope_filling_of_hole_year_of_adjustment",
                "building_envelope_filling_of_hole_technical_condition",
                "building_envelope_roof_construction",
                "building_envelope_roof_area",
                "building_envelope_roof_thermal_insulation",
                "building_envelope_roof_year_of_adjustment",
                "building_envelope_roof_technical_condition",
                "building_envelope_floor_of_the_lowest_heated_floor_construction",
                "building_envelope_floor_of_the_lowest_heated_floor_area",
                "floor_of_the_lowest_heated_floor_thermal_insulation",
                "building_envelope_floor_of_the_lowest_heated_floor_year_of_adju",
                "floor_of_the_lowest_heated_floor_technical_condition",
                "fuel_and_energy_coal",
                "fuel_and_energy_gas",
                "fuel_and_energy_electricity",
                "fuel_and_energy_czt",
                "fuel_and_energy_oze",
                "fuel_and_energy_other",
                "technical_equipment_heating_main_source_of_heat",
                "technical_equipment_heating_heat_percentage",
                "technical_equipment_heating_secondary_source_of_heat",
                "technical_equipment_heating_heating_system",
                "technical_equipment_heating_year",
                "technical_equipment_heating_technical_condition",
                "technical_equipment_cooling_cooling_system",
                "technical_equipment_cooling_cooling_area_percentage",
                "technical_equipment_cooling_year",
                "technical_equipment_cooling_technical_condition",
                "technical_equipment_ventilation_ventilation",
                "technical_equipment_ventilation_year",
                "technical_equipment_ventilation_technical_condition",
                "technical_equipment_humidity_adjustment_humidity_adjustment",
                "technical_equipment_humidity_adjustment_year",
                "technical_equipment_humidity_adjustment_technical_condition",
                "technical_equipment_hot_water_predominant_way_of_heating_tv",
                "technical_equipment_hot_water_hot_water_source",
                "technical_equipment_hot_water_year",
                "technical_equipment_hot_water_technical_condition",
                "technical_equipment_lighting_lighting",
                "technical_equipment_lighting_year",
                "technical_equipment_lighting_technical_condition",
                "technical_equipment_lighting_other_technological_elements",
                "technical_equipment_lighting_measurement_method",
                "oze_energy_production_solar_energy_photovoltaic",
                "oze_energy_production_solar_energy_photothermal",
                "oze_energy_production_integrated_turbines_wind_energy",
                "oze_energy_production_heat_pump",
                "waste_and_emissions_solid_waste_production",
                "waste_and_emissions_tied_co2_emissions",
                "waste_and_emissions_sox_emissions",
                "waste_and_emissions_operating_co_2emissions",
            ],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
