interface IBuildingsTransformationOutput {
    id: number;
    name: string | null;
    buildingAddressCode: string | null;
    link: string;
}
