import { Geometry } from "@golemio/core/dist/shared/geojson";

export interface ISecondaryBuildings {
    id: number;
    building_address_code: string | null;
    building_label: string | null;
    current_note: string | null;
    method_of_protection: string | null;
    built_up_area: number | null;
    heated_bulding_volume: number | null;
    students_count: number | null;
    employees_count: number | null;
    classrooms_count: number | null;
    beds_count: number | null;
    eno_id: string | null;
    csu_code: string | null;
    ku_code: string | null;
    allotment_number: string | null;
    registration_unit: string | null;
    energetic_management: string | null;
    opening_hours: string | null;
    weekend_opening_hours: string | null;
    location: Geometry;
    address_street: string | null;
    address_house_number: string | null;
    address_city: string | null;
    address_country: string | null;
    address_mail: string | null;
    address_phone: string | null;
    address_web_address: string | null;
    penb_penbnumber: number | null;
    penb_issue_date: string | null;
    penb_total_building_envelope_area: number | null;
    penb_volume_factor_of_avshape: string | null;
    penb_total_energy_reference_area: number | null;
    penb_total_provided_energy: number | null;
    penb_total_provided_energy_category: string | null;
    penb_primary_non_renewable_energy: number | null;
    penb_primary_non_renewable_energy_category: string | null;
    penb_building_envelope: number | null;
    penb_building_envelope_category: string | null;
    penb_heating: number | null;
    penb_heating_category: string | null;
    penb_cooling: number | null;
    penb_cooling_category: string | null;
    penb_ventilation: number | null;
    penb_ventilation_category: string | null;
    penb_humidity_adjustment: number | null;
    penb_humidity_adjustment_category: string | null;
    penb_warm_water: number | null;
    penb_warm_water_category: string | null;
    penb_lighting: number | null;
    penb_lighting_category: string | null;
    energy_audits_energy_audit: string | null;
    energy_audits_earegistration_number: string | null;
    energy_audits_created_at: string | null;
    building_envelope_side_wall_prevailing_construction: string | null;
    building_envelope_side_wall_area: number | null;
    building_envelope_side_wall_heat_insulation: string | null;
    building_envelope_side_wall_year_of_adjustment: string | null;
    building_envelope_side_wall_technical_condition: string | null;
    building_envelope_filling_of_hole_construction: string | null;
    building_envelope_filling_of_hole_area: number | null;
    building_envelope_filling_of_hole_year_of_adjustment: string | null;
    building_envelope_filling_of_hole_technical_condition: string | null;
    building_envelope_roof_construction: string | null;
    building_envelope_roof_area: number | null;
    building_envelope_roof_thermal_insulation: string | null;
    building_envelope_roof_year_of_adjustment: string | null;
    building_envelope_roof_technical_condition: string | null;
    building_envelope_floor_of_the_lowest_heated_floor_construction: string | null;
    building_envelope_floor_of_the_lowest_heated_floor_area: number | null;
    floor_of_the_lowest_heated_floor_thermal_insulation: string | null;
    building_envelope_floor_of_the_lowest_heated_floor_year_of_adju: string | null;
    floor_of_the_lowest_heated_floor_technical_condition: string | null;
    fuel_and_energy_coal: boolean | null;
    fuel_and_energy_gas: boolean | null;
    fuel_and_energy_electricity: boolean | null;
    fuel_and_energy_czt: boolean | null;
    fuel_and_energy_oze: boolean | null;
    fuel_and_energy_other: boolean | null;
    technical_equipment_heating_main_source_of_heat: string | null;
    technical_equipment_heating_heat_percentage: number | null;
    technical_equipment_heating_secondary_source_of_heat: string | null;
    technical_equipment_heating_heating_system: string | null;
    technical_equipment_heating_year: string | null;
    technical_equipment_heating_technical_condition: string | null;
    technical_equipment_cooling_cooling_system: string | null;
    technical_equipment_cooling_cooling_area_percentage: number | null;
    technical_equipment_cooling_year: string | null;
    technical_equipment_cooling_technical_condition: string | null;
    technical_equipment_ventilation_ventilation: string | null;
    technical_equipment_ventilation_year: string | null;
    technical_equipment_ventilation_technical_condition: string | null;
    technical_equipment_humidity_adjustment_humidity_adjustment: string | null;
    technical_equipment_humidity_adjustment_year: string | null;
    technical_equipment_humidity_adjustment_technical_condition: string | null;
    technical_equipment_hot_water_predominant_way_of_heating_tv: string | null;
    technical_equipment_hot_water_hot_water_source: string | null;
    technical_equipment_hot_water_year: string | null;
    technical_equipment_hot_water_technical_condition: string | null;
    technical_equipment_lighting_lighting: string | null;
    technical_equipment_lighting_year: string | null;
    technical_equipment_lighting_technical_condition: string | null;
    technical_equipment_lighting_other_technological_elements: string | null;
    technical_equipment_lighting_measurement_method: string | null;
    oze_energy_production_solar_energy_photovoltaic: string | null;
    oze_energy_production_solar_energy_photothermal: string | null;
    oze_energy_production_integrated_turbines_wind_energy: string | null;
    oze_energy_production_heat_pump: string | null;
    waste_and_emissions_solid_waste_production: string | null;
    waste_and_emissions_tied_co2_emissions: string | null;
    waste_and_emissions_sox_emissions: string | null;
    waste_and_emissions_operating_co_2emissions: string | null;
}
