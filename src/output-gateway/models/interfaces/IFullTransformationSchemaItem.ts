import { IGeoCoordinatesPoint } from "@golemio/core/dist/output-gateway/Geo";

export interface IFullTransformationSchemaItem {
    id: number;
    name: string | null;
    main_use: string | null;
    method_of_protection: string | null;
    address: {
        city: string | null;
        country: string | null;
        house_number: string | null;
        mail: string | null;
        phone: string | null;
        street: string | null;
        web_address: string | null;
    } | null;
    allotment_number: string | null;
    beds_count: number | null;
    building_address_code: string | null;
    building_envelope: {
        filling_of_hole: {
            area: number | null;
            construction: string | null;
            technical_condition: string | null;
            year_of_adjustment: string | null;
        } | null;
        floor_of_the_lowest_heated_floor: {
            area: number | null;
            construction: string | null;
            year_of_adjustment: string | null;
            technical_condition: string | null;
            thermal_insulation: string | null;
        } | null;
        roof: {
            area: number | null;
            construction: string | null;
            technical_condition: string | null;
            thermal_insulation: string | null;
            year_of_adjustment: string | null;
        } | null;
        side_wall: {
            area: number | null;
            heat_insulation: string | null;
            prevailing_construction: string | null;
            technical_condition: string | null;
            year_of_adjustment: string | null;
        } | null;
    } | null;
    label: string | null;
    built_up_area: number | null;
    classrooms_count: number | null;
    csu_code: string | null;
    current_note: string | null;
    description: string | null;
    employees_count: number | null;
    energetic_management: string | null;
    energy_audits: {
        created_at: string | null;
        ea_registration_number: string | null;
        energy_audit: string | null;
    } | null;
    eno_id: string | null;
    fuel_and_energy: {
        coal: boolean | null;
        czt: boolean | null;
        electricity: boolean | null;
        gas: boolean | null;
        other: boolean | null;
        oze: boolean | null;
    } | null;
    heated_bulding_volume: number | null;
    ku_code: string | null;
    location: IGeoCoordinatesPoint | null;
    link: string;
    oze_energy_production: {
        heat_pump: string | null;
        integrated_turbines_wind_energy: string | null;
        solar_energy_photothermal: string | null;
        solar_energy_photovoltaic: string | null;
    } | null;
    penb: {
        building_envelope: number | null;
        building_envelope_category: string | null;
        cooling: number | null;
        cooling_category: string | null;
        heating: number | null;
        heating_category: string | null;
        humidity_adjustment: number | null;
        humidity_adjustment_category: string | null;
        issue_date: string | null;
        lighting: number | null;
        lighting_category: string | null;
        penb_number: number | null;
        primary_non_renewable_energy: number | null;
        primary_non_renewable_energy_category: string | null;
        total_building_envelope_area: number | null;
        total_energy_reference_area: number | null;
        total_provided_energy: number | null;
        total_provided_energy_category: string | null;
        ventilation: number | null;
        ventilation_category: string | null;
        volume_factor_of_av_shape: string | null;
        warm_water: number | null;
        warm_water_category: string | null;
    } | null;
    registration_unit: string | null;
    secondary_use: string | null;
    students_count: number | null;
    technical_equipment: {
        cooling: {
            cooling_area_percentage: number | null;
            cooling_system: string | null;
            technical_condition: string | null;
            year: string | null;
        } | null;
        heating: {
            heat_percentage: number | null;
            heating_system: string | null;
            main_source_of_heat: string | null;
            secondary_source_of_heat: string | null;
            technical_condition: string | null;
            year: string | null;
        } | null;
        hot_water: {
            hot_water_source: string | null;
            predominant_way_of_heating_tv: string | null;
            technical_condition: string | null;
            year: string | null;
        } | null;
        humidity_adjustment: {
            humidity_adjustment: string | null;
            technical_condition: string | null;
            year: string | null;
        } | null;
        lighting: {
            lighting: string | null;
            measurement_method: string | null;
            other_technological_elements: string | null;
            technical_condition: string | null;
            year: string | null;
        } | null;
        ventilation: {
            technical_condition: string | null;
            ventilation: string | null;
            year: string | null;
        } | null;
    } | null;
    waste_and_emissions: {
        operating_co2_emissions: string | null;
        solid_waste_production: string | null;
        sox_emissions: string | null;
        tied_co2_emissions: string | null;
    } | null;
    weekend_opening_hours: string | null;
    opening_hours: string | null;
    year_of_construction: number | null;
}
