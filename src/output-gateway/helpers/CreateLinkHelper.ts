import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { RouteVersion } from "#og/constants/RouteVersion";

@injectable()
export class CreateLinkHelper {
    private readonly baseUrl: string;

    constructor(@inject(CoreToken.SimpleConfig) config: ISimpleConfig) {
        this.baseUrl = new URL(config.getValue("env.API_URL_PREFIX", "https://api.golemio.cz")).origin;
    }

    public getLinkUrl(path: string, id: number) {
        return new URL(`${RouteVersion.v2}/${path}/${id}`, this.baseUrl).toString();
    }
}
