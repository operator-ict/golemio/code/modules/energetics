export const ORGANIZATION_PATH = `energetics/organizations`;
export const BUILDING_PATH = `energetics/buildings`;
export const DEVICES_PATH = `energetics/devices`;
export const MEASUREMENTS_PATH = `energetics/measurements`;
