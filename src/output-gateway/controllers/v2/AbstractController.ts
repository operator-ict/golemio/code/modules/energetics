import { AccessLimitationRepository } from "#og/repositories/AccessLimitationRepository";
import { Request } from "@golemio/core/dist/shared/express";

export class AbstractController {
    protected accessLimitationRepository: AccessLimitationRepository;

    constructor(accessLimitationRepository: AccessLimitationRepository) {
        this.accessLimitationRepository = accessLimitationRepository;
    }

    protected async getAllowedOrganizationIds(req: Request) {
        const accessLimit = req.query.accessLimit as string;

        if (accessLimit === "admin") {
            return null;
        }

        return await this.accessLimitationRepository.getOrganizationsByGroupName(accessLimit);
    }
}
