import { parsePaginationParams } from "#og/controllers/helpers/parsePaginationParams";
import { IPaginationParams } from "#og/controllers/interfaces/IPaginationParams";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AccessLimitationRepository } from "#og/repositories/AccessLimitationRepository";
import { DevicesRepository } from "#og/repositories/DevicesRepository";
import { DeviceDtoTransformation } from "#og/transformations/DeviceDtoTransformation";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractController } from "./AbstractController";

@injectable()
export class V2DevicesController extends AbstractController {
    constructor(
        @inject(ModuleContainerToken.DevicesRepository) private devicesRepository: DevicesRepository,
        @inject(ModuleContainerToken.DeviceDtoTransformation)
        private transformation: DeviceDtoTransformation,
        @inject(ModuleContainerToken.AccessLimitationRepository) accessLimitationRepository: AccessLimitationRepository
    ) {
        super(accessLimitationRepository);
    }

    public getAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const allowedOrganizations = await this.getAllowedOrganizationIds(req);
            const params: IPaginationParams = parsePaginationParams(req);
            const result = await this.devicesRepository.GetAll(params, allowedOrganizations);
            return res.json(this.transformation.transformArray(result));
        } catch (err) {
            return next(err);
        }
    };

    public getOne = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const allowedOrganizations = await this.getAllowedOrganizationIds(req);
            const id = req.params.id;
            const result = await this.devicesRepository.GetOne(id, allowedOrganizations);

            if (!result) {
                return next(
                    new GeneralError("not_found", this.constructor.name, `Device with id ${req.params.id} not found`, 404)
                );
            }

            return res.json(this.transformation.transformElement(result));
        } catch (err) {
            return next(err);
        }
    };
}
