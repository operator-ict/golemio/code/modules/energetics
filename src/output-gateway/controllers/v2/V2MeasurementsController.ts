import { parsePaginationParams } from "#og/controllers/helpers/parsePaginationParams";
import { IMeasurements, IMeasurementsMonthly } from "#og/controllers/interfaces/IMeasurementsParams";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AccessLimitationRepository } from "#og/repositories/AccessLimitationRepository";
import { MeasurementRepository } from "#og/repositories/MeasurementRepository";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractController } from "./AbstractController";

@injectable()
export class V2MeasurementsController extends AbstractController {
    constructor(
        @inject(ModuleContainerToken.MeasurementRepository) private measurementRepository: MeasurementRepository,
        @inject(ModuleContainerToken.AccessLimitationRepository) accessLimitationRepository: AccessLimitationRepository
    ) {
        super(accessLimitationRepository);
    }

    public getMeasurements = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const allowedOrganizations = await this.getAllowedOrganizationIds(req);
            const params: IMeasurements = {
                variable: req.query.variable as string,
                type: req.query.type as string,
                addr1: req.query.addr1 as string,
                addr2: req.query.addr2 as string,
                dateFrom: req.query.dateFrom as string,
                dateTo: req.query.dateTo as string,
                ...parsePaginationParams(req),
            };
            const result = await this.measurementRepository.getMeasurements(params, allowedOrganizations);
            return res.json(result);
        } catch (err) {
            return next(err);
        }
    };

    public getMonthlyReadings = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const allowedOrganizations = await this.getAllowedOrganizationIds(req);
            const month = parseInt(req.query.month as string);
            const year = parseInt(req.query.year as string);

            const startDateUtc = DateTime.fromJSDate(new Date(year, month - 1, 1), { zone: "Europe/Prague" });
            const fromDate = startDateUtc.startOf("month").toISO();
            const toDate = startDateUtc.endOf("month").toISO();

            const params: IMeasurementsMonthly = {
                address: `/${req.query.addr1}/${req.query.addr2}`,
                variable: req.query.variable as string,
                from: fromDate,
                to: toDate,
            };

            const result = await this.measurementRepository.getMonthlyReadings(params, allowedOrganizations);
            return res.json(result.map((el) => ({ time: fromDate, ...el })));
        } catch (err) {
            return next(err);
        }
    };
}
