import { parsePaginationParams } from "#og/controllers/helpers/parsePaginationParams";
import { IPaginationParams } from "#og/controllers/interfaces/IPaginationParams";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AccessLimitationRepository } from "#og/repositories/AccessLimitationRepository";
import { OrganizationsRepository } from "#og/repositories/OrganizationsRepository";
import { OrganizationDetailDtoTransformation } from "#og/transformations/OrganizationDetailDtoTransformation";
import { OrganizationDtoTransformation } from "#og/transformations/OrganizationDtoTransformation";
import { parseBooleanQueryParam } from "@golemio/core/dist/output-gateway/Utils";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractController } from "./AbstractController";

@injectable()
export class V2OrganizationsController extends AbstractController {
    constructor(
        @inject(ModuleContainerToken.OrganizationsRepository) private organizationsRepository: OrganizationsRepository,
        @inject(ModuleContainerToken.OrganizationDtoTransformation) private transformation: OrganizationDtoTransformation,
        @inject(ModuleContainerToken.OrganizationDetailDtoTransformation)
        private transformationDetail: OrganizationDetailDtoTransformation,
        @inject(ModuleContainerToken.AccessLimitationRepository) accessLimitationRepository: AccessLimitationRepository
    ) {
        super(accessLimitationRepository);
    }

    public getAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const allowedOrganizations = await this.getAllowedOrganizationIds(req);
            const params: IPaginationParams = parsePaginationParams(req);
            const full = parseBooleanQueryParam(req.query.full as string) || false;
            let result = [];

            if (!full) {
                result = this.transformation.transformArray(
                    await this.organizationsRepository.GetAllPublic(params, allowedOrganizations)
                );
            } else {
                result = this.transformationDetail.transformArray(
                    await this.organizationsRepository.GetAll(params, allowedOrganizations)
                );
            }

            return res.json(result);
        } catch (err) {
            return next(err);
        }
    };

    public getOne = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const allowedOrganizations = await this.getAllowedOrganizationIds(req);
            const id = req.params.id;
            const result = await this.organizationsRepository.GetOne(id, allowedOrganizations);

            if (!result) {
                return next(
                    new GeneralError("not_found", this.constructor.name, `Organization with id ${req.params.id} not found`, 404)
                );
            }

            return res.json(this.transformationDetail.transformElement(result));
        } catch (err) {
            return next(err);
        }
    };
}
