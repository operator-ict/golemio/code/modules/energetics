import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AccessLimitationRepository } from "#og/repositories/AccessLimitationRepository";
import { PrimaryBuildingsRepository } from "#og/repositories/PrimaryBuildingsRepository";
import { BuildingTransformationFull } from "#og/transformations/BuildingTransformationFull";
import { BuildingsTransformation } from "#og/transformations/BuildingsTransformation";
import { parseBooleanQueryParam } from "@golemio/core/dist/output-gateway/Utils";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractController } from "./AbstractController";

@injectable()
export class BuildingsController extends AbstractController {
    private buildingsRepository: PrimaryBuildingsRepository;
    private transformation: BuildingsTransformation;

    constructor(
        @inject(ModuleContainerToken.AccessLimitationRepository) accessLimitationRepository: AccessLimitationRepository,
        @inject(ModuleContainerToken.BuildingTransformationFull) private fullDataTransformation: BuildingTransformationFull
    ) {
        super(accessLimitationRepository);
        this.buildingsRepository = new PrimaryBuildingsRepository();
        this.transformation = new BuildingsTransformation();
    }

    public getAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const allowedOrganizations = await this.getAllowedOrganizationIds(req);
            const fullParam = parseBooleanQueryParam(req.query.full as string);
            const transformedResult = fullParam
                ? await this.getFullData(allowedOrganizations)
                : await this.getPrimaryData(allowedOrganizations);
            res.json(transformedResult);
        } catch (err) {
            next(err);
        }
    };

    public getOne = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const allowedOrganizations = await this.getAllowedOrganizationIds(req);
            const id = parseInt(req.params.id);
            const result = await this.buildingsRepository.GetOne(id, allowedOrganizations);

            if (result === null) {
                throw new GeneralError("not_found", "V2BuildingsController", undefined, 404);
            }
            const transformedResult = this.fullDataTransformation.transformElement(result);
            res.json(transformedResult);
        } catch (err) {
            next(err);
        }
    };

    private getFullData = async (allowedOrganizations: number[] | null) => {
        const result = await this.buildingsRepository.GetAllFull(allowedOrganizations);
        const transformedResult = this.fullDataTransformation.transformArray(result);
        return transformedResult;
    };

    private getPrimaryData = async (allowedOrganizations: number[] | null) => {
        const result = await this.buildingsRepository.GetAll(allowedOrganizations);
        const transformedResult = this.transformation.transformArray(result);
        return transformedResult;
    };
}
