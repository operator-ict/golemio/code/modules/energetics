import { IPaginationParams } from "#og/controllers/interfaces/IPaginationParams";

export interface IMeasurements extends IPaginationParams {
    variable?: string;
    type?: string;
    addr1?: string;
    addr2?: string;
    dateFrom?: string;
    dateTo?: string;
}

export interface IMeasurementsMonthly {
    address: string;
    variable: string;
    from: string;
    to: string;
}
