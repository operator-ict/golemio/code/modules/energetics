import { Request } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IPaginationParams } from "#og/controllers/interfaces/IPaginationParams";

export function parsePaginationParams(req: Request): IPaginationParams {
    try {
        return {
            limit: req.query.limit ? parseInt(req.query.limit as string) : undefined,
            offset: req.query.offset ? parseInt(req.query.offset as string) : undefined,
        };
    } catch (err) {
        throw new GeneralError("Param parsing error", "ParsePaginationParams", err, 500);
    }
}
