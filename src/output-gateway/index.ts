import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { V2BuildingsRouter } from "./routers/v2/V2BuildingsRouter";
import { V2OrganizationsRouter } from "#og/routers/v2/V2OrganizationsRouter";
import { V2MeasurementsRouter } from "#og/routers/v2/V2MeasurementsRouter";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";

const v2BuildingsRouter = OGEnergeticsContainer.resolve<V2BuildingsRouter>(ModuleContainerToken.V2BuildingsRouter);
const v2OrganizationsRouter = OGEnergeticsContainer.resolve<V2OrganizationsRouter>(ModuleContainerToken.V2OrganizationsRouter);
const v2DevicesRouter = OGEnergeticsContainer.resolve<V2OrganizationsRouter>(ModuleContainerToken.V2DevicesRouter);
const v2MeasurementsRouter = OGEnergeticsContainer.resolve<V2MeasurementsRouter>(ModuleContainerToken.V2MeasurementsRouter);

export const routers: AbstractRouter[] = [v2BuildingsRouter, v2OrganizationsRouter, v2DevicesRouter, v2MeasurementsRouter];
