export interface IOrganizationDto {
    id: number;
    name: string | null;
}
