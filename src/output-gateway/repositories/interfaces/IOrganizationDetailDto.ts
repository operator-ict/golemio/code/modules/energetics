import { IOrganizationDto } from "#og/repositories/interfaces/IOrganizationDto";
import { IOrganizationBuildingDto } from "#og/repositories/interfaces/IOrganizationBuildingDto";
import { IOrganizationResponsibleUserDto } from "#og/repositories/interfaces/IOrganizationResponsibleUserDto";

export interface IOrganizationDetailDto extends IOrganizationDto {
    label: string | null;
    address_street: string | null;
    address_house_number: string | null;
    address_city: string | null;
    address_country: string | null;
    address_mail: string | null;
    address_phone: string | null;
    address_web_address: string | null;
    category: string | null;
    created_by_id: number | null;
    grafana_url: string | null;
    responsible_user: IOrganizationResponsibleUserDto[];
    buildings: IOrganizationBuildingDto[];
}
