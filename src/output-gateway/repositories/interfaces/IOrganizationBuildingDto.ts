export interface IOrganizationBuildingDto {
    id: number;
    building_name: string | null;
    building_address_code: string | null;
}
