export interface IOrganizationResponsibleUserDto {
    first_name: string | null;
    last_name: string | null;
    position: string | null;
    phone: string | null;
    mail: string | null;
    company: string | null;
}
