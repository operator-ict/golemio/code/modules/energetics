import { ModelStatic } from "@golemio/core/dist/shared/sequelize";
import { OrganizationsModel } from "#sch/models/OrganizationsModel";
import { OrganizationResponsibleUsersModel } from "#sch/models/OrganizationResponsibleUsersModel";
import { OrganizationBuildingModel } from "#sch/models/OrganizationBuildingModel";
import { BuildingsModel } from "#sch/models";

export class OrganizationsAssociations {
    public static associateResponsibleUsers(
        primaryTable: ModelStatic<OrganizationsModel>,
        secondaryTable: ModelStatic<OrganizationResponsibleUsersModel>
    ) {
        primaryTable.hasMany(secondaryTable, {
            foreignKey: "organization_id",
            sourceKey: "id",
            as: "responsible_user",
        });
        secondaryTable.belongsTo(primaryTable, {
            foreignKey: "organization_id",
            targetKey: "id",
        });
    }

    public static associateBuildings(
        primaryTable: ModelStatic<OrganizationsModel>,
        secondaryTable: ModelStatic<BuildingsModel>,
        relationTable: ModelStatic<OrganizationBuildingModel>
    ) {
        primaryTable.belongsToMany(secondaryTable, {
            through: relationTable,
            foreignKey: "organization_id",
            otherKey: "building_id",
            as: "buildings",
        });
        secondaryTable.belongsToMany(primaryTable, {
            through: relationTable,
            foreignKey: "building_id",
            otherKey: "organization_id",
        });
        relationTable.belongsTo(primaryTable, {
            targetKey: "id",
            foreignKey: "organization_id",
        });
        relationTable.belongsTo(secondaryTable, {
            targetKey: "id",
            foreignKey: "building_id",
        });
        primaryTable.hasMany(relationTable, {
            sourceKey: "id",
            foreignKey: "organization_id",
        });
        secondaryTable.hasMany(relationTable, {
            sourceKey: "id",
            foreignKey: "building_id",
        });
    }
}
