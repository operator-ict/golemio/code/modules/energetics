import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { EnergeticsSchema } from "#sch";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { OrganizationsModel } from "#sch/models/OrganizationsModel";
import { IPaginationParams } from "#og/controllers/interfaces/IPaginationParams";
import { OrganizationResponsibleUsersRepository } from "#og/repositories/OrganizationResponsibleUsersRepository";
import { IOrganizationDto } from "#og/repositories/interfaces/IOrganizationDto";
import { IOrganizationDetailDto } from "#og/repositories/interfaces/IOrganizationDetailDto";
import { PrimaryBuildingsRepository } from "#og/repositories/PrimaryBuildingsRepository";
import { OrganizationsAssociations } from "#og/repositories/helpers/OrganizationsAssociations";
import { OrganizationBuildingsRepository } from "#og/repositories/OrganizationBuildingsRepository";
import { Op } from "@golemio/core/dist/shared/sequelize";

@injectable()
export class OrganizationsRepository extends SequelizeModel {
    constructor(
        @inject(ModuleContainerToken.OrganizationResponsibleUsersRepository)
        private userRepository: OrganizationResponsibleUsersRepository,
        @inject(ModuleContainerToken.OrganizationBuildingsRepository)
        private organizationBuildingsRepository: OrganizationBuildingsRepository,
        @inject(ModuleContainerToken.PrimaryBuildingsRepository) private buildingsRepository: PrimaryBuildingsRepository
    ) {
        super("OrganizationsRepository", OrganizationsModel.TABLE_NAME, OrganizationsModel.attributeModel, {
            schema: EnergeticsSchema.pgSchema,
        });

        this.associate();
    }

    public associate() {
        OrganizationsAssociations.associateResponsibleUsers(this.sequelizeModel, this.userRepository.sequelizeModel);
        OrganizationsAssociations.associateBuildings(
            this.sequelizeModel,
            this.buildingsRepository.sequelizeModel,
            this.organizationBuildingsRepository.sequelizeModel
        );
    }

    public async GetAllPublic(
        params: IPaginationParams,
        allowedOrganizations: number[] | null = []
    ): Promise<IOrganizationDto[]> {
        try {
            return await this.sequelizeModel.findAll({
                attributes: ["id", "name"],
                where: allowedOrganizations === null ? {} : { id: { [Op.in]: allowedOrganizations } },
                order: [["id", "ASC"]],
                limit: params.limit,
                offset: params.offset,
            });
        } catch (err) {
            throw new GeneralError("Database error - GetAll", this.name, err, 500);
        }
    }

    public async GetAll(
        params: IPaginationParams,
        allowedOrganizations: number[] | null = []
    ): Promise<IOrganizationDetailDto[]> {
        try {
            return await this.sequelizeModel.findAll({
                include: [
                    {
                        model: this.userRepository.sequelizeModel,
                        as: "responsible_user",
                        attributes: { exclude: ["id", "organization_id"] },
                    },
                    {
                        model: this.buildingsRepository.sequelizeModel,
                        as: "buildings",
                        attributes: ["id", "building_name", "building_address_code"],
                        through: { attributes: [] },
                    },
                ],
                where: allowedOrganizations === null ? {} : { id: { [Op.in]: allowedOrganizations } },
                order: [
                    ["id", "ASC"],
                    [{ model: this.userRepository.sequelizeModel, as: "responsible_user" }, "id", "ASC"],
                    [{ model: this.buildingsRepository.sequelizeModel, as: "buildings" }, "id", "ASC"],
                ],
                limit: params.limit,
                offset: params.offset,
            });
        } catch (err) {
            throw new GeneralError("Database error - GetAllDetail", this.name, err, 500);
        }
    }

    public async GetOne(id: string, allowedOrganizations: number[] | null = []): Promise<IOrganizationDetailDto | null> {
        try {
            return await this.sequelizeModel.findOne({
                include: [
                    {
                        model: this.userRepository.sequelizeModel,
                        as: "responsible_user",
                    },
                    {
                        model: this.buildingsRepository.sequelizeModel,
                        as: "buildings",
                        attributes: ["id", "building_name", "building_address_code"],
                        through: { attributes: [] },
                    },
                ],
                where: { [Op.and]: [{ id }, allowedOrganizations === null ? {} : { id: { [Op.in]: allowedOrganizations } }] },
                order: [
                    [{ model: this.userRepository.sequelizeModel, as: "responsible_user" }, "id", "ASC"],
                    [{ model: this.buildingsRepository.sequelizeModel, as: "buildings" }, "id", "ASC"],
                ],
            });
        } catch (err) {
            throw new GeneralError("Database error - GetOne", this.name, err, 500);
        }
    }
}
