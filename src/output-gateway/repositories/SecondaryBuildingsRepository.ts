import { BuildingsSecondaryModel } from "#og/models/BuildingsSecondaryModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class SecondaryBuildingsRepository extends SequelizeModel {
    constructor() {
        super(BuildingsSecondaryModel.modelName, BuildingsSecondaryModel.tableName, BuildingsSecondaryModel.attributeModel, {
            schema: "energetics",
        });
    }

    GetAll(): never {
        throw new Error("Not implemented");
    }

    GetOne(): never {
        throw new Error("Not implemented");
    }
}
