import { EnergeticsSchema } from "#sch";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { OrganizationBuildingModel } from "#sch/models/OrganizationBuildingModel";

@injectable()
export class OrganizationBuildingsRepository extends SequelizeModel {
    constructor() {
        super("OrganizationBuildingsRepository", OrganizationBuildingModel.TABLE_NAME, OrganizationBuildingModel.attributeModel, {
            schema: EnergeticsSchema.pgSchema,
        });
    }

    public async GetAll(): Promise<never> {
        throw new Error("OrganizationBuildingsRepository - GetAll not implemented");
    }

    public async GetOne(): Promise<never> {
        throw new Error("OrganizationBuildingsRepository - GetOne not implemented");
    }
}
