import { EnergeticsSchema } from "#sch";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { OrganizationResponsibleUsersModel } from "#sch/models/OrganizationResponsibleUsersModel";

@injectable()
export class OrganizationResponsibleUsersRepository extends SequelizeModel {
    constructor() {
        super(
            "OrganizationResponsibleUsersRepository",
            OrganizationResponsibleUsersModel.TABLE_NAME,
            OrganizationResponsibleUsersModel.attributeModel,
            {
                schema: EnergeticsSchema.pgSchema,
            }
        );
    }

    public async GetAll(): Promise<never> {
        throw new Error("OrganizationResponsibleUsersRepository - GetAll not implemented");
    }

    public async GetOne(): Promise<never> {
        throw new Error("OrganizationResponsibleUsersRepository - GetOne not implemented");
    }
}
