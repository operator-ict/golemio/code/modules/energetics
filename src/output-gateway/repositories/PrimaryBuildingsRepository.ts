import { ISecondaryBuildingsOutput } from "#og/models/interfaces/ISecondaryBuildingsOutput";
import { BuildingsPrimaryModel } from "#sch/models/BuildingsPrimaryModel";
import { IBuildingsPrimary } from "#sch/models/interfaces/IPrimaryBuildings";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { SecondaryBuildingsRepository } from "./SecondaryBuildingsRepository";
import { Op, literal } from "@golemio/core/dist/shared/sequelize";
import { PG_SCHEMA } from "#sch/constants";

@injectable()
export class PrimaryBuildingsRepository extends SequelizeModel {
    private secondaryBuildingRepository: SecondaryBuildingsRepository;
    constructor() {
        super(BuildingsPrimaryModel.modelName, BuildingsPrimaryModel.tableName, BuildingsPrimaryModel.attributeModel, {
            schema: "energetics",
        });
        this.secondaryBuildingRepository = new SecondaryBuildingsRepository();
        this.associate();
    }

    public associate = () => {
        this.sequelizeModel.hasOne(this.secondaryBuildingRepository.sequelizeModel, {
            as: "secondary",
            foreignKey: "id",
            sourceKey: "id",
        });
    };

    public async GetAll(allowedOrganizations: number[] | null): Promise<IBuildingsPrimary[]> {
        try {
            return this.sequelizeModel.findAll<BuildingsPrimaryModel>({
                where: allowedOrganizations === null ? {} : { id: this.getWhereAccessLimitation(allowedOrganizations) },
                order: [["id", "DESC"]],
                raw: true,
            });
        } catch (err) {
            throw new GeneralError("Database error ~ GetAll BuildingsRepository", this.name, err, 500);
        }
    }

    public async GetAllFull(
        allowedOrganizations: number[] | null
    ): Promise<Array<IBuildingsPrimary & { secondary: ISecondaryBuildingsOutput }>> {
        try {
            return this.sequelizeModel.findAll<BuildingsPrimaryModel & { secondary: ISecondaryBuildingsOutput }>({
                include: [{ as: "secondary", model: this.secondaryBuildingRepository.sequelizeModel }],
                where: allowedOrganizations === null ? {} : { id: this.getWhereAccessLimitation(allowedOrganizations) },
                order: [["id", "DESC"]],
            });
        } catch (err) {
            throw new GeneralError("Database error ~ GetAll BuildingsRepository", this.name, err, 500);
        }
    }

    public override async GetOne(
        id: number,
        allowedOrganizations: number[] | null = []
    ): Promise<(IBuildingsPrimary & { secondary: ISecondaryBuildingsOutput }) | null> {
        try {
            return await this.sequelizeModel.findOne<BuildingsPrimaryModel & { secondary: ISecondaryBuildingsOutput }>({
                include: [{ as: "secondary", model: this.secondaryBuildingRepository.sequelizeModel }],
                where: {
                    [Op.and]: [
                        { id: id },
                        allowedOrganizations === null ? {} : { id: this.getWhereAccessLimitation(allowedOrganizations) },
                    ],
                },
            });
        } catch (err) {
            throw new GeneralError("Database error ~ GetOne BuildingsRepository", this.name, err, 500);
        }
    }

    private getWhereAccessLimitation = (allowedOrganizations: number[]) => {
        return {
            [Op.in]: literal(`(select distinct building_id from ${PG_SCHEMA}.organizations_buildings
where organization_id = any('{${allowedOrganizations.join(",")}}'::integer[]))`),
        };
    };
}
