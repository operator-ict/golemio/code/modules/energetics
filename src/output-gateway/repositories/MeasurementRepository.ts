import { EnergeticsSchema } from "#sch";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { Op, QueryTypes, WhereOptions, col, fn, literal } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IMeasurements, IMeasurementsMonthly } from "#og/controllers/interfaces/IMeasurementsParams";
import { IMeasurementResponse } from "#og/routers/interfaces/IMeasurementResponse";
import { MeasurementModel } from "#sch/models/MeasurementsModel";
import { PG_SCHEMA } from "#sch/constants";

@injectable()
export class MeasurementRepository extends SequelizeModel {
    constructor() {
        super("MeasurementsRepository", MeasurementModel.tableName, MeasurementModel.attributeModel, {
            schema: EnergeticsSchema.pgSchema,
        });
    }

    public async getMeasurements(options: IMeasurements, allowedOrganizations: number[] | null): Promise<IMeasurementResponse[]> {
        try {
            const where: WhereOptions = {
                [Op.and]: [
                    ...(options.variable ? [{ var: { [Op.eq]: options.variable } }] : []),
                    ...(options.type ? [{ type: { [Op.eq]: options.type } }] : []),
                    ...(options.addr1 && options.addr2 ? [{ addr: { [Op.eq]: `/${options.addr1}/${options.addr2}` } }] : []),
                    ...(options.dateFrom ? [{ timestamp: { [Op.gte]: options.dateFrom } }] : []),
                    ...(options.dateTo ? [{ timestamp: { [Op.lte]: options.dateTo } }] : []),
                    allowedOrganizations === null
                        ? {}
                        : {
                              addr: this.getWhereAccessLimitation(allowedOrganizations),
                          },
                ],
            };

            return await this.sequelizeModel.findAll({
                attributes: [["timestamp", "time"], "value", "addr", "meter", "type", "var"],
                where,
                order: [["timestamp", "DESC"]],
                limit: options.limit,
                offset: options.offset,
            });
        } catch (err) {
            throw new GeneralError("MeasurementsRepository error - getMeasurements", this.name, err, 500);
        }
    }

    public async getMonthlyReadings(
        options: IMeasurementsMonthly,
        allowedOrganizations: number[] | null
    ): Promise<Array<Omit<IMeasurementResponse, "time">>> {
        try {
            const result = await this.sequelizeModel.sequelize!.query<Omit<IMeasurementResponse, "time">>(
                `SELECT * from ${PG_SCHEMA}.monthly_report($address, $variable, $date_from, $date_to, $org_ids);`,
                {
                    bind: {
                        address: options.address,
                        variable: options.variable,
                        date_from: options.from,
                        date_to: options.to,
                        org_ids: allowedOrganizations ? allowedOrganizations.join(",") : null,
                    },
                    raw: true,
                    type: QueryTypes.SELECT,
                }
            );

            return result;
        } catch (err) {
            throw new GeneralError("MeasurementsRepository error - getMonthlyReadings", this.name, err, 500);
        }
    }

    public async GetAll(): Promise<never> {
        throw new Error("MeasurementsRepository - GetOne not implemented");
    }

    public async GetOne(): Promise<never> {
        throw new Error("MeasurementsRepository - GetOne not implemented");
    }

    private getWhereAccessLimitation = (allowedOrganizations: number[]) => {
        return {
            [Op.in]: literal(`(select distinct d.addr from ${PG_SCHEMA}.organizations_buildings b 
                inner join ${PG_SCHEMA}.consumption_energy_devices d on	b.building_id = d.building_id 
                where b.organization_id = any('{${allowedOrganizations.join(",")}}'::integer[]))`),
        };
    };
}
