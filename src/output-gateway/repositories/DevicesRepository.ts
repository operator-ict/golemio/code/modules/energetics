import { IPaginationParams } from "#og/controllers/interfaces/IPaginationParams";
import { EnergeticsSchema } from "#sch";
import { PG_SCHEMA } from "#sch/constants";
import { DevicesModel } from "#sch/models";
import { IDevicesModel } from "#sch/models/interfaces";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Op, literal } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class DevicesRepository extends SequelizeModel {
    constructor() {
        super("DevicesRepository", DevicesModel.TABLE_NAME, DevicesModel.attributeModel, {
            schema: EnergeticsSchema.pgSchema,
        });
    }

    public async GetAll(params: IPaginationParams, allowedOrganizations: number[] | null = []): Promise<IDevicesModel[]> {
        try {
            return await this.sequelizeModel.findAll({
                where: allowedOrganizations === null ? {} : { building_id: this.getWhereAccessLimitation(allowedOrganizations) },
                order: [["id", "ASC"]],
                limit: params.limit,
                offset: params.offset,
            });
        } catch (err) {
            throw new GeneralError("Database error - GetAll", this.name, err, 500);
        }
    }

    public async GetOne(id: string, allowedOrganizations: number[] | null = []): Promise<IDevicesModel | null> {
        try {
            return await this.sequelizeModel.findOne({
                where: {
                    [Op.and]: [
                        { id: id },
                        allowedOrganizations === null ? {} : { building_id: this.getWhereAccessLimitation(allowedOrganizations) },
                    ],
                },
            });
        } catch (err) {
            throw new GeneralError("Database error - GetOne", this.name, err, 500);
        }
    }

    private getWhereAccessLimitation = (allowedOrganizations: number[]) => {
        return {
            [Op.in]: literal(`(select distinct building_id from ${PG_SCHEMA}.organizations_buildings
where organization_id = any('{${allowedOrganizations.join(",")}}'::integer[]))`),
        };
    };
}
