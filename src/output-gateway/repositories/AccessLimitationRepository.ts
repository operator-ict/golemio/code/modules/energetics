import { EnergeticsSchema } from "#sch";
import { AccessLimitationModel } from "#sch/models/AccessLimitationModel";
import { IAccessLimitation } from "#sch/models/interfaces/IAccessLimitation";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractCachedRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractCachedRepository";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export class AccessLimitationRepository extends AbstractCachedRepository<IAccessLimitation> {
    schema = EnergeticsSchema.pgSchema;
    tableName = AccessLimitationModel.TABLE_NAME;

    constructor(connector: IDatabaseConnector, log: ILogger, cacheTTLInSeconds: number = 60 * 5) {
        super(connector, log, cacheTTLInSeconds);
        AccessLimitationModel.init(AccessLimitationModel.attributeModel, {
            tableName: AccessLimitationModel.TABLE_NAME,
            schema: EnergeticsSchema.pgSchema,
            sequelize: this.connector.getConnection(),
        });
    }

    protected getAllInternal(): Promise<IAccessLimitation[]> {
        return AccessLimitationModel.findAll({ raw: true });
    }

    public async getOrganizationsByGroupName(groupName: string): Promise<number[]> {
        try {
            const data = await this.getAll();
            const limitation = data.find((d) => d.group_name === groupName);
            if (!limitation) {
                throw new GeneralError(
                    `Access limitation for group ${groupName} not found`,
                    this.constructor.name,
                    undefined,
                    500
                );
            }

            return limitation.organization_ids;
        } catch (err) {
            if (err instanceof GeneralError) {
                throw err;
            }

            throw new GeneralError("Database error - getOrganizationsByGroupName", this.constructor.name, err, 500);
        }
    }
}
