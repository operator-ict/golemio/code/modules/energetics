import { BUILDING_PATH, ORGANIZATION_PATH } from "#og/constants/Links";
import { CreateLinkHelper } from "#og/helpers/CreateLinkHelper";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IOrganizationDetailDto } from "#og/repositories/interfaces/IOrganizationDetailDto";
import { IOrganizationDetailResponse } from "#og/routers/interfaces/IOrganizationDetailResponse";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class OrganizationDetailDtoTransformation extends AbstractTransformation<
    IOrganizationDetailDto,
    IOrganizationDetailResponse
> {
    public name: string = "OrganizationDetailDtoTransformation";
    private createLinkHelper: CreateLinkHelper;

    constructor() {
        super();
        this.createLinkHelper = OGEnergeticsContainer.resolve(ModuleContainerToken.CreateLinkHelper);
    }

    protected transformInternal = (element: IOrganizationDetailDto): IOrganizationDetailResponse => {
        return {
            id: element.id,
            name: element.name,
            label: element.label,
            category: element.category,
            created_by_id: element.created_by_id?.toString() ?? null,
            grafana_url: element.grafana_url,
            address: {
                street: element.address_street,
                house_number: element.address_house_number,
                city: element.address_city,
                country: element.address_country,
                mail: element.address_mail,
                phone: element.address_phone,
                web_address: element.address_web_address,
            },
            responsible_user: element.responsible_user.map((user) => ({
                name: user.first_name,
                last_name: user.last_name,
                position: user.position,
                phone: user.phone,
                mail: user.mail,
                company: user.company,
            })),
            buildings: element.buildings.map((building) => ({
                id: building.id,
                name: building.building_name,
                building_address_code: building.building_address_code,
                link: this.createLinkHelper.getLinkUrl(BUILDING_PATH, building.id),
            })),
            link: this.createLinkHelper.getLinkUrl(ORGANIZATION_PATH, element.id),
        };
    };
}
