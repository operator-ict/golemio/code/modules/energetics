import { BUILDING_PATH } from "#og/constants/Links";
import { CreateLinkHelper } from "#og/helpers/CreateLinkHelper";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IBuildingsPrimary } from "#sch/models/interfaces/IPrimaryBuildings";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class BuildingsTransformation extends AbstractTransformation<IBuildingsPrimary, IBuildingsTransformationOutput> {
    public name: string = "BuildingsTransformation";
    private createLinkHelper: CreateLinkHelper;

    constructor() {
        super();
        this.createLinkHelper = OGEnergeticsContainer.resolve(ModuleContainerToken.CreateLinkHelper);
    }

    protected transformInternal = (element: IBuildingsPrimary): IBuildingsTransformationOutput => {
        return {
            id: element.id,
            name: element.building_name ?? null,
            buildingAddressCode: element.building_address_code ?? null,
            link: this.createLinkHelper.getLinkUrl(BUILDING_PATH, element.id),
        };
    };
}
