import { BUILDING_PATH } from "#og/constants/Links";
import { CreateLinkHelper } from "#og/helpers/CreateLinkHelper";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IFullTransformationSchemaItem } from "#og/models/interfaces/IFullTransformationSchemaItem";
import { ISecondaryBuildingsOutput } from "#og/models/interfaces/ISecondaryBuildingsOutput";
import { IBuildingsPrimary } from "#sch/models/interfaces/IPrimaryBuildings";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { GeoCoordinatesType } from "@golemio/core/dist/output-gateway/Geo";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class BuildingTransformationFull extends AbstractTransformation<
    IBuildingsPrimary & { secondary: ISecondaryBuildingsOutput },
    IFullTransformationSchemaItem
> {
    public name: string = "BuildingTransformationFull";
    private createLinkHelper: CreateLinkHelper;

    constructor() {
        super();
        this.createLinkHelper = OGEnergeticsContainer.resolve(ModuleContainerToken.CreateLinkHelper);
    }

    protected transformInternal = (
        element: IBuildingsPrimary & { secondary: ISecondaryBuildingsOutput }
    ): IFullTransformationSchemaItem => {
        return {
            id: element.id,
            name: element.building_name,
            main_use: element.main_use,
            method_of_protection: element.secondary?.method_of_protection ?? null,
            address: {
                city: element.secondary?.address_city ?? null,
                country: element.secondary?.address_country ?? null,
                house_number: element.secondary?.address_house_number ?? null,
                mail: element.secondary?.address_mail ?? null,
                phone: element.secondary?.address_phone ?? null,
                street: element.secondary?.address_street ?? null,
                web_address: element.secondary?.address_web_address ?? null,
            },
            allotment_number: element.secondary?.allotment_number ?? null,
            beds_count: element.secondary?.beds_count ? parseFloat(element.secondary.beds_count) : null,
            building_address_code: element.secondary?.building_address_code ?? null,
            building_envelope: {
                filling_of_hole: {
                    area: element.secondary?.building_envelope_filling_of_hole_area
                        ? parseFloat(element.secondary.building_envelope_filling_of_hole_area)
                        : null,
                    construction: element.secondary?.building_envelope_filling_of_hole_construction ?? null,
                    technical_condition: element.secondary?.building_envelope_filling_of_hole_technical_condition ?? null,
                    year_of_adjustment: element.secondary?.building_envelope_filling_of_hole_year_of_adjustment ?? null,
                },
                floor_of_the_lowest_heated_floor: {
                    area: element.secondary?.building_envelope_floor_of_the_lowest_heated_floor_area
                        ? parseFloat(element.secondary.building_envelope_floor_of_the_lowest_heated_floor_area)
                        : null,
                    construction: element.secondary?.building_envelope_floor_of_the_lowest_heated_floor_construction ?? null,
                    year_of_adjustment:
                        element.secondary?.building_envelope_floor_of_the_lowest_heated_floor_year_of_adju ?? null,
                    technical_condition: element.secondary?.floor_of_the_lowest_heated_floor_technical_condition ?? null,
                    thermal_insulation: element.secondary?.floor_of_the_lowest_heated_floor_thermal_insulation ?? null,
                },
                roof: {
                    area: element.secondary?.building_envelope_roof_area
                        ? parseFloat(element.secondary.building_envelope_roof_area)
                        : null,
                    construction: element.secondary?.building_envelope_roof_construction ?? null,
                    technical_condition: element.secondary?.building_envelope_roof_technical_condition ?? null,
                    thermal_insulation: element.secondary?.building_envelope_roof_thermal_insulation ?? null,
                    year_of_adjustment: element.secondary?.building_envelope_roof_year_of_adjustment ?? null,
                },
                side_wall: {
                    area: element.secondary?.building_envelope_side_wall_area
                        ? parseFloat(element.secondary.building_envelope_side_wall_area)
                        : null,
                    heat_insulation: element.secondary?.building_envelope_side_wall_heat_insulation ?? null,
                    prevailing_construction: element.secondary?.building_envelope_side_wall_prevailing_construction ?? null,
                    technical_condition: element.secondary?.building_envelope_side_wall_technical_condition ?? null,
                    year_of_adjustment: element.secondary?.building_envelope_side_wall_year_of_adjustment ?? null,
                },
            },
            label: element.secondary?.building_label ?? null,
            built_up_area: element.secondary?.built_up_area ? parseFloat(element.secondary.built_up_area) : null,
            classrooms_count: element.secondary?.classrooms_count ? parseFloat(element.secondary.classrooms_count) : null,
            csu_code: element.secondary?.csu_code ?? null,
            current_note: element.secondary?.current_note ?? null,
            description: element.description ?? null,
            employees_count: element.secondary?.employees_count ? parseFloat(element.secondary.employees_count) : null,
            energetic_management: element.secondary?.energetic_management ?? null,
            energy_audits: {
                created_at: element.secondary?.energy_audits_created_at ?? null,
                ea_registration_number: element.secondary?.energy_audits_earegistration_string ?? null,
                energy_audit: element.secondary?.energy_audits_energy_audit ?? null,
            },
            eno_id: element.secondary?.eno_id ?? null,
            fuel_and_energy: {
                coal: element.secondary?.fuel_and_energy_coal ?? null,
                czt: element.secondary?.fuel_and_energy_czt ?? null,
                electricity: element.secondary?.fuel_and_energy_electricity ?? null,
                gas: element.secondary?.fuel_and_energy_gas ?? null,
                other: element.secondary?.fuel_and_energy_other ?? null,
                oze: element.secondary?.fuel_and_energy_oze ?? null,
            },

            heated_bulding_volume: element.secondary?.heated_bulding_volume
                ? parseFloat(element.secondary.heated_bulding_volume)
                : null,
            ku_code: element.secondary?.ku_code ?? null,
            location: element.secondary?.location?.coordinates
                ? {
                      type: "Point" as GeoCoordinatesType.Point,
                      coordinates: [...element.secondary.location.coordinates],
                  }
                : null,
            link: this.createLinkHelper.getLinkUrl(BUILDING_PATH, element.id) ?? null,
            oze_energy_production: {
                heat_pump: element.secondary?.oze_energy_production_heat_pump ?? null,
                integrated_turbines_wind_energy: element.secondary?.oze_energy_production_integrated_turbines_wind_energy ?? null,
                solar_energy_photothermal: element.secondary?.oze_energy_production_solar_energy_photothermal ?? null,
                solar_energy_photovoltaic: element.secondary?.oze_energy_production_solar_energy_photovoltaic ?? null,
            },
            penb: {
                building_envelope: element.secondary?.penb_building_envelope
                    ? parseFloat(element.secondary.penb_building_envelope)
                    : null,
                building_envelope_category: element.secondary?.penb_building_envelope_category ?? null,
                cooling: element.secondary?.penb_cooling ? parseFloat(element.secondary.penb_cooling) : null,
                cooling_category: element.secondary?.penb_cooling_category ?? null,
                heating: element.secondary?.penb_heating ? parseFloat(element.secondary.penb_heating) : null,
                heating_category: element.secondary?.penb_heating_category ?? null,
                humidity_adjustment: element.secondary?.penb_humidity_adjustment
                    ? parseFloat(element.secondary.penb_humidity_adjustment)
                    : null,
                humidity_adjustment_category: element.secondary?.penb_humidity_adjustment_category ?? null,
                issue_date: element.secondary?.penb_issue_date ?? null,
                lighting: element.secondary?.penb_lighting ? parseFloat(element.secondary.penb_lighting) : null,
                lighting_category: element.secondary?.penb_lighting_category ?? null,
                penb_number: element.secondary?.penb_penbnumber ?? null,
                primary_non_renewable_energy: element.secondary?.penb_primary_non_renewable_energy
                    ? parseFloat(element.secondary.penb_primary_non_renewable_energy)
                    : null,
                primary_non_renewable_energy_category: element.secondary?.penb_primary_non_renewable_energy_category ?? null,
                total_building_envelope_area: element.secondary?.penb_total_building_envelope_area
                    ? parseFloat(element.secondary.penb_total_building_envelope_area)
                    : null,
                total_energy_reference_area: element.secondary?.penb_total_energy_reference_area
                    ? parseFloat(element.secondary.penb_total_energy_reference_area)
                    : null,
                total_provided_energy: element.secondary?.penb_total_provided_energy
                    ? parseFloat(element.secondary.penb_total_provided_energy)
                    : null,
                total_provided_energy_category: element.secondary?.penb_total_provided_energy_category ?? null,
                ventilation: element.secondary?.penb_ventilation ? parseFloat(element.secondary.penb_ventilation) : null,
                ventilation_category: element.secondary?.penb_ventilation_category ?? null,
                volume_factor_of_av_shape: element.secondary?.penb_volume_factor_of_avshape ?? null,
                warm_water: element.secondary?.penb_warm_water ? parseFloat(element.secondary.penb_warm_water) : null,
                warm_water_category: element.secondary?.penb_warm_water_category ?? null,
            },
            registration_unit: element.secondary?.registration_unit ?? null,
            secondary_use: element.secondary_use ?? null,
            students_count: element.secondary?.students_count ? parseFloat(element.secondary.students_count) : null,
            technical_equipment: {
                cooling: {
                    cooling_area_percentage: element.secondary?.technical_equipment_cooling_cooling_area_percentage
                        ? parseFloat(element.secondary.technical_equipment_cooling_cooling_area_percentage)
                        : null,
                    cooling_system: element.secondary?.technical_equipment_cooling_cooling_system ?? null,
                    technical_condition: element.secondary?.technical_equipment_cooling_technical_condition ?? null,
                    year: element.secondary?.technical_equipment_cooling_year ?? null,
                },
                heating: {
                    heat_percentage: element.secondary?.technical_equipment_heating_heat_percentage
                        ? parseFloat(element.secondary.technical_equipment_heating_heat_percentage)
                        : null,
                    heating_system: element.secondary?.technical_equipment_heating_heating_system ?? null,
                    main_source_of_heat: element.secondary?.technical_equipment_heating_main_source_of_heat ?? null,
                    secondary_source_of_heat: element.secondary?.technical_equipment_heating_secondary_source_of_heat ?? null,
                    technical_condition: element.secondary?.technical_equipment_heating_technical_condition ?? null,
                    year: element.secondary?.technical_equipment_heating_year ?? null,
                },
                hot_water: {
                    hot_water_source: element.secondary?.technical_equipment_hot_water_hot_water_source ?? null,
                    predominant_way_of_heating_tv:
                        element.secondary?.technical_equipment_hot_water_predominant_way_of_heating_tv ?? null,
                    technical_condition: element.secondary?.technical_equipment_hot_water_technical_condition ?? null,
                    year: element.secondary?.technical_equipment_hot_water_year ?? null,
                },
                humidity_adjustment: {
                    humidity_adjustment: element.secondary?.technical_equipment_humidity_adjustment_humidity_adjustment ?? null,
                    technical_condition: element.secondary?.technical_equipment_humidity_adjustment_technical_condition ?? null,
                    year: element.secondary?.technical_equipment_humidity_adjustment_year ?? null,
                },
                lighting: {
                    lighting: element.secondary?.technical_equipment_lighting_lighting ?? null,
                    measurement_method: element.secondary?.technical_equipment_lighting_measurement_method ?? null,
                    other_technological_elements:
                        element.secondary?.technical_equipment_lighting_other_technological_elements ?? null,
                    technical_condition: element.secondary?.technical_equipment_lighting_technical_condition ?? null,
                    year: element.secondary?.technical_equipment_lighting_year ?? null,
                },
                ventilation: {
                    technical_condition: element.secondary?.technical_equipment_ventilation_technical_condition ?? null,
                    ventilation: element.secondary?.technical_equipment_ventilation_ventilation ?? null,
                    year: element.secondary?.technical_equipment_ventilation_year ?? null,
                },
            },
            waste_and_emissions: {
                operating_co2_emissions: element.secondary?.waste_and_emissions_operating_co_2emissions ?? null,
                solid_waste_production: element.secondary?.waste_and_emissions_solid_waste_production ?? null,
                sox_emissions: element.secondary?.waste_and_emissions_sox_emissions ?? null,
                tied_co2_emissions: element.secondary?.waste_and_emissions_tied_co2_emissions ?? null,
            },
            weekend_opening_hours: element.secondary?.weekend_opening_hours ?? null,
            opening_hours: element.secondary?.opening_hours ?? null,
            year_of_construction: element?.year_of_construction ?? null,
        };
    };
}
