import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IDeviceResponse } from "#og/routers/interfaces/IDeviceResponse";
import { IDevicesModel } from "#sch/models/interfaces";

@injectable()
export class DeviceDtoTransformation extends AbstractTransformation<IDevicesModel, IDeviceResponse> {
    public name: string = "DeviceDtoTransformation";

    constructor() {
        super();
    }

    protected transformInternal = (element: IDevicesModel): IDeviceResponse => {
        return {
            id: element.id,
            addr: element.addr,
            description: element.description,
            meter_number: element.meter_number,
            meter_index: !isNaN(parseInt(element.meter_index)) ? parseInt(element.meter_index) : null,
            location_number: element.location_number ? element.location_number : null,
            location_description: element.location_description,
            include_in_evaluation: element.include_in_evaluation,
            meter_type: element.meter_type,
            category: element.category,
            unit: element.unit,
            replaced_meter_id: element.replaced_meter_id ? element.replaced_meter_id : null,
            deleted: element.deleted,
            building_id: element.building_id,
        };
    };
}
