import { ORGANIZATION_PATH } from "#og/constants/Links";
import { CreateLinkHelper } from "#og/helpers/CreateLinkHelper";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IOrganizationDto } from "#og/repositories/interfaces/IOrganizationDto";
import { IOrganizationResponse } from "#og/routers/interfaces/IOrganizationResponse";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class OrganizationDtoTransformation extends AbstractTransformation<IOrganizationDto, IOrganizationResponse> {
    public name: string = "OrganizationDtoTransformation";
    private createLinkHelper: CreateLinkHelper;

    constructor() {
        super();
        this.createLinkHelper = OGEnergeticsContainer.resolve(ModuleContainerToken.CreateLinkHelper);
    }

    protected transformInternal = (element: IOrganizationDto): IOrganizationResponse => {
        return {
            id: element.id,
            name: element.name,
            link: this.createLinkHelper.getLinkUrl(ORGANIZATION_PATH, element.id),
        };
    };
}
