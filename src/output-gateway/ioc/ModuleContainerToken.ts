const ModuleContainerToken = {
    /** transformations */
    OrganizationDtoTransformation: Symbol("OrganizationDtoTransformation"),
    OrganizationDetailDtoTransformation: Symbol("OrganizationDetailDtoTransformation"),
    DeviceDtoTransformation: Symbol("DeviceDtoTransformation"),
    BuildingTransformationFull: Symbol("BuildingTransformationFull"),
    /** repositories */
    PrimaryBuildingsRepository: Symbol("PrimaryBuildingsRepository"),
    OrganizationsRepository: Symbol("OrganizationsRepository"),
    OrganizationBuildingsRepository: Symbol("OrganizationBuildingsRepository"),
    OrganizationResponsibleUsersRepository: Symbol("OrganizationResponsibleUsersRepository"),
    DevicesRepository: Symbol("DevicesRepository"),
    MeasurementRepository: Symbol("MeasurementRepository"),
    AccessLimitationRepository: Symbol(),
    /** controllers */
    BuildingsController: Symbol("BuildingController"),
    V2OrganizationsController: Symbol("V2OrganizationsController"),
    V2DevicesController: Symbol("V2DevicesController"),
    V2MeasurementsController: Symbol("V2MeasurementsController"),
    /** routers */
    V2BuildingsRouter: Symbol("V2BuildingsRouter"),
    V2OrganizationsRouter: Symbol("V2OrganizationsRouter"),
    V2DevicesRouter: Symbol("V2DevicesRouter"),
    V2MeasurementsRouter: Symbol("V2MeasurementsRouter"),
    /** helpers */
    CreateLinkHelper: Symbol("AbstractCreateLinkHelper"),
};

export { ModuleContainerToken };
