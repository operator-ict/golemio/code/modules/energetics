import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { DependencyContainer, instanceCachingFactory } from "@golemio/core/dist/shared/tsyringe";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { OrganizationsRepository } from "#og/repositories/OrganizationsRepository";
import { V2OrganizationsController } from "#og/controllers/v2/V2OrganizationsController";
import { OrganizationResponsibleUsersRepository } from "#og/repositories/OrganizationResponsibleUsersRepository";
import { PrimaryBuildingsRepository } from "#og/repositories/PrimaryBuildingsRepository";
import { OrganizationBuildingsRepository } from "#og/repositories/OrganizationBuildingsRepository";
import { DevicesRepository } from "#og/repositories/DevicesRepository";
import { V2DevicesController } from "#og/controllers/v2/V2DevicesController";
import { V2OrganizationsRouter } from "#og/routers/v2/V2OrganizationsRouter";
import { V2DevicesRouter } from "#og/routers/v2/V2DevicesRouter";
import { OrganizationDtoTransformation } from "#og/transformations/OrganizationDtoTransformation";
import { OrganizationDetailDtoTransformation } from "#og/transformations/OrganizationDetailDtoTransformation";
import { DeviceDtoTransformation } from "#og/transformations/DeviceDtoTransformation";
import { BuildingTransformationFull } from "#og/transformations/BuildingTransformationFull";
import { CreateLinkHelper } from "#og/helpers/CreateLinkHelper";
import { MeasurementRepository } from "#og/repositories/MeasurementRepository";
import { V2MeasurementsRouter } from "#og/routers/v2/V2MeasurementsRouter";
import { V2MeasurementsController } from "#og/controllers/v2/V2MeasurementsController";
import { V2BuildingsRouter } from "#og/routers/v2/V2BuildingsRouter";
import { AccessLimitationRepository } from "#og/repositories/AccessLimitationRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { BuildingsController } from "#og/controllers/v2/BuildingsController";

//#region Initialization
const energeticsContainer: DependencyContainer = OutputGatewayContainer.createChildContainer();
//#endregion

//#region Transformations
energeticsContainer
    .registerSingleton(ModuleContainerToken.OrganizationDtoTransformation, OrganizationDtoTransformation)
    .registerSingleton(ModuleContainerToken.OrganizationDetailDtoTransformation, OrganizationDetailDtoTransformation)
    .registerSingleton(ModuleContainerToken.DeviceDtoTransformation, DeviceDtoTransformation)
    .registerSingleton(ModuleContainerToken.BuildingTransformationFull, BuildingTransformationFull);
//#endregion

//#region Repositories
energeticsContainer
    .registerSingleton<PrimaryBuildingsRepository>(ModuleContainerToken.PrimaryBuildingsRepository, PrimaryBuildingsRepository)
    .registerSingleton<OrganizationsRepository>(ModuleContainerToken.OrganizationsRepository, OrganizationsRepository)
    .registerSingleton<OrganizationBuildingsRepository>(
        ModuleContainerToken.OrganizationBuildingsRepository,
        OrganizationBuildingsRepository
    )
    .registerSingleton<OrganizationResponsibleUsersRepository>(
        ModuleContainerToken.OrganizationResponsibleUsersRepository,
        OrganizationResponsibleUsersRepository
    )
    .registerSingleton<DevicesRepository>(ModuleContainerToken.DevicesRepository, DevicesRepository)
    .registerSingleton<MeasurementRepository>(ModuleContainerToken.MeasurementRepository, MeasurementRepository)
    .register<AccessLimitationRepository>(ModuleContainerToken.AccessLimitationRepository, {
        useFactory: instanceCachingFactory(
            (c) => new AccessLimitationRepository(c.resolve(CoreToken.PostgresConnector), c.resolve(CoreToken.Logger))
        ),
    });
//#endregion

//#region Controllers
energeticsContainer
    .registerSingleton<BuildingsController>(ModuleContainerToken.BuildingsController, BuildingsController)
    .registerSingleton<V2OrganizationsController>(ModuleContainerToken.V2OrganizationsController, V2OrganizationsController)
    .registerSingleton<V2DevicesController>(ModuleContainerToken.V2DevicesController, V2DevicesController)
    .registerSingleton<V2MeasurementsController>(ModuleContainerToken.V2MeasurementsController, V2MeasurementsController);
//#endregion

//#region Routers
energeticsContainer
    .registerSingleton<V2BuildingsRouter>(ModuleContainerToken.V2BuildingsRouter, V2BuildingsRouter)
    .registerSingleton<V2OrganizationsRouter>(ModuleContainerToken.V2OrganizationsRouter, V2OrganizationsRouter)
    .registerSingleton<V2DevicesRouter>(ModuleContainerToken.V2DevicesRouter, V2DevicesRouter)
    .registerSingleton<V2MeasurementsRouter>(ModuleContainerToken.V2MeasurementsRouter, V2MeasurementsRouter);
//#endregion

//#region Helpers
energeticsContainer.registerSingleton<CreateLinkHelper>(ModuleContainerToken.CreateLinkHelper, CreateLinkHelper);
//#endregion

export { energeticsContainer as OGEnergeticsContainer };
