import { DEVICES_PATH } from "#og/constants/Links";
import { RouteVersion } from "#og/constants/RouteVersion";
import { V2DevicesController } from "#og/controllers/v2/V2DevicesController";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
@injectable()
export class V2DevicesRouter extends AbstractRouter {
    private cacheHeaderMiddleware: CacheHeaderMiddleware;
    private maxAge = 30 * 60; // 30 minutes
    private staleWhileRevalidate = 60; // 1 minute

    constructor(@inject(ModuleContainerToken.V2DevicesController) private controller: V2DevicesController) {
        super(RouteVersion.v2, DEVICES_PATH);

        this.cacheHeaderMiddleware = OGEnergeticsContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.initRoutes();
    }

    protected initRoutes(): void {
        this.router.get(
            "/",
            query("accessLimit").exists(),
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.path),
            this.cacheHeaderMiddleware.getMiddleware(this.maxAge, this.staleWhileRevalidate),
            this.controller.getAll
        );

        this.router.get(
            "/:id",
            param("id").exists().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
            query("accessLimit").exists(),
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(this.maxAge, this.staleWhileRevalidate),
            this.controller.getOne
        );
    }
}
