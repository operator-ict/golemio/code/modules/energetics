import { MEASUREMENTS_PATH } from "#og/constants/Links";
import { RouteVersion } from "#og/constants/RouteVersion";
import { V2MeasurementsController } from "#og/controllers/v2/V2MeasurementsController";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { MEASUREMENT_TYPES, MEASUREMENT_VARIABLES } from "#sch/models/constants/MeasurementConstants";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { oneOf, query } from "@golemio/core/dist/shared/express-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class V2MeasurementsRouter extends AbstractRouter {
    private cacheHeaderMiddleware: CacheHeaderMiddleware;
    private maxAge = 30 * 60; // 30 minutes
    private staleWhileRevalidate = 60; // 1 minute

    constructor(@inject(ModuleContainerToken.V2MeasurementsController) private controller: V2MeasurementsController) {
        super(RouteVersion.v2, MEASUREMENTS_PATH);

        this.cacheHeaderMiddleware = OGEnergeticsContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.initRoutes();
    }

    protected initRoutes(): void {
        this.router.get(
            "/",
            [
                query("variable")
                    .optional()
                    .not()
                    .isEmpty({ ignore_whitespace: true })
                    .not()
                    .isArray()
                    .isIn(MEASUREMENT_VARIABLES),
                query("type").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray().isIn(MEASUREMENT_TYPES),
                query("addr1").optional(),
                query("addr2").optional(),
                oneOf([
                    [
                        query("addr1").exists().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                        query("addr2").exists().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                    ],
                    [query("addr1").not().exists(), query("addr2").not().exists()],
                ]),
                query("dateFrom").optional().isISO8601().not().isArray(),
                query("dateTo").optional().isISO8601().not().isArray(),
                query("accessLimit").exists(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.path),
            this.cacheHeaderMiddleware.getMiddleware(this.maxAge, this.staleWhileRevalidate),
            this.controller.getMeasurements
        );

        this.router.get(
            "/monthly-reading",
            [
                query("addr1").exists().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("addr2").exists().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("variable").exists().not().isEmpty({ ignore_whitespace: true }).not().isArray().isIn(MEASUREMENT_VARIABLES),
                query("year").exists().isInt().not().isArray(),
                query("month").exists().isInt({ min: 1, max: 12 }).not().isArray(),
                query("accessLimit").exists(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.path),
            this.cacheHeaderMiddleware.getMiddleware(this.maxAge, this.staleWhileRevalidate),
            this.controller.getMonthlyReadings
        );
    }
}
