import { BUILDING_PATH } from "#og/constants/Links";
import { RouteVersion } from "#og/constants/RouteVersion";
import { BuildingsController } from "#og/controllers/v2/BuildingsController";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway";
import { checkErrors } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class V2BuildingsRouter extends AbstractRouter {
    private controller: BuildingsController;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;
    private maxAge = 6 * 60 * 60; // 6 hours
    private staleWhileRevalidate = 60; // 1 minute

    constructor() {
        super(RouteVersion.v2, BUILDING_PATH);
        this.controller = OGEnergeticsContainer.resolve<BuildingsController>(ModuleContainerToken.BuildingsController);
        this.cacheHeaderMiddleware = OGEnergeticsContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.initRoutes();
    }

    protected initRoutes(): void {
        this.router.get(
            "/",
            query("full").optional().isBoolean(),
            query("accessLimit").exists(),
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(this.maxAge, this.staleWhileRevalidate),
            this.controller.getAll
        );
        this.router.get(
            "/:id",
            param("id").exists().isNumeric().not().isArray(),
            query("accessLimit").exists(),
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(this.maxAge, this.staleWhileRevalidate),
            this.controller.getOne
        );
    }
}
