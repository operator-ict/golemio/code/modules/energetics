import { IDevicesModel } from "#sch/models/interfaces";

export interface IDeviceResponse extends Omit<IDevicesModel, "location_number" | "replaced_meter_id" | "meter_index"> {
    location_number: string | null;
    replaced_meter_id: string | null;
    meter_index: number | null;
}
