export interface IMeasurementResponse {
    time: string;
    value: string;
    meter: string;
    type: string;
    addr: string;
    var: string;
}
