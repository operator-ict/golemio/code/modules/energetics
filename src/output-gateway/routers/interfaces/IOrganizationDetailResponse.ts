import { IOrganizationResponse } from "#og/routers/interfaces/IOrganizationResponse";

interface IResponsibleUserResponse {
    name: string | null;
    last_name: string | null;
    position: string | null;
    phone: string | null;
    mail: string | null;
    company: string | null;
}

interface IBuildingResponse {
    id: number;
    name: string | null;
    building_address_code: string | null;
    link: string;
}

export interface IOrganizationDetailResponse extends IOrganizationResponse {
    label: string | null;
    category: string | null;
    created_by_id: string | null;
    grafana_url: string | null;
    address: {
        street: string | null;
        house_number: string | null;
        city: string | null;
        country: string | null;
        mail: string | null;
        phone: string | null;
        web_address: string | null;
    };
    responsible_user: IResponsibleUserResponse[];
    buildings: IBuildingResponse[];
    link: string;
}
