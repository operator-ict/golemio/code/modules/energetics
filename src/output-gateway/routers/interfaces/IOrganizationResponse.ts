import { IOrganizationDto } from "#og/repositories/interfaces/IOrganizationDto";

export interface IOrganizationResponse extends IOrganizationDto {
    link: string;
}
