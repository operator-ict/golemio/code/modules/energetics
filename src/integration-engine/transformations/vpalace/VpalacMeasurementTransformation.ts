import { getUniqueBy } from "#ie/helpers/ParserHelpers";
import { EnergeticsSchema } from "#sch";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

export interface IVpalacMeasurement {
    var_id: string;
    time_measurement: string;
    value: number;
}
@injectable()
export class VpalacMeasurementTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = EnergeticsSchema.vpalac.measurement.name;
    }

    protected transformElement = (data: any): IVpalacMeasurement[] | undefined => {
        const { var_id, values } = data;

        if (!values.length) return;

        const result = getUniqueBy<any>(values, "timestamp").map((el: any) => ({
            time_measurement: new Date(el.timestamp).toISOString(),
            value: el.value,
            var_id,
        }));

        return result;
    };
}
