import { toStringOrEmpty } from "#ie/helpers/ParserHelpers";
import { IOictConsumptionInput } from "#sch/datasources/interfaces";
import { IConsumptionModel } from "#sch/models/interfaces";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";

export class ConsumptionTransformation extends BaseTransformation implements ITransformation {
    public name = "ConsumptionTransformation";

    protected transformElement = async (element: IOictConsumptionInput): Promise<IConsumptionModel> => {
        return {
            // PK
            addr: toStringOrEmpty(element.addr),
            meter: toStringOrEmpty(element.meter),
            time_utc: element.time,
            variable: toStringOrEmpty(element.var),

            type: toStringOrEmpty(element.type),
            value: Number(element.value),
        };
    };
}
