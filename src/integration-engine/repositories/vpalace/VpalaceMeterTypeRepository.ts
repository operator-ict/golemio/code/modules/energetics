import { EnergeticsSchema } from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { PostgresModel } from "@golemio/core/dist/integration-engine";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class VpalaceMeterTypeRepository extends PostgresModel implements IModel {
    constructor(@inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector) {
        super(
            EnergeticsSchema.vpalac.meterType.name + "Model",
            {
                outputSequelizeAttributes: EnergeticsSchema.vpalac.meterType.outputSequelizeAttributes,
                pgTableName: EnergeticsSchema.vpalac.meterType.pgTableName,
                pgSchema: EnergeticsSchema.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                EnergeticsSchema.vpalac.meterType.name + "ModelValidator",
                EnergeticsSchema.vpalac.meterType.outputJsonSchema
            )
        );
    }

    public saveBulk = async (data: any, t: Transaction) => {
        await this.validate(data);
        await this.sequelizeModel.bulkCreate(data, {
            updateOnDuplicate: Object.keys(data[0]).concat("updated_at"),
            transaction: t,
        });
    };
}
