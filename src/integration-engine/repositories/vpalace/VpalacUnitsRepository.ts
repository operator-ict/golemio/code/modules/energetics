import { EnergeticsSchema } from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { PostgresModel } from "@golemio/core/dist/integration-engine";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class VpalaceUnitsRepository extends PostgresModel implements IModel {
    constructor(@inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector) {
        super(
            EnergeticsSchema.vpalac.units.name + "Model",
            {
                outputSequelizeAttributes: EnergeticsSchema.vpalac.units.outputSequelizeAttributes,
                pgTableName: EnergeticsSchema.vpalac.units.pgTableName,
                pgSchema: EnergeticsSchema.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                EnergeticsSchema.vpalac.units.name + "ModelValidator",
                EnergeticsSchema.vpalac.units.outputJsonSchema
            )
        );
    }

    public saveBulk = async (data: any, t: Transaction) => {
        await this.validate(data);
        await this.sequelizeModel.bulkCreate(data, {
            updateOnDuplicate: Object.keys(data[0]).concat("updated_at"),
            transaction: t,
        });
    };
}
