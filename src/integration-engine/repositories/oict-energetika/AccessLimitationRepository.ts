import { EnergeticsSchema } from "#sch";
import { AccessLimitationModel } from "#sch/models/AccessLimitationModel";
import { IAccessLimitation } from "#sch/models/interfaces/IAccessLimitation";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IModel } from "@golemio/core/dist/integration-engine/models/IModel";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models/PostgresModel";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class AccessLimitationRepository extends PostgresModel implements IModel {
    constructor(@inject(CoreToken.Logger) private logger: ILogger) {
        super(
            "AccessLimitationRepository",
            {
                pgTableName: AccessLimitationModel.TABLE_NAME,
                pgSchema: EnergeticsSchema.pgSchema,
                outputSequelizeAttributes: AccessLimitationModel.attributeModel,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("AccessLimitationRepositoryValidator", AccessLimitationModel.jsonSchema)
        );
    }

    public async updateData(data: IAccessLimitation[]): Promise<void> {
        const dateBeforeUpdated = new Date();
        const model = this["sequelizeModel"];
        const transaction = await model.sequelize?.transaction();

        if (!transaction) {
            throw new GeneralError("Unable to get transaction for updating access limitations.", this.constructor.name);
        }

        try {
            await this.validate(data);
            await this.bulkSave<IAccessLimitation>(data, undefined, undefined, undefined, transaction);
            await model.destroy({
                where: {
                    updated_at: {
                        [Op.lte]: dateBeforeUpdated,
                    },
                },
                transaction: transaction,
            });
            await transaction.commit();
        } catch (error) {
            this.logger.error("Error while updating access limitations", error);
            await transaction.rollback();
        }

        return;
    }
}
