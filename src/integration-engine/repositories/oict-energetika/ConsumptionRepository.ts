import { EnergeticsSchema } from "#sch";
import { ConsumptionModel } from "#sch/models/ConsumptionModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class ConsumptionRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "ConsumptionRepository",
            {
                pgTableName: ConsumptionModel.TABLE_NAME,
                pgSchema: EnergeticsSchema.pgSchema,
                outputSequelizeAttributes: ConsumptionModel.attributeModel,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("ConsumptionRepositoryValidator", ConsumptionModel.arrayJsonSchema)
        );
    }

    public saveData = async (data: ConsumptionModel[]): Promise<ConsumptionModel[]> => {
        try {
            return await this.sequelizeModel.bulkCreate<ConsumptionModel>(data, { ignoreDuplicates: true });
        } catch (err) {
            throw new GeneralError(`[${this.constructor.name}] Could not save data: ${err.message}`, this.constructor.name, err);
        }
    };
}
