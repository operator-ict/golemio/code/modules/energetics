import { IFetchConsumptionInput } from "#ie/interfaces/oict-energetika/IFetchConsumptionInput";
import { IFetchDataInput } from "#ie/interfaces/oict-energetika/IFetchDataInput";
import { FetchDataValidationSchema } from "#ie/schema/oict-energetika/FetchDataSchema";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { DateTime, Interval } from "@golemio/core/dist/shared/luxon";

export class FetchDataTask extends AbstractTask<IFetchDataInput> {
    public readonly queueName = "fetchData";
    public readonly queueTtl = 29 * 60 * 1000; // 29 minutes
    public readonly schema = FetchDataValidationSchema;

    constructor(queuePrefix: string) {
        super(queuePrefix);
    }

    protected async execute({ targetDays }: IFetchDataInput) {
        for (const dateParams of this.generateDateParams(targetDays)) {
            await QueueManager.sendMessageToExchange(this.queuePrefix, "fetchConsumption", dateParams);
        }
    }

    private *generateDateParams(targetDays: number): Generator<IFetchConsumptionInput> {
        const currentDate = DateTime.now().startOf("day");
        const leftBoundDate = currentDate.minus({ days: targetDays });
        const intervals = Interval.fromDateTimes(leftBoundDate, currentDate).splitBy({ months: 1 });

        for (const interval of intervals) {
            const start = interval.start.startOf("day");
            const end = interval.end.startOf("day");

            yield {
                dateFrom: start.toISODate(),
                dateTo: end.toISODate(),
            };
        }
    }
}
