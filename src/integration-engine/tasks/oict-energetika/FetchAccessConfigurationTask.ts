import { AccessConfigurationDataSource } from "#ie/commodity-providers/datasources/static-data/AccessConfigurationDataSource";
import { CommodityContainer } from "#ie/commodity-providers/ioc/Di";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { AccessLimitationRepository } from "#ie/repositories/oict-energetika/AccessLimitationRepository";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";

export class FetchAccessConfigurationTask extends AbstractEmptyTask {
    queueName: string = "fetchAccessConfiguration";
    public readonly queueTtl = 10 * 60 * 1000; // 10 minutes
    private accessConfigurationDataSource: AccessConfigurationDataSource;
    private accessRepository: AccessLimitationRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.accessConfigurationDataSource = CommodityContainer.resolve<AccessConfigurationDataSource>(
            WorkerContainerToken.AccessConfigurationDataSource
        );
        this.accessRepository = CommodityContainer.resolve<AccessLimitationRepository>(
            WorkerContainerToken.AccessLimitationRepository
        );
    }

    protected async execute(): Promise<void> {
        const results = await this.accessConfigurationDataSource.getDataSource().getAll();
        await this.accessRepository.updateData(results);
    }
}
