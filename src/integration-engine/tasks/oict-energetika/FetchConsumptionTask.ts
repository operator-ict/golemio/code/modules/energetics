import { OictResourceType } from "#ie/datasources/oict-energetika/helpers";
import { OictDataSourceFactory } from "#ie/datasources/oict-energetika/OictDataSourceFactory";
import { IFetchConsumptionInput } from "#ie/interfaces/oict-energetika/IFetchConsumptionInput";
import { ConsumptionRepository } from "#ie/repositories/oict-energetika/ConsumptionRepository";
import { FetchConsumptionValidationSchema } from "#ie/schema/oict-energetika/FetchConsumptionSchema";
import { ConsumptionTransformation } from "#ie/transformations/oict-energetika/ConsumptionTransformation";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime, Interval } from "@golemio/core/dist/shared/luxon";

export class FetchConsumptionTask extends AbstractTask<IFetchConsumptionInput> {
    public readonly queueName = "fetchConsumption";
    public readonly queueTtl = 10 * 60 * 60 * 1000; // 10 hours
    public readonly schema = FetchConsumptionValidationSchema;

    private readonly consumptionRepository: ConsumptionRepository;
    private readonly consumptionTransformation: ConsumptionTransformation;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.consumptionRepository = new ConsumptionRepository();
        this.consumptionTransformation = new ConsumptionTransformation();
    }

    protected async execute(dateParams: IFetchConsumptionInput) {
        const dataSources = this.getDataSources(dateParams);

        for (const dataSource of dataSources) {
            try {
                const data = await dataSource.getAll();
                const transformedData = await this.consumptionTransformation.transform(data);
                await this.consumptionRepository.saveData(transformedData);
            } catch (err) {
                log.error(err);
                throw new GeneralError("Failed to fetch consumption data", this.constructor.name, err);
            }
        }
    }

    private *getDataSources(dateParams: IFetchConsumptionInput): Generator<DataSource> {
        const dateFrom = DateTime.fromISO(dateParams.dateFrom).startOf("day");
        const dateTo = DateTime.fromISO(dateParams.dateTo).startOf("day");
        const intervals = Interval.fromDateTimes(dateFrom, dateTo).splitBy({ days: 5 });

        for (const interval of intervals) {
            const start = interval.start.startOf("day");
            const end = interval.end.startOf("day");

            yield OictDataSourceFactory.getDataSource(OictResourceType.Consumption, {
                dateFrom: start.toISODate(),
                dateTo: end.toISODate(),
            });

            yield OictDataSourceFactory.getDataSource(OictResourceType.ConsumptionVisapp, {
                dateFrom: start.toISODate(),
                dateTo: end.toISODate(),
            });
        }
    }
}
