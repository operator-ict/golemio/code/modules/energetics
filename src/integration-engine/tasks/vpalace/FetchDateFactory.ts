import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MeasurementDataFetcher } from "./MeasurementDataFetcher";
import { MeasuringEquipmentFetcher } from "./MeasuringEquipmentFetcher";
import { MeterTypeFetcher } from "./MeterTypeFetcher";
import { TypeMeasuringEquipmentFetcher } from "./TypeMeasuringEquipmentFetcher";
import { UnitDataFetcher } from "./UnitDataFetcher";

@injectable()
export class FetchDateFactory {
    constructor(
        @inject(ModuleContainerToken.MeasurementDataFetcher)
        private readonly fetchMeasurementData: MeasurementDataFetcher,

        @inject(ModuleContainerToken.MeasuringEquipmentFetcher)
        private readonly fetchMeasuringEquipment: MeasuringEquipmentFetcher,

        @inject(ModuleContainerToken.MeterTypeFetcher)
        private readonly fetchMeterType: MeterTypeFetcher,

        @inject(ModuleContainerToken.TypeMeasuringEquipmentFetcher)
        private readonly fetchTypeMeasuringEquipment: TypeMeasuringEquipmentFetcher,

        @inject(ModuleContainerToken.UnitDataFetcher)
        private readonly fetchUnitData: UnitDataFetcher,

        @inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector
    ) {}

    public async fetchAndSaveData(from: string, to: string, authCookie: string) {
        const connection = this.databaseConnector.getConnection();
        const t = await connection.transaction();
        try {
            await this.fetchMeasurementData.execute(from, to, authCookie, t);
            await this.fetchMeasuringEquipment.execute(from, to, authCookie, t);
            await this.fetchMeterType.execute(from, to, authCookie, t);
            await this.fetchTypeMeasuringEquipment.execute(from, to, authCookie, t);
            await this.fetchUnitData.execute(from, to, authCookie, t);
            await t.commit();
        } catch (error) {
            await t.rollback();
            throw new GeneralError("Error while saving data FetchXDaysDataTask", this.constructor.name, error);
        }
    }
}
