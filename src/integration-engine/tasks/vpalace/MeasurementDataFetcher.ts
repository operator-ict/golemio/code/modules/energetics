import { VpalaceMeasurementDataSource } from "#ie/datasources/vpalace/VpalaceMeasurementDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { VpalaceMeasurementRepository } from "#ie/repositories/vpalace/VpalaceMeasurementRepository";
import { VpalacMeasurementTransformation } from "#ie/transformations/vpalace/VpalacMeasurementTransformation";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class MeasurementDataFetcher {
    constructor(
        //measurements
        @inject(ModuleContainerToken.VpalacMeasurementTransformation)
        private readonly transformationVpalaceMeasurement: VpalacMeasurementTransformation,
        @inject(ModuleContainerToken.VpalaceMeasurementRepository)
        private readonly modelVpalaceMeasurement: VpalaceMeasurementRepository,
        @inject(ModuleContainerToken.VpalaceMeasurementDataSource)
        private readonly datasourceVpalaceMeasurement: VpalaceMeasurementDataSource
    ) {}

    public execute = async (from: string, to: string, authCookie: string, t: Transaction) => {
        const dataMeasurement = await (await this.datasourceVpalaceMeasurement.getDataSource(from, to, authCookie)).getAll();

        const transformedDataMeasurement = await this.transformationVpalaceMeasurement.transform(dataMeasurement);
        for (const items of transformedDataMeasurement) {
            await this.modelVpalaceMeasurement.saveBulk(items, t);
        }
    };
}
