import { VpalaceMeterTypeDataSource } from "#ie/datasources/vpalace/VpalaceMeterTypeDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { VpalaceMeterTypeRepository } from "#ie/repositories/vpalace/VpalaceMeterTypeRepository";
import { VpalacMeterTypeTransformation } from "#ie/transformations/vpalace/VpalacMeterTypeTransformation";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class MeterTypeFetcher {
    constructor(
        //meter type
        @inject(ModuleContainerToken.VpalaceMeterTypeDataSource)
        private readonly datasourceVpalaceMeterType: VpalaceMeterTypeDataSource,
        @inject(ModuleContainerToken.VpalaceMeterTypeRepository)
        private readonly modelVpalaceMeterType: VpalaceMeterTypeRepository,
        @inject(ModuleContainerToken.VpalacMeterTypeTransformation)
        private readonly transformationVpalaceMeterType: VpalacMeterTypeTransformation
    ) {}

    public execute = async (from: string, to: string, authCookie: string, t: Transaction) => {
        const dataMeterType = await (await this.datasourceVpalaceMeterType.getDataSource(from, to, authCookie)).getAll();
        const transformedDataMeterType = await this.transformationVpalaceMeterType.transform(dataMeterType);
        await this.modelVpalaceMeterType.saveBulk(transformedDataMeterType, t);
    };
}
