import { VpalaceMeasuringEquipmentDataSource } from "#ie/datasources/vpalace/VpalaceMeasuringEquipmentDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { VpalaceMeasuringEquipmentRepository } from "#ie/repositories/vpalace/VpalaceMeasuringEquipmentRepository";
import { VpalacMeasuringEquipmentTransformation } from "#ie/transformations/vpalace/VpalacMeasuringEquipmentTransformation";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class MeasuringEquipmentFetcher {
    constructor(
        //measuring equipment
        @inject(ModuleContainerToken.VpalaceMeasuringEquipmentDataSource)
        private readonly datasourceVpalaceMeasuringEquipment: VpalaceMeasuringEquipmentDataSource,
        @inject(ModuleContainerToken.VpalaceMeasuringEquipmentRepository)
        private readonly modelVpalaceMeasuringEquipment: VpalaceMeasuringEquipmentRepository,
        @inject(ModuleContainerToken.VpalacMeasuringEquipmentTransformation)
        private readonly transformationVpalaceMeasuringEquipment: VpalacMeasuringEquipmentTransformation
    ) {}

    public execute = async (from: string, to: string, authCookie: string, t: Transaction) => {
        const dataMeasuringEquipment = await (
            await this.datasourceVpalaceMeasuringEquipment.getDataSource(from, to, authCookie)
        ).getAll();
        const transformedDataMeasuringEquipment = await this.transformationVpalaceMeasuringEquipment.transform(
            dataMeasuringEquipment
        );
        await this.modelVpalaceMeasuringEquipment.saveBulk(transformedDataMeasuringEquipment, t);
    };
}
