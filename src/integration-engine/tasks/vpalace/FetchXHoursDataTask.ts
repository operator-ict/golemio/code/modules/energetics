import { ICemConfig, UnimonitorCemApiHelper } from "#ie/helpers";
import { IFetchXHoursDataInput } from "#ie/interfaces/vpalace/IFetchXHoursDataInput";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { FetchXHoursDataValidationSchema } from "#ie/schema/vpalace/FetchXHoursDataValidationSchema";
import { EnergeticsSchema } from "#sch";
import { dateTime } from "@golemio/core/dist/helpers";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers/AbstractTask";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { FetchDateFactory } from "./FetchDateFactory";

@injectable()
export default class FetchXHoursDataTask extends AbstractTask<IFetchXHoursDataInput> {
    public readonly queueName = "fetchVpalacXHoursData";
    public readonly schema = FetchXHoursDataValidationSchema;
    public readonly queueTtl = 60 * 60 * 1000; //> 1 hour
    private readonly unimonitorCemApi: UnimonitorCemApiHelper;

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(ModuleContainerToken.FetchDateFactory)
        private readonly fetchDataFactory: FetchDateFactory
    ) {
        super(EnergeticsSchema.name.toLowerCase());

        const { url, config: cemConfig } = this.getProviderConfig();
        this.unimonitorCemApi = new UnimonitorCemApiHelper(url, cemConfig);
    }

    protected async execute(msg: IFetchXHoursDataInput): Promise<void> {
        const authCookie = await this.unimonitorCemApi.createSession();
        try {
            const targetHours = msg.targetHours;
            const now = dateTime(new Date()).setTimeZone(UnimonitorCemApiHelper.API_DATE_TZ);
            const timeFrom = now.clone().subtract(targetHours, "hours").valueOf().toString();
            const timeTo = now.valueOf().toString();
            await this.fetchDataFactory.fetchAndSaveData(timeFrom, timeTo, authCookie);
        } catch (err) {
            throw new GeneralError(`Error while executing FetchXHoursDataTask.`, this.constructor.name, err);
        } finally {
            await this.unimonitorCemApi.terminateSession(authCookie);
        }
    }

    private getProviderConfig(): { url: string; config: ICemConfig } {
        return {
            url: this.config.getValue("module.UnimonitorCemApiEnergetics.url") as string,
            config: this.config.getValue("module.UnimonitorCemApiEnergetics") as ICemConfig,
        };
    }
}
