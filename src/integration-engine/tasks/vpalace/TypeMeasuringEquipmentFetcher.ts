import { VpalaceTypeMeasuringEquipmentDataSource } from "#ie/datasources/vpalace/VpalaceTypeMeasuringEquipmentDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { VpalaceTypeMeasuringEquipmentRepository } from "#ie/repositories/vpalace/VpalaceTypeMeasuringEquipmentRepository";
import { VpalacTypeMeasuringEquipmentTransformation } from "#ie/transformations/vpalace/VpalacTypeMeasuringEquipmentTransformation";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class TypeMeasuringEquipmentFetcher {
    constructor(
        //type measuring equipment
        @inject(ModuleContainerToken.VpalaceTypeMeasuringEquipmentDataSource)
        private readonly datasourceVpalaceTypeMeasuringEquipment: VpalaceTypeMeasuringEquipmentDataSource,
        @inject(ModuleContainerToken.VpalaceTypeMeasuringEquipmentRepository)
        private readonly modelVpalaceTypeMeasuringEquipment: VpalaceTypeMeasuringEquipmentRepository,
        @inject(ModuleContainerToken.VpalacTypeMeasuringEquipmentTransformation)
        private readonly transformationVpalaceTypeMeasuringEquipment: VpalacTypeMeasuringEquipmentTransformation
    ) {}

    public execute = async (from: string, to: string, authCookie: string, t: Transaction) => {
        const dataTypeMeasuringEquipment = await (
            await this.datasourceVpalaceTypeMeasuringEquipment.getDataSource(from, to, authCookie)
        ).getAll();
        const transformedDataTypeMeasuringEquipment = await this.transformationVpalaceTypeMeasuringEquipment.transform(
            dataTypeMeasuringEquipment
        );
        await this.modelVpalaceTypeMeasuringEquipment.saveBulk(transformedDataTypeMeasuringEquipment, t);
    };
}
