import { VpalaceUnitsDataSource } from "#ie/datasources/vpalace/VpalaceUnitsDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { VpalaceUnitsRepository } from "#ie/repositories/vpalace/VpalacUnitsRepository";
import { VpalacUnitsTransformation } from "#ie/transformations/vpalace/VpalacUnitsTransformation";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class UnitDataFetcher {
    constructor(
        //units
        @inject(ModuleContainerToken.VpalaceUnitsDataSource)
        private readonly datasourceVpalaceUnits: VpalaceUnitsDataSource,
        @inject(ModuleContainerToken.VpalaceUnitsRepository)
        private readonly modelVpalaceUnits: VpalaceUnitsRepository,
        @inject(ModuleContainerToken.VpalacUnitsTransformation)
        private readonly transformationVpalaceUnits: VpalacUnitsTransformation
    ) {}

    public execute = async (from: string, to: string, authCookie: string, t: Transaction) => {
        const dataUnits = await (await this.datasourceVpalaceUnits.getDataSource(from, to, authCookie)).getAll();
        const transformedDataUnits = await this.transformationVpalaceUnits.transform(dataUnits);
        await this.modelVpalaceUnits.saveBulk(transformedDataUnits, t);
    };
}
