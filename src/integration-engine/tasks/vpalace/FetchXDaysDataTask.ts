import { ICemConfig, UnimonitorCemApiHelper } from "#ie/helpers";
import { IFetchXDaysData } from "#ie/interfaces/vpalace/IFetchXDaysDataInput";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { FetchXDaysDataValidationSchema } from "#ie/schema/vpalace/FetchXDaysDataValidationSchema";
import { EnergeticsSchema } from "#sch";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers/AbstractTask";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime, Interval } from "@golemio/core/dist/shared/luxon";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { FetchDateFactory } from "./FetchDateFactory";

@injectable()
export default class FetchXDaysDataTask extends AbstractTask<IFetchXDaysData> {
    public readonly queueName = "fetchVpalacXDaysData";
    public readonly schema = FetchXDaysDataValidationSchema;
    public readonly queueTtl = 24 * 60 * 60 * 1000; // 24 hours
    private readonly unimonitorCemApi: UnimonitorCemApiHelper;

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(ModuleContainerToken.FetchDateFactory)
        private readonly fetchDataFactory: FetchDateFactory
    ) {
        super(EnergeticsSchema.name.toLowerCase());

        const { url, config: cemConfig } = this.getProviderConfig();
        this.unimonitorCemApi = new UnimonitorCemApiHelper(url, cemConfig);
    }

    protected async execute(msg: IFetchXDaysData): Promise<void> {
        const authCookie = await this.unimonitorCemApi.createSession();
        try {
            const targetDays = msg.targetDays;
            const currentDate = DateTime.now().startOf("day");
            const dateFrom = currentDate.minus({ days: targetDays });
            const intervals = Interval.fromDateTimes(dateFrom, currentDate).splitBy({ days: 1 });

            for (const interval of intervals) {
                const from = interval.start.startOf("day").toFormat(UnimonitorCemApiHelper.API_DATE_FORMAT);
                const to = interval.end.startOf("day").toFormat(UnimonitorCemApiHelper.API_DATE_FORMAT);

                await this.fetchDataFactory.fetchAndSaveData(from, to, authCookie);
            }
        } catch (err) {
            throw new GeneralError(`Error while executing FetchXDaysDataTask.`, this.constructor.name, err);
        } finally {
            await this.unimonitorCemApi.terminateSession(authCookie);
        }
    }

    private getProviderConfig(): { url: string; config: ICemConfig } {
        return {
            url: this.config.getValue("module.UnimonitorCemApiEnergetics.url") as string,
            config: this.config.getValue("module.UnimonitorCemApiEnergetics") as ICemConfig,
        };
    }
}
