import { PG_SCHEMA } from "#sch/constants";
import { StaticMeterSettingsModel } from "#sch/models/StaticMeterSettingsModel";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { StaticDataResourceType } from "#sch/models/helpers/StaticDataResourceTypeEnum";
import { ResourceTypeDict } from "#sch/models/interfaces/IStaticDataOutputDto";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractValidatableRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractValidatableRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError, ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class StaticMeterSettingsRepository extends AbstractValidatableRepository {
    public validator: JSONSchemaValidator;
    public schema = PG_SCHEMA;
    public tableName = StaticMeterSettingsModel.tableName;

    private sequelizeModel: ModelStatic<StaticMeterSettingsModel>;

    constructor(@inject(CoreToken.PostgresConnector) connector: IDatabaseConnector, @inject(CoreToken.Logger) logger: ILogger) {
        super(connector, logger);
        this.validator = new JSONSchemaValidator("static_data", StaticMeterSettingsModel.jsonSchema);
        this.sequelizeModel = connector
            .getConnection()
            .define(this.tableName, StaticMeterSettingsModel.attributeModel, { schema: this.schema });
    }

    public async saveData<T extends StaticDataResourceType>(
        provider: CommodityProviderType,
        resourceType: T,
        data: ResourceTypeDict[T]
    ): Promise<void> {
        try {
            await this.validator.Validate({ provider, resource_type: resourceType, data });
        } catch (err) {
            throw new ValidationError("Static data validation error", this.constructor.name, err);
        }

        try {
            await this.sequelizeModel.upsert({ provider, resource_type: resourceType, data });
        } catch (err) {
            throw new GeneralError("Static data saving error", this.constructor.name, err);
        }
    }

    public async getData<T extends StaticDataResourceType>(
        provider: CommodityProviderType,
        resourceType: T
    ): Promise<ResourceTypeDict[T] | null> {
        try {
            const result = await this.sequelizeModel.findOne({
                attributes: ["data"],
                where: { provider, resource_type: resourceType },
            });
            return (result?.data as ResourceTypeDict[T]) ?? null;
        } catch (err) {
            throw new GeneralError("Static data fetching error", this.constructor.name, err);
        }
    }
}
