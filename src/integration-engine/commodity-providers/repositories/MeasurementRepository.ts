import { PG_SCHEMA } from "#sch/constants";
import { MeasurementModel } from "#sch/models/MeasurementsModel";
import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractValidatableRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractValidatableRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError, ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class MeasurementRepository extends AbstractValidatableRepository {
    public validator: JSONSchemaValidator;
    public schema = PG_SCHEMA;
    public tableName = MeasurementModel.tableName;

    private sequelizeModel: ModelStatic<MeasurementModel>;

    constructor(@inject(CoreToken.PostgresConnector) connector: IDatabaseConnector, @inject(CoreToken.Logger) logger: ILogger) {
        super(connector, logger);
        this.validator = new JSONSchemaValidator(MeasurementModel.tableName, MeasurementModel.arrayJsonSchema);
        this.sequelizeModel = connector
            .getConnection()
            .define(this.tableName, MeasurementModel.attributeModel, { schema: this.schema });
    }

    public async bulkSave(data: IMeasurementDto[]): Promise<void> {
        try {
            await this.validator.Validate(data);
        } catch (err) {
            throw new ValidationError("Static data validation error", this.constructor.name, err);
        }

        try {
            await this.sequelizeModel.bulkCreate(data, {
                updateOnDuplicate: MeasurementModel.attributesToUpdate as Array<keyof MeasurementModel>,
            });
        } catch (error) {
            throw new GeneralError("Error while saving data", this.constructor.name, error);
        }
    }
}
