import { CommodityProviderDataSourceFactory } from "#ie/commodity-providers/datasources/CommodityProviderDataSourceFactory";
import { CemApiDataSourceProvider } from "#ie/commodity-providers/datasources/cem-api/CemApiDataSourceProvider";
import { StaticMeterSettingsDataSourceProvider } from "#ie/commodity-providers/datasources/static-data/StaticMeterSettingsDataSourceProvider";
import { MeasurementRepository } from "#ie/commodity-providers/repositories/MeasurementRepository";
import { CemMeasurementTransformation } from "#ie/commodity-providers/transformations/CemMeasurementTransformation";
import { FetchMeasurementOptionsStaticDataTask } from "#ie/commodity-providers/workers/tasks/FetchMeasurementOptionsStaticDataTask";
import { FetchVeoliaMeasurementsTask } from "#ie/commodity-providers/workers/tasks/FetchVeoliaMeasurementsTask";
import { VPalaceContainer } from "#ie/ioc/Di";
import { AccessLimitationRepository } from "#ie/repositories/oict-energetika/AccessLimitationRepository";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { PpasAveApiDataSourceProvider } from "../datasources/ppas-ave-api/PpasAveApiDataSourceProvider";
import { PreElectroApiDataSourceProvider } from "../datasources/pre-elektro-api/PreElectroApiDataSourceProvider";
import { PreElectroHelper } from "../datasources/pre-elektro-api/helpers/PreElectroHelper";
import { AccessConfigurationDataSource } from "../datasources/static-data/AccessConfigurationDataSource";
import { StaticMeterSettingsRepository } from "../repositories/StaticMeterSettingsRepository";
import { AveMeasurementTransformation } from "../transformations/AveMeasurementTransformation";
import { PreElectroTransformation } from "../transformations/PreElectroTransformation";
import { PtasWebMeasurementTransformation } from "../transformations/PtasWebMeasurementTransformation";
import { FetchMeasurementsFromEEProvidersTask } from "../workers/tasks/FetchMeasurementsFromEEProvidersTask";
import { FetchMeasurementsFromProvidersTask } from "../workers/tasks/FetchMeasurementsFromProvidersTask";
import { FetchPpasEEMeasurementsTask } from "../workers/tasks/FetchPpasEEMeasurementsTask";
import { FetchPpasMeasurementsTask } from "../workers/tasks/FetchPpasMeasurementsTask";
import { FetchPreMeasurementDataTask } from "../workers/tasks/FetchPreMeasurementDataTask";
import { FetchPtasWebScrapedMeasurementsTask } from "../workers/tasks/FetchPtasWebScrapedMeasurementsTask";
import { WorkerContainerToken } from "./WorkerContainerToken";
import { PreMeasurementTransformation } from "#ie/commodity-providers/transformations/PreMeasurementTransformation";
import { ProcessPreEanMeasurementsTask } from "#ie/commodity-providers/workers/tasks/ProcessPreEanMeasurementsTask";
import { StaticMeterSettingsDataSourceCapitalDs } from "../datasources/static-data/StaticMeterSettingsDataSourceCapitalDs";

//#region Initialization
const CommodityContainer: DependencyContainer = VPalaceContainer.createChildContainer();
//#endregion

//#region Datasource
CommodityContainer.register(WorkerContainerToken.CemApiDataSourceProvider, CemApiDataSourceProvider);
CommodityContainer.register(WorkerContainerToken.PpasAveApiDataSourceProvider, PpasAveApiDataSourceProvider);
CommodityContainer.register(WorkerContainerToken.CommodityProviderDataSourceFactory, CommodityProviderDataSourceFactory);
CommodityContainer.register(WorkerContainerToken.StaticMeterSettingsDataSourceProvider, StaticMeterSettingsDataSourceProvider);
CommodityContainer.register(WorkerContainerToken.PreElectroApiDataSourceProvider, PreElectroApiDataSourceProvider);
CommodityContainer.register(WorkerContainerToken.AccessConfigurationDataSource, AccessConfigurationDataSource);
//#endregion

//#region Repositories
CommodityContainer.register(WorkerContainerToken.MeasurementRepository, MeasurementRepository);
CommodityContainer.register(WorkerContainerToken.StaticMeterSettingsRepository, StaticMeterSettingsRepository);
CommodityContainer.register(WorkerContainerToken.StaticMeterSettingsDataSourceCapitalDs, StaticMeterSettingsDataSourceCapitalDs);
CommodityContainer.registerSingleton<AccessLimitationRepository>(
    WorkerContainerToken.AccessLimitationRepository,
    AccessLimitationRepository
);
//#endregion

//#region Transformation
CommodityContainer.registerSingleton(WorkerContainerToken.CemMeasurementTransformation, CemMeasurementTransformation);
CommodityContainer.registerSingleton(WorkerContainerToken.AveMeasurementTransformation, AveMeasurementTransformation);
CommodityContainer.registerSingleton(WorkerContainerToken.PreElectroTransformation, PreElectroTransformation);
CommodityContainer.registerSingleton(WorkerContainerToken.PtasWebMeasurementTransformation, PtasWebMeasurementTransformation);
CommodityContainer.registerSingleton(WorkerContainerToken.PreMeasurementTransformation, PreMeasurementTransformation);
//#endregion

//#region Factory
//#endregion

//#region Tasks
CommodityContainer.register(WorkerContainerToken.FetchMeasurementsFromProvidersTask, FetchMeasurementsFromProvidersTask);
CommodityContainer.register(WorkerContainerToken.FetchMeasurementsFromEEProvidersTask, FetchMeasurementsFromEEProvidersTask);
CommodityContainer.register(WorkerContainerToken.FetchVeoliaMeasurementsTask, FetchVeoliaMeasurementsTask);
CommodityContainer.register(WorkerContainerToken.FetchPpasMeasurementsTask, FetchPpasMeasurementsTask);
CommodityContainer.register(WorkerContainerToken.FetchPpasEEMeasurementsTask, FetchPpasEEMeasurementsTask);
CommodityContainer.register(WorkerContainerToken.FetchMeasurementOptionsStaticDataTask, FetchMeasurementOptionsStaticDataTask);
CommodityContainer.register(WorkerContainerToken.FetchPreMeasurementDataTask, FetchPreMeasurementDataTask);
CommodityContainer.registerSingleton(
    WorkerContainerToken.FetchPtasWebScrapedMeasurementsTask,
    FetchPtasWebScrapedMeasurementsTask
);
CommodityContainer.register(WorkerContainerToken.ProcessPreEanMeasurementsTask, ProcessPreEanMeasurementsTask);
//#endregion

//#region Helpers
CommodityContainer.register(WorkerContainerToken.PreElectroHelper, PreElectroHelper);
//#endregion
export { CommodityContainer };
