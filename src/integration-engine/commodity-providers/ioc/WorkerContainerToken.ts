import { ProcessPreEanMeasurementsTask } from "#ie/commodity-providers/workers/tasks/ProcessPreEanMeasurementsTask";

const WorkerContainerToken = {
    CemApiDataSourceProvider: Symbol(),
    PpasAveApiDataSourceProvider: Symbol(),
    CommodityProviderDataSourceFactory: Symbol(),
    StaticMeterSettingsDataSourceProvider: Symbol(),
    StaticMeterSettingsDataSourceCapitalDs: Symbol(),
    CemMeasurementTransformation: Symbol(),
    AveMeasurementTransformation: Symbol(),
    MeasurementRepository: Symbol(),
    StaticMeterSettingsRepository: Symbol(),
    FetchMeasurementsFromProvidersTask: Symbol(),
    FetchMeasurementsFromEEProvidersTask: Symbol(),
    FetchVeoliaMeasurementsTask: Symbol(),
    FetchPpasMeasurementsTask: Symbol(),
    FetchPpasEEMeasurementsTask: Symbol(),
    FetchMeasurementOptionsStaticDataTask: Symbol(),
    FetchPreMeasurementDataTask: Symbol(),
    PreElectroTransformation: Symbol(),
    PreElectroApiDataSourceProvider: Symbol(),
    PreElectroHelper: Symbol(),
    PtasWebMeasurementTransformation: Symbol(),
    FetchPtasWebScrapedMeasurementsTask: Symbol(),
    PrimaryBuildingsRepositoryIE: Symbol(),
    AccessConfigurationDataSource: Symbol(),
    AccessLimitationRepository: Symbol(),
    PreMeasurementTransformation: Symbol(),
    ProcessPreEanMeasurementsTask: Symbol(),
};

export { WorkerContainerToken };
