import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { WorkerContainerToken } from "../ioc/WorkerContainerToken";
import { CommodityProvider } from "./helpers/CommodityProviderEnum";
import {
    AveApiDtoUnion,
    CemApiDtoUnion,
    DataSourceProviderDict,
    DataSourceReturnType,
    ICommodityProviderDataSourceFactory,
} from "./interfaces/ICommodityProviderDataSourceFactory";
import { IDataSourceProvider } from "./interfaces/IDataSourceProvider";

@injectable()
export class CommodityProviderDataSourceFactory implements ICommodityProviderDataSourceFactory {
    private readonly dataSourceProviderDict: DataSourceProviderDict;

    constructor(
        @inject(WorkerContainerToken.CemApiDataSourceProvider)
        cemApiDataSourceProvider: IDataSourceProvider<CemApiDtoUnion[]>,
        @inject(WorkerContainerToken.PpasAveApiDataSourceProvider)
        ppasAveApiDataSourceProvider: IDataSourceProvider<AveApiDtoUnion[]>
    ) {
        this.dataSourceProviderDict = {
            [CommodityProvider.UnimonitorCemApi]: cemApiDataSourceProvider,
            [CommodityProvider.PpasInternetAveApi]: ppasAveApiDataSourceProvider,
        };
    }

    public getDataSource<T extends CommodityProvider>(
        presetLogsProvider: T,
        ...params: Parameters<DataSourceProviderDict[T]["getDataSource"]>
    ): DataSourceReturnType<T> {
        return this.dataSourceProviderDict[presetLogsProvider].getDataSource(...params) as DataSourceReturnType<T>;
    }
}
