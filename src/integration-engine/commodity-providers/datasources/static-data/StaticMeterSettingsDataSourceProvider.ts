import { staticMeterSettingsJsonSchema } from "#sch/datasources/StaticMeterSettingsJsonSchema";
import { IStaticMeterSettings } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { ProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/ProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IDataSourceProvider } from "../interfaces/IDataSourceProvider";

@injectable()
export class StaticMeterSettingsDataSourceProvider implements IDataSourceProvider<IStaticMeterSettings> {
    private static readonly DATASOURCE_NAME = "StaticMeterSettings";

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getDataSource(): IDataSource<IStaticMeterSettings> {
        const baseUrl = this.config.getValue<string>("module.energetics.commodityProviders.staticData.baseUrl");
        const urlPath = this.config.getValue<string>("module.energetics.commodityProviders.staticData.metersettings.path");
        const url = new URL(urlPath, baseUrl);

        return new DataSource<IStaticMeterSettings>(
            StaticMeterSettingsDataSourceProvider.DATASOURCE_NAME,
            this.getProtocolStrategy(url),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(StaticMeterSettingsDataSourceProvider.DATASOURCE_NAME, staticMeterSettingsJsonSchema)
        );
    }

    private getProtocolStrategy(url: URL): ProtocolStrategy {
        return new HTTPFetchProtocolStrategy({
            headers: {
                Accept: "application/json",
            },
            method: "GET",
            timeoutInSeconds: 20,
            url: url.toString(),
        });
    }
}
