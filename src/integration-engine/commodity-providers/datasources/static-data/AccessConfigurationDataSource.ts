import { accessConfigurationJsonSchema } from "#sch/datasources/AccessConfigurationJsonSchema";
import { IAccessLimitation } from "#sch/models/interfaces/IAccessLimitation";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IDataSourceProvider } from "../interfaces/IDataSourceProvider";

@injectable()
export class AccessConfigurationDataSource implements IDataSourceProvider<IAccessLimitation[]> {
    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getDataSource(): IDataSource<IAccessLimitation[]> {
        const baseUrl = this.config.getValue<string>("module.energetics.commodityProviders.staticData.baseUrl");
        const urlPath = this.config.getValue<string>("module.energetics.commodityProviders.staticData.acccesConfigurationPath");
        const url = new URL(urlPath, baseUrl);

        return new DataSource<IAccessLimitation[]>(
            "AccessConfigurationDataSource",
            new HTTPFetchProtocolStrategy({
                headers: {
                    Accept: "application/json",
                },
                method: "GET",
                timeoutInSeconds: 20,
                url: url.toString(),
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator("AccessConfigurationDatasourceValidator", accessConfigurationJsonSchema)
        );
    }
}
