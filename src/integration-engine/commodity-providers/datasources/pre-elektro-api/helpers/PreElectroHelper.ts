import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DateTime, Interval } from "@golemio/core/dist/shared/luxon";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AccessOptions, Client } from "basic-ftp";

interface FileInfo {
    name: string;
}

@injectable()
export class PreElectroHelper {
    private ftpClient: Client;
    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {
        this.ftpClient = new Client();
    }

    public async listAllFiles(): Promise<FileInfo[]> {
        try {
            await this.ftpClient.access({
                ...(this.config.getValue(`module.energetics.commodityProviders.pre_electro_ftp`) as AccessOptions),
            });
            return await this.ftpClient.list();
        } catch (err) {
            return [];
        } finally {
            this.ftpClient.close();
        }
    }

    public async filterFilesByDate(from: string, to: string) {
        const namesInInterval = [];

        const dateFrom = DateTime.fromISO(from).startOf("day");
        const dateTo = DateTime.fromISO(to).plus({ days: 1 }).startOf("day");
        const interval = Interval.fromDateTimes(dateFrom, dateTo);

        const allFiles = await this.listAllFiles();

        for (const file of allFiles) {
            if (!file.name.includes(".csv")) {
                continue;
            }
            const getDateFromFileName = file.name.substring(0, file.name.lastIndexOf("-"));
            const converttoDate = DateTime.fromISO(getDateFromFileName);

            if (interval.contains(converttoDate)) {
                namesInInterval.push(file.name);
            }
        }
        return namesInInterval;
    }
}
