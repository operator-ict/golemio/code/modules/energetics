import { preElectricConsumptionJson } from "#sch/datasources/PreElectricConsumptionJson";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { RetryDataSource } from "@golemio/core/dist/integration-engine/datasources/RetryDataSource";
import { CSVDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources/datatype-strategy/CSVDataTypeStrategy";
import { FTPProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/FTPProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AccessOptions } from "basic-ftp";

@injectable()
export class PreElectroApiDataSourceProvider {
    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getMeasurements(filename: string) {
        return new RetryDataSource(
            "PreElectroApiDataSourceProvider",
            new FTPProtocolStrategy({
                filename: filename,
                path: "/",
                url: this.config.getValue(`module.energetics.commodityProviders.pre_electro_ftp`) as AccessOptions,
                encoding: "utf8",
            }),
            new CSVDataTypeStrategy({
                fastcsvParams: { delimiter: ";", headers: true },
                subscribe: (json: any) => json,
            }),
            new JSONSchemaValidator("PreElectroApiDataSourceProvider", preElectricConsumptionJson)
        ).getAll();
    }
}
