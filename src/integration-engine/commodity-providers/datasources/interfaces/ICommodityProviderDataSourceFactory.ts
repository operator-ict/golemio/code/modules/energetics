import { IAveApiDeviceDataDto } from "#sch/datasources/commodity-providers/interfaces/IAveApiDeviceDataDto";
import { IAveApiPlaceDto } from "#sch/datasources/commodity-providers/interfaces/IAveApiPlaceDto";
import { ICemApiCounterDto } from "#sch/datasources/commodity-providers/interfaces/ICemApiCounterDto";
import { ICemApiDeviceDto } from "#sch/datasources/commodity-providers/interfaces/ICemApiDeviceDto";
import { ICemApiMeasurementDto } from "#sch/datasources/commodity-providers/interfaces/ICemApiMeasurementDto";
import { CommodityProvider } from "../helpers/CommodityProviderEnum";
import { IDataSourceProvider } from "./IDataSourceProvider";

export type CemApiDtoUnion = ICemApiDeviceDto | ICemApiCounterDto | ICemApiMeasurementDto;
export type AveApiDtoUnion = IAveApiPlaceDto | IAveApiDeviceDataDto;

export type DataSourceProviderDict = {
    [CommodityProvider.UnimonitorCemApi]: IDataSourceProvider<CemApiDtoUnion[]>;
    [CommodityProvider.PpasInternetAveApi]: IDataSourceProvider<AveApiDtoUnion[]>;
};

export type DataSourceReturnType<T extends CommodityProvider> = ReturnType<DataSourceProviderDict[T]["getDataSource"]>;

export interface ICommodityProviderDataSourceFactory {
    getDataSource<T extends CommodityProvider>(
        presetLogsProvider: T,
        ...params: Parameters<DataSourceProviderDict[T]["getDataSource"]>
    ): DataSourceReturnType<T>;
}
