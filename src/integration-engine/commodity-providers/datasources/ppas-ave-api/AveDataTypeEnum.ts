// takeover from original C# scraper [OIct.ECG.AveDataGrabber](https://gitlab.com/operator-ict/oddeleni-vyvoje/energetika/spotreby-energii-mestskych-budov/stahovani-dat-meridel/-/blob/main/src/OIct.ECG.AveDataGrabber/Models/DataType.cs?ref_type=heads)
export enum AveDataType {
    RawData = 0,
    HourlyData = 1,
    DailyData = 2,
    MonthlyData = 3,
}
