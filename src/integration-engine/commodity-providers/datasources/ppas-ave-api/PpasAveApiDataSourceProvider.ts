import { aveApiDeviceDataJsonSchema } from "#sch/datasources/commodity-providers/AveApiDeviceDataJsonSchema";
import { aveApiPlacesJsonSchema } from "#sch/datasources/commodity-providers/AveApiPlacesJsonSchema";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { ProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/ProtocolStrategy";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { CemApiDtoUnion } from "../interfaces/ICommodityProviderDataSourceFactory";
import { IDataSourceProvider } from "../interfaces/IDataSourceProvider";
import { PpasAveApiHelper } from "./helpers/PpasAveApiHelper";
import { IAveApiDeviceDataParams } from "./interfaces/IAveApiDeviceDataParams";

@injectable()
export class PpasAveApiDataSourceProvider implements IDataSourceProvider<CemApiDtoUnion[]> {
    private static readonly DATASOURCE_NAME = "PpasAveApiDataSource";

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getDataSource(
        resourceType: string,
        sessionId: string,
        params?: IAveApiDeviceDataParams
    ): IDataSource<CemApiDtoUnion[]> {
        const baseUrl = this.config.getValue<string>("module.energetics.commodityProviders.ppas_ave_api.baseUrl");
        const url = new URL(baseUrl);
        url.pathname += "/" + resourceType;
        url.searchParams.append("sessionId", sessionId);

        if (params) {
            url.searchParams.append("placeId", params.placeId.toString());
            url.searchParams.append("dateFrom", params.dateFrom);
            url.searchParams.append("dateTo", params.dateTo);
            url.searchParams.append("isoDate", params.isoDate);
            url.searchParams.append("dataType", params.dataType);
        }

        return new DataSource<CemApiDtoUnion[]>(
            PpasAveApiDataSourceProvider.DATASOURCE_NAME,
            this.getProtocolStrategy(url, sessionId),
            this.getDataTypeStrategy(resourceType),
            this.getJsonSchemaValidator(resourceType)
        );
    }

    private getProtocolStrategy(url: URL, sessionId: string): ProtocolStrategy {
        return new HTTPFetchProtocolStrategy({
            method: "GET",
            headers: {
                Accept: "application/json",
            },
            timeoutInSeconds: 20,
            url: url.toString(),
        });
    }

    private getDataTypeStrategy(resourceType: string): JSONDataTypeStrategy {
        switch (resourceType) {
            case PpasAveApiHelper.resourceType.Places:
                return new JSONDataTypeStrategy({ resultsPath: "PageItems" });
            case PpasAveApiHelper.resourceType.DeviceData:
                return new JSONDataTypeStrategy({ resultsPath: "" });
            default:
                throw new GeneralError(`Unknown resource type: ${resourceType}`, PpasAveApiDataSourceProvider.name);
        }
    }

    private getJsonSchemaValidator(resourceType: string): JSONSchemaValidator {
        switch (resourceType) {
            case PpasAveApiHelper.resourceType.Places:
                return new JSONSchemaValidator(PpasAveApiDataSourceProvider.DATASOURCE_NAME + "Places", aveApiPlacesJsonSchema);
            case PpasAveApiHelper.resourceType.DeviceData:
                return new JSONSchemaValidator(
                    PpasAveApiDataSourceProvider.DATASOURCE_NAME + "DeviceData",
                    aveApiDeviceDataJsonSchema
                );
            default:
                throw new GeneralError(`Unknown resource type: ${resourceType}`, PpasAveApiDataSourceProvider.name);
        }
    }
}
