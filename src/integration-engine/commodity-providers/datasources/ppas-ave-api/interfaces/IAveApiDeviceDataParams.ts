export interface IAveApiDeviceDataParams {
    placeId: number;
    dateFrom: string;
    dateTo: string;
    isoDate: string;
    dataType: string;
}
