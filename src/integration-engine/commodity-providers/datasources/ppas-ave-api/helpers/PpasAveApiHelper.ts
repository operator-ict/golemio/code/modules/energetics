import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export interface IAveConfig {
    user: string;
    pass: string;
}
/**
 * Helper class for requesting additional data from PPAS InternetAVE API
 */
class PpasAveApiHelper {
    // based on analysis it seems API PPAS is handling timezones incorrectly, we deciced to follow PPAS UI approach to timezones.
    // https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/37#note_2010344938
    public static API_DATE_TZ = "UTC+1";

    constructor(private baseUrl: string, private config: IAveConfig) {}
    /**
     * Create API session and return session ID
     */
    public createSession = async (): Promise<string> => {
        const { user, pass } = this.config;

        try {
            const url = new URL(this.baseUrl);
            url.pathname += "/" + PpasAveApiHelper.resourceType.Logon;

            const result = await fetch(url.toString(), {
                method: "post",
                headers: new Headers({
                    Accept: "application/json",
                    "Content-Type": "application/x-www-form-urlencoded",
                }),
                body: new URLSearchParams({
                    name: user,
                    password: pass,
                }),
                signal: AbortSignal.timeout(10000),
            });

            const data = await result.json();

            if (data?.success && typeof data?.sessionId === "string") {
                return data.sessionId;
            }

            throw new GeneralError(
                "Cannot parse PPAS AVE Session ID from the server response",
                PpasAveApiHelper.name,
                new Error(data?.message)
            );
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError("Cannot retrieve PPAS AVE Session ID " + err.message, PpasAveApiHelper.name, err);
        }
    };

    /**
     * Terminate current session/invalidate session ID
     */
    public terminateSession = async (sessionId: string): Promise<void> => {
        try {
            const url = new URL(this.baseUrl);
            url.pathname += "/" + PpasAveApiHelper.resourceType.LogOff;
            url.searchParams.append("sessionId", sessionId);

            await fetch(url.toString(), {
                method: "get",
                signal: AbortSignal.timeout(10000),
            });
        } catch (err) {
            throw new GeneralError("Cannot terminate PPAS AVE API session", PpasAveApiHelper.name, err);
        }
    };

    /**
     * Return resource types/identifiers
     */
    public static get resourceType() {
        return {
            Logon: "logon.rails",
            LogOff: "LogOff.rails",
            Places: "GetPlaces.rails",
            DeviceData: "GetDeviceData.rails",
        } as const;
    }
}

export { PpasAveApiHelper };
