export interface ICemApiMeasurementsParams {
    counterId: number;
    dateFrom: string;
    dateTo: string;
}
