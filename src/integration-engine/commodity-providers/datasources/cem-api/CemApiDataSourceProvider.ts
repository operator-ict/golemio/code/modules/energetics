import { UnimonitorCemApiHelper } from "#ie/helpers";
import { cemApiCountersJsonSchema } from "#sch/datasources/commodity-providers/CemApiCountersJsonSchema";
import { cemApiDevicesJsonSchema } from "#sch/datasources/commodity-providers/CemApiDevicesJsonSchema";
import { cemApiMeasurementsJsonSchema } from "#sch/datasources/commodity-providers/CemApiMeasurementsJsonSchema";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { ProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/ProtocolStrategy";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { CemApiDtoUnion } from "../interfaces/ICommodityProviderDataSourceFactory";
import { IDataSourceProvider } from "../interfaces/IDataSourceProvider";
import { ICemApiMeasurementsParams } from "./interfaces/ICemApiMeasurementsParams";

@injectable()
export class CemApiDataSourceProvider implements IDataSourceProvider<CemApiDtoUnion[]> {
    private static readonly DATASOURCE_NAME = "CemApiDataSource";

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getDataSource(
        resourceType: string,
        authCookie: string,
        params?: ICemApiMeasurementsParams
    ): IDataSource<CemApiDtoUnion[]> {
        const baseUrl = this.config.getValue<string>("module.UnimonitorCemApiEnergetics.url");
        const url = new URL(baseUrl);
        url.searchParams.append("id", resourceType);

        if (params) {
            url.searchParams.append("var_id", params.counterId.toString());
            url.searchParams.append("from", params.dateFrom);
            url.searchParams.append("to", params.dateTo);
        }

        return new DataSource<CemApiDtoUnion[]>(
            CemApiDataSourceProvider.DATASOURCE_NAME,
            this.getProtocolStrategy(url, authCookie),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            this.getJsonSchemaValidator(resourceType)
        );
    }

    private getProtocolStrategy(url: URL, authCookie: string): ProtocolStrategy {
        return new HTTPFetchProtocolStrategy({
            headers: {
                Cookie: authCookie,
            },
            method: "GET",
            timeoutInSeconds: 20,
            url: url.toString(),
        });
    }

    private getJsonSchemaValidator(resourceType: string): JSONSchemaValidator {
        switch (resourceType) {
            case UnimonitorCemApiHelper.resourceType.Devices:
                return new JSONSchemaValidator(CemApiDataSourceProvider.DATASOURCE_NAME + "Devices", cemApiDevicesJsonSchema);
            case UnimonitorCemApiHelper.resourceType.Counters:
                return new JSONSchemaValidator(CemApiDataSourceProvider.DATASOURCE_NAME + "Counters", cemApiCountersJsonSchema);
            case UnimonitorCemApiHelper.resourceType.MeasurementsByCounter:
                return new JSONSchemaValidator(
                    CemApiDataSourceProvider.DATASOURCE_NAME + "Measurements",
                    cemApiMeasurementsJsonSchema
                );
            default:
                throw new GeneralError(`Unknown resource type: ${resourceType}`, CemApiDataSourceProvider.name);
        }
    }
}
