import { IPreEanMeasurement } from "#ie/commodity-providers/workers/interfaces/IPreEanMeasurement";

export class PreMeasurementFilter {
    public static sumDuplicates(measurementInput: IPreEanMeasurement): IPreEanMeasurement {
        const clean: Map<string, number> = new Map();

        for (const row of measurementInput.T1_Wh_raw_data) {
            clean.has(row[0]) ? clean.set(row[0], clean.get(row[0])! + row[1]) : clean.set(row[0], row[1]);
        }

        const T1_Wh_raw_data: Array<[string, number]> = Array.from(clean, ([k, v]) => [k, v]);

        return { ean: measurementInput.ean, T1_Wh_raw_data };
    }
}
