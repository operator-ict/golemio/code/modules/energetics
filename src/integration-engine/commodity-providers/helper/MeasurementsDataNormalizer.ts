import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";

// Filter out measurements with null values, negative values and future timestamps (issue prevzeti-ed#37)
export class MeasurementsDataNormalizer {
    public static filter(data: IMeasurementDto[]) {
        return data.filter((item) => item.value !== null && item.value > 0 && item.timestamp < new Date());
    }
}
