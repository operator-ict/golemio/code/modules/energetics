import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { IsEnum } from "@golemio/core/dist/shared/class-validator";
import { IPpasMeasurementsParams } from "../interfaces/IPpasMeasurementsParams";
import { MeasurementsValidationSchema } from "./MeasurementsSchema";

export class PpasMeasurementsValidationSchema extends MeasurementsValidationSchema implements IPpasMeasurementsParams {
    @IsEnum([
        CommodityProviderType.PpasAveV,
        CommodityProviderType.PpasAveM,
        CommodityProviderType.PpasAveA,
        CommodityProviderType.PpasAveC,
        CommodityProviderType.PpasAveMO,
    ])
    providerType!:
        | CommodityProviderType.PpasAveV
        | CommodityProviderType.PpasAveM
        | CommodityProviderType.PpasAveA
        | CommodityProviderType.PpasAveC
        | CommodityProviderType.PpasAveMO;
}
