import { IsISO8601 } from "@golemio/core/dist/shared/class-validator";
import { IMeasurementsParams } from "../interfaces/IMeasurementsParams";

export class MeasurementsValidationSchema implements IMeasurementsParams {
    @IsISO8601()
    dateFrom!: string;

    @IsISO8601()
    dateTo!: string;
}
