import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { IsEnum } from "@golemio/core/dist/shared/class-validator";
import { IVeoliaMeasurementsParams } from "../interfaces/IVeoliaMeasurementsParams";
import { MeasurementsValidationSchema } from "./MeasurementsSchema";

export class VeoliaMeasurementsValidationSchema extends MeasurementsValidationSchema implements IVeoliaMeasurementsParams {
    @IsEnum([CommodityProviderType.CemApiVeoliaElectro, CommodityProviderType.CemApiVeoliaWater])
    providerType!: CommodityProviderType.CemApiVeoliaElectro | CommodityProviderType.CemApiVeoliaWater;
}
