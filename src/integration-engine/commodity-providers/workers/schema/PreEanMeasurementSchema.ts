import { IPreEanMeasurement } from "#ie/commodity-providers/workers/interfaces/IPreEanMeasurement";
import { IsString, IsArray } from "@golemio/core/dist/shared/class-validator";

export class PreEanMeasurementSchema implements IPreEanMeasurement {
    @IsArray()
    T1_Wh_raw_data!: Array<[string, number]>;

    @IsString()
    ean!: string;
}
