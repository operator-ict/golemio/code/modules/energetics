import { IsInt } from "@golemio/core/dist/shared/class-validator";
import { IDataEntrypointParams } from "../interfaces/IDataEntrypointParams";

export class DataEntrypointValidationSchema implements IDataEntrypointParams {
    @IsInt()
    targetDays!: number;
}
