import { WORKER_NAME } from "#ie/commodity-providers/constants";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { MeasurementRepository } from "#ie/commodity-providers/repositories/MeasurementRepository";
import { StaticMeterSettingsRepository } from "#ie/commodity-providers/repositories/StaticMeterSettingsRepository";
import { PtasWebMeasurementTransformation } from "#ie/commodity-providers/transformations/PtasWebMeasurementTransformation";
import { ptasWebScrapingJsonSchema } from "#sch/datasources/PtasWebScrapingJsonSchema";
import { IMeasurementOption, ISelectedMeters } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { IPtasMeasurement, IPtasWebScrapingInput } from "#sch/datasources/interfaces/PtasWebScrapingInput";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { StaticDataResourceType } from "#sch/models/helpers/StaticDataResourceTypeEnum";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
@injectable()
export class FetchPtasWebScrapedMeasurementsTask extends AbstractTaskJsonSchema<IPtasWebScrapingInput> {
    protected schema = new JSONSchemaValidator("FetchPtasWebScrapedMeasurementsTask", ptasWebScrapingJsonSchema);
    public readonly queueName = "savePtasMeasurements";
    public readonly queueTtl = 60 * 60 * 1000;

    constructor(
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(WorkerContainerToken.StaticMeterSettingsRepository)
        private meterSettingsRepository: StaticMeterSettingsRepository,
        @inject(WorkerContainerToken.MeasurementRepository) protected measurementRepository: MeasurementRepository,
        @inject(WorkerContainerToken.PtasWebMeasurementTransformation) protected transformation: PtasWebMeasurementTransformation
    ) {
        super(WORKER_NAME);
    }

    protected async execute(data: IPtasWebScrapingInput): Promise<void> {
        const measurementOptions = await this.meterSettingsRepository.getData(
            CommodityProviderType.PtasOptions,
            StaticDataResourceType.MeasurementOptions
        );

        if (!measurementOptions) {
            throw new GeneralError("Measurement options not found", this.constructor.name);
        }

        const selectedMeters = await this.meterSettingsRepository.getData(
            CommodityProviderType.PtasOptions,
            StaticDataResourceType.SelectedMeters
        );

        if (!selectedMeters) {
            throw new GeneralError("Static data of selected meters not found", this.constructor.name);
        }

        //podle původního scraperu z C# bereme pouze teplo horké vody
        let hotWaterMeasurements = data.measurements.filter((item) => item["Druh registru"] === "Teplo horké vody");
        hotWaterMeasurements = this.handleDuplicates(data["placeId"], hotWaterMeasurements, selectedMeters);

        let batch = [];

        for (const measurement of hotWaterMeasurements) {
            const option = measurementOptions.find((item: IMeasurementOption) => item.Meter === data["placeId"]);

            if (!option) {
                this.logger.warn(
                    `Incoming Meter id ${data["placeId"]} is missing in the static data file (measurement options).`
                );
                continue;
            }

            const consumptionData = this.transformation.transformElement({ option, measurement });

            batch.push(consumptionData);
        }

        await this.measurementRepository.bulkSave(batch);
    }

    // temporary solution to finish project "převzetí energetické databáze"
    private handleDuplicates(placeId: string, measurements: IPtasMeasurement[], selectedMeters: ISelectedMeters) {
        if (Object.keys(selectedMeters).includes(placeId)) {
            return measurements.filter((item) => item["Výrobní číslo"] === selectedMeters[placeId]);
        }

        // create list of unique item["Výrobní číslo"]
        const uniqueSerialNumbers = Array.from(new Set(measurements.map((item) => item["Výrobní číslo"])));

        if (uniqueSerialNumbers.length > 1) {
            this.logger.warn(`Ptas integration found multiple serial numbers for placeId ${placeId}.`);
        }

        return measurements;
    }
}
