import { CommodityProvider } from "#ie/commodity-providers/datasources/helpers/CommodityProviderEnum";
import {
    AveApiDtoUnion,
    ICommodityProviderDataSourceFactory,
} from "#ie/commodity-providers/datasources/interfaces/ICommodityProviderDataSourceFactory";
import { AveDataType } from "#ie/commodity-providers/datasources/ppas-ave-api/AveDataTypeEnum";
import { IAveConfig, PpasAveApiHelper } from "#ie/commodity-providers/datasources/ppas-ave-api/helpers/PpasAveApiHelper";
import { IAveApiDeviceDataParams } from "#ie/commodity-providers/datasources/ppas-ave-api/interfaces/IAveApiDeviceDataParams";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { MeasurementRepository } from "#ie/commodity-providers/repositories/MeasurementRepository";
import { StaticMeterSettingsRepository } from "#ie/commodity-providers/repositories/StaticMeterSettingsRepository";
import { AveMeasurementTransformation } from "#ie/commodity-providers/transformations/AveMeasurementTransformation";
import { IAveApiDeviceDataDto } from "#sch/datasources/commodity-providers/interfaces/IAveApiDeviceDataDto";
import { IAveApiPlaceDto } from "#sch/datasources/commodity-providers/interfaces/IAveApiPlaceDto";
import { IMeasurementOption } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { StaticDataResourceType } from "#sch/models/helpers/StaticDataResourceTypeEnum";
import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { ILogger } from "@golemio/core/dist/helpers";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime, Interval } from "@golemio/core/dist/shared/luxon";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IPpasMeasurementsParams } from "../interfaces/IPpasMeasurementsParams";
import { PpasMeasurementsValidationSchema } from "../schema/PpasMeasurementsSchema";
import { AbstractFetchMeasurementsTask } from "./AbstractFetchMeasurementsTask";

@injectable()
export class FetchPpasMeasurementsTask extends AbstractFetchMeasurementsTask<IPpasMeasurementsParams> {
    public queueName = "fetchPpasMeasurements";
    public queueTtl = 60 * 60 * 1000;
    public schema = PpasMeasurementsValidationSchema;

    constructor(
        @inject(CoreToken.SimpleConfig) protected config: ISimpleConfig,
        @inject(CoreToken.Logger) protected logger: ILogger,
        @inject(WorkerContainerToken.CommodityProviderDataSourceFactory)
        protected dataSourceFactory: ICommodityProviderDataSourceFactory,
        @inject(WorkerContainerToken.AveMeasurementTransformation)
        protected measurementTransformation: AveMeasurementTransformation,
        @inject(WorkerContainerToken.MeasurementRepository) protected measurementRepository: MeasurementRepository,
        @inject(WorkerContainerToken.StaticMeterSettingsRepository)
        protected meterSettingsRepository: StaticMeterSettingsRepository
    ) {
        super();
    }

    public async execute(data: IPpasMeasurementsParams): Promise<void> {
        let measurementOptions = await this.meterSettingsRepository.getData(
            data.providerType,
            StaticDataResourceType.MeasurementOptions
        );
        if (!measurementOptions) {
            throw new GeneralError("Measurement options not found", this.constructor.name);
        } else {
            measurementOptions = this.splitOptionsByVars(measurementOptions);
        }

        const { baseUrl, config } = this.getProviderConfig(data);
        const aveApiHelper = new PpasAveApiHelper(baseUrl, config);
        const sessionId = await aveApiHelper.createSession();

        try {
            const places = await this.getDataFromDataSource<IAveApiPlaceDto>("Places", sessionId);
            let consumptionData: IMeasurementDto[] = [];

            for (const place of places) {
                const options = measurementOptions.filter((item) => item.MeterSerialNumber === place.DeviceSerialNumber);
                if (options.length === 0) {
                    continue;
                }

                this.logger.info(`Fetching data for device ${place.DeviceSerialNumber}`);
                const measurementChunkGenerator = this.generateMeasurements({ ...data, placeId: place.Id }, sessionId);

                for await (const measurements of measurementChunkGenerator) {
                    for (const option of options) {
                        const consumptionBatch = measurements.map((item) =>
                            this.measurementTransformation.transformElement({
                                ...item,
                                ...option,
                            })
                        );

                        consumptionData = consumptionData.concat(consumptionBatch);
                    }
                }
            }

            await this.saveConsumptionData(consumptionData);
        } finally {
            await aveApiHelper.terminateSession(sessionId);
        }
    }

    protected getProviderConfig(data: IPpasMeasurementsParams): { baseUrl: string; config: IAveConfig } {
        return {
            baseUrl: this.config.getValue("module.energetics.commodityProviders.ppas_ave_api.baseUrl") as string,
            config: this.config.getValue(`module.energetics.commodityProviders.${data.providerType}`) as IAveConfig,
        };
    }

    /** Splits the measurement options into individual options (multiple variables) */
    protected splitOptionsByVars(options: IMeasurementOption[]): IMeasurementOption[] {
        let result: IMeasurementOption[] = [];

        for (const option of options) {
            const vars = option.Var.split("|");
            result = result.concat(vars.map((v) => ({ ...option, Var: v })));
        }

        return result;
    }

    protected async *generateMeasurements(
        data: IPpasMeasurementsParams & { placeId: number },
        sessionId: string
    ): AsyncGenerator<IAveApiDeviceDataDto[]> {
        const dateFrom = DateTime.fromISO(data.dateFrom).startOf("day");
        const dateTo = DateTime.fromISO(data.dateTo).startOf("day");
        const intervals = Interval.fromDateTimes(dateFrom, dateTo).splitBy({ days: 5 });

        for (const interval of intervals) {
            const start = interval.start.startOf("day");
            const end = interval.end.startOf("day");

            yield this.getDataFromDataSource<IAveApiDeviceDataDto>("DeviceData", sessionId, {
                placeId: data.placeId,
                dateFrom: start.toISODate(),
                dateTo: end.toISODate(),
                isoDate: "1", // return ISO string instead of Date object (invalid JSON response)
                // fill all measurement values (we need the OperatingAmount and ConvertDifference values)
                dataType: AveDataType.RawData.toString(),
            });
        }
    }

    protected getDataFromDataSource<T extends AveApiDtoUnion>(
        resourceTypeKey: keyof typeof PpasAveApiHelper.resourceType,
        sessionId: string,
        params?: IAveApiDeviceDataParams
    ): Promise<T[]> {
        const dataSource = this.dataSourceFactory.getDataSource(
            CommodityProvider.PpasInternetAveApi,
            PpasAveApiHelper.resourceType[resourceTypeKey],
            sessionId,
            params
        );

        return dataSource.getAll() as Promise<T[]>;
    }
}
