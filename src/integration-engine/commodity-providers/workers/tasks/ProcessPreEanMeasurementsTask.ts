import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { PreMeasurementTransformation } from "#ie/commodity-providers/transformations/PreMeasurementTransformation";
import { StaticMeterSettingsRepository } from "#ie/commodity-providers/repositories/StaticMeterSettingsRepository";
import { MeasurementRepository } from "#ie/commodity-providers/repositories/MeasurementRepository";
import { IPreEanMeasurement } from "#ie/commodity-providers/workers/interfaces/IPreEanMeasurement";
import { PreEanMeasurementSchema } from "#ie/commodity-providers/workers/schema/PreEanMeasurementSchema";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { StaticDataResourceType } from "#sch/models/helpers/StaticDataResourceTypeEnum";
import { IMeasurementOption } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { WORKER_NAME } from "#ie/commodity-providers/constants";
import { PreMeasurementFilter } from "#ie/commodity-providers/helper/PreMeasurementFilter";

@injectable()
export class ProcessPreEanMeasurementsTask extends AbstractTask<IPreEanMeasurement> {
    protected schema = PreEanMeasurementSchema;
    public readonly queueName = "processPreEanMeasurements";
    public readonly queueTtl = 60 * 60 * 1000;

    constructor(
        @inject(WorkerContainerToken.StaticMeterSettingsRepository)
        private meterSettingsRepository: StaticMeterSettingsRepository,
        @inject(WorkerContainerToken.MeasurementRepository) protected measurementRepository: MeasurementRepository,
        @inject(WorkerContainerToken.PreMeasurementTransformation) protected transformation: PreMeasurementTransformation
    ) {
        super(WORKER_NAME);
    }

    protected async execute(measurementInput: IPreEanMeasurement): Promise<void> {
        const measurementOptions = await this.meterSettingsRepository.getData(
            CommodityProviderType.PreInputOptions,
            StaticDataResourceType.MeasurementOptions
        );

        if (!measurementOptions) {
            throw new GeneralError("Measurement options not found", this.constructor.name);
        }

        const option = measurementOptions.find((item: IMeasurementOption) => item.Meter === measurementInput.ean);

        if (!option) {
            throw new GeneralError("Measurement option not found for meter " + measurementInput.ean, this.constructor.name);
        }

        const measurements: IMeasurementDto[] = this.transformation.transformElement({
            option,
            measurementInput: PreMeasurementFilter.sumDuplicates(measurementInput),
        });

        await this.measurementRepository.bulkSave(measurements);
    }
}
