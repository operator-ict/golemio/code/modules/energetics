import { ICommodityProviderDataSourceFactory } from "#ie/commodity-providers/datasources/interfaces/ICommodityProviderDataSourceFactory";
import { PpasAveApiHelper } from "#ie/commodity-providers/datasources/ppas-ave-api/helpers/PpasAveApiHelper";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { MeasurementRepository } from "#ie/commodity-providers/repositories/MeasurementRepository";
import { StaticMeterSettingsRepository } from "#ie/commodity-providers/repositories/StaticMeterSettingsRepository";
import { AveMeasurementTransformation } from "#ie/commodity-providers/transformations/AveMeasurementTransformation";
import { IAveApiPlaceDto } from "#sch/datasources/commodity-providers/interfaces/IAveApiPlaceDto";
import { StaticDataResourceType } from "#sch/models/helpers/StaticDataResourceTypeEnum";
import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { ILogger } from "@golemio/core/dist/helpers";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IPpasMeasurementsParams } from "../interfaces/IPpasMeasurementsParams";
import { PpasMeasurementsValidationSchema } from "../schema/PpasMeasurementsSchema";
import { FetchPpasMeasurementsTask } from "./FetchPpasMeasurementsTask";

@injectable()
export class FetchPpasEEMeasurementsTask extends FetchPpasMeasurementsTask {
    public override queueName = "fetchPpasEEMeasurements";
    public override queueTtl = 60 * 60 * 1000;
    public override schema = PpasMeasurementsValidationSchema;

    constructor(
        @inject(CoreToken.SimpleConfig) config: ISimpleConfig,
        @inject(CoreToken.Logger) logger: ILogger,
        @inject(WorkerContainerToken.CommodityProviderDataSourceFactory)
        dataSourceFactory: ICommodityProviderDataSourceFactory,
        @inject(WorkerContainerToken.AveMeasurementTransformation)
        measurementTransformation: AveMeasurementTransformation,
        @inject(WorkerContainerToken.MeasurementRepository) measurementRepository: MeasurementRepository,
        @inject(WorkerContainerToken.StaticMeterSettingsRepository) meterSettingsRepository: StaticMeterSettingsRepository
    ) {
        super(config, logger, dataSourceFactory, measurementTransformation, measurementRepository, meterSettingsRepository);
    }

    public override async execute(data: IPpasMeasurementsParams): Promise<void> {
        let measurementOptions = await this.meterSettingsRepository.getData(
            data.providerType,
            StaticDataResourceType.MeasurementOptions
        );

        if (!measurementOptions) {
            throw new GeneralError("Measurement options not found", this.constructor.name);
        } else {
            measurementOptions = this.splitOptionsByVars(measurementOptions);
        }

        const { baseUrl, config } = this.getProviderConfig(data);
        const aveApiHelper = new PpasAveApiHelper(baseUrl, config);
        const sessionId = await aveApiHelper.createSession();

        try {
            const places = await this.getDataFromDataSource<IAveApiPlaceDto>("Places", sessionId);

            let consumptionData: IMeasurementDto[] = [];

            for (const place of places) {
                const options = measurementOptions.filter((item) => item.MeterSerialNumber === place.DeviceSerialNumber);
                if (options.length === 0) {
                    continue;
                }

                this.logger.info(`Fetching data for device ${place.DeviceSerialNumber}`);
                const measurementChunkGenerator = this.generateMeasurements({ ...data, placeId: place.Id }, sessionId);

                for await (const measurements of measurementChunkGenerator) {
                    for (const option of options) {
                        const consumptionBatch = measurements.map((item) =>
                            this.measurementTransformation.transformElement({
                                ...item,
                                ...option,
                            })
                        );

                        consumptionData = consumptionData.concat(consumptionBatch);
                    }
                }
            }

            await this.saveConsumptionData(consumptionData);
        } finally {
            await aveApiHelper.terminateSession(sessionId);
        }
    }
}
