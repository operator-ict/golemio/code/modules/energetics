import { ICemApiMeasurementsParams } from "#ie/commodity-providers/datasources/cem-api/interfaces/ICemApiMeasurementsParams";
import { CommodityProvider } from "#ie/commodity-providers/datasources/helpers/CommodityProviderEnum";
import {
    CemApiDtoUnion,
    ICommodityProviderDataSourceFactory,
} from "#ie/commodity-providers/datasources/interfaces/ICommodityProviderDataSourceFactory";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { MeasurementRepository } from "#ie/commodity-providers/repositories/MeasurementRepository";
import { StaticMeterSettingsRepository } from "#ie/commodity-providers/repositories/StaticMeterSettingsRepository";
import { CemMeasurementTransformation } from "#ie/commodity-providers/transformations/CemMeasurementTransformation";
import { ICemConfig, UnimonitorCemApiHelper } from "#ie/helpers";
import { ICemApiCounterDto } from "#sch/datasources/commodity-providers/interfaces/ICemApiCounterDto";
import { ICemApiDeviceDto } from "#sch/datasources/commodity-providers/interfaces/ICemApiDeviceDto";
import { ICemApiMeasurementDto } from "#sch/datasources/commodity-providers/interfaces/ICemApiMeasurementDto";
import { StaticDataResourceType } from "#sch/models/helpers/StaticDataResourceTypeEnum";
import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { ILogger } from "@golemio/core/dist/helpers";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime, Interval } from "@golemio/core/dist/shared/luxon";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IVeoliaMeasurementsParams } from "../interfaces/IVeoliaMeasurementsParams";
import { VeoliaMeasurementsValidationSchema } from "../schema/VeoliaMeasurementsSchema";
import { AbstractFetchMeasurementsTask } from "./AbstractFetchMeasurementsTask";

@injectable()
export class FetchVeoliaMeasurementsTask extends AbstractFetchMeasurementsTask<IVeoliaMeasurementsParams> {
    public readonly queueName = "fetchVeoliaMeasurements";
    public readonly queueTtl = 60 * 60 * 1000;
    public readonly schema = VeoliaMeasurementsValidationSchema;

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(WorkerContainerToken.CommodityProviderDataSourceFactory)
        private dataSourceFactory: ICommodityProviderDataSourceFactory,
        @inject(WorkerContainerToken.CemMeasurementTransformation)
        private measurementTransformation: CemMeasurementTransformation,
        @inject(WorkerContainerToken.MeasurementRepository) protected measurementRepository: MeasurementRepository,
        @inject(WorkerContainerToken.StaticMeterSettingsRepository) private meterSettingsRepository: StaticMeterSettingsRepository
    ) {
        super();
    }

    public async execute(data: IVeoliaMeasurementsParams): Promise<void> {
        const measurementOptions = await this.meterSettingsRepository.getData(
            data.providerType,
            StaticDataResourceType.MeasurementOptions
        );
        if (!measurementOptions) {
            throw new GeneralError("Measurement options not found", this.constructor.name);
        }

        const { url, config } = this.getProviderConfig(data);
        const cemApiHelper = new UnimonitorCemApiHelper(url, config);
        const authCookie = await cemApiHelper.createSession();

        try {
            const devices = await this.getDataFromDataSource<ICemApiDeviceDto>("Devices", authCookie);
            const counters = await this.getDataFromDataSource<ICemApiCounterDto>("Counters", authCookie);
            let consumptionData: IMeasurementDto[] = [];

            for (const device of devices) {
                const options = measurementOptions.find((item) => item.MeterSerialNumber === device.me_serial);
                if (!options) {
                    continue;
                }

                const relevantCounters = counters.filter((item) => item.me_id === device.me_id);
                for (const counter of relevantCounters) {
                    this.logger.info(`Fetching data for device ${device.me_serial} and counter ${counter.var_id}`);

                    const measurementChunkGenerator = this.generateMeasurements(
                        { ...data, counterId: counter.var_id },
                        authCookie
                    );

                    for await (const measurements of measurementChunkGenerator) {
                        const consumptionBatch = measurements.map((item) =>
                            this.measurementTransformation.transformElement({
                                ...item,
                                ...options,
                                counterTypeId: counter.pot_id,
                            })
                        );

                        consumptionData = consumptionData.concat(consumptionBatch);
                    }
                }
            }

            await this.saveConsumptionData(consumptionData, true);
        } finally {
            await cemApiHelper.terminateSession(authCookie);
        }
    }

    private getProviderConfig(data: IVeoliaMeasurementsParams): { url: string; config: ICemConfig } {
        return {
            url: this.config.getValue(`module.UnimonitorCemApiEnergetics.url`) as string,
            config: this.config.getValue(`module.energetics.commodityProviders.${data.providerType}`) as ICemConfig,
        };
    }

    private async *generateMeasurements(
        data: IVeoliaMeasurementsParams & { counterId: number },
        authCookie: string
    ): AsyncGenerator<ICemApiMeasurementDto[]> {
        const dateFrom = DateTime.fromISO(data.dateFrom).startOf("day");
        const dateTo = DateTime.fromISO(data.dateTo).startOf("day");
        const intervals = Interval.fromDateTimes(dateFrom, dateTo).splitBy({ days: 5 });

        for (const interval of intervals) {
            const start = interval.start.startOf("day");
            const end = interval.end.startOf("day");

            yield this.getDataFromDataSource<ICemApiMeasurementDto>("MeasurementsByCounter", authCookie, {
                counterId: data.counterId,
                dateFrom: start.toISODate(),
                dateTo: end.toISODate(),
            });
        }
    }

    private getDataFromDataSource<T extends CemApiDtoUnion>(
        resourceTypeKey: keyof typeof UnimonitorCemApiHelper.resourceType,
        authCookie: string,
        params?: ICemApiMeasurementsParams
    ): Promise<T[]> {
        const dataSource = this.dataSourceFactory.getDataSource(
            CommodityProvider.UnimonitorCemApi,
            UnimonitorCemApiHelper.resourceType[resourceTypeKey],
            authCookie,
            params
        );

        return dataSource.getAll() as Promise<T[]>;
    }
}
