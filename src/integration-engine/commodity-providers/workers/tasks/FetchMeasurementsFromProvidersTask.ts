import { WORKER_NAME } from "#ie/commodity-providers/constants";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { AbstractTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { DateTime, Interval } from "@golemio/core/dist/shared/luxon";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IDataEntrypointParams } from "../interfaces/IDataEntrypointParams";
import { IMeasurementsParams } from "../interfaces/IMeasurementsParams";
import { DataEntrypointValidationSchema } from "../schema/DataEntrypointSchema";

@injectable()
export class FetchMeasurementsFromProvidersTask extends AbstractTask<IDataEntrypointParams> {
    public readonly queueName = "fetchMeasurementsFromProviders";
    public readonly queueTtl = 60 * 60 * 1000;
    public readonly schema = DataEntrypointValidationSchema;

    constructor() {
        super(WORKER_NAME);
    }

    public async execute(data: IDataEntrypointParams): Promise<void> {
        for (const { dateFrom, dateTo } of this.generateDateParams(data)) {
            // Veolia
            QueueManager.sendMessageToExchange(this.queuePrefix, "fetchVeoliaMeasurements", {
                providerType: CommodityProviderType.CemApiVeoliaWater,
                dateFrom,
                dateTo,
            });

            QueueManager.sendMessageToExchange(this.queuePrefix, "fetchVeoliaMeasurements", {
                providerType: CommodityProviderType.CemApiVeoliaElectro,
                dateFrom,
                dateTo,
            });

            // PPAS - Pražská plynárenská
            QueueManager.sendMessageToExchange(this.queuePrefix, "fetchPpasMeasurements", {
                providerType: CommodityProviderType.PpasAveV,
                dateFrom,
                dateTo,
            });

            QueueManager.sendMessageToExchange(this.queuePrefix, "fetchPpasMeasurements", {
                providerType: CommodityProviderType.PpasAveM,
                dateFrom,
                dateTo,
            });

            //PRE
            QueueManager.sendMessageToExchange(this.queuePrefix, "fetchPreElectroMeasurementsTask", {
                dateFrom,
                dateTo,
            });
        }
    }

    private *generateDateParams(data: IDataEntrypointParams): Generator<IMeasurementsParams> {
        const currentDate = DateTime.now().startOf("day");
        const leftBoundDate = currentDate.minus({ days: data.targetDays });
        const intervals = Interval.fromDateTimes(leftBoundDate, currentDate.plus({ days: 1 })).splitBy({ months: 1 });

        for (const interval of intervals) {
            const start = interval.start.startOf("day");
            const end = interval.end.startOf("day");

            yield {
                dateFrom: start.toISODate(),
                dateTo: end.toISODate(),
            };
        }
    }
}
