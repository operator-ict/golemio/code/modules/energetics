import { WORKER_NAME } from "#ie/commodity-providers/constants";
import { PreElectroApiDataSourceProvider } from "#ie/commodity-providers/datasources/pre-elektro-api/PreElectroApiDataSourceProvider";
import { PreElectroHelper } from "#ie/commodity-providers/datasources/pre-elektro-api/helpers/PreElectroHelper";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { MeasurementRepository } from "#ie/commodity-providers/repositories/MeasurementRepository";
import { StaticMeterSettingsRepository } from "#ie/commodity-providers/repositories/StaticMeterSettingsRepository";
import { PreElectroTransformation } from "#ie/commodity-providers/transformations/PreElectroTransformation";
import { IMeasurementOption } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { StaticDataResourceType } from "#sch/models/helpers/StaticDataResourceTypeEnum";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IPreMeasurementsParams } from "../interfaces/IPreMeasurementParams";
import { PreMeasurementsSchema } from "../schema/PreMeasurementsSchema";

@injectable()
export class FetchPreMeasurementDataTask extends AbstractTask<IPreMeasurementsParams> {
    protected schema = PreMeasurementsSchema;
    public readonly queueName = "fetchPreElectroMeasurementsTask";
    public readonly queueTtl = 60 * 60 * 1000;

    constructor(
        @inject(WorkerContainerToken.StaticMeterSettingsRepository)
        private meterSettingsRepository: StaticMeterSettingsRepository,
        @inject(WorkerContainerToken.MeasurementRepository) protected measurementRepository: MeasurementRepository,
        @inject(WorkerContainerToken.PreElectroTransformation) protected transformation: PreElectroTransformation,
        @inject(WorkerContainerToken.PreElectroApiDataSourceProvider)
        protected dataSourceProvider: PreElectroApiDataSourceProvider,
        @inject(WorkerContainerToken.PreElectroHelper)
        protected helper: PreElectroHelper
    ) {
        super(WORKER_NAME);
    }

    protected async execute(data: IPreMeasurementsParams): Promise<void> {
        const measurementOptions = await this.meterSettingsRepository.getData(
            CommodityProviderType.PreOptions,
            StaticDataResourceType.MeasurementOptions
        );

        if (!measurementOptions) {
            throw new GeneralError("Measurement options not found", this.constructor.name);
        }

        const files = await this.helper.filterFilesByDate(data.dateFrom, data.dateTo);

        if (!files) {
            throw new GeneralError("No files to import", this.constructor.name);
        }

        for (const file of files) {
            let batch = [];
            const { data: measurements } = await this.dataSourceProvider.getMeasurements(file);

            for (const measurement of measurements) {
                const option = measurementOptions.find(
                    (item: IMeasurementOption) =>
                        item.MeterSerialNumber === measurement["ELM"] && item.Meter === measurement["EAN"]
                );

                if (!option) {
                    continue;
                }

                const consumptionData = this.transformation.transformElement({ option, measurement });

                batch.push(consumptionData);
            }

            await this.measurementRepository.bulkSave(batch);
        }
    }
}
