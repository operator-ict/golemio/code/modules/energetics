import { WORKER_NAME } from "#ie/commodity-providers/constants";
import { MeasurementsDataNormalizer } from "#ie/commodity-providers/helper/MeasurementsDataNormalizer";
import { MeasurementRepository } from "#ie/commodity-providers/repositories/MeasurementRepository";
import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { IMeasurementsParams } from "../interfaces/IMeasurementsParams";

const MEASUREMENTS_BATCH_SIZE = 5000;

export abstract class AbstractFetchMeasurementsTask<T extends IMeasurementsParams> extends AbstractTask<T> {
    protected abstract measurementRepository: MeasurementRepository;

    constructor() {
        super(WORKER_NAME);
    }

    protected async saveConsumptionData(data: IMeasurementDto[], skipFilter: boolean = false): Promise<void> {
        for (let i = 0; i < data.length; i += MEASUREMENTS_BATCH_SIZE) {
            let batch = data.slice(i, i + MEASUREMENTS_BATCH_SIZE);

            if (!skipFilter) {
                batch = MeasurementsDataNormalizer.filter(batch);
            }

            // Remove duplicated measurements from the batch
            batch = batch.filter(
                (item, index, self) =>
                    index ===
                    self.findIndex(
                        (t) =>
                            t.timestamp.getTime() === item.timestamp.getTime() &&
                            t.addr === item.addr &&
                            t.variable === item.variable &&
                            t.meter === item.meter
                    )
            );

            await this.measurementRepository.bulkSave(batch);
        }
    }
}
