import { WORKER_NAME } from "#ie/commodity-providers/constants";
import { StaticMeterSettingsDataSourceProvider } from "#ie/commodity-providers/datasources/static-data/StaticMeterSettingsDataSourceProvider";
import { StaticMeterSettingsDataSourceCapitalDs } from "#ie/commodity-providers/datasources/static-data/StaticMeterSettingsDataSourceCapitalDs";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { StaticMeterSettingsRepository } from "#ie/commodity-providers/repositories/StaticMeterSettingsRepository";
import { IMeasurementOption, IStaticMeterSettings } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { StaticDataResourceType } from "#sch/models/helpers/StaticDataResourceTypeEnum";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

const optionLookupKeys = {
    [CommodityProviderType.CemApiVeoliaWater]: "CemWaterOptions",
    [CommodityProviderType.CemApiVeoliaElectro]: "CemElectroOptions",
    [CommodityProviderType.PpasAveV]: "AveVOptions",
    [CommodityProviderType.PpasAveM]: "AveMOptions",
    [CommodityProviderType.PpasAveA]: "AveHl.m.Prahaptions",
    [CommodityProviderType.PpasAveC]: "AveHlavni.m.Prahaptions",
    [CommodityProviderType.PpasAveMO]: "AveHlavni.mesto.Praha.64581ptions",
    [CommodityProviderType.PreOptions]: "PreOptions",
    [CommodityProviderType.PtasOptions]: "PtasOptions",
    [CommodityProviderType.PreInputOptions]: "PreInputOptions",
};

@injectable()
export class FetchMeasurementOptionsStaticDataTask extends AbstractEmptyTask {
    public readonly queueName = "fetchMeasurementOptionsStaticData";
    public readonly queueTtl = 59 * 60 * 1000; // 59min

    constructor(
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(WorkerContainerToken.StaticMeterSettingsDataSourceProvider)
        private staticDataSourceProvider: StaticMeterSettingsDataSourceProvider,
        @inject(WorkerContainerToken.StaticMeterSettingsRepository)
        private meterSettingsRepository: StaticMeterSettingsRepository,
        @inject(WorkerContainerToken.StaticMeterSettingsDataSourceCapitalDs)
        private meterSettingsRepositoryCapitalDistrict: StaticMeterSettingsDataSourceCapitalDs
    ) {
        super(WORKER_NAME);
    }

    protected async execute(): Promise<void> {
        const dataSource = this.staticDataSourceProvider.getDataSource();
        const staticDataSourceCapitalDistrict = this.meterSettingsRepositoryCapitalDistrict.getDataSource();
        const meterSettingsCapitalDistrict = await staticDataSourceCapitalDistrict.getAll();
        const meterSettings = await dataSource.getAll();

        for (const [commodityProvider, key] of Object.entries(optionLookupKeys)) {
            const options = this.lookupMeasurementsOptionsByKey(
                { ...meterSettings, ...meterSettingsCapitalDistrict },
                key as keyof IStaticMeterSettings
            );
            if (options.length === 0) {
                continue;
            }

            this.logger.info(`Fetched ${options.length} measurement options for ${commodityProvider}`);
            await this.meterSettingsRepository.saveData(
                commodityProvider as CommodityProviderType,
                StaticDataResourceType.MeasurementOptions,
                options
            );
        }
    }

    private lookupMeasurementsOptionsByKey(
        meterSettings: IStaticMeterSettings,
        key: keyof IStaticMeterSettings
    ): IMeasurementOption[] {
        const settings = meterSettings[key];
        if (!settings?.MeasurementOptions) {
            throw new GeneralError(`No measurements options found for key: ${key}`);
        }
        return settings?.MeasurementOptions;
    }
}
