import { AbstractWorker } from "@golemio/core/dist/integration-engine";
import { ITask } from "@golemio/core/dist/integration-engine/workers/interfaces/ITask";
import { WORKER_NAME } from "../constants";
import { CommodityContainer } from "../ioc/Di";
import { WorkerContainerToken } from "../ioc/WorkerContainerToken";

export class CommodityWorker extends AbstractWorker {
    protected name = WORKER_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(CommodityContainer.resolve<ITask>(WorkerContainerToken.FetchMeasurementsFromProvidersTask));

        this.registerTask(CommodityContainer.resolve<ITask>(WorkerContainerToken.FetchMeasurementsFromEEProvidersTask));

        this.registerTask(CommodityContainer.resolve<ITask>(WorkerContainerToken.FetchMeasurementOptionsStaticDataTask));

        this.registerTask(CommodityContainer.resolve<ITask>(WorkerContainerToken.FetchVeoliaMeasurementsTask));

        this.registerTask(CommodityContainer.resolve<ITask>(WorkerContainerToken.FetchPpasMeasurementsTask));

        this.registerTask(CommodityContainer.resolve<ITask>(WorkerContainerToken.FetchPpasEEMeasurementsTask));

        this.registerTask(CommodityContainer.resolve<ITask>(WorkerContainerToken.FetchPreMeasurementDataTask));

        this.registerTask(CommodityContainer.resolve<ITask>(WorkerContainerToken.FetchPtasWebScrapedMeasurementsTask));

        this.registerTask(CommodityContainer.resolve<ITask>(WorkerContainerToken.ProcessPreEanMeasurementsTask));
    }

    public registerTask = (task: ITask): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
