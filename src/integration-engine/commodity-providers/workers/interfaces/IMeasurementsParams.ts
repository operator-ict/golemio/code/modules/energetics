export interface IMeasurementsParams {
    dateFrom: string;
    dateTo: string;
}
