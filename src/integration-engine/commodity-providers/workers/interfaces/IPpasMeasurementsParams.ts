import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { IMeasurementsParams } from "./IMeasurementsParams";

export interface IPpasMeasurementsParams extends IMeasurementsParams {
    providerType:
        | CommodityProviderType.PpasAveV
        | CommodityProviderType.PpasAveM
        // capital district users
        | CommodityProviderType.PpasAveA
        | CommodityProviderType.PpasAveC
        | CommodityProviderType.PpasAveMO;
}
