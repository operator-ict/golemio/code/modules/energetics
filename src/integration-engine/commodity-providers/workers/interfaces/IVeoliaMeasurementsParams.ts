import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { IMeasurementsParams } from "./IMeasurementsParams";

export interface IVeoliaMeasurementsParams extends IMeasurementsParams {
    providerType: CommodityProviderType.CemApiVeoliaElectro | CommodityProviderType.CemApiVeoliaWater;
}
