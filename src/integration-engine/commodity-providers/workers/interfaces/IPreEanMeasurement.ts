export interface IPreEanMeasurement {
    ean: string;
    T1_Wh_raw_data: Array<[string, number]>;
}
