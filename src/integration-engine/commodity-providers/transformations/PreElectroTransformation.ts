import { IPreElectricConsumptionInput } from "#sch/datasources/interfaces/IPreElectricConsumptionInput";
import { IMeasurementOption } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PreElectroTransformation extends AbstractTransformation<
    { option: IMeasurementOption } & { measurement: IPreElectricConsumptionInput },
    IMeasurementDto
> {
    public name = "PreElectroTransformation";

    protected transformInternal = (
        data: { option: IMeasurementOption } & { measurement: IPreElectricConsumptionInput }
    ): IMeasurementDto => {
        return {
            addr: data.option.Addr,
            meter: data.option.Meter,
            timestamp: new Date(data.measurement["Čas měření"] + " UTC"),
            variable: data.option.Var,
            type: data.option.Type,
            value: Number(data.measurement["Stav [kWh]"]),
            source: CommodityProviderType.PreOptions,
            measurement_category: data.option.Name,
        };
    };
}
