import { IAveApiDeviceDataDto } from "#sch/datasources/commodity-providers/interfaces/IAveApiDeviceDataDto";
import { IMeasurementOption } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { CommodityProvider } from "../datasources/helpers/CommodityProviderEnum";
import { PpasAveApiHelper } from "../datasources/ppas-ave-api/helpers/PpasAveApiHelper";
import { MeasurementVariable } from "#sch/datasources/helpers/MeasurementVariableEnum";

@injectable()
export class AveMeasurementTransformation extends AbstractTransformation<
    IMeasurementOption & IAveApiDeviceDataDto,
    IMeasurementDto
> {
    public name = "AveMeasurementTransformation";

    protected transformInternal = (data: IMeasurementOption & IAveApiDeviceDataDto): IMeasurementDto => {
        return {
            source: CommodityProvider.PpasInternetAveApi,
            measurement_category: data.Name,
            addr: data.Addr,
            meter: data.Meter,
            timestamp: DateTime.fromISO(data.StandardTime, {
                zone: PpasAveApiHelper.API_DATE_TZ,
            }).toJSDate(),
            variable: data.Var,
            type: data.Type,
            value: this.getValue(data, data.Var as MeasurementVariable),
        };
    };

    private getValue = (data: IAveApiDeviceDataDto, variable: MeasurementVariable): number => {
        switch (variable) {
            case MeasurementVariable.Core:
            default:
                return data.OperatingAmount;
            case MeasurementVariable.Core2:
                return data.ConvertDifference;
        }
    };
}
