import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { IPreEanMeasurement } from "#ie/commodity-providers/workers/interfaces/IPreEanMeasurement";
import { DateTime } from "@golemio/core/dist/helpers";
import { IMeasurementOption } from "#sch/datasources/interfaces/IStaticMeterSettings";

@injectable()
export class PreMeasurementTransformation extends AbstractTransformation<
    { option: IMeasurementOption } & { measurementInput: IPreEanMeasurement },
    IMeasurementDto[]
> {
    public name = "PreMeasurementTransformation";

    protected transformInternal = (
        data: { option: IMeasurementOption } & { measurementInput: IPreEanMeasurement }
    ): IMeasurementDto[] => {
        const output: IMeasurementDto[] = [];
        for (const measurement of data.measurementInput.T1_Wh_raw_data) {
            output.push({
                addr: data.option.Addr,
                meter: data.measurementInput.ean,
                timestamp: DateTime.fromFormat(measurement[0], "dd.MM.yyyy T", { timeZone: "UTC" }).toDate(),
                variable: data.option.Var,
                type: data.option.Type,
                value: Number(measurement[1]),
                source: CommodityProviderType.PreInputOptions,
                measurement_category: data.option.Name,
            });
        }
        return output;
    };
}
