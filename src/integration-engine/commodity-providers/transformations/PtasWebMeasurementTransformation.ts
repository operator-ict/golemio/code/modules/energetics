import { IMeasurementOption } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { IPtasMeasurement } from "#sch/datasources/interfaces/PtasWebScrapingInput";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PtasWebMeasurementTransformation extends AbstractTransformation<
    { option: IMeasurementOption } & { measurement: IPtasMeasurement },
    IMeasurementDto
> {
    public name = "PtasWebMeasurementTransformation";
    private dateFormatSetting = "d.M.yyyy H:mm";

    protected transformInternal = (data: { option: IMeasurementOption } & { measurement: IPtasMeasurement }): IMeasurementDto => {
        return {
            addr: data.option.Addr,
            source: CommodityProviderType.PtasOptions,
            meter: data.option.Meter,
            timestamp: DateTime.fromFormat(
                `${data.measurement["Datum odečtu"]} ${data.measurement["Čas odečtu"]}`,
                this.dateFormatSetting,
                {
                    zone: "Europe/Prague",
                }
            )
                .toUTC()
                .toJSDate(),
            variable: data.option.Var,
            type: data.option.Type,
            value: parseFloat(data.measurement["Odečet"].replaceAll(" ", "").replace(",", ".")),
            measurement_category: data.option.Name,
        };
    };
}
