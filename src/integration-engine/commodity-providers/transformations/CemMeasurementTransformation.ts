import { ICemApiMeasurementDto } from "#sch/datasources/commodity-providers/interfaces/ICemApiMeasurementDto";
import { IMeasurementOption } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { IMeasurementDto } from "#sch/models/interfaces/IMeasurementDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { CommodityProvider } from "../datasources/helpers/CommodityProviderEnum";
import { UnimonitorCemApiHelper } from "#ie/helpers";
import { MeasurementVariable } from "#sch/datasources/helpers/MeasurementVariableEnum";

@injectable()
export class CemMeasurementTransformation extends AbstractTransformation<
    IMeasurementOption & ICemApiMeasurementDto & { counterTypeId: number },
    IMeasurementDto
> {
    public name = "CemMeasurementTransformation";

    protected transformInternal = (
        data: IMeasurementOption & ICemApiMeasurementDto & { counterTypeId: number }
    ): IMeasurementDto => {
        return {
            source: CommodityProvider.UnimonitorCemApi,
            measurement_category: data.Name,
            addr: data.Addr,
            meter: data.Meter,
            timestamp: new Date(data.timestamp),
            variable: this.getVariable(data.Var, data.counterTypeId),
            type: data.Type,
            value: data.value,
        };
    };

    private getVariable = (variable: string, counterTypeId: number): string => {
        if (counterTypeId === UnimonitorCemApiHelper.counterType.ElHighTariff) {
            return "VT"; // Vysoky tarif
        } else if (counterTypeId === UnimonitorCemApiHelper.counterType.ElLowTariff) {
            return "NT"; // Nizky tarif
        }

        return variable;
    };
}
