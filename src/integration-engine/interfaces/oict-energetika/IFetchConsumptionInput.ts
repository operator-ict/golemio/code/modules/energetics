export interface IFetchConsumptionInput {
    dateFrom: string;
    dateTo: string;
}
