export function toStringOrEmpty(input: any): string {
    return (input ?? "").toString();
}

export function getUniqueBy<T extends Record<string, never>>(arr: T[], prop: string): T[] {
    const set = new Set<any>();
    return arr.filter((o) => !set.has(o[prop]) && set.add(o[prop]));
}
