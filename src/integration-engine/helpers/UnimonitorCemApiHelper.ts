import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export interface ICemConfig {
    authcookiename: string;
    user: string;
    pass: string;
}
/**
 * Helper class for requesting additional data from Unimonitor CEM API
 */
class UnimonitorCemApiHelper {
    public static API_DATE_FORMAT = "yyyy-LL-dd"; // Luxon token format: https://moment.github.io/luxon/#/formatting
    public static API_DATE_TZ = "Europe/Prague";
    private static COOKIE_KV_SEPARATOR = "=";

    constructor(private url: string, private config: ICemConfig) {}
    /**
     * Create API session and return authorization cookie
     */
    public createSession = async (): Promise<string> => {
        try {
            const { authcookiename, user, pass } = this.config;
            const params = new URLSearchParams({
                id: UnimonitorCemApiHelper.resourceType.UserLogin,
                pass,
                user,
            });

            const absoluteUrl = `${this.url}?${params}`;

            const result = await fetch(absoluteUrl, {
                method: "get",
                signal: AbortSignal.timeout(10000),
            });

            const cookieHeader = this.getAuthCookie(result.headers);

            return UnimonitorCemApiHelper.processAndFilterAuthCookie(cookieHeader, authcookiename);
        } catch (err) {
            throw new GeneralError(
                "Cannot retrieve Unimonitor CEM API authorization token " + err.message,
                UnimonitorCemApiHelper.name,
                err
            );
        }
    };

    /**
     * Terminate current session/invalidate auth cookie
     */
    public terminateSession = async (authCookie: string): Promise<void> => {
        const url = this.url;

        const params = new URLSearchParams({
            id: UnimonitorCemApiHelper.resourceType.UserLogout,
        });

        const absoluteUrl = `${url}?${params}`;

        try {
            await fetch(absoluteUrl, {
                headers: {
                    Cookie: authCookie,
                },
                method: "GET",
                signal: AbortSignal.timeout(10000),
            });
        } catch (err) {
            throw new GeneralError("Cannot terminate Unimonitor CEM API session", UnimonitorCemApiHelper.name, err);
        }
    };

    private getAuthCookie(headers: Headers) {
        // method getSetCookie is missing in types see https://developer.mozilla.org/en-US/docs/Web/API/Headers/getSetCookie
        // it should be possible to remove any with next version of typescript / node
        const cookieHeader = (headers as any).getSetCookie() as string[];

        if (cookieHeader.length === 0) {
            throw new GeneralError("Cannot retrieve Unimonitor CEM API authorization cookie", UnimonitorCemApiHelper.name);
        }

        return cookieHeader[0];
    }

    /**
     * Return resource types/identifiers
     */
    public static get resourceType() {
        return {
            Devices: "46",
            Counters: "45",
            MeasurementsByCounter: "3",
            Measurement: "20",
            MeasuringEquipment: "6",
            MeterType: "14",
            TypeMeasuringEquipment: "11",
            Units: "7",
            UserLogin: "4",
            UserLogout: "5",
        } as const;
    }

    /** Return counter types/identifiers */
    public static get counterType() {
        return {
            ElHighTariff: 1,
            ElLowTariff: 2,
        } as const;
    }

    /**
     * Process and filter auth cookie from the original cookie header
     */
    private static processAndFilterAuthCookie = (cookieHeader: string | undefined, authCookieName: string): string => {
        const rawCookies = cookieHeader?.split(";") ?? [];

        for (const rawCookie of rawCookies) {
            const rawCookieArray = rawCookie.split(UnimonitorCemApiHelper.COOKIE_KV_SEPARATOR).map((prop) => prop.trim());

            const [cookieName, cookieValue] = rawCookieArray;

            if (cookieName === authCookieName && !!cookieValue) {
                return rawCookieArray.join(UnimonitorCemApiHelper.COOKIE_KV_SEPARATOR);
            }
        }

        return "";
    };
}

export { UnimonitorCemApiHelper };
