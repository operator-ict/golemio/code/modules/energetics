import { IFetchXHoursDataInput } from "#ie/interfaces/vpalace/IFetchXHoursDataInput";
import { IsInt, Min } from "@golemio/core/dist/shared/class-validator";

export class FetchXHoursDataValidationSchema implements IFetchXHoursDataInput {
    @IsInt()
    @Min(1)
    targetHours!: number;
}
