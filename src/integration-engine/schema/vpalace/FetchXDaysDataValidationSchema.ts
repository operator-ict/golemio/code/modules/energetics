import { IFetchXDaysData } from "#ie/interfaces/vpalace/IFetchXDaysDataInput";
import { IsInt, Min } from "@golemio/core/dist/shared/class-validator";

export class FetchXDaysDataValidationSchema implements IFetchXDaysData {
    @IsInt()
    @Min(1)
    targetDays!: number;
}
