import { IFetchDataInput } from "#ie/interfaces/oict-energetika/IFetchDataInput";
import { IsInt, Min } from "@golemio/core/dist/shared/class-validator";

export class FetchDataValidationSchema implements IFetchDataInput {
    @IsInt()
    @Min(1)
    targetDays!: number;
}
