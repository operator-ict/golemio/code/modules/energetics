import { IFetchConsumptionInput } from "#ie/interfaces/oict-energetika/IFetchConsumptionInput";
import { IsISO8601 } from "@golemio/core/dist/shared/class-validator";

export class FetchConsumptionValidationSchema implements IFetchConsumptionInput {
    @IsISO8601({ strict: true })
    dateFrom!: string;

    @IsISO8601({ strict: true })
    dateTo!: string;
}
