import { OictEnergetikaWorker } from "#ie/workers/oict-energetika/OictEnergetikaWorker";
import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { CommodityWorker } from "./commodity-providers/workers/CommodityWorker";
import { EnergeticsNewVpalaceWorker } from "./workers/vpalace/EnergeticsNewVpalaceWorker";

export const workers: Array<new () => AbstractWorker> = [OictEnergetikaWorker, EnergeticsNewVpalaceWorker, CommodityWorker];
