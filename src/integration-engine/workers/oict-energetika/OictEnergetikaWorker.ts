import { FetchAccessConfigurationTask } from "#ie/tasks/oict-energetika/FetchAccessConfigurationTask";
import { FetchConsumptionTask } from "#ie/tasks/oict-energetika/FetchConsumptionTask";
import { FetchDataTask } from "#ie/tasks/oict-energetika/FetchDataTask";
import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers/AbstractWorker";

export class OictEnergetikaWorker extends AbstractWorker {
    protected name = "energeticsOICT";

    constructor() {
        super();

        // Register tasks
        this.registerTask(new FetchDataTask(this.getQueuePrefix()));
        this.registerTask(new FetchConsumptionTask(this.getQueuePrefix()));
        this.registerTask(new FetchAccessConfigurationTask(this.getQueuePrefix()));
    }
}
