import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { VPalaceContainer } from "#ie/ioc/Di";
import FetchXDaysDataTask from "#ie/tasks/vpalace/FetchXDaysDataTask";
import FetchXHoursDataTask from "#ie/tasks/vpalace/FetchXHoursDataTask";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine";

export class EnergeticsNewVpalaceWorker extends AbstractWorker {
    protected name = "energetics";

    constructor() {
        super();

        // Register tasks
        this.registerTask(VPalaceContainer.resolve<FetchXDaysDataTask>(FetchXDaysDataTask));
        this.registerTask(VPalaceContainer.resolve<FetchXHoursDataTask>(FetchXHoursDataTask));
    }
}
