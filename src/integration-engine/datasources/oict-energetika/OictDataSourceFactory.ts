import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { OictApiHelper, OictResourceType } from "./helpers";

interface IDateParams {
    dateFrom: string;
    dateTo: string;
}

export class OictDataSourceFactory {
    public static getDataSource(resourceType: OictResourceType, dateParams?: IDateParams): DataSource {
        const url = new URL(OictApiHelper.getResourcePath(resourceType), config.datasources.OICTEnergetikaApiUrl);

        if (dateParams) {
            url.searchParams.set("dateFrom", dateParams.dateFrom);
            url.searchParams.set("dateTo", dateParams.dateTo);
        }

        return new DataSource(
            OictApiHelper.API_NAME + resourceType + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: config.datasources.OICTEnergetikaApiHeaders,
                method: "GET",
                timeoutInSeconds: 60,
                url: url.toString(),
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            OictApiHelper.getValidator(resourceType)
        );
    }
}
