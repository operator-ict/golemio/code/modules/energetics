import { oictBuildingsJsonSchema, oictConsumptionJsonSchema, oictDevicesJsonSchema } from "#sch/datasources";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export enum OictResourceType {
    Buildings = "Buildings",
    Consumption = "Consumption",
    ConsumptionVisapp = "ConsumptionVisapp",
    Devices = "Devices",
}

export class OictApiHelper {
    public static API_NAME = "OICTEnergetika";

    public static getResourcePath(resourceType: OictResourceType): string {
        switch (resourceType) {
            // List of buildings in full format
            case OictResourceType.Buildings:
                return "buildings/full";

            // Meter data from commodity providers' APIs
            case OictResourceType.Consumption:
                return "data/consumption";

            // Data from Dot Controls systems
            case OictResourceType.ConsumptionVisapp:
                return "data/visapp";

            // List of meters (water, electricity, gas, etc) in full format
            case OictResourceType.Devices:
                return "devices/full";
        }
    }

    public static getValidator(resourceType: OictResourceType): JSONSchemaValidator {
        const validatorName = `${OictApiHelper.API_NAME}${resourceType}Validator`;

        switch (resourceType) {
            case OictResourceType.Buildings:
                return new JSONSchemaValidator(validatorName, oictBuildingsJsonSchema);

            case OictResourceType.Consumption:
            case OictResourceType.ConsumptionVisapp:
                return new JSONSchemaValidator(validatorName, oictConsumptionJsonSchema);

            case OictResourceType.Devices:
                return new JSONSchemaValidator(validatorName, oictDevicesJsonSchema);

            default:
                throw new GeneralError(`Unknown resource type: ${resourceType}`, "OictApiHelper");
        }
    }
}
