import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { ProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/ProtocolStrategy";

export abstract class BaseVpalaceFactory {
    private url: string;
    constructor(url: string) {
        this.url = url;
    }
    protected getConnectionSettings = async (
        resourceType: string,
        dateFrom: string,
        dateTo: string,
        authCookie: string
    ): Promise<ProtocolStrategy> => {
        const params = new URLSearchParams({
            ...(resourceType === "11" && { cis: "135" }),
            id: resourceType,
            ...(Number.isInteger(+dateFrom) ? { from_ms: dateFrom } : { from: dateFrom }),
            ...(Number.isInteger(+dateTo) ? { to_ms: dateTo } : { to: dateTo }),
        });

        return new HTTPFetchProtocolStrategy({
            headers: {
                Cookie: authCookie,
            },
            method: "GET",
            timeoutInSeconds: 20,
            url: `${this.url}?${params}`,
        });
    };
}
