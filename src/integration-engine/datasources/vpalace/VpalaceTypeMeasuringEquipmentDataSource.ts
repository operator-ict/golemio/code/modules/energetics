import { UnimonitorCemApiHelper } from "#ie/helpers";
import { EnergeticsSchema } from "#sch";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { BaseVpalaceFactory } from "./BaseVpalaceFactory";

@injectable()
export class VpalaceTypeMeasuringEquipmentDataSource extends BaseVpalaceFactory {
    private readonly resourceType = UnimonitorCemApiHelper.resourceType.TypeMeasuringEquipment;
    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {
        super(config.getValue<string>("module.UnimonitorCemApiEnergetics.url"));
    }
    public async getDataSource(from: string, to: string, authCookie: string): Promise<DataSource> {
        const sourceHTTPProtocolStrategy = await this.getConnectionSettings(this.resourceType, from, to, authCookie);

        return new DataSource(
            EnergeticsSchema.vpalac.typeMeasuringEquipment.name + "DataSource",
            sourceHTTPProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                EnergeticsSchema.vpalac.typeMeasuringEquipment.name + "DataSource",
                EnergeticsSchema.vpalac.typeMeasuringEquipment.datasourceJsonSchema
            )
        );
    }
}
