import { UnimonitorCemApiHelper } from "#ie/helpers";
import { EnergeticsSchema } from "#sch";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { BaseVpalaceFactory } from "./BaseVpalaceFactory";

@injectable()
export class VpalaceMeasurementDataSource extends BaseVpalaceFactory {
    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {
        super(config.getValue<string>("module.UnimonitorCemApiEnergetics.url"));
    }
    private readonly resourceType = UnimonitorCemApiHelper.resourceType.Measurement;
    public async getDataSource(from: string, to: string, authCookie: string): Promise<DataSource> {
        const sourceHTTPProtocolStrategy = await this.getConnectionSettings(this.resourceType, from, to, authCookie);

        return new DataSource(
            EnergeticsSchema.vpalac.measurement.name + "DataSource",
            sourceHTTPProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                EnergeticsSchema.vpalac.measurement.name + "DataSource",
                EnergeticsSchema.vpalac.measurement.datasourceJsonSchema
            )
        );
    }
}
