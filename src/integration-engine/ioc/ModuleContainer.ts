const ModuleContainerToken = {
    //measurement
    VpalaceMeasurementRepository: Symbol(),
    VpalaceMeasurementDataSource: Symbol(),
    VpalacMeasurementTransformation: Symbol(),
    //measuring equipment
    VpalaceMeasuringEquipmentDataSource: Symbol(),
    VpalaceMeasuringEquipmentRepository: Symbol(),
    VpalacMeasuringEquipmentTransformation: Symbol(),
    //meter type
    VpalaceMeterTypeDataSource: Symbol(),
    VpalaceMeterTypeRepository: Symbol(),
    VpalacMeterTypeTransformation: Symbol(),
    //type measuring equipment
    VpalaceTypeMeasuringEquipmentDataSource: Symbol(),
    VpalaceTypeMeasuringEquipmentRepository: Symbol(),
    VpalacTypeMeasuringEquipmentTransformation: Symbol(),
    //units
    VpalaceUnitsDataSource: Symbol(),
    VpalaceUnitsRepository: Symbol(),
    VpalacUnitsTransformation: Symbol(),
    //factory
    FetchDateFactory: Symbol(),
    //tasks
    FetchXDaysDataTask: Symbol(),
    FetchXHoursDataTask: Symbol(),
    //data tasks
    MeasurementDataFetcher: Symbol(),
    MeasuringEquipmentFetcher: Symbol(),
    MeterTypeFetcher: Symbol(),
    TypeMeasuringEquipmentFetcher: Symbol(),
    UnitDataFetcher: Symbol(),
};

export { ModuleContainerToken };
