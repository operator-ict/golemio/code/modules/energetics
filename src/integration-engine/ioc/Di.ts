import { VpalaceMeasurementDataSource } from "#ie/datasources/vpalace/VpalaceMeasurementDataSource";
import { VpalaceMeasuringEquipmentDataSource } from "#ie/datasources/vpalace/VpalaceMeasuringEquipmentDataSource";
import { VpalaceMeterTypeDataSource } from "#ie/datasources/vpalace/VpalaceMeterTypeDataSource";
import { VpalaceTypeMeasuringEquipmentDataSource } from "#ie/datasources/vpalace/VpalaceTypeMeasuringEquipmentDataSource";
import { VpalaceUnitsDataSource } from "#ie/datasources/vpalace/VpalaceUnitsDataSource";
import { VpalaceMeasurementRepository } from "#ie/repositories/vpalace/VpalaceMeasurementRepository";
import { VpalaceMeasuringEquipmentRepository } from "#ie/repositories/vpalace/VpalaceMeasuringEquipmentRepository";
import { VpalaceMeterTypeRepository } from "#ie/repositories/vpalace/VpalaceMeterTypeRepository";
import { VpalaceTypeMeasuringEquipmentRepository } from "#ie/repositories/vpalace/VpalaceTypeMeasuringEquipmentRepository";
import { VpalaceUnitsRepository } from "#ie/repositories/vpalace/VpalacUnitsRepository";
import { FetchDateFactory } from "#ie/tasks/vpalace/FetchDateFactory";

import FetchXDaysDataTask from "#ie/tasks/vpalace/FetchXDaysDataTask";
import FetchXHoursDataTask from "#ie/tasks/vpalace/FetchXHoursDataTask";
import { MeasurementDataFetcher } from "#ie/tasks/vpalace/MeasurementDataFetcher";
import { MeasuringEquipmentFetcher } from "#ie/tasks/vpalace/MeasuringEquipmentFetcher";
import { MeterTypeFetcher } from "#ie/tasks/vpalace/MeterTypeFetcher";
import { TypeMeasuringEquipmentFetcher } from "#ie/tasks/vpalace/TypeMeasuringEquipmentFetcher";
import { UnitDataFetcher } from "#ie/tasks/vpalace/UnitDataFetcher";
import {
    VpalacMeasurementTransformation,
    VpalacMeasuringEquipmentTransformation,
    VpalacMeterTypeTransformation,
    VpalacTypeMeasuringEquipmentTransformation,
    VpalacUnitsTransformation,
} from "#ie/transformations/vpalace";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainer";

//#region Initialization
const VPalaceContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Helpers
//#endregion

//#region Datasource
VPalaceContainer.register(ModuleContainerToken.VpalaceMeasurementDataSource, VpalaceMeasurementDataSource);
VPalaceContainer.register(ModuleContainerToken.VpalaceMeasuringEquipmentDataSource, VpalaceMeasuringEquipmentDataSource);
VPalaceContainer.register(ModuleContainerToken.VpalaceMeterTypeDataSource, VpalaceMeterTypeDataSource);
VPalaceContainer.register(ModuleContainerToken.VpalaceTypeMeasuringEquipmentDataSource, VpalaceTypeMeasuringEquipmentDataSource);
VPalaceContainer.register(ModuleContainerToken.VpalaceUnitsDataSource, VpalaceUnitsDataSource);
//#endregion

//#region Repositories
VPalaceContainer.register(ModuleContainerToken.VpalaceMeasurementRepository, VpalaceMeasurementRepository);
VPalaceContainer.register(ModuleContainerToken.VpalaceMeasuringEquipmentRepository, VpalaceMeasuringEquipmentRepository);
VPalaceContainer.register(ModuleContainerToken.VpalaceMeterTypeRepository, VpalaceMeterTypeRepository);
VPalaceContainer.register(ModuleContainerToken.VpalaceTypeMeasuringEquipmentRepository, VpalaceTypeMeasuringEquipmentRepository);
VPalaceContainer.register(ModuleContainerToken.VpalaceUnitsRepository, VpalaceUnitsRepository);
//#endregion

//#region Transformation
VPalaceContainer.register(ModuleContainerToken.VpalacMeasurementTransformation, VpalacMeasurementTransformation);
VPalaceContainer.register(ModuleContainerToken.VpalacMeasuringEquipmentTransformation, VpalacMeasuringEquipmentTransformation);
VPalaceContainer.register(ModuleContainerToken.VpalacMeterTypeTransformation, VpalacMeterTypeTransformation);
VPalaceContainer.register(
    ModuleContainerToken.VpalacTypeMeasuringEquipmentTransformation,
    VpalacTypeMeasuringEquipmentTransformation
);
VPalaceContainer.register(ModuleContainerToken.VpalacUnitsTransformation, VpalacUnitsTransformation);
//#endregion

//#region Factory
VPalaceContainer.register(ModuleContainerToken.FetchDateFactory, FetchDateFactory);
//#endregion

//#region Tasks
VPalaceContainer.registerSingleton<MeasurementDataFetcher>(ModuleContainerToken.MeasurementDataFetcher, MeasurementDataFetcher);
VPalaceContainer.registerSingleton<MeasuringEquipmentFetcher>(
    ModuleContainerToken.MeasuringEquipmentFetcher,
    MeasuringEquipmentFetcher
);
VPalaceContainer.registerSingleton<MeterTypeFetcher>(ModuleContainerToken.MeterTypeFetcher, MeterTypeFetcher);
VPalaceContainer.registerSingleton<TypeMeasuringEquipmentFetcher>(
    ModuleContainerToken.TypeMeasuringEquipmentFetcher,
    TypeMeasuringEquipmentFetcher
);
VPalaceContainer.registerSingleton<UnitDataFetcher>(ModuleContainerToken.UnitDataFetcher, UnitDataFetcher);

VPalaceContainer.registerSingleton<FetchXDaysDataTask>(ModuleContainerToken.FetchXDaysDataTask, FetchXDaysDataTask);
VPalaceContainer.registerSingleton<FetchXHoursDataTask>(ModuleContainerToken.FetchXHoursDataTask, FetchXHoursDataTask);
//#endregion

export { VPalaceContainer };
