import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { checkContentTypeMiddleware } from "@golemio/core/dist/input-gateway/helpers";
import { PreJsonInputController } from "#ig/pre/PreJsonInputController";
import { PreDataExtractor } from "#ig/helpers/PreDataExtractor";
import { ContainerToken, InputGatewayContainer } from "@golemio/core/dist/input-gateway/ioc";
import { CsvParserMiddleware } from "@golemio/core/dist/input-gateway/middleware/CsvParserMiddleware";
import { PreCsvInputController } from "#ig/pre/PreCsvInputController";
import { PreCsvInputTransformation } from "#ig/transformations/PreCsvInputTransformation";

export class PreInputRouter {
    public router: Router;
    private jsonInputController: PreJsonInputController;
    private csvInputController: PreCsvInputController;
    private csvParser: CsvParserMiddleware;

    constructor() {
        this.router = Router();
        this.jsonInputController = new PreJsonInputController(new PreDataExtractor());
        this.csvInputController = new PreCsvInputController(new PreCsvInputTransformation());
        this.csvParser = InputGatewayContainer.resolve<CsvParserMiddleware>(ContainerToken.CsvParserMiddleware);
        this.initRoutes();
    }

    private initRoutes = (): void => {
        this.router.post("/", checkContentTypeMiddleware(["application/json"]), this.postPreJson);
        this.router.post(
            "/csv",
            checkContentTypeMiddleware(["text/csv"]),
            this.csvParser.getMiddleware({
                delimiter: ";",
                shouldIncludeHeaders: true,
            }),
            this.postPreCsv
        );
    };

    private postPreJson = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.jsonInputController.processData(req.body);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private postPreCsv = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.csvInputController.processData(req.body);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const preInputRouter = new PreInputRouter().router;
export { preInputRouter };
