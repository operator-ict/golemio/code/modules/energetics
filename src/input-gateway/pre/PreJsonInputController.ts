import { BaseController } from "@golemio/core/dist/input-gateway/controllers";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { preJsonInputSchema } from "#sch/datasources/PreJsonInputSchema";
import { IPreJsonInput } from "#sch/datasources/interfaces/IPreJsonInput";
import { PreDataExtractor } from "#ig/helpers/PreDataExtractor";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { WORKER_NAME } from "#ie/commodity-providers/constants";

export class PreJsonInputController extends BaseController {
    constructor(private preDataExtractor: PreDataExtractor) {
        super(WORKER_NAME, new JSONSchemaValidator("PreJsonInputController", preJsonInputSchema));
    }

    public processData = async (inputData: IPreJsonInput) => {
        try {
            await this.validator.Validate(inputData);
            const eanMeasurements = this.preDataExtractor.extractEanMeasurements(inputData);
            for (const eanMeasurement of eanMeasurements) {
                await this.sendMessageToExchange(
                    "input." + this.queuePrefix + ".processPreEanMeasurements",
                    JSON.stringify(eanMeasurement),
                    {
                        persistent: true,
                    }
                );
            }
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.name, err, 422);
            }
        }
    };
}
