import { BaseController } from "@golemio/core/dist/input-gateway/controllers";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { WORKER_NAME } from "#ie/commodity-providers/constants";
import { PreCsvInputTransformation } from "#ig/transformations/PreCsvInputTransformation";
import { preCsvInputSchema } from "#sch/datasources/PreCsvInputSchema";
import { IPreCsvInput } from "#sch/datasources/interfaces/IPreCsvInput";

export class PreCsvInputController extends BaseController {
    constructor(private transformation: PreCsvInputTransformation) {
        super(WORKER_NAME, new JSONSchemaValidator("PreCsvInputController", preCsvInputSchema));
    }

    public processData = async (inputData: IPreCsvInput[]) => {
        try {
            await this.validator.Validate(inputData);
            const eanMeasurement = this.transformation.transformElement(inputData);
            await this.sendMessageToExchange(
                "input." + this.queuePrefix + ".processPreEanMeasurements",
                JSON.stringify(eanMeasurement),
                {
                    persistent: true,
                }
            );
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.name, err, 422);
            }
        }
    };
}
