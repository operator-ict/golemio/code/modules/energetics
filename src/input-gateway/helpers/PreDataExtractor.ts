import { IPreJsonInput } from "#sch/datasources/interfaces/IPreJsonInput";
import { IPreEanMeasurement } from "#ie/commodity-providers/workers/interfaces/IPreEanMeasurement";

export class PreDataExtractor {
    public extractEanMeasurements(inputData: IPreJsonInput): IPreEanMeasurement[] {
        const eans: IPreEanMeasurement[] = [];

        for (const company of Object.values(inputData.stored_month.companies)) {
            for (const [ean, eanData] of Object.entries(company.EAN_list)) {
                const dataArray = eanData.daily_data_Wh_per_EAN.T1_Wh_raw_data.flat();
                eans.push({
                    ean,
                    T1_Wh_raw_data: dataArray,
                });
            }
        }

        return eans;
    }
}
