import { PtasWebScrapingController } from "#ig/ptas/PtasWebScrapingController";
import { checkContentTypeMiddleware } from "@golemio/core/dist/input-gateway/helpers";
import { ContainerToken } from "@golemio/core/dist/input-gateway/ioc/ContainerToken";
import { InputGatewayContainer } from "@golemio/core/dist/input-gateway/ioc/Di";
import { CsvParserMiddleware } from "@golemio/core/dist/input-gateway/middleware/CsvParserMiddleware";
import { NextFunction, Response, Router } from "@golemio/core/dist/shared/express";
import { IExtendedPtasRequest } from "./interfaces/IExtendedPtasRequest";

export class PtasRouter {
    public router: Router;
    private csvParser: CsvParserMiddleware;
    private controller: PtasWebScrapingController;

    constructor() {
        this.router = Router();
        this.csvParser = InputGatewayContainer.resolve<CsvParserMiddleware>(ContainerToken.CsvParserMiddleware);
        this.controller = new PtasWebScrapingController();
        this.initRoutes();
    }

    private initRoutes = (): void => {
        this.router.post(
            "/:placeId",
            checkContentTypeMiddleware(["text/csv"]),
            this.csvParser.getMiddleware({
                delimiter: ";",
                shouldIncludeHeaders: true,
            }),
            this.post
        );
    };

    private post = async (req: IExtendedPtasRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.processData({
                placeId: req.params.placeId,
                measurements: req.body,
            });
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const ptasRouter = new PtasRouter().router;

export { ptasRouter };
