import { IPtasMeasurement } from "#sch/datasources/interfaces/PtasWebScrapingInput";
import { Request } from "@golemio/core/dist/shared/express";

export interface IExtendedPtasRequest extends Request {
    params: {
        placeId: string;
    };
    body: IPtasMeasurement[];
}
