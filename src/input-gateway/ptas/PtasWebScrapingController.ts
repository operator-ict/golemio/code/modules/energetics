import { WORKER_NAME } from "#ie/commodity-providers/constants";
import { ptasWebScrapingJsonSchema } from "#sch/datasources/PtasWebScrapingJsonSchema";
import { IPtasWebScrapingInput } from "#sch/datasources/interfaces/PtasWebScrapingInput";
import { BaseController } from "@golemio/core/dist/input-gateway/controllers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class PtasWebScrapingController extends BaseController {
    public name!: string;
    protected validator!: JSONSchemaValidator;
    protected queuePrefix!: string;

    constructor() {
        super(WORKER_NAME, new JSONSchemaValidator("PtasWebScrapingController", ptasWebScrapingJsonSchema));
    }

    processData = async (inputData: IPtasWebScrapingInput): Promise<void> => {
        try {
            await this.validator.Validate(inputData);
            const queue = "input." + this.queuePrefix + ".savePtasMeasurements";

            await this.sendMessageToExchange(queue, JSON.stringify(inputData), {
                persistent: true,
            });
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.name, err, 422);
            }
        }
    };
}
