import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IPreCsvInput } from "#sch/datasources/interfaces/IPreCsvInput";
import { IPreEanMeasurement } from "#ie/commodity-providers/workers/interfaces/IPreEanMeasurement";

export class PreCsvInputTransformation extends AbstractTransformation<IPreCsvInput[], IPreEanMeasurement> {
    public name = "PreCsvInputTransformation";

    protected transformInternal = (data: IPreCsvInput[]): IPreEanMeasurement => {
        const T1_Wh_raw_data: Array<[string, number]> = [];
        const ean = this.getEan(data[0]);

        for (const row of data) {
            const consumptionValue = row[Object.keys(row)[2]];
            T1_Wh_raw_data.push([row["Konec intervalu"], parseInt(consumptionValue)]);
        }

        return { ean, T1_Wh_raw_data };
    };

    private getEan = (data: IPreCsvInput): string => {
        const keys = Object.keys(data);
        return keys[2].substring(0, 18);
    };
}
