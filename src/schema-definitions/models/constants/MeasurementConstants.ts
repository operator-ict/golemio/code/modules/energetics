export const MEASUREMENT_VARIABLES = [
    "core",
    "core2",
    "EFwActi",
    "EFwActiVT",
    "ERvReact",
    "EFwReact",
    "raw",
    "T",
    "CO2",
    "RH",
    "EFwActiNT",
    "VOC",
    "NT",
    "VT",
];

export const MEASUREMENT_TYPES = ["monthly_reading", "provider_value", "dotcontrols_value"];
