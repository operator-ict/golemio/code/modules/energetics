export interface IBuildingsPrimary {
    id: number;
    building_name: string | null;
    description: string | null;
    building_address_code: string | null;
    building_label: string | null;
    main_use: string | null;
    secondary_use: string | null;
    year_of_construction: number | null;
}
