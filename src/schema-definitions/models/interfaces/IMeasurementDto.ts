export interface IMeasurementDto {
    timestamp: Date;
    value: number;
    addr: string;
    variable: string;
    type: string;
    meter: string;
    source: string;
    measurement_category: string;
}
