export interface IOrganizationsModel {
    id: number;
    name: string | null;
    label: string | null;
    address_street: string | null;
    address_house_number: string | null;
    address_city: string | null;
    address_country: string | null;
    address_mail: string | null;
    address_phone: string | null;
    address_web_address: string | null;
    category: string | null;
    created_by_id: number | null;
    grafana_url: string | null;
}
