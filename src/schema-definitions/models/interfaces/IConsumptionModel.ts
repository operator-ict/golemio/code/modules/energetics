export interface IConsumptionModel {
    time_utc: string;
    value: number;
    addr: string;
    variable: string;
    type: string;
    meter: string;
}
