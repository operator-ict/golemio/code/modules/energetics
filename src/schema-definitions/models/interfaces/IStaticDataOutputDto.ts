import { IMeasurementOption, ISelectedMeters } from "#sch/datasources/interfaces/IStaticMeterSettings";
import { CommodityProviderType } from "../helpers/CommodityProviderTypeEnum";
import { StaticDataResourceType } from "../helpers/StaticDataResourceTypeEnum";

export interface IStaticDataOutputDto {
    provider: CommodityProviderType;
    resource_type: StaticDataResourceType;
    data: ResourceTypeDict[StaticDataResourceType];
}

export type ResourceTypeDict = {
    [StaticDataResourceType.MeasurementOptions]: IMeasurementOption[];
    [StaticDataResourceType.SelectedMeters]: ISelectedMeters;
};
