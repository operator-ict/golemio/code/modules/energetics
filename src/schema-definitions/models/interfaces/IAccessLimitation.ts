export interface IAccessLimitation {
    group_name: string;
    organization_ids: number[];
}
