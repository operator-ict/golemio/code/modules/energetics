export interface IDevicesModel {
    id: number;
    addr: string;
    description: string;
    meter_number: string;
    meter_index: string;
    location_number: string;
    location_description: string;
    include_in_evaluation: string;
    meter_type: string;
    category: string;
    unit: string;
    replaced_meter_id: string;
    deleted: string;
    building_id: number;
}
