export interface IOrganizationBuildingsModel {
    organization_id: number;
    building_id: number;
}
