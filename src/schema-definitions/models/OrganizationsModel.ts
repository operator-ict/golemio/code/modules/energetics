import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IOrganizationsModel } from "#sch/models/interfaces/IOrganizationsModel";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export class OrganizationsModel extends Model<OrganizationsModel> implements IOrganizationsModel {
    public static TABLE_NAME = "organizations";

    declare id: number;
    declare name: string | null;
    declare label: string | null;
    declare address_street: string | null;
    declare address_house_number: string | null;
    declare address_city: string | null;
    declare address_country: string | null;
    declare address_mail: string | null;
    declare address_phone: string | null;
    declare address_web_address: string | null;
    declare category: string | null;
    declare created_by_id: number | null;
    declare grafana_url: string | null;

    public static attributeModel: ModelAttributes<OrganizationsModel> = {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        name: { type: DataTypes.STRING(100) },
        label: { type: DataTypes.STRING(50) },
        address_street: { type: DataTypes.STRING(100) },
        address_house_number: { type: DataTypes.STRING(100) },
        address_city: { type: DataTypes.STRING(100) },
        address_country: { type: DataTypes.STRING(100) },
        address_mail: { type: DataTypes.STRING(100) },
        address_phone: { type: DataTypes.STRING(50) },
        address_web_address: { type: DataTypes.STRING(100) },
        category: { type: DataTypes.STRING(80) },
        created_by_id: { type: DataTypes.INTEGER },
        grafana_url: { type: DataTypes.STRING(255) },
    };

    public static arrayJsonSchema: JSONSchemaType<IOrganizationsModel[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "number" },
                name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                label: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                address_street: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                address_house_number: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                address_city: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                address_country: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                address_mail: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                address_phone: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                address_web_address: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                category: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                created_by_id: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                grafana_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            additionalProperties: false,
            required: [
                "id",
                "address_street",
                "address_house_number",
                "address_city",
                "address_country",
                "address_mail",
                "address_phone",
                "address_web_address",
            ],
        },
    };
}
