import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IConsumptionModel } from "./interfaces";

export class ConsumptionModel extends Model<ConsumptionModel> implements IConsumptionModel {
    public static TABLE_NAME = "consumption_energy_consumption";

    declare time_utc: string;
    declare value: number;
    declare addr: string;
    declare variable: string;
    declare type: string;
    declare meter: string;

    public static attributeModel: ModelAttributes<ConsumptionModel> = {
        time_utc: {
            primaryKey: true,
            type: DataTypes.DATE,
        },
        value: DataTypes.NUMBER({
            precision: 30,
            scale: 15,
        }),
        addr: {
            primaryKey: true,
            type: DataTypes.STRING(255),
        },
        variable: {
            primaryKey: true,
            type: DataTypes.STRING(255),
            field: "var",
        },
        type: DataTypes.STRING(255),
        commodity: DataTypes.STRING(255),
        unit: DataTypes.STRING(255),
        meter: {
            primaryKey: true,
            type: DataTypes.STRING(255),
        },
    };

    public static arrayJsonSchema: JSONSchemaType<IConsumptionModel[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                time_utc: { type: "string" },
                value: { type: "number" },
                addr: { type: "string" },
                variable: { type: "string" },
                type: { type: "string" },
                meter: { type: "string" },
            },
            required: ["time_utc", "value", "addr", "variable", "type", "meter"],
        },
    };
}
