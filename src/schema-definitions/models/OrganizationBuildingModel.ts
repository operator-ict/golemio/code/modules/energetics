import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IOrganizationBuildingsModel } from "#sch/models/interfaces/IOrganizationBuildingsModel";

export class OrganizationBuildingModel extends Model<OrganizationBuildingModel> implements IOrganizationBuildingsModel {
    public static TABLE_NAME = "organizations_buildings";

    declare organization_id: number;
    declare building_id: number;

    public static attributeModel: ModelAttributes<OrganizationBuildingModel> = {
        organization_id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        building_id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
            allowNull: false,
        },
    };

    public static arrayJsonSchema: JSONSchemaType<IOrganizationBuildingsModel[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                organization_id: { type: "number" },
                building_id: { type: "number" },
            },
            additionalProperties: false,
            required: ["organization_id", "building_id"],
        },
    };
}
