import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBuildingsModel } from "./interfaces";

export class BuildingsModel extends Model<BuildingsModel> implements IBuildingsModel {
    public static TABLE_NAME = "consumption_energy_buildings";

    declare id: number;
    declare building_name: string;
    declare description: string;
    declare building_address_code: string;
    declare building_label: string;
    declare current_note: string;
    declare main_use: string;
    declare secondary_use: string;
    declare year_of_construction: string;
    declare method_of_protection: string;
    declare built_up_area: string;
    declare heated_bulding_volume: string;
    declare students_count: string;
    declare employees_count: string;
    declare classrooms_count: string;
    declare beds_count: string;
    declare eno_id: string;
    declare csu_code: string;
    declare ku_code: string;
    declare allotment_number: string;
    declare registration_unit: string;
    declare gas_consumption_normatives: string;
    declare heat_consumption_normatives: string;
    declare water_consumption_normatives: string;
    declare electricity_consumption_normatives_per_person: string;
    declare electricity_consumption_normatives: string;
    declare energetic_management: string;
    declare opening_hours: string;
    declare weekend_opening_hours: string;
    declare latitude: string;
    declare longitude: string;
    declare address_street: string;
    declare address_house_number: string;
    declare address_city: string;
    declare address_country: string;
    declare address_mail: string;
    declare address_phone: string;
    declare address_web_address: string;
    declare penb_penbnumber: string;
    declare penb_issue_date: string;
    declare penb_total_building_envelope_area: string;
    declare penb_volume_factor_of_avshape: string;
    declare penb_total_energy_reference_area: string;
    declare penb_total_provided_energy: string;
    declare penb_total_provided_energy_category: string;
    declare penb_primary_non_renewable_energy: string;
    declare penb_primary_non_renewable_energy_category: string;
    declare penb_building_envelope: string;
    declare penb_building_envelope_category: string;
    declare penb_heating: string;
    declare penb_heating_category: string;
    declare penb_cooling: string;
    declare penb_cooling_category: string;
    declare penb_ventilation: string;
    declare penb_ventilation_category: string;
    declare penb_humidity_adjustment: string;
    declare penb_humidity_adjustment_category: string;
    declare penb_warm_water: string;
    declare penb_warm_water_category: string;
    declare penb_lighting: string;
    declare penb_lighting_category: string;
    declare energy_audits_energy_audit: string;
    declare energy_audits_earegistration_number: string;
    declare energy_audits_created_at: string;
    declare building_envelope_side_wall_prevailing_construction: string;
    declare building_envelope_side_wall_area: string;
    declare building_envelope_side_wall_heat_insulation: string;
    declare building_envelope_side_wall_year_of_adjustment: string;
    declare building_envelope_side_wall_technical_condition: string;
    declare building_envelope_filling_of_hole_construction: string;
    declare building_envelope_filling_of_hole_area: string;
    declare building_envelope_filling_of_hole_year_of_adjustment: string;
    declare building_envelope_filling_of_hole_technical_condition: string;
    declare building_envelope_roof_construction: string;
    declare building_envelope_roof_area: string;
    declare building_envelope_roof_thermal_insulation: string;
    declare building_envelope_roof_year_of_adjustment: string;
    declare building_envelope_roof_technical_condition: string;
    declare building_envelope_floor_of_the_lowest_heated_floor_construction: string;
    declare building_envelope_floor_of_the_lowest_heated_floor_area: string;
    declare floor_of_the_lowest_heated_floor_thermal_insulation: string;
    declare building_envelope_floor_of_the_lowest_heated_floor_year_of_adjustment: string;
    declare floor_of_the_lowest_heated_floor_technical_condition: string;
    declare fuel_and_energy_coal: string;
    declare fuel_and_energy_gas: string;
    declare fuel_and_energy_electricity: string;
    declare fuel_and_energy_czt: string;
    declare fuel_and_energy_oze: string;
    declare fuel_and_energy_other: string;
    declare technical_equipment_heating_main_source_of_heat: string;
    declare technical_equipment_heating_heat_percentage: string;
    declare technical_equipment_heating_secondary_source_of_heat: string;
    declare technical_equipment_heating_heating_system: string;
    declare technical_equipment_heating_year: string;
    declare technical_equipment_heating_technical_condition: string;
    declare technical_equipment_cooling_cooling_system: string;
    declare technical_equipment_cooling_cooling_area_percentage: string;
    declare technical_equipment_cooling_year: string;
    declare technical_equipment_cooling_technical_condition: string;
    declare technical_equipment_ventilation_ventilation: string;
    declare technical_equipment_ventilation_year: string;
    declare technical_equipment_ventilation_technical_condition: string;
    declare technical_equipment_humidity_adjustment_humidity_adjustment: string;
    declare technical_equipment_humidity_adjustment_year: string;
    declare technical_equipment_humidity_adjustment_technical_condition: string;
    declare technical_equipment_hot_water_predominant_way_of_heating_tv: string;
    declare technical_equipment_hot_water_hot_water_source: string;
    declare technical_equipment_hot_water_year: string;
    declare technical_equipment_hot_water_technical_condition: string;
    declare technical_equipment_lighting_lighting: string;
    declare technical_equipment_lighting_year: string;
    declare technical_equipment_lighting_technical_condition: string;
    declare technical_equipment_lighting_other_technological_elements: string;
    declare technical_equipment_lighting_measurement_method: string;
    declare oze_energy_production_solar_energy_photovoltaic: string;
    declare oze_energy_production_solar_energy_photothermal: string;
    declare oze_energy_production_integrated_turbines_wind_energy: string;
    declare oze_energy_production_heat_pump: string;
    declare waste_and_emissions_solid_waste_production: string;
    declare waste_and_emissions_tied_co2_emissions: string;
    declare waste_and_emissions_sox_emissions: string;
    declare waste_and_emissions_operating_co_2emissions: string;
    declare link: string;

    public static attributeModel: ModelAttributes<BuildingsModel> = {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        building_name: DataTypes.STRING(255),
        description: DataTypes.STRING(255),
        building_address_code: DataTypes.STRING(255),
        building_label: DataTypes.STRING(255),
        current_note: DataTypes.STRING(255),
        main_use: DataTypes.STRING(255),
        secondary_use: DataTypes.STRING(255),
        year_of_construction: DataTypes.STRING(255),
        method_of_protection: DataTypes.STRING(255),
        built_up_area: DataTypes.STRING(255),
        heated_bulding_volume: DataTypes.STRING(255),
        students_count: DataTypes.STRING(255),
        employees_count: DataTypes.STRING(255),
        classrooms_count: DataTypes.STRING(255),
        beds_count: DataTypes.STRING(255),
        eno_id: DataTypes.STRING(255),
        csu_code: DataTypes.STRING(255),
        ku_code: DataTypes.STRING(255),
        allotment_number: DataTypes.STRING(255),
        registration_unit: DataTypes.STRING(255),
        gas_consumption_normatives: DataTypes.STRING(255),
        heat_consumption_normatives: DataTypes.STRING(255),
        water_consumption_normatives: DataTypes.STRING(255),
        electricity_consumption_normatives_per_person: DataTypes.STRING(255),
        electricity_consumption_normatives: DataTypes.STRING(255),
        energetic_management: DataTypes.STRING(255),
        opening_hours: DataTypes.STRING(255),
        weekend_opening_hours: DataTypes.STRING(255),
        latitude: DataTypes.STRING(255),
        longitude: DataTypes.STRING(255),
        address_street: DataTypes.STRING(255),
        address_house_number: DataTypes.STRING(255),
        address_city: DataTypes.STRING(255),
        address_country: DataTypes.STRING(255),
        address_mail: DataTypes.STRING(255),
        address_phone: DataTypes.STRING(255),
        address_web_address: DataTypes.STRING(255),
        penb_penbnumber: DataTypes.STRING(255),
        penb_issue_date: DataTypes.STRING(255),
        penb_total_building_envelope_area: DataTypes.STRING(255),
        penb_volume_factor_of_avshape: DataTypes.STRING(255),
        penb_total_energy_reference_area: DataTypes.STRING(255),
        penb_total_provided_energy: DataTypes.STRING(255),
        penb_total_provided_energy_category: DataTypes.STRING(255),
        penb_primary_non_renewable_energy: DataTypes.STRING(255),
        penb_primary_non_renewable_energy_category: DataTypes.STRING(255),
        penb_building_envelope: DataTypes.STRING(255),
        penb_building_envelope_category: DataTypes.STRING(255),
        penb_heating: DataTypes.STRING(255),
        penb_heating_category: DataTypes.STRING(255),
        penb_cooling: DataTypes.STRING(255),
        penb_cooling_category: DataTypes.STRING(255),
        penb_ventilation: DataTypes.STRING(255),
        penb_ventilation_category: DataTypes.STRING(255),
        penb_humidity_adjustment: DataTypes.STRING(255),
        penb_humidity_adjustment_category: DataTypes.STRING(255),
        penb_warm_water: DataTypes.STRING(255),
        penb_warm_water_category: DataTypes.STRING(255),
        penb_lighting: DataTypes.STRING(255),
        penb_lighting_category: DataTypes.STRING(255),
        energy_audits_energy_audit: DataTypes.STRING(255),
        energy_audits_earegistration_number: DataTypes.STRING(255),
        energy_audits_created_at: DataTypes.STRING(255),
        building_envelope_side_wall_prevailing_construction: DataTypes.STRING(255),
        building_envelope_side_wall_area: DataTypes.STRING(255),
        building_envelope_side_wall_heat_insulation: DataTypes.STRING(255),
        building_envelope_side_wall_year_of_adjustment: DataTypes.STRING(255),
        building_envelope_side_wall_technical_condition: DataTypes.STRING(255),
        building_envelope_filling_of_hole_construction: DataTypes.STRING(255),
        building_envelope_filling_of_hole_area: DataTypes.STRING(255),
        building_envelope_filling_of_hole_year_of_adjustment: DataTypes.STRING(255),
        building_envelope_filling_of_hole_technical_condition: DataTypes.STRING(255),
        building_envelope_roof_construction: DataTypes.STRING(255),
        building_envelope_roof_area: DataTypes.STRING(255),
        building_envelope_roof_thermal_insulation: DataTypes.STRING(255),
        building_envelope_roof_year_of_adjustment: DataTypes.STRING(255),
        building_envelope_roof_technical_condition: DataTypes.STRING(255),
        building_envelope_floor_of_the_lowest_heated_floor_construction: DataTypes.STRING(255),
        building_envelope_floor_of_the_lowest_heated_floor_area: DataTypes.STRING(255),
        floor_of_the_lowest_heated_floor_thermal_insulation: DataTypes.STRING(255),
        building_envelope_floor_of_the_lowest_heated_floor_year_of_adjustment: DataTypes.STRING(255),
        floor_of_the_lowest_heated_floor_technical_condition: DataTypes.STRING(255),
        fuel_and_energy_coal: DataTypes.STRING(255),
        fuel_and_energy_gas: DataTypes.STRING(255),
        fuel_and_energy_electricity: DataTypes.STRING(255),
        fuel_and_energy_czt: DataTypes.STRING(255),
        fuel_and_energy_oze: DataTypes.STRING(255),
        fuel_and_energy_other: DataTypes.STRING(255),
        technical_equipment_heating_main_source_of_heat: DataTypes.STRING(255),
        technical_equipment_heating_heat_percentage: DataTypes.STRING(255),
        technical_equipment_heating_secondary_source_of_heat: DataTypes.STRING(255),
        technical_equipment_heating_heating_system: DataTypes.STRING(255),
        technical_equipment_heating_year: DataTypes.STRING(255),
        technical_equipment_heating_technical_condition: DataTypes.STRING(255),
        technical_equipment_cooling_cooling_system: DataTypes.STRING(255),
        technical_equipment_cooling_cooling_area_percentage: DataTypes.STRING(255),
        technical_equipment_cooling_year: DataTypes.STRING(255),
        technical_equipment_cooling_technical_condition: DataTypes.STRING(255),
        technical_equipment_ventilation_ventilation: DataTypes.STRING(255),
        technical_equipment_ventilation_year: DataTypes.STRING(255),
        technical_equipment_ventilation_technical_condition: DataTypes.STRING(255),
        technical_equipment_humidity_adjustment_humidity_adjustment: DataTypes.STRING(255),
        technical_equipment_humidity_adjustment_year: DataTypes.STRING(255),
        technical_equipment_humidity_adjustment_technical_condition: DataTypes.STRING(255),
        technical_equipment_hot_water_predominant_way_of_heating_tv: DataTypes.STRING(255),
        technical_equipment_hot_water_hot_water_source: DataTypes.STRING(255),
        technical_equipment_hot_water_year: DataTypes.STRING(255),
        technical_equipment_hot_water_technical_condition: DataTypes.STRING(255),
        technical_equipment_lighting_lighting: DataTypes.STRING(255),
        technical_equipment_lighting_year: DataTypes.STRING(255),
        technical_equipment_lighting_technical_condition: DataTypes.STRING(255),
        technical_equipment_lighting_other_technological_elements: DataTypes.STRING(255),
        technical_equipment_lighting_measurement_method: DataTypes.STRING(255),
        oze_energy_production_solar_energy_photovoltaic: DataTypes.STRING(255),
        oze_energy_production_solar_energy_photothermal: DataTypes.STRING(255),
        oze_energy_production_integrated_turbines_wind_energy: DataTypes.STRING(255),
        oze_energy_production_heat_pump: DataTypes.STRING(255),
        waste_and_emissions_solid_waste_production: DataTypes.STRING(255),
        waste_and_emissions_tied_co2_emissions: DataTypes.STRING(255),
        waste_and_emissions_sox_emissions: DataTypes.STRING(255),
        waste_and_emissions_operating_co_2emissions: DataTypes.STRING(255),
        link: DataTypes.STRING(255),
    };

    public static arrayJsonSchema: JSONSchemaType<IBuildingsModel[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                building_name: { type: "string" },
                description: { type: "string" },
                building_address_code: { type: "string" },
                building_label: { type: "string" },
                current_note: { type: "string" },
                main_use: { type: "string" },
                secondary_use: { type: "string" },
                year_of_construction: { type: "string" },
                method_of_protection: { type: "string" },
                built_up_area: { type: "string" },
                heated_bulding_volume: { type: "string" },
                students_count: { type: "string" },
                employees_count: { type: "string" },
                classrooms_count: { type: "string" },
                beds_count: { type: "string" },
                eno_id: { type: "string" },
                csu_code: { type: "string" },
                ku_code: { type: "string" },
                allotment_number: { type: "string" },
                registration_unit: { type: "string" },
                gas_consumption_normatives: { type: "string" },
                heat_consumption_normatives: { type: "string" },
                water_consumption_normatives: { type: "string" },
                electricity_consumption_normatives_per_person: { type: "string" },
                electricity_consumption_normatives: { type: "string" },
                energetic_management: { type: "string" },
                opening_hours: { type: "string" },
                weekend_opening_hours: { type: "string" },
                latitude: { type: "string" },
                longitude: { type: "string" },
                address_street: { type: "string" },
                address_house_number: { type: "string" },
                address_city: { type: "string" },
                address_country: { type: "string" },
                address_mail: { type: "string" },
                address_phone: { type: "string" },
                address_web_address: { type: "string" },
                penb_penbnumber: { type: "string" },
                penb_issue_date: { type: "string" },
                penb_total_building_envelope_area: { type: "string" },
                penb_volume_factor_of_avshape: { type: "string" },
                penb_total_energy_reference_area: { type: "string" },
                penb_total_provided_energy: { type: "string" },
                penb_total_provided_energy_category: { type: "string" },
                penb_primary_non_renewable_energy: { type: "string" },
                penb_primary_non_renewable_energy_category: { type: "string" },
                penb_building_envelope: { type: "string" },
                penb_building_envelope_category: { type: "string" },
                penb_heating: { type: "string" },
                penb_heating_category: { type: "string" },
                penb_cooling: { type: "string" },
                penb_cooling_category: { type: "string" },
                penb_ventilation: { type: "string" },
                penb_ventilation_category: { type: "string" },
                penb_humidity_adjustment: { type: "string" },
                penb_humidity_adjustment_category: { type: "string" },
                penb_warm_water: { type: "string" },
                penb_warm_water_category: { type: "string" },
                penb_lighting: { type: "string" },
                penb_lighting_category: { type: "string" },
                energy_audits_energy_audit: { type: "string" },
                energy_audits_earegistration_number: { type: "string" },
                energy_audits_created_at: { type: "string" },
                building_envelope_side_wall_prevailing_construction: { type: "string" },
                building_envelope_side_wall_area: { type: "string" },
                building_envelope_side_wall_heat_insulation: { type: "string" },
                building_envelope_side_wall_year_of_adjustment: { type: "string" },
                building_envelope_side_wall_technical_condition: { type: "string" },
                building_envelope_filling_of_hole_construction: { type: "string" },
                building_envelope_filling_of_hole_area: { type: "string" },
                building_envelope_filling_of_hole_year_of_adjustment: { type: "string" },
                building_envelope_filling_of_hole_technical_condition: { type: "string" },
                building_envelope_roof_construction: { type: "string" },
                building_envelope_roof_area: { type: "string" },
                building_envelope_roof_thermal_insulation: { type: "string" },
                building_envelope_roof_year_of_adjustment: { type: "string" },
                building_envelope_roof_technical_condition: { type: "string" },
                building_envelope_floor_of_the_lowest_heated_floor_construction: { type: "string" },
                building_envelope_floor_of_the_lowest_heated_floor_area: { type: "string" },
                floor_of_the_lowest_heated_floor_thermal_insulation: { type: "string" },
                building_envelope_floor_of_the_lowest_heated_floor_year_of_adjustment: { type: "string" },
                floor_of_the_lowest_heated_floor_technical_condition: { type: "string" },
                fuel_and_energy_coal: { type: "string" },
                fuel_and_energy_gas: { type: "string" },
                fuel_and_energy_electricity: { type: "string" },
                fuel_and_energy_czt: { type: "string" },
                fuel_and_energy_oze: { type: "string" },
                fuel_and_energy_other: { type: "string" },
                technical_equipment_heating_main_source_of_heat: { type: "string" },
                technical_equipment_heating_heat_percentage: { type: "string" },
                technical_equipment_heating_secondary_source_of_heat: { type: "string" },
                technical_equipment_heating_heating_system: { type: "string" },
                technical_equipment_heating_year: { type: "string" },
                technical_equipment_heating_technical_condition: { type: "string" },
                technical_equipment_cooling_cooling_system: { type: "string" },
                technical_equipment_cooling_cooling_area_percentage: { type: "string" },
                technical_equipment_cooling_year: { type: "string" },
                technical_equipment_cooling_technical_condition: { type: "string" },
                technical_equipment_ventilation_ventilation: { type: "string" },
                technical_equipment_ventilation_year: { type: "string" },
                technical_equipment_ventilation_technical_condition: { type: "string" },
                technical_equipment_humidity_adjustment_humidity_adjustment: { type: "string" },
                technical_equipment_humidity_adjustment_year: { type: "string" },
                technical_equipment_humidity_adjustment_technical_condition: { type: "string" },
                technical_equipment_hot_water_predominant_way_of_heating_tv: { type: "string" },
                technical_equipment_hot_water_hot_water_source: { type: "string" },
                technical_equipment_hot_water_year: { type: "string" },
                technical_equipment_hot_water_technical_condition: { type: "string" },
                technical_equipment_lighting_lighting: { type: "string" },
                technical_equipment_lighting_year: { type: "string" },
                technical_equipment_lighting_technical_condition: { type: "string" },
                technical_equipment_lighting_other_technological_elements: { type: "string" },
                technical_equipment_lighting_measurement_method: { type: "string" },
                oze_energy_production_solar_energy_photovoltaic: { type: "string" },
                oze_energy_production_solar_energy_photothermal: { type: "string" },
                oze_energy_production_integrated_turbines_wind_energy: { type: "string" },
                oze_energy_production_heat_pump: { type: "string" },
                waste_and_emissions_solid_waste_production: { type: "string" },
                waste_and_emissions_tied_co2_emissions: { type: "string" },
                waste_and_emissions_sox_emissions: { type: "string" },
                waste_and_emissions_operating_co_2emissions: { type: "string" },
                link: { type: "string" },
            },
            required: [
                "id",
                "building_name",
                "description",
                "building_address_code",
                "building_label",
                "current_note",
                "main_use",
                "secondary_use",
                "year_of_construction",
                "method_of_protection",
                "built_up_area",
                "heated_bulding_volume",
                "students_count",
                "employees_count",
                "classrooms_count",
                "beds_count",
                "eno_id",
                "csu_code",
                "ku_code",
                "allotment_number",
                "registration_unit",
                "gas_consumption_normatives",
                "heat_consumption_normatives",
                "water_consumption_normatives",
                "electricity_consumption_normatives_per_person",
                "electricity_consumption_normatives",
                "energetic_management",
                "opening_hours",
                "weekend_opening_hours",
                "latitude",
                "longitude",
                "address_street",
                "address_house_number",
                "address_city",
                "address_country",
                "address_mail",
                "address_phone",
                "address_web_address",
                "penb_penbnumber",
                "penb_issue_date",
                "penb_total_building_envelope_area",
                "penb_volume_factor_of_avshape",
                "penb_total_energy_reference_area",
                "penb_total_provided_energy",
                "penb_total_provided_energy_category",
                "penb_primary_non_renewable_energy",
                "penb_primary_non_renewable_energy_category",
                "penb_building_envelope",
                "penb_building_envelope_category",
                "penb_heating",
                "penb_heating_category",
                "penb_cooling",
                "penb_cooling_category",
                "penb_ventilation",
                "penb_ventilation_category",
                "penb_humidity_adjustment",
                "penb_humidity_adjustment_category",
                "penb_warm_water",
                "penb_warm_water_category",
                "penb_lighting",
                "penb_lighting_category",
                "energy_audits_energy_audit",
                "energy_audits_earegistration_number",
                "energy_audits_created_at",
                "building_envelope_side_wall_prevailing_construction",
                "building_envelope_side_wall_area",
                "building_envelope_side_wall_heat_insulation",
                "building_envelope_side_wall_year_of_adjustment",
                "building_envelope_side_wall_technical_condition",
                "building_envelope_filling_of_hole_construction",
                "building_envelope_filling_of_hole_area",
                "building_envelope_filling_of_hole_year_of_adjustment",
                "building_envelope_filling_of_hole_technical_condition",
                "building_envelope_roof_construction",
                "building_envelope_roof_area",
                "building_envelope_roof_thermal_insulation",
                "building_envelope_roof_year_of_adjustment",
                "building_envelope_roof_technical_condition",
                "building_envelope_floor_of_the_lowest_heated_floor_construction",
                "building_envelope_floor_of_the_lowest_heated_floor_area",
                "floor_of_the_lowest_heated_floor_thermal_insulation",
                "building_envelope_floor_of_the_lowest_heated_floor_year_of_adjustment",
                "floor_of_the_lowest_heated_floor_technical_condition",
                "fuel_and_energy_coal",
                "fuel_and_energy_gas",
                "fuel_and_energy_electricity",
                "fuel_and_energy_czt",
                "fuel_and_energy_oze",
                "fuel_and_energy_other",
                "technical_equipment_heating_main_source_of_heat",
                "technical_equipment_heating_heat_percentage",
                "technical_equipment_heating_secondary_source_of_heat",
                "technical_equipment_heating_heating_system",
                "technical_equipment_heating_year",
                "technical_equipment_heating_technical_condition",
                "technical_equipment_cooling_cooling_system",
                "technical_equipment_cooling_cooling_area_percentage",
                "technical_equipment_cooling_year",
                "technical_equipment_cooling_technical_condition",
                "technical_equipment_ventilation_ventilation",
                "technical_equipment_ventilation_year",
                "technical_equipment_ventilation_technical_condition",
                "technical_equipment_humidity_adjustment_humidity_adjustment",
                "technical_equipment_humidity_adjustment_year",
                "technical_equipment_humidity_adjustment_technical_condition",
                "technical_equipment_hot_water_predominant_way_of_heating_tv",
                "technical_equipment_hot_water_hot_water_source",
                "technical_equipment_hot_water_year",
                "technical_equipment_hot_water_technical_condition",
                "technical_equipment_lighting_lighting",
                "technical_equipment_lighting_year",
                "technical_equipment_lighting_technical_condition",
                "technical_equipment_lighting_other_technological_elements",
                "technical_equipment_lighting_measurement_method",
                "oze_energy_production_solar_energy_photovoltaic",
                "oze_energy_production_solar_energy_photothermal",
                "oze_energy_production_integrated_turbines_wind_energy",
                "oze_energy_production_heat_pump",
                "waste_and_emissions_solid_waste_production",
                "waste_and_emissions_tied_co2_emissions",
                "waste_and_emissions_sox_emissions",
                "waste_and_emissions_operating_co_2emissions",
                "link",
            ],
        },
    };
}
