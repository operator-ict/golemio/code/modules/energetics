import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IAccessLimitation } from "./interfaces/IAccessLimitation";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export class AccessLimitationModel extends Model<AccessLimitationModel> implements IAccessLimitation {
    public static TABLE_NAME = "access_limitation";

    declare group_name: string;
    declare organization_ids: number[];

    public static attributeModel: ModelAttributes<AccessLimitationModel> = {
        group_name: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        organization_ids: {
            type: DataTypes.ARRAY(DataTypes.INTEGER),
        },
    };

    public static jsonSchema: JSONSchemaType<IAccessLimitation[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                group_name: { type: "string" },
                organization_ids: {
                    type: "array",
                    items: { type: "integer" },
                },
            },
            required: ["group_name", "organization_ids"],
        },
    };
}
