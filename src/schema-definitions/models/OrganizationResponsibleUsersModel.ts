import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IOrganizationResponsibleUsersModel } from "#sch/models/interfaces/IOrganizationResponsibleUsersModel";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export class OrganizationResponsibleUsersModel
    extends Model<OrganizationResponsibleUsersModel>
    implements IOrganizationResponsibleUsersModel
{
    public static TABLE_NAME = "organization_responsible_users";

    declare id: number;
    declare first_name: string | null;
    declare last_name: string | null;
    declare position: string | null;
    declare phone: string | null;
    declare mail: string | null;
    declare company: string | null;
    declare organization_id: number;

    public static attributeModel: ModelAttributes<OrganizationResponsibleUsersModel> = {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        first_name: { type: DataTypes.STRING(100) },
        last_name: { type: DataTypes.STRING(100) },
        position: { type: DataTypes.STRING(100) },
        phone: { type: DataTypes.STRING(50) },
        mail: { type: DataTypes.STRING(50) },
        company: { type: DataTypes.STRING(100) },
        organization_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
    };

    public static arrayJsonSchema: JSONSchemaType<IOrganizationResponsibleUsersModel[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "number" },
                first_name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                last_name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                position: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                phone: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                mail: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                company: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                organization_id: { type: "number" },
            },
            additionalProperties: false,
            required: ["id", "organization_id"],
        },
    };
}
