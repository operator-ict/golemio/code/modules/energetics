import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { CommodityProviderType } from "./helpers/CommodityProviderTypeEnum";
import { StaticDataResourceType } from "./helpers/StaticDataResourceTypeEnum";
import { IStaticDataOutputDto, ResourceTypeDict } from "./interfaces/IStaticDataOutputDto";

export class StaticMeterSettingsModel extends Model<IStaticDataOutputDto> implements IStaticDataOutputDto {
    public static tableName = "commodity_static_meter_settings";

    declare provider: CommodityProviderType;
    declare resource_type: StaticDataResourceType;
    declare data: ResourceTypeDict[StaticDataResourceType];

    public static attributeModel: ModelAttributes<StaticMeterSettingsModel, IStaticDataOutputDto> = {
        provider: {
            type: DataTypes.STRING(30),
            primaryKey: true,
        },
        resource_type: {
            type: DataTypes.STRING(30),
            primaryKey: true,
        },
        data: {
            type: DataTypes.JSON,
            allowNull: false,
        },
    };

    public static jsonSchema: JSONSchemaType<IStaticDataOutputDto> = {
        type: "object",
        properties: {
            provider: { type: "string" },
            resource_type: { type: "string" },
            data: {
                type: "array",
                items: { type: "object", required: [] },
            },
        },
        required: ["provider", "resource_type", "data"],
    };
}
