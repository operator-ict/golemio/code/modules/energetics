export enum CommodityProviderType {
    CemApiVeoliaWater = "cem_api_veolia_water",
    CemApiVeoliaElectro = "cem_api_veolia_electro",
    PpasAveV = "ppas_ave_api_v",
    PpasAveM = "ppas_ave_api_m",
    PpasAveA = "ppas_ave_api_a",
    PpasAveC = "ppas_ave_api_c",
    PpasAveMO = "ppas_ave_api_mo",
    PreOptions = "pre_electro_ftp",
    PtasOptions = "ptas_hot_water",
    PreInputOptions = "pre_electro_input",
}
