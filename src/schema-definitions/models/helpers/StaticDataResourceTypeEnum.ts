export enum StaticDataResourceType {
    MeasurementOptions = "MeasurementOptions",
    SelectedMeters = "SelectedMeters",
}
