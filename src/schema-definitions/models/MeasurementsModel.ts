import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IMeasurementDto } from "./interfaces/IMeasurementDto";
import { MEASUREMENT_TYPES, MEASUREMENT_VARIABLES } from "#sch/models/constants/MeasurementConstants";

export class MeasurementModel extends Model<MeasurementModel, IMeasurementDto> implements IMeasurementDto {
    public static tableName = "consumption_energy_measurements";

    declare timestamp: Date;
    declare value: number;
    declare addr: string;
    declare variable: string;
    declare type: string;
    declare meter: string;
    declare source: string;
    declare measurement_category: string;

    public static attributeModel: ModelAttributes<MeasurementModel, IMeasurementDto> = {
        timestamp: {
            primaryKey: true,
            type: DataTypes.DATE,
        },
        value: DataTypes.NUMBER({
            precision: 30,
            scale: 15,
        }),
        addr: {
            primaryKey: true,
            type: DataTypes.STRING(255),
        },
        variable: {
            primaryKey: true,
            type: DataTypes.STRING(255),
            field: "var",
        },
        type: DataTypes.STRING(255),
        meter: {
            type: DataTypes.STRING(255),
        },
        source: DataTypes.STRING(20),
        measurement_category: DataTypes.STRING(50),
    };

    public static attributesToUpdate = Object.keys(MeasurementModel.attributeModel).concat("updated_at");

    public static arrayJsonSchema: JSONSchemaType<IMeasurementDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                timestamp: {
                    type: "object",
                    required: ["toISOString"],
                },
                value: { type: "number" },
                addr: { type: "string" },
                variable: { type: "string", enum: MEASUREMENT_VARIABLES },
                type: { type: "string", enum: MEASUREMENT_TYPES },
                meter: { type: "string" },
                source: { type: "string" },
                measurement_category: { type: "string" },
            },
            required: ["timestamp", "value", "addr", "variable", "type", "meter", "source", "measurement_category"],
        },
    };
}
