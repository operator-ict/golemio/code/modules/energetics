import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IDevicesModel } from "./interfaces";

export class DevicesModel extends Model<DevicesModel> implements IDevicesModel {
    public static TABLE_NAME = "consumption_energy_devices";

    declare id: number;
    declare addr: string;
    declare description: string;
    declare meter_number: string;
    declare meter_index: string;
    declare location_number: string;
    declare location_description: string;
    declare include_in_evaluation: string;
    declare meter_type: string;
    declare category: string;
    declare unit: string;
    declare replaced_meter_id: string;
    declare deleted: string;
    declare building_id: number;

    public static attributeModel: ModelAttributes<DevicesModel> = {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        addr: DataTypes.STRING(255),
        description: DataTypes.STRING(255),
        meter_number: DataTypes.STRING(255),
        meter_index: DataTypes.STRING(255),
        location_number: DataTypes.STRING(255),
        location_description: DataTypes.STRING(255),
        include_in_evaluation: DataTypes.STRING(255),
        meter_type: DataTypes.STRING(255),
        category: DataTypes.STRING(255),
        unit: DataTypes.STRING(255),
        replaced_meter_id: DataTypes.STRING(255),
        deleted: DataTypes.STRING(255),
        building_id: DataTypes.INTEGER,
    };

    public static arrayJsonSchema: JSONSchemaType<IDevicesModel[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                addr: { type: "string" },
                description: { type: "string" },
                meter_number: { type: "string" },
                meter_index: { type: "string" },
                location_number: { type: "string" },
                location_description: { type: "string" },
                include_in_evaluation: { type: "string" },
                meter_type: { type: "string" },
                category: { type: "string" },
                unit: { type: "string" },
                replaced_meter_id: { type: "string" },
                deleted: { type: "string" },
                building_id: { type: "integer" },
            },
            required: [
                "id",
                "addr",
                "description",
                "meter_number",
                "meter_index",
                "location_number",
                "location_description",
                "include_in_evaluation",
                "meter_type",
                "category",
                "unit",
                "replaced_meter_id",
                "deleted",
                "building_id",
            ],
        },
    };
}
