import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBuildingsPrimary } from "./interfaces/IPrimaryBuildings";

export class BuildingsPrimaryModel extends Model<IBuildingsPrimary> implements IBuildingsPrimary {
    public static modelName = "BuildingsPrimaryModelData";
    public static tableName = "consumption_energy_buildings_primary";

    declare id: number;
    declare building_name: string;
    declare description: string;
    declare building_address_code: string;
    declare building_label: string;
    declare main_use: string;
    declare secondary_use: string;
    declare year_of_construction: number;

    public static attributeModel: ModelAttributes<BuildingsPrimaryModel> = {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        building_name: DataTypes.STRING(100),
        description: DataTypes.STRING(100),
        building_address_code: DataTypes.STRING(100),
        building_label: DataTypes.STRING(100),
        main_use: DataTypes.STRING(100),
        secondary_use: DataTypes.STRING(100),
        year_of_construction: DataTypes.INTEGER,
    };

    public static arrayJsonSchema: JSONSchemaType<IBuildingsPrimary[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                building_name: { type: "string" },
                description: { type: "string" },
                main_use: { type: "string" },
                building_address_code: { type: "string" },
                building_label: { type: "string" },
                secondary_use: { type: "string" },
                year_of_construction: { type: "integer" },
            },
            required: [
                "id",
                "building_name",
                "description",
                "building_address_code",
                "building_label",
                "main_use",
                "secondary_use",
                "year_of_construction",
            ],
        },
    };
}
