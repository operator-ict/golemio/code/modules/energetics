export interface IPreJsonInput {
    stored_month: {
        companies: {
            [key: string]: {
                EAN_list: {
                    [ean: string]: {
                        daily_data_Wh_per_EAN: {
                            T1_Wh_raw_data: Array<Array<[date: string, value: number]>>;
                        };
                    };
                };
            };
        };
    };
}
