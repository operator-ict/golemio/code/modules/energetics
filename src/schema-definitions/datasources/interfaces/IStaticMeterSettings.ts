import { MeasurementVariable } from "../helpers/MeasurementVariableEnum";

export interface IStaticMeterSettings {
    [providerOptionKey: string]: {
        MeasurementOptions: IMeasurementOption[];
    };
}

export interface IMeasurementOption {
    MeterSerialNumber: string;
    Name: string;
    Addr: string;
    Meter: string;
    Type: string;
    Var: MeasurementVariable | string;
}

export interface ISelectedMeters {
    [placeId: string]: string;
}
