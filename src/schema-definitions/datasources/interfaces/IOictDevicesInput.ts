export interface IOictDevicesInput {
    id: number;
    addr: string | null;
    description: string | null;
    meterNumber: string | null;
    meterIndex: string | number | null;
    locationNumber: string | null;
    locationDescription: string | null;
    includeInEvaluation: string | null;
    meterType: string | null;
    category: string | null;
    unit: string | null;
    replacedMeterId: string | number | null;
    deleted: string | null;
    buildingId: number;
}
