export interface IPtasWebScrapingInput {
    placeId: string;
    measurements: IPtasMeasurement[];
}

export interface IPtasMeasurement {
    "Datum odečtu": string;
    "Čas odečtu": string;
    "Výrobní číslo": string;
    "Druh registru": string;
    Odečet: string;
    "MJ odečtu": string;
    "MJ zúčt.": string;
    "OM závislé": string;
    "Typ odečtu": string;
    "Status odečtu": string;
}
