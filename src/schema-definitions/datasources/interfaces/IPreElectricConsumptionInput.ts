export interface IPreElectricConsumptionInput {
    EAN: string;
    ELM: string;
    "Čas měření": string;
    "Stav [kWh]": string;
}
