export interface IOictBuildingsInput {
    id: number;
    name: string | null;
    description: string | null;
    buildingAddressCode: string | null;
    label: string | null;
    currentNote: string | null;
    mainUse: string | null;
    secondaryUse: string | null;
    yearOfConstruction: string | null;
    methodOfProtection: string | null;
    builtUpArea: string | null;
    heatedBuldingVolume: string | null;
    studentsCount: string | null;
    employeesCount: string | null;
    classroomsCount: string | null;
    bedsCount: string | null;
    enoId: string | null;
    csuCode: string | null;
    kuCode: string | null;
    allotmentNumber: string | null;
    registrationUnit: string | null;
    gasConsumptionNormatives: string | null;
    heatConsumptionNormatives: string | null;
    waterConsumptionNormatives: string | null;
    electricityConsumptionNormativesPerPerson: string | null;
    electricityConsumptionNormatives: string | null;
    energeticManagement: string | null;
    openingHours: string | null;
    weekendOpeningHours: string | null;
    latitude: string | null;
    longitude: string | null;
    link: string | null;
    address: IOictBuildingAddress;
    penb: IOictBuildingPenb;
    energyAudits: IOictBuildingEnergyAudits;
    buildingEnvelope: IOictBuildingEnvelope;
    fuelAndEnergy: IOictBuildingFuelAndEnergy;
    technicalEquipment: IOictBuildingTechnicalEquipment;
    ozeEnergyProduction: IOictBuildingOzeEnergyProduction;
    wasteAndEmissions: IOictBuildingWasteAndEmissions;
}

interface IOictBuildingAddress {
    street: string | null;
    houseNumber: string | null;
    city: string | null;
    country: string | null;
    mail: string | null;
    phone: string | number | null;
    webAddress: string | null;
}

interface IOictBuildingPenb {
    penbNumber: string | number | boolean | null;
    issueDate: string | null;
    totalBuildingEnvelopeArea: string | null;
    volumeFactorOfAVShape: string | null;
    totalEnergyReferenceArea: string | null;
    totalProvidedEnergy: string | null;
    totalProvidedEnergyCategory: string | null;
    primaryNonRenewableEnergy: string | null;
    primaryNonRenewableEnergyCategory: string | null;
    buildingEnvelope: string | null;
    buildingEnvelopeCategory: string | null;
    heating: string | null;
    heatingCategory: string | null;
    cooling: string | null;
    coolingCategory: string | null;
    ventilation: string | null;
    ventilationCategory: string | null;
    humidityAdjustment: string | null;
    humidityAdjustmentCategory: string | null;
    warmWater: string | null;
    warmWaterCategory: string | null;
    lighting: string | null;
    lightingCategory: string | null;
}

interface IOictBuildingEnergyAudits {
    energyAudit: string | number | null;
    eaRegistrationNumber: string | number | null;
    createdAt: string | number | null;
}

interface IOictBuildingEnvelope {
    sideWall: {
        prevailingConstruction: string | null;
        area: string | null;
        heatInsulation: string | null;
        yearOfAdjustment: string | number | null;
        technicalCondition: string | number | null;
    };
    fillingOfHole: {
        construction: string | null;
        area: string | null;
        yearOfAdjustment: string | number | null;
        technicalCondition: string | number | null;
    };
    roof: {
        construction: string | null;
        area: string | null;
        thermalInsulation: string | null;
        yearOfAdjustment: string | number | null;
        technicalCondition: string | number | null;
    };
    floorOfTheLowestHeatedFloor: {
        construction: string | null;
        area: string | null;
        thermalInsulation: string | null;
        yearOfAdjustment: string | number | null;
        technicalCondition: string | number | null;
    };
}

interface IOictBuildingFuelAndEnergy {
    coal: string | null;
    gas: string | null;
    electricity: string | null;
    czt: string | null;
    oze: string | null;
    other: string | null;
}

interface IOictBuildingTechnicalEquipment {
    heating: {
        mainSourceOfHeat: string | null;
        heatPercentage: string | null;
        secondarySourceOfHeat: string | null;
        heatingSystem: string | null;
        year: string | number | null;
        technicalCondition: string | number | null;
    };
    cooling: {
        coolingSystem: string | null;
        coolingAreaPercentage: string | number | null;
        year: string | number | null;
        technicalCondition: string | number | null;
    };
    ventilation: {
        ventilation: string | null;
        year: string | number | null;
        technicalCondition: string | number | null;
    };
    humidityAdjustment: {
        humidityAdjustment: string | null;
        year: string | number | null;
        technicalCondition: string | number | null;
    };
    hotWater: {
        predominantWayOfHeatingTv: string | null;
        hotWaterSource: string | null;
        year: string | number | null;
        technicalCondition: string | number | null;
    };
    lighting: {
        lighting: string | null;
        year: string | number | null;
        technicalCondition: string | number | null;
        otherTechnologicalElements: string | number | null;
        measurementMethod: string | null;
    };
}

interface IOictBuildingOzeEnergyProduction {
    solarEnergyPhotovoltaic: string | number | null;
    solarEnergyPhotothermal: string | number | null;
    integratedTurbinesWindEnergy: string | number | null;
    heatPump: string | number | null;
}

interface IOictBuildingWasteAndEmissions {
    solidWasteProduction: string | number | null;
    tiedCo2Emissions: string | number | null;
    soxEmissions: string | number | null;
    operatingCo2Emissions: string | number | null;
}
