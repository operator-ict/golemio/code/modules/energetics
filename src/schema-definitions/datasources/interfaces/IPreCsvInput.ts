export interface IPreCsvInput {
    "Počátek intervalu": string;
    "Konec intervalu": string;
    [eanCinna: string]: string;
}
