export interface IOictConsumptionInput {
    time: string;
    value: string;
    addr: string | null;
    var: string | null;
    type: string | null;
    meter: string | null;
}
