import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IStaticMeterSettings } from "./interfaces/IStaticMeterSettings";

export const staticMeterSettingsJsonSchema: JSONSchemaType<IStaticMeterSettings> = {
    type: "object",
    additionalProperties: {
        type: "object",
        properties: {
            MeasurementOptions: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        MeterSerialNumber: { type: "string" },
                        Name: { type: "string" },
                        Addr: { type: "string" },
                        Meter: { type: "string" },
                        Type: { type: "string" },
                        Var: { type: "string" },
                    },
                    required: ["Name", "Addr", "Meter", "Type", "Var"],
                },
            },
        },
        required: ["MeasurementOptions"],
    },
    required: [],
};
