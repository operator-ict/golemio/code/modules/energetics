import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IOictDevicesInput } from "./interfaces";

export const oictDevicesJsonSchema: JSONSchemaType<IOictDevicesInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "integer" },
            addr: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            description: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            meterNumber: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            meterIndex: { oneOf: [{ type: "string" }, { type: "integer" }, { type: "null", nullable: true }] },
            locationNumber: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            locationDescription: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            includeInEvaluation: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            meterType: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            category: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            unit: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            replacedMeterId: { oneOf: [{ type: "string" }, { type: "number" }, { type: "null", nullable: true }] },
            deleted: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            buildingId: { type: "number" },
        },
        required: [
            "id",
            "addr",
            "description",
            "meterNumber",
            "meterIndex",
            "locationNumber",
            "locationDescription",
            "includeInEvaluation",
            "meterType",
            "category",
            "unit",
            "replacedMeterId",
            "deleted",
            "buildingId",
        ],
    },
};
