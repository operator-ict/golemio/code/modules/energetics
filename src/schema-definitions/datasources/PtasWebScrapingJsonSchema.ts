import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IPtasWebScrapingInput } from "./interfaces/PtasWebScrapingInput";

export const ptasWebScrapingJsonSchema: JSONSchemaType<IPtasWebScrapingInput> = {
    type: "object",
    properties: {
        placeId: { type: "string" },
        measurements: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    "Datum odečtu": { type: "string", pattern: "^\\d{1,2}.\\d{1,2}.\\d{4}$" },
                    "Čas odečtu": { type: "string" },
                    "Výrobní číslo": { type: "string" },
                    "Druh registru": { type: "string" },
                    Odečet: { type: "string" },
                    "MJ odečtu": { type: "string" },
                    "MJ zúčt.": { type: "string" },
                    "OM závislé": { type: "string" },
                    "Typ odečtu": { type: "string" },
                    "Status odečtu": { type: "string" },
                },
                required: [
                    "Datum odečtu",
                    "Čas odečtu",
                    "Druh registru",
                    "Výrobní číslo",
                    "Odečet",
                    "MJ odečtu",
                    "MJ zúčt.",
                    "OM závislé",
                    "Typ odečtu",
                    "Status odečtu",
                ],
            },
        },
    },
    required: ["placeId", "measurements"],
};
