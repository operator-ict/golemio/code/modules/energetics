import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IPreElectricConsumptionInput } from "./interfaces/IPreElectricConsumptionInput";

export const preElectricConsumptionJson: JSONSchemaType<IPreElectricConsumptionInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            EAN: { type: "string" },
            ELM: { type: "string" },
            "Čas měření": { type: "string" },
            "Stav [kWh]": { type: "string" },
        },
        required: ["EAN", "ELM", "Čas měření", "Stav [kWh]"],
    },
};
