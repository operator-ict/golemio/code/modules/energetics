import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IOictConsumptionInput } from "./interfaces";

export const oictConsumptionJsonSchema: JSONSchemaType<IOictConsumptionInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            time: { type: "string" },
            value: { type: "string" },
            addr: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            var: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            meter: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        },
        required: ["time", "value", "addr", "var", "type", "meter"],
    },
};
