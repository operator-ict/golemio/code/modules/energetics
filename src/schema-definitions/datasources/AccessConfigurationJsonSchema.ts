import { IAccessLimitation } from "#sch/models/interfaces/IAccessLimitation";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export const accessConfigurationJsonSchema: JSONSchemaType<IAccessLimitation[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            group_name: { type: "string" },
            organization_ids: {
                type: "array",
                items: { type: "integer" },
            },
        },
        required: ["group_name", "organization_ids"],
    },
};
