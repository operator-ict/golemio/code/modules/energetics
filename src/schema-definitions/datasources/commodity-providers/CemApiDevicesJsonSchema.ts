import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ICemApiDeviceDto } from "./interfaces/ICemApiDeviceDto";

export const cemApiDevicesJsonSchema: JSONSchemaType<ICemApiDeviceDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            me_id: { type: "number" },
            me_serial: { type: "string" },
        },
        required: ["me_id", "me_serial"],
    },
};
