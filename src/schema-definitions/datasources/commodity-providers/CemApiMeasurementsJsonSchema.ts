import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ICemApiMeasurementDto } from "./interfaces/ICemApiMeasurementDto";

export const cemApiMeasurementsJsonSchema: JSONSchemaType<ICemApiMeasurementDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            value: { type: "number" },
            timestamp: { type: "number" },
        },
        required: ["value", "timestamp"],
    },
};
