import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IAveApiPlaceDto } from "./interfaces/IAveApiPlaceDto";

export const aveApiPlacesJsonSchema: JSONSchemaType<IAveApiPlaceDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            Id: { type: "number" },
            Eic: { type: "string" },
            DeviceSerialNumber: { type: "string" },
        },
        required: ["Id", "Eic", "DeviceSerialNumber"],
    },
};
