import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ICemApiCounterDto } from "./interfaces/ICemApiCounterDto";

export const cemApiCountersJsonSchema: JSONSchemaType<ICemApiCounterDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            me_id: { type: "number" },
            var_id: { type: "number" },
            pot_id: { type: "number" },
        },
        required: ["me_id", "var_id", "pot_id"],
    },
};
