import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IAveApiDeviceDataDto } from "./interfaces/IAveApiDeviceDataDto";

export const aveApiDeviceDataJsonSchema: JSONSchemaType<IAveApiDeviceDataDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            StandardTime: { type: "string" },
            OperatingAmount: { type: "number" },
            ConvertDifference: { type: "number" },
        },
        required: ["StandardTime", "OperatingAmount", "ConvertDifference"],
    },
};
