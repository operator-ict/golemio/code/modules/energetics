export interface ICemApiMeasurementDto {
    value: number;
    timestamp: number;
}
