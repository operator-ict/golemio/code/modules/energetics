export interface IAveApiPlaceDto {
    Id: number;
    Eic: string;
    DeviceSerialNumber: string;
}
