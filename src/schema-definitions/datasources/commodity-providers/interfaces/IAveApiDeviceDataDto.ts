export interface IAveApiDeviceDataDto {
    StandardTime: string;
    OperatingAmount: number;
    ConvertDifference: number;
}
