export interface ICemApiDeviceDto {
    me_id: number;
    me_serial: string;
}
