import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IPreJsonInput } from "#sch/datasources/interfaces/IPreJsonInput";

export const preJsonInputSchema: JSONSchemaType<IPreJsonInput> = {
    type: "object",
    properties: {
        stored_month: {
            type: "object",
            properties: {
                companies: {
                    type: "object",
                    patternProperties: {
                        "^[0-9]+$": {
                            type: "object",
                            properties: {
                                EAN_list: {
                                    type: "object",
                                    patternProperties: {
                                        "^[0-9]+$": {
                                            type: "object",
                                            properties: {
                                                daily_data_Wh_per_EAN: {
                                                    type: "object",
                                                    additionalProperties: true,
                                                    required: ["T1_Wh_raw_data"],
                                                },
                                            },
                                            additionalProperties: true,
                                            required: ["daily_data_Wh_per_EAN"],
                                        },
                                    },
                                    additionalProperties: true,
                                    required: [],
                                },
                            },
                            additionalProperties: true,
                            required: ["EAN_list"],
                        },
                    },
                    additionalProperties: true,
                    required: [],
                },
            },
            additionalProperties: true,
            required: ["companies"],
        },
    },
    additionalProperties: true,
    required: ["stored_month"],
};
