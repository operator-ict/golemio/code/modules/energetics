import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IPreCsvInput } from "#sch/datasources/interfaces/IPreCsvInput";

export const preCsvInputSchema: JSONSchemaType<IPreCsvInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            "Počátek intervalu": { type: "string", pattern: "^\\d{1,2}.\\d{1,2}.\\d{4} \\d{1,2}:\\d{2}$" },
            "Konec intervalu": { type: "string", pattern: "^\\d{1,2}.\\d{1,2}.\\d{4} \\d{1,2}:\\d{2}$" },
        },
        patternProperties: {
            "^[0-9]{18} - Činná - spotřeba [kW]$": { type: "string" },
        },
        additionalProperties: true,
        required: ["Konec intervalu"],
    },
};
