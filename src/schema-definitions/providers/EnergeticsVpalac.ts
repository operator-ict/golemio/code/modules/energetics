import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

// SDMA = Sequelize DefineModelAttributes
// Vpalac = Vrtbovsky palac
const datasourceVpalacMeasurementJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            values: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        timestamp: {
                            type: "integer",
                        },
                        value: {
                            type: ["null", "number"],
                        },
                    },
                    required: ["timestamp", "value"],
                },
                additionalItems: true,
            },
            var_id: {
                type: "string",
            },
        },
        required: ["values", "var_id"],
        additionalProperties: true,
    },
};

const datasourceVpalacMeasuringEquipmentJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            me_do: {
                type: ["null", "string"],
            },
            me_extid: {
                type: ["null", "string"],
            },
            me_fakt: {
                type: ["null", "boolean"],
            },
            me_id: {
                type: "integer",
            },
            me_od: {
                type: ["null", "string"],
            },
            me_plom: {
                type: ["null", "string"],
            },
            me_serial: {
                type: ["null", "string"],
            },
            me_zapoc: {
                type: ["null", "boolean"],
            },
            met_id: {
                type: ["null", "integer"],
            },
            mis_id: {
                type: ["null", "integer"],
            },
            mis_nazev: {
                type: ["null", "string"],
            },
            poc_typode: {
                type: ["null", "integer"],
            },
            pot_id: {
                type: "integer",
            },
            umisteni: {
                type: ["null", "string"],
            },
            var_id: {
                type: ["null", "integer"],
            },
        },
        required: [
            "me_do",
            "me_extid",
            "me_fakt",
            "me_id",
            "me_od",
            "me_plom",
            "me_serial",
            "me_zapoc",
            "met_id",
            "mis_id",
            "mis_nazev",
            "poc_typode",
            "pot_id",
            "umisteni",
            "var_id",
        ],
        additionalProperties: true,
    },
};

const datasourceVpalacMeterTypeJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            fir_id: {
                type: ["null", "integer"],
            },
            medium: {
                type: ["null", "integer"],
            },
            met_druh: {
                type: ["null", "integer"],
            },
            met_id: {
                type: "integer",
            },
            met_kod: {
                type: ["null", "string"],
            },
            met_nazev: {
                type: ["null", "string"],
            },
            met_ziv: {
                type: ["null", "number"],
            },
            vyr_zkr: {
                type: ["null", "string"],
            },
        },
        required: ["fir_id", "medium", "met_druh", "met_id", "met_kod", "met_nazev", "met_ziv", "vyr_zkr"],
        additionalProperties: true,
    },
};

const datasourceVpalacTypeMeasuringEquipmentJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            cik_akt: {
                type: ["null", "boolean"],
            },
            cik_char: {
                type: ["null", "string"],
            },
            cik_cislo: {
                type: ["null", "integer"],
            },
            cik_cislo2: {
                type: ["null", "integer"],
            },
            cik_double: {
                type: ["null", "number"],
            },
            cik_fk: {
                type: ["null", "integer"],
            },
            cik_nazev: {
                type: ["null", "string"],
            },
            cik_pzn: {
                type: ["null", "string"],
            },
            cik_zprac: {
                type: ["null", "string"],
            },
            lt_key: {
                type: "string",
            },
        },
        required: [
            "cik_akt",
            "cik_char",
            "cik_cislo",
            "cik_cislo2",
            "cik_double",
            "cik_fk",
            "cik_nazev",
            "cik_pzn",
            "cik_zprac",
            "lt_key",
        ],
        additionalProperties: true,
    },
};

const datasourceVpalacUnitsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            jed_id: {
                type: ["null", "integer"],
            },
            jed_nazev: {
                type: ["null", "string"],
            },
            jed_zkr: {
                type: ["null", "string"],
            },
            lt_key: {
                type: "string",
            },
            pot_defcolor: {
                type: ["null", "string"],
            },
            pot_id: {
                type: ["null", "integer"],
            },
            pot_type: {
                type: ["null", "integer"],
            },
            ptv_id: {
                type: ["null", "integer"],
            },
        },
        required: ["jed_id", "jed_nazev", "jed_zkr", "lt_key", "pot_defcolor", "pot_id", "pot_type", "ptv_id"],
        additionalProperties: true,
    },
};

// Vpalac Measurement - Output Sequelize attributes
const outputVpalacMeasurementSDMA: ModelAttributes<any> = {
    time_measurement: {
        primaryKey: true,
        type: DataTypes.DATE,
    },
    value: DataTypes.REAL,
    var_id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
    },

    // Audit fields
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
};

// Vpalac Measuring Equipment - Output Sequelize attributes
const outputVpalacMeasuringEquipmentSDMA: ModelAttributes<any> = {
    me_do: DataTypes.TIME,
    me_extid: DataTypes.STRING(50),
    me_fakt: DataTypes.BOOLEAN,
    me_id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    me_od: DataTypes.DATE,
    me_plom: DataTypes.STRING(250),
    me_serial: DataTypes.STRING(50),
    me_zapoc: DataTypes.BOOLEAN,
    met_id: DataTypes.INTEGER,
    mis_id: DataTypes.INTEGER,
    mis_nazev: DataTypes.STRING(255),
    poc_typode: DataTypes.INTEGER,
    pot_id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    umisteni: DataTypes.STRING(250),
    var_id: DataTypes.INTEGER,

    // Audit fields
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
};

// Vpalac Meter Type - Output Sequelize attributes
const outputVpalacMeterTypeSDMA: ModelAttributes<any> = {
    fir_id: DataTypes.INTEGER,
    medium: DataTypes.INTEGER,
    met_druh: DataTypes.INTEGER,
    met_id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    met_kod: DataTypes.STRING(50),
    met_nazev: DataTypes.STRING(150),
    met_ziv: DataTypes.REAL,
    vyr_zkr: DataTypes.STRING(50),

    // Audit fields
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
};

// Vpalac Type Measuring Equipment - Output Sequelize attributes
const outputVpalacTypeMeasuringEquipmentSDMA: ModelAttributes<any> = {
    cik_akt: DataTypes.BOOLEAN,
    cik_char: DataTypes.STRING(50),
    cik_cislo: DataTypes.INTEGER,
    cik_cislo2: DataTypes.INTEGER,
    cik_double: DataTypes.REAL,
    cik_fk: DataTypes.INTEGER,
    cik_nazev: DataTypes.STRING(150),
    cik_pzn: DataTypes.STRING(50),
    cik_zprac: DataTypes.STRING(50),
    lt_key: {
        primaryKey: true,
        type: DataTypes.STRING(50),
    },

    // Audit fields
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
};

// Vpalac Units - Output Sequelize attributes
const outputVpalacUnitsSDMA: ModelAttributes<any> = {
    jed_id: DataTypes.INTEGER,
    jed_nazev: DataTypes.STRING(150),
    jed_zkr: DataTypes.STRING(50),
    lt_key: {
        primaryKey: true,
        type: DataTypes.STRING(50),
    },
    pot_defcolor: DataTypes.STRING(20),
    pot_id: DataTypes.INTEGER,
    pot_type: DataTypes.INTEGER,
    ptv_id: DataTypes.INTEGER,

    // Audit fields
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
};

// Vpalac Measurement - Output schema for the validator
const outputVpalacMeasurementJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            time_measurement: {
                type: "string",
            },
            value: {
                type: ["null", "number"],
            },
            var_id: {
                type: "string",
            },
        },
        required: ["time_measurement", "value", "var_id"],
        additionalProperties: true,
    },
};

// Vpalac Measuring Equipment - Output schema for the validator
// (same as the datasource schema)
const outputVpalacMeasuringEquipmentJsonSchema = datasourceVpalacMeasuringEquipmentJsonSchema;

// Vpalac Meter Type - Output schema for the validator
// (same as the datasource schema)
const outputVpalacMeterTypeJsonSchema = datasourceVpalacMeterTypeJsonSchema;

// Vpalac Type Measuring Equipment - Output schema for the validator
// (same as the datasource schema)
const outputVpalacTypeMeasuringEquipmentJsonSchema = datasourceVpalacTypeMeasuringEquipmentJsonSchema;

// Vpalac Units - Output schema for the validator
// (same as the datasource schema)
const outputVpalacUnitsJsonSchema = datasourceVpalacUnitsJsonSchema;

const vpalacPrefix = {
    name: "EnergeticsVpalac",
    pgTableName: "vpalac",
};

const EnergeticsVpalac = {
    measurement: {
        datasourceJsonSchema: datasourceVpalacMeasurementJsonSchema,
        name: `${vpalacPrefix.name}Measurement`,
        outputJsonSchema: outputVpalacMeasurementJsonSchema,
        outputSequelizeAttributes: outputVpalacMeasurementSDMA,
        pgTableName: `${vpalacPrefix.pgTableName}_measurement`,
    },
    measuringEquipment: {
        datasourceJsonSchema: datasourceVpalacMeasuringEquipmentJsonSchema,
        name: `${vpalacPrefix.name}MeasuringEquipment`,
        outputJsonSchema: outputVpalacMeasuringEquipmentJsonSchema,
        outputSequelizeAttributes: outputVpalacMeasuringEquipmentSDMA,
        pgTableName: `${vpalacPrefix.pgTableName}_measuring_equipment`,
    },
    meterType: {
        datasourceJsonSchema: datasourceVpalacMeterTypeJsonSchema,
        name: `${vpalacPrefix.name}MeterType`,
        outputJsonSchema: outputVpalacMeterTypeJsonSchema,
        outputSequelizeAttributes: outputVpalacMeterTypeSDMA,
        pgTableName: `${vpalacPrefix.pgTableName}_meter_type`,
    },
    typeMeasuringEquipment: {
        datasourceJsonSchema: datasourceVpalacTypeMeasuringEquipmentJsonSchema,
        name: `${vpalacPrefix.name}TypeMeasuringEquipment`,
        outputJsonSchema: outputVpalacTypeMeasuringEquipmentJsonSchema,
        outputSequelizeAttributes: outputVpalacTypeMeasuringEquipmentSDMA,
        pgTableName: `${vpalacPrefix.pgTableName}_type_measuring_equipment`,
    },
    units: {
        datasourceJsonSchema: datasourceVpalacUnitsJsonSchema,
        name: `${vpalacPrefix.name}Units`,
        outputJsonSchema: outputVpalacUnitsJsonSchema,
        outputSequelizeAttributes: outputVpalacUnitsSDMA,
        pgTableName: `${vpalacPrefix.pgTableName}_units`,
    },
};

export { EnergeticsVpalac };
