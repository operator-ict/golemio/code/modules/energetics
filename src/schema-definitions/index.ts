import { EnergeticsVpalac } from "./providers";

interface ISchema {
    name: string;
    pgSchema: string;
    vpalac: typeof EnergeticsVpalac;
}

const EnergeticsSchema: ISchema = {
    name: "Energetics",
    pgSchema: "energetics",

    // Providers
    vpalac: EnergeticsVpalac,
};

export { EnergeticsSchema };
