# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.4.9] - 2025-01-30

### Added

-   add PRE input JSON and CSV integration ([p0262#52](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/52))
-   PTAS meters filtering to avoid meter conflicts ([p0262#57](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/57))

## [1.4.8] - 2024-11-22

### Fixed

-   global filter for measurments limited to data from PPAS ([p0262#50](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/50#note_2223746509))

## [1.4.7] - 2024-10-24

### Fixed

-   asyncAPI prefixes

## [1.4.6] - 2024-10-22

### Added

-   asyncAPI docs file ([integration-engine#261](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/261))

## [1.4.5] - 2024-09-12

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.4.4] - 2024-08-20

### Added

-   add cache-control header to all responses ([core#110](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/110))

### Removed

-   remove redis useCacheMiddleware ([core#110](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/110))

## [1.4.3] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.4.2] - 2024-07-30

### Fixed

-   monthly reporting API ([prevzeti-ed#37](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/37))
-   duplicate removal from PTAS datasource ([prevzeti-ed#37](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/37))
-   flag for Ave datasource set to raw (hourly) data ([prevzeti-ed#37](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/37))
-   measurements in future or none/zero values are not saved ([prevzeti-ed#37](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/37))

## [1.4.1] - 2024-07-17

### Fixed

-   api validations after adding checkExact in core ([core#109](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/109))

## [1.4.0] - 2024-07-15

### Added

-   Integration of PPAS for rest of the city apart from Prague 10 ([prevzeti-ed#22](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/22))

## [1.3.9] - 2024-07-10

### Added

-   access restriction to API ([prevzeti-ed#18](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/18))

### Changed

-   remove `variable` from column `addr` in table `consumption_energy_measurements` ([prevzeti-ed#18](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/18))

## [1.3.8] - 2024-07-04

### Changed

-   Utilize CsvParserMiddleware to parse csv push data ([ig#82](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/82))

## [1.3.7] - 2024-06-24

### Changed

-   Partitioning on `consumption_energy_measurements` table ([prevzeti-ed#13](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/13))

### Added

-   Measurement endpoints ([prevzeti-ed#19](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/19)

## [1.3.6] - 2024-06-03

### Added

-   Buildings endpoints and change of the db structure ([prevzeti-ed#14](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/14))
-   Organization & devices endpoints ([prevzeti-ed#14](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/20))

## [1.3.5] - 2024-05-22

### Added

-   Integration Web Scraping Portál PTAS ([prevzeti-ed#4](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/4)

## [1.3.4] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.3.3] - 2024-04-10

### Added

-   Integration PRE Elektro Data ([prevzeti-ed#5](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/5)

## [1.3.2] - 2024-04-08

### Changed

-   axios exchanged for native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.3.1] - 2024-03-25

### Added

-   Fetch measurement data directly from PPAS (Pražská plynárenská) AVE API ([prevzeti-ed#3](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/3)

## [1.3.0] - 2024-03-20

### Added

-   New worker for fetching measurements directly from commodities' providers ([energetics#9](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/2))

## [1.2.11] - 2024-01-29

### Changed

-   Refactoring energetics palace worker saving batches ([energetics#8]https://gitlab.com/operator-ict/golemio/code/modules/energetics/-/issues/8))

## [1.2.10] - 2023-11-15

### Changed

-   Removing schema definitions ([schema-definitions#55](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/-/issues/55))

## [1.2.9] - 2023-08-16

### Fixed

-   Bigint -> timestamptz rounding error ([energetics#7](https://gitlab.com/operator-ict/golemio/code/modules/energetics/-/issues/7))

## [1.2.8] - 2023-08-16

### Changed

-   Replace bigint timestamps with timestamptz ([energetics#7](https://gitlab.com/operator-ict/golemio/code/modules/energetics/-/issues/7))

## [1.2.7] - 2023-07-31

### Changed

-   Replace moment.js with dateTime helper ([core#68](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/68))

## [1.2.6] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.2.5] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.2.4] - 2023-02-27

### Fixed

-   Fix migrační historie aby nezavisela na tabulkach ve schematu python

## [1.2.3] - 2023-02-22

### Changed

-   change view v_gas_reserve to remove prague 10 (https://gitlab.com/operator-ict/golemio/projekty/energetika/praha-10-spoluprace/-/issues/3)
-   change view v_consumption_electricity_oict_api to depend on data from energetics and not python https://gitlab.com/operator-ict/golemio/projekty/energetika/spotreby-energii-mestskych-budov/-/issues/72
-   Modified view v_consumption_energy_buildings_real_mc_nogeom (https://gitlab.com/operator-ict/golemio/projekty/energetika/praha-10-spoluprace/-/issues/3)
-   Modified views for energetics (https://gitlab.com/operator-ict/golemio/projekty/energetika/praha-10-spoluprace/-/issues/3)
-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.2.2] - 2023-02-13

### Added

-   nová a přepracovaná views: `v_consumption_energy_last_update`,`v_consumption_energy_last_update_prague_10`, `v_consumption_energy_missing_devices_last_update`,`v_consumption_energy_missing_devices_last_update_prague_10` (https://gitlab.com/operator-ict/golemio/projekty/energetika/praha-10-spoluprace/-/issues/2#note_1263094820)

### Changed

-   Correct view `v_consumption_energy_missing_devices_last_update_prague_10` (https://gitlab.com/operator-ict/golemio/projekty/energetika/praha-10-spoluprace/-/issues/2)

## [1.2.1] - 2023-01-25

-   optimized SQL for view `v_vpalac_apartments_daily_consumption` (https://gitlab.com/operator-ict/golemio/projekty/energetika/d0075.-vrbovsk-palac-spotreby-energii/-/issues/15)

## [1.2.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.1.1] - 2023-01-18

### Changed

-   Correct view `v_consumption_energy_last_update`( https://gitlab.com/operator-ict/golemio/code/modules/energetics/-/issues/6)

## [1.1.0] - 2023-01-10

### Added

-   Implementation docs ([energetics#4](https://gitlab.com/operator-ict/golemio/code/modules/energetics/-/issues/4))

### Changed

-   Replace Enesa data source with OICT Energetika ([energetics#6](https://gitlab.com/operator-ict/golemio/code/modules/energetics/-/issues/6))

## [1.0.13] - 2023-01-04

### Changed

-   Updates in database due to a api source change ([importers#7](https://gitlab.com/operator-ict/golemio/extras/importers/-/issues/7))

## [1.0.12] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.11] - 2022-09-21

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.0.10] - 2022-09-06

### Changed

-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))

## [1.0.9] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

## [1.0.8] - 2022-05-18

### Fixed

-   Json validation schema fixed to work with strict option

## [1.0.7] - 2022-02-16

### Changed

-   Request-promise deprecated to Axios ([core#24](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/24))

## [1.0.6] - 2022-01-06

### Added

-   Add migrations

