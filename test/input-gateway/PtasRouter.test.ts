import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import express, { Request, Response, NextFunction } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { log } from "@golemio/core/dist/output-gateway";
import { AMQPConnector } from "@golemio/core/dist/input-gateway/connectors";
import { ptasRouter } from "#ig/ptas";

chai.use(chaiAsPromised);

describe("PtasRouter", () => {
    const app = express();
    app.use(express.raw({ type: "text/csv" }));

    let testDataCoupons: string;

    before(async () => {
        await AMQPConnector.connect();

        app.use("/ptas", ptasRouter);
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    });

    describe("Ptas data", () => {
        beforeEach(() => {
            testDataCoupons =
                "Datum odečtu;Čas odečtu;Výrobní číslo;Druh registru;" +
                "Odečet;MJ odečtu;MJ zúčt.;OM závislé;Typ odečtu;Status odečtu\r\n" +
                "25.4.2024;00:56;68837353;Teplo horké vody;2 969,03;GJ;GJ;;;nenalezeno\r\n" +
                "24.4.2024;21:36;68837353;Teplo horké vody;2 968,59;GJ;GJ;;;nenalezeno\r\n";
        });

        it("should respond with 204 to POST", (done) => {
            request(app)
                .post("/ptas/12345")
                .send(testDataCoupons)
                .set("Content-Type", "text/csv")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST with bad content type", (done) => {
            request(app)
                .post("/ptas/12345")
                .send("value=0")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST with no content type", (done) => {
            request(app)
                .post("/ptas/12345")
                .send(testDataCoupons)
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST with invalid data", (done) => {
            testDataCoupons =
                "Datum odečtu;Čas odečtu;Výrobní číslo;Druh registru;" +
                "Odečet;MJ odečtu;MJ zúčt.;OM závislé;Typ odečtu;Status odečtu\r\n" +
                "251.4.2024;00:56;68837353;Teplo horké vody;2 969,03;GJ;GJ;;;nenalezeno\r\n" +
                "24.4.2024;21:36;68837353;Teplo horké vody;2 968,59;GJ;GJ;;;nenalezeno\r\n";
            request(app)
                .post("/ptas/12345")
                .send(testDataCoupons)
                .set("Content-Type", "text/csv")
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });
});
