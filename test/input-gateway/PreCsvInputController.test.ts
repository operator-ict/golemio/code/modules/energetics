import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { PreCsvInputController } from "#ig/pre/PreCsvInputController";
import fs from "fs";
import { PreCsvInputTransformation } from "#ig/transformations/PreCsvInputTransformation";

describe("PreCsvInputController", () => {
    let sandbox: SinonSandbox;
    let controller: PreCsvInputController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        controller = new PreCsvInputController(new PreCsvInputTransformation());
        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
        sandbox.stub(controller["transformation"], "transformElement" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("calls correct methods", async () => {
        const data = fs.readFileSync(__dirname + "/data/pre-input-fixture.csv");

        await controller.processData(data as any);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["transformation"].transformElement as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });
});
