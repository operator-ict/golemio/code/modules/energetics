import { PreCsvInputTransformation } from "#ig/transformations/PreCsvInputTransformation";
import { expect } from "chai";

describe("PreCsvInputTransformation", () => {
    const transformation = new PreCsvInputTransformation();

    it("transforms data correctly", () => {
        const input = [
            {
                "Počátek intervalu": "01.01.2021 00:00",
                "Konec intervalu": "01.01.2021 00:15",
                "859182400300027238 - Činná - spotřeba [kW]": "57,000",
                "859182400300027238 - Induktivní - spotřeba [kVAr]": "0,000",
                "859182400300027238 - Kapacitní - spotřeba [kVAr]": "5,000",
                "": "",
            },
            {
                "Počátek intervalu": "01.01.2021 00:15",
                "Konec intervalu": "01.01.2021 00:30",
                "859182400300027238 - Činná - spotřeba [kW]": "59,000",
                "859182400300027238 - Induktivní - spotřeba [kVAr]": "1,000",
                "859182400300027238 - Kapacitní - spotřeba [kVAr]": "5,000",
                "": "",
            },
            {
                "Počátek intervalu": "01.01.2021 00:30",
                "Konec intervalu": "01.01.2021 00:45",
                "859182400300027238 - Činná - spotřeba [kW]": "56,000",
                "859182400300027238 - Induktivní - spotřeba [kVAr]": "0,000",
                "859182400300027238 - Kapacitní - spotřeba [kVAr]": "5,000",
                "": "",
            },
        ];
        const actual = transformation.transformElement(input);

        const expected = {
            ean: "859182400300027238",
            T1_Wh_raw_data: [
                ["01.01.2021 00:15", 57],
                ["01.01.2021 00:30", 59],
                ["01.01.2021 00:45", 56],
            ],
        };

        expect(actual).to.deep.eq(expected);
    });
});
