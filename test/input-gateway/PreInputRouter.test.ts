import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import express, { Request, Response, NextFunction } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { log } from "@golemio/core/dist/output-gateway";
import { AMQPConnector } from "@golemio/core/dist/input-gateway/connectors";
import { preInputRouter } from "#ig/pre";
import fs from "fs";

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

chai.use(chaiAsPromised);

describe("PreInputRouter", () => {
    const app = express();
    app.use(
        bodyParser.json({
            limit: "2MB",
        })
    );
    app.use(express.raw({ type: "text/csv" }));

    before(async () => {
        await AMQPConnector.connect();

        app.use("/pre", preInputRouter);
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    });

    it("should accept correct data on JSON endpoint", (done) => {
        const preDataTestInput = JSON.stringify(require(__dirname + "/data/pre-input-fixture.json"));
        request(app)
            .post("/pre")
            .send(preDataTestInput)
            .set("Content-Type", "application/json")
            .expect(204)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 422 to POST with invalid JSON data", (done) => {
        request(app)
            .post("/pre")
            .send('{ "wtf": "some data" }')
            .set("Content-Type", "application/json")
            .expect(422)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should accept correct data on CSV endpoint", (done) => {
        const preDataTestInput = fs.readFileSync(__dirname + "/data/pre-input-fixture.csv");

        request(app)
            .post("/pre/csv")
            .send(preDataTestInput)
            .set("Content-Type", "text/csv")
            .expect(204)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 422 to POST with invalid CSV data", (done) => {
        const data =
            "Počátek;Konec;859182400300027238 - spotřeba [kW];\n" + "01.01.2021 00:00;01.01.2021 00:15;57,000;0,000;5,000;";
        request(app)
            .post("/pre/csv")
            .send(data)
            .set("Content-Type", "text/csv")
            .expect(422)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });
});
