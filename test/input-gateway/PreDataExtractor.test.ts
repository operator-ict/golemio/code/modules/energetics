import { PreDataExtractor } from "#ig/helpers/PreDataExtractor";
import fs from "fs";
import { preEanMeasurement } from "./data/preEanMeasurement";
import { expect } from "chai";

describe("PreDataExtractor", () => {
    const extractor = new PreDataExtractor();

    it("extracts data from JSON correctly", () => {
        const data = JSON.parse(fs.readFileSync(__dirname + "/data/pre-input-fixture.json", "utf8"));
        const actual = extractor.extractEanMeasurements(data);
        expect(actual).to.deep.eq([preEanMeasurement]);
    });
});
