import { IPreEanMeasurement } from "#ie/commodity-providers/workers/interfaces/IPreEanMeasurement";

export const preCsvEanMeasurement: IPreEanMeasurement = {
    ean: "859182400300027238",
    T1_Wh_raw_data: [
        ["01.01.2021 00:15", 57],
        ["01.01.2021 00:30", 59],
        ["01.01.2021 00:45", 56],
    ],
};
