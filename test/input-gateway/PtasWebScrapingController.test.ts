import { PtasWebScrapingController } from "#ig/ptas/PtasWebScrapingController";
import Papa from "papaparse";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

describe("PtasWebScrapingController", () => {
    let sandbox: SinonSandbox;
    let controller: PtasWebScrapingController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        controller = new PtasWebScrapingController();
        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("calls correct methods", async () => {
        const fs = require("fs");

        const rawData = fs.readFileSync(__dirname + "/data/ptas-hot-water-data-input.csv").toString() as string;

        const { data } = Papa.parse(rawData, {
            header: true,
            delimiter: ";",
        });

        await controller.processData(data as any);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });
});
