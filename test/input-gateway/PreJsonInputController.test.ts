import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { PreJsonInputController } from "#ig/pre";
import { PreDataExtractor } from "#ig/helpers/PreDataExtractor";

describe("PreJsonInputController", () => {
    let sandbox: SinonSandbox;
    let controller: PreJsonInputController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        controller = new PreJsonInputController(new PreDataExtractor());
        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("calls correct methods", async () => {
        const data = require(__dirname + "/data/pre-input-fixture.json");

        await controller.processData(data as any);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });
});
