// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const http = require("http");
const express = require("@golemio/core/dist/shared/express");
const { ContainerToken, OutputGatewayContainer } = require("@golemio/core/dist/output-gateway/ioc");
const { OGEnergeticsContainer} = require("#og/ioc/Di");
const { ModuleContainerToken} = require("#og/ioc/ModuleContainerToken");

const app = express();
const server = http.createServer(app);
const postgresConnector = OutputGatewayContainer.resolve(ContainerToken.PostgresDatabase);

const start = async () => {
    await postgresConnector.connect();

    const v2BuildingsRouter = OGEnergeticsContainer.resolve(ModuleContainerToken.V2BuildingsRouter)
    const v2OrganizationsRouter = OGEnergeticsContainer.resolve(ModuleContainerToken.V2OrganizationsRouter)
    const v2DevicesRouter = OGEnergeticsContainer.resolve(ModuleContainerToken.V2DevicesRouter)
    const v2MeasurementsRouter = OGEnergeticsContainer.resolve(ModuleContainerToken.V2MeasurementsRouter)

    app.use(v2BuildingsRouter.getPath(), v2BuildingsRouter.getRouter());
    app.use(v2OrganizationsRouter.getPath(), v2OrganizationsRouter.getRouter());
    app.use(v2DevicesRouter.getPath(), v2DevicesRouter.getRouter());
    app.use(v2MeasurementsRouter.getPath(), v2MeasurementsRouter.getRouter());

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await postgresConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};
