// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const http = require("http");
const express = require("express");
const { AMQPConnector } = require("@golemio/core/dist/input-gateway/connectors");
const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

const app = express();
const server = http.createServer(app);

const start = async () => {
    await AMQPConnector.connect();

    const { PtasRouter } = require("#ig/ptas/PtasRouter");

    app.use(express.raw({ type: "text/csv" }));

    app.use("/ptas", new PtasRouter().router);

    const { PreInputRouter } = require("#ig/pre/PreInputRouter");

    app.use(
        bodyParser.json({
            limit: "2MB",
        })
    );

    app.use("/pre", new PreInputRouter().router);

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await AMQPConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};
