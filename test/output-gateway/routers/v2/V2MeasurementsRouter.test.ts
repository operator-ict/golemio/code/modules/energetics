import { V2MeasurementsRouter } from "#og/routers/v2/V2MeasurementsRouter";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { ILogger } from "@golemio/core/dist/helpers";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { measurementsResponse } from "../../data/measurements-response";

chai.use(chaiAsPromised);

describe("V2MeasurementsRouter", () => {
    const app = express();

    before(async () => {
        const logger = OutputGatewayContainer.resolve<ILogger>(CoreToken.Logger);
        const v2MeasurementsRouter = OGEnergeticsContainer.resolve<V2MeasurementsRouter>(
            ModuleContainerToken.V2MeasurementsRouter
        );

        app.use(v2MeasurementsRouter.getPath(), v2MeasurementsRouter.getRouter());
        app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, logger);
            logger.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond correctly to GET /measurements", async () => {
        const response = await request(app)
            .get("/v2/energetics/measurements?accessLimit=admin")
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
        expect(response.body).to.eql(measurementsResponse);
    });

    it("should respond correctly to GET /measurements with query params", async () => {
        const query = `variable=core&type=provider_value&addr1=2.9&addr2=PF1&dateFrom=2023-05-01&dateTo=2023-05-02`;
        const response = await request(app)
            .get(`/v2/energetics/measurements?${query}&accessLimit=admin`)
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
        expect(response.body).to.eql(
            measurementsResponse.filter(
                (el) => el.addr === "/2.9/PF1" && el.time < "2023-05-02T00:00:00.001Z" && el.var === "core"
            )
        );
    });

    it("should throw validation error if part of address is missing while GET /measurements with query params", async () => {
        const response = await request(app)
            .get("/v2/energetics/measurements?variable=core&type=provider_value&addr1=2.9&accessLimit=test")
            .set("Accept", "application/json");

        expect(response.status).to.equal(400);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.contain({
            error_message: "Bad request",
            error_status: 400,
        });
    });

    it("should respond correctly to GET /measurements/monthly-reading ", async () => {
        const response = await request(app)
            .get(
                // eslint-disable-next-line max-len
                "/v2/energetics/measurements/monthly-reading?variable=core2&year=2023&month=5&addr1=2.9&addr2=PF1&accessLimit=admin"
            )
            .set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
        expect(response.body).to.eql([
            {
                time: "2023-05-01T00:00:00.000+02:00",
                value: "20.000000000000000",
                meter: "27ZG100Z0000521V",
                type: "provider_value",
                addr: "/2.9/PF1",
                var: "core2",
            },
        ]);
    });
});
