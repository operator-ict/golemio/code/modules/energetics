import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { V2OrganizationsRouter } from "#og/routers/v2/V2OrganizationsRouter";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers";
import { organizationsResponse } from "../../data/organizations-response";
import { IOrganizationResponse } from "#og/routers/interfaces/IOrganizationResponse";

chai.use(chaiAsPromised);

describe("V2OrganizationsRouter", () => {
    const app = express();

    before(async () => {
        const logger = OutputGatewayContainer.resolve<ILogger>(CoreToken.Logger);
        const v2OrganizationsRouter = OGEnergeticsContainer.resolve<V2OrganizationsRouter>(
            ModuleContainerToken.V2OrganizationsRouter
        );

        app.use(v2OrganizationsRouter.getPath(), v2OrganizationsRouter.getRouter());
        app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, logger);
            logger.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond correctly to GET /organizations", async () => {
        const response = await request(app)
            .get("/v2/energetics/organizations?accessLimit=testAll")
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
        expect(response.body).to.eql(
            organizationsResponse.map((org: IOrganizationResponse) => ({ id: org.id, name: org.name, link: org.link }))
        );
    });

    it("should respond correctly to GET /organizations?full=true", async () => {
        const response = await request(app)
            .get("/v2/energetics/organizations?full=true&accessLimit=testAll")
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
        expect(response.body).to.eql(organizationsResponse);
    });

    it("should respond correctly to GET /organizations/:id ", async () => {
        const response = await request(app)
            .get("/v2/energetics/organizations/3?accessLimit=test2")
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
        expect(response.body).to.eql(organizationsResponse[2]);
    });
});
