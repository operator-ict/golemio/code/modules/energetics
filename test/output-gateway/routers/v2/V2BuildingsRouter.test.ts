import { V2BuildingsRouter } from "#og/routers/v2/V2BuildingsRouter";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { ILogger } from "@golemio/core/dist/helpers";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";

chai.use(chaiAsPromised);

describe("Buildings Router test", () => {
    const app = express();

    before(async () => {
        const logger = OutputGatewayContainer.resolve<ILogger>(CoreToken.Logger);
        const v2BuildingsRouter = OGEnergeticsContainer.resolve<V2BuildingsRouter>(ModuleContainerToken.V2BuildingsRouter);

        app.use(v2BuildingsRouter.getPath(), v2BuildingsRouter.getRouter());
        app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, logger);
            logger.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /buildings", (done) => {
        request(app)
            .get("/v2/energetics/buildings?accessLimit=test")
            .then((response: any) => {
                expect(response.status).to.equal(200);
                expect(response.headers["cache-control"]).to.eq("public, s-maxage=21600, stale-while-revalidate=60");
                expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
                done();
            });
    });

    it("should return the first building GET /buildings/:id ", (done) => {
        request(app)
            .get("/v2/energetics/buildings/7?accessLimit=test")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.status).to.equal(200);
                expect(response.headers["cache-control"]).to.eq("public, s-maxage=21600, stale-while-revalidate=60");
                expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
                done();
            })
            .catch((err) => done(err));
    });

    it("should limit access to building GET /buildings/:id ", (done) => {
        request(app)
            .get("/v2/energetics/buildings/100?accessLimit=test")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(404)
            .then((response) => {
                expect(response.status).to.equal(404);
                done();
            })
            .catch((err) => done(err));
    });
});
