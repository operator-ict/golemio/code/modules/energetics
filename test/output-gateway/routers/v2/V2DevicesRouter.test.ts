import { V2DevicesRouter } from "#og/routers/v2/V2DevicesRouter";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { ILogger } from "@golemio/core/dist/helpers";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { devicesResponse } from "../../data/devices-response";

chai.use(chaiAsPromised);

describe("V2DevicesRouter", () => {
    const app = express();

    before(async () => {
        const logger = OutputGatewayContainer.resolve<ILogger>(CoreToken.Logger);
        const v2DevicesRouter = OGEnergeticsContainer.resolve<V2DevicesRouter>(ModuleContainerToken.V2DevicesRouter);

        app.use(v2DevicesRouter.getPath(), v2DevicesRouter.getRouter());
        app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, logger);
            logger.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond correctly to GET /devices", async () => {
        const response = await request(app).get("/v2/energetics/devices?accessLimit=test2").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.eql(devicesResponse);
    });

    it("should respond correctly to GET /devices/:id ", async () => {
        const response = await request(app).get("/v2/energetics/devices/16?accessLimit=test2").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.eql(devicesResponse[0]);
    });

    it("should limit access to GET /devices/:id ", async () => {
        const response = await request(app).get("/v2/energetics/devices/16?accessLimit=test").set("Accept", "application/json");

        expect(response.status).to.equal(404);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=1800, stale-while-revalidate=60");
    });
});
