import { expect } from "chai";
import { DeviceDtoTransformation } from "#og/transformations/DeviceDtoTransformation";
import { deviceModelData } from "./fixture/device-model-data";

describe("DeviceDtoTransformation", () => {
    const transformation = new DeviceDtoTransformation();

    it("should transform", () => {
        const result = transformation.transformElement(deviceModelData[0]);
        expect(result).to.deep.equal({
            id: 16,
            addr: "/2.2/PF1",
            description: "Fakturační plynoměr škola",
            meter_number: "743037",
            meter_index: 1,
            location_number: "27ZG100Z00008560",
            location_description: "Škola",
            include_in_evaluation: "true",
            meter_type: "Plynoměr",
            category: "Hlavní (fakturační)",
            unit: "m^3",
            replaced_meter_id: null,
            deleted: "false",
            building_id: 10,
        });
    });
});
