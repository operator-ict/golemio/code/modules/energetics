import { IOrganizationDto } from "#og/repositories/interfaces/IOrganizationDto";

export const organizationModelData: IOrganizationDto[] = [
    {
        id: 1,
        name: "SŠAI Weilova",
    },
    {
        id: 2,
        name: "Gymnázium Na Vítězné pláni",
    },
    {
        id: 3,
        name: "Gymnázium a Hudební škola hlavního města Prahy ",
    },
];
