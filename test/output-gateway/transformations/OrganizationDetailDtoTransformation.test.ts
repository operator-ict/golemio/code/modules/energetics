import { expect } from "chai";
import { OrganizationDetailDtoTransformation } from "#og/transformations/OrganizationDetailDtoTransformation";
import { organizationDetailModelData } from "./fixture/organization-detail-model-data";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";

describe("OrganizationDetailDtoTransformation", () => {
    const transformation = OGEnergeticsContainer.resolve<OrganizationDetailDtoTransformation>(
        ModuleContainerToken.OrganizationDetailDtoTransformation
    );

    it("should transform", () => {
        const result = transformation.transformElement(organizationDetailModelData[0]);
        expect(result).to.deep.equal({
            id: 1,
            name: "SŠAI Weilova",
            label: "SO01 SŠAI",
            address: {
                city: "Praha 10 - Hostivař",
                country: "Česká Republika",
                house_number: "12704",
                mail: "mailbox@skolahostivar.cz",
                phone: "242 456 100",
                street: "Weilova",
                web_address: "http://www.skolahostivar.cz",
            },
            category: "Školství",
            created_by_id: "64",
            grafana_url: "http://10.200.0.73:3000/d/l8OJLI2ik/01-ss-automobilni-a-informatiky-weilova?orgId=2",
            responsible_user: [
                {
                    name: "Milan",
                    last_name: "Vorel",
                    position: "ředitel",
                    phone: null,
                    mail: "milan.vorel@skolahostivar.cz",
                    company: null,
                },
                {
                    name: "Zdeněk",
                    last_name: "Kočárek",
                    position: "vedoucí technické správy",
                    phone: null,
                    mail: "zdenek.kocarek@skolahostivar.cz",
                    company: null,
                },
                {
                    name: "Jaroslav",
                    last_name: "Malý",
                    position: "správa budov",
                    phone: null,
                    mail: "jaroslav.maly@skolahostivar.cz",
                    company: null,
                },
            ],
            buildings: [
                {
                    id: 7,
                    name: "SŠAI Weilova",
                    building_address_code: "2.1",
                    link: "https://api.golemio.cz/v2/energetics/buildings/7",
                },
                {
                    id: 36,
                    name: "SŠAI Weilova – Bohdalec",
                    building_address_code: "2.1.2",
                    link: "https://api.golemio.cz/v2/energetics/buildings/36",
                },
                {
                    id: 37,
                    name: "SŠAI Weilova – Libuš",
                    building_address_code: "2.1.3",
                    link: "https://api.golemio.cz/v2/energetics/buildings/37",
                },
                {
                    id: 38,
                    name: "SŠAI Weilova - Školní chata",
                    building_address_code: "2.1.4",
                    link: "https://api.golemio.cz/v2/energetics/buildings/38",
                },
            ],
            link: "https://api.golemio.cz/v2/energetics/organizations/1",
        });
    });
});
