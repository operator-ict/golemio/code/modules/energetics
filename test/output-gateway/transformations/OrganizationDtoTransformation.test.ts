import { expect } from "chai";
import { OrganizationDtoTransformation } from "#og/transformations/OrganizationDtoTransformation";
import { organizationModelData } from "./fixture/organization-model-data";
import { OGEnergeticsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";

describe("OrganizationDtoTransformation", () => {
    const transformation = OGEnergeticsContainer.resolve<OrganizationDtoTransformation>(
        ModuleContainerToken.OrganizationDtoTransformation
    );

    it("should transform", () => {
        const result = transformation.transformElement(organizationModelData[0]);
        expect(result).to.deep.equal({
            id: 1,
            name: "SŠAI Weilova",
            link: "https://api.golemio.cz/v2/energetics/organizations/1",
        });
    });
});
