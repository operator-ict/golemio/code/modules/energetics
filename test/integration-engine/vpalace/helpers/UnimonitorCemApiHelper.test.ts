import { UnimonitorCemApiHelper } from "#ie/helpers";
import { VPalaceContainer } from "#ie/ioc/Di";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import "mocha";
import sinon from "sinon";

chai.use(chaiAsPromised);
describe("UnimonitorCemApiHelper", () => {
    let sandbox: sinon.SinonSandbox;
    let unimonitorCemApi: UnimonitorCemApiHelper;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        unimonitorCemApi = new UnimonitorCemApiHelper("https://test.neco.eu/api", {
            authcookiename: "CEMAPI",
            pass: "6",
            user: "admin",
        });
    });

    afterEach(() => {
        sandbox.restore();
        VPalaceContainer.clearInstances();
    });

    it("createSession should return an auth cookie", async () => {
        const fetchStub = sandbox.stub().resolves({
            status: 200,
            headers: {
                has: () => true,
                get: () => ["SOMETHING=owo; CEMAPI=SOMEVALUE;"],
                getSetCookie: () => ["SOMETHING=owo; CEMAPI=SOMEVALUE;"],
            },
        });
        sandbox.replace(globalThis, "fetch", fetchStub);

        const authCookie = await unimonitorCemApi.createSession();

        expect(authCookie).to.equal("CEMAPI=SOMEVALUE");
        // @ts-ignore
        sandbox.assert.calledOnce(globalThis.fetch);
    });

    it("createSession should just reject", async () => {
        sandbox.replace(globalThis, "fetch", sandbox.stub().rejects(new Error("some_error")));

        await expect(unimonitorCemApi.createSession()).to.be.rejectedWith(GeneralError);
        // @ts-ignore
        sandbox.assert.calledOnce(globalThis.fetch);
    });

    it("terminateSession should call fetch and resolve", async () => {
        sandbox.replace(globalThis, "fetch", sandbox.stub().resolves());

        expect(await unimonitorCemApi.terminateSession("")).to.be.undefined;
        // @ts-ignore
        sandbox.assert.calledOnce(globalThis.fetch);
    });

    it("terminateSession should call fetch and reject", async () => {
        sandbox.replace(globalThis, "fetch", sandbox.stub().rejects(new Error("some_error")));

        await expect(unimonitorCemApi.terminateSession("")).to.be.rejectedWith(GeneralError);
        // @ts-ignore
        sandbox.assert.calledOnce(globalThis.fetch);
    });

    it("resourceType getter should return an object", () => {
        expect(typeof UnimonitorCemApiHelper.resourceType).to.equal("object");
    });

    it("processAndFilterAuthCookie should return an empty string (cookie header is null)", () => {
        const authCookie = UnimonitorCemApiHelper["processAndFilterAuthCookie"](null as any, "");

        expect(authCookie).to.equal("");
    });

    it("processAndFilterAuthCookie should return an empty string (cookie name does not match)", () => {
        const cookieHeader = "SOMEAPIKEY=24a24b24c;";
        const cookieName = "NONEXISTENTKEY";
        const authCookie = UnimonitorCemApiHelper["processAndFilterAuthCookie"](cookieHeader, cookieName);

        expect(authCookie).to.equal("");
    });

    it("processAndFilterAuthCookie should return a cookie string with auth token (simple cookie header)", () => {
        const cookieHeader = "SOMEAPIKEY=24a24b24c;";
        const cookieName = "SOMEAPIKEY";
        const authCookie = UnimonitorCemApiHelper["processAndFilterAuthCookie"](cookieHeader, cookieName);

        expect(authCookie).to.equal("SOMEAPIKEY=24a24b24c");
    });

    it("processAndFilterAuthCookie should return a cookie string with auth token (complex cookie header)", () => {
        const cookieHeader = "TEST=2; SOMEAPIKEY=24a24b24c; SOMETHING=somethingelse;";
        const cookieName = "SOMEAPIKEY";
        const authCookie = UnimonitorCemApiHelper["processAndFilterAuthCookie"](cookieHeader, cookieName);

        expect(authCookie).to.equal("SOMEAPIKEY=24a24b24c");
    });
});
