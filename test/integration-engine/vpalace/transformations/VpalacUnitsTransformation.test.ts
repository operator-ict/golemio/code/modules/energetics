import { EnergeticsSchema } from "#sch";
import fs from "fs";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai from "chai";
import { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import "mocha";
import { VpalacUnitsTransformation } from "#ie/transformations/vpalace";

chai.use(chaiAsPromised);

describe("VpalacUnitsTransformation", () => {
    let transformation: VpalacUnitsTransformation;
    let testSourceData: Record<string, any>;
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator(
            EnergeticsSchema.vpalac.units.name + "ModelPostgresValidator",
            EnergeticsSchema.vpalac.units.outputJsonSchema
        );
    });

    beforeEach(() => {
        transformation = new VpalacUnitsTransformation();
        const rawData = fs.readFileSync(__dirname + "/../../data/energetics-vpalac-units-datasource.json") as unknown;
        testSourceData = JSON.parse(rawData as string);
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("EnergeticsVpalacUnits");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validator.Validate(data)).to.be.fulfilled;

        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("jed_id");
            expect(data[i]).to.have.property("jed_nazev");
            expect(data[i]).to.have.property("jed_zkr");
            expect(data[i]).to.have.property("lt_key");
            expect(data[i]).to.have.property("pot_defcolor");
            expect(data[i]).to.have.property("pot_id");
            expect(data[i]).to.have.property("pot_type");
            expect(data[i]).to.have.property("ptv_id");
        }
    });
});
