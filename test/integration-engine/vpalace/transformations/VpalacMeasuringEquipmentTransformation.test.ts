import { EnergeticsSchema } from "#sch";
import fs from "fs";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai from "chai";
import { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import "mocha";
import { VpalacMeasuringEquipmentTransformation } from "#ie/transformations/vpalace";

chai.use(chaiAsPromised);

describe("VpalacMeasuringEquipmentTransformation", () => {
    let transformation: VpalacMeasuringEquipmentTransformation;
    let testSourceData: Record<string, any>;
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator(
            EnergeticsSchema.vpalac.measuringEquipment.name + "ModelPostgresValidator",
            EnergeticsSchema.vpalac.measuringEquipment.outputJsonSchema
        );
    });

    beforeEach(() => {
        transformation = new VpalacMeasuringEquipmentTransformation();
        const rawData = fs.readFileSync(
            __dirname + "/../../data/energetics-vpalac-measuringequipment-datasource.json"
        ) as unknown;
        testSourceData = JSON.parse(rawData as string);
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("EnergeticsVpalacMeasuringEquipment");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validator.Validate(data)).to.be.fulfilled;

        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("me_do");
            expect(data[i]).to.have.property("me_extid");
            expect(data[i]).to.have.property("me_fakt");
            expect(data[i]).to.have.property("me_id");
            expect(data[i]).to.have.property("me_od");
            expect(data[i]).to.have.property("me_plom");
            expect(data[i]).to.have.property("me_serial");
            expect(data[i]).to.have.property("me_zapoc");
            expect(data[i]).to.have.property("met_id");
            expect(data[i]).to.have.property("mis_id");
            expect(data[i]).to.have.property("mis_nazev");
            expect(data[i]).to.have.property("poc_typode");
            expect(data[i]).to.have.property("pot_id");
            expect(data[i]).to.have.property("umisteni");
            expect(data[i]).to.have.property("var_id");
        }
    });
});
