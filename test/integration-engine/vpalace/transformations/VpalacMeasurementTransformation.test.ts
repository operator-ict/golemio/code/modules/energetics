import { EnergeticsSchema } from "#sch";
import fs from "fs";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai from "chai";
import { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import "mocha";
import { VpalacMeasurementTransformation } from "#ie/transformations/vpalace";

chai.use(chaiAsPromised);

describe("VpalacMeasurementTransformation", () => {
    let transformation: VpalacMeasurementTransformation;
    let testSourceData: Record<string, any>;
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator(
            EnergeticsSchema.vpalac.measurement.name + "ModelPostgresValidator",
            EnergeticsSchema.vpalac.measurement.outputJsonSchema
        );
    });

    beforeEach(() => {
        transformation = new VpalacMeasurementTransformation();
        const rawData = fs.readFileSync(__dirname + "/../../data/energetics-vpalac-measurement-datasource.json") as unknown;
        testSourceData = JSON.parse(rawData as string);
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("EnergeticsVpalacMeasurement");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);

        expect(data).to.have.length(5);
        for (const measurements of data) {
            await expect(validator.Validate(measurements)).to.be.fulfilled;
        }
    });
});
