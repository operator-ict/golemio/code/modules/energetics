import { EnergeticsSchema } from "#sch";
import fs from "fs";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai from "chai";
import { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import "mocha";
import { VpalacTypeMeasuringEquipmentTransformation } from "#ie/transformations/vpalace";

chai.use(chaiAsPromised);

describe("VpalacTypeMeasuringEquipmentTransformation", () => {
    let transformation: VpalacTypeMeasuringEquipmentTransformation;
    let testSourceData: Record<string, any>;
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator(
            EnergeticsSchema.vpalac.typeMeasuringEquipment.name + "ModelPostgresValidator",
            EnergeticsSchema.vpalac.typeMeasuringEquipment.outputJsonSchema
        );
    });

    beforeEach(() => {
        transformation = new VpalacTypeMeasuringEquipmentTransformation();
        const rawData = fs.readFileSync(
            __dirname + "/../../data/energetics-vpalac-typemeasuringequipment-datasource.json"
        ) as unknown;
        testSourceData = JSON.parse(rawData as string);
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("EnergeticsVpalacTypeMeasuringEquipment");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validator.Validate(data)).to.be.fulfilled;

        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("cik_akt");
            expect(data[i]).to.have.property("cik_char");
            expect(data[i]).to.have.property("cik_cislo");
            expect(data[i]).to.have.property("cik_cislo2");
            expect(data[i]).to.have.property("cik_double");
            expect(data[i]).to.have.property("cik_fk");
            expect(data[i]).to.have.property("cik_nazev");
            expect(data[i]).to.have.property("cik_pzn");
            expect(data[i]).to.have.property("cik_zprac");
            expect(data[i]).to.have.property("lt_key");
        }
    });
});
