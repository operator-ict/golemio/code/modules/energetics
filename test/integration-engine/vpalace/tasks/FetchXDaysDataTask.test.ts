import { VPalaceContainer } from "#ie/ioc/Di";
import FetchXDaysDataTask from "#ie/tasks/vpalace/FetchXDaysDataTask";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("FetchXDaysDataTask", () => {
    let sandbox: SinonSandbox;

    before(() => {
        sandbox = sinon.createSandbox();
        PostgresConnector.connect();
    });

    beforeEach(() => {
        VPalaceContainer.registerInstance<ISimpleConfig>(
            CoreToken.SimpleConfig,
            new SimpleConfig({
                module: {
                    UnimonitorCemApiEnergetics: {
                        authCookieName: "CEMAPI",
                        pass: "admin",
                        url: "https://test.neco.eu/api",
                        user: "admin",
                    },
                },
            })
        );
    });

    afterEach(() => {
        sandbox?.restore();
        VPalaceContainer.clearInstances();
    });

    it("should call fetch and save and correct methods", async () => {
        const task = VPalaceContainer.resolve<FetchXDaysDataTask>(FetchXDaysDataTask);
        const fetchAndSaveData = sandbox.stub(task["fetchDataFactory"], "fetchAndSaveData").resolves();
        const fetchToken = sandbox.stub(task["unimonitorCemApi"], "createSession").resolves();
        const terminateSession = sandbox.stub(task["unimonitorCemApi"], "terminateSession").resolves();
        await task.consume({ content: Buffer.from('{"targetDays":2}') } as any);
        expect(fetchAndSaveData.callCount).to.be.equal(2);
        expect(fetchToken.callCount).to.be.equal(1);
        expect(terminateSession.callCount).to.be.equal(1);
    });
});
