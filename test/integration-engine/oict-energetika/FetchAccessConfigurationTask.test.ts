import { CommodityContainer } from "#ie/commodity-providers/ioc/Di";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { AccessLimitationRepository } from "#ie/repositories/oict-energetika/AccessLimitationRepository";
import { FetchAccessConfigurationTask } from "#ie/tasks/oict-energetika/FetchAccessConfigurationTask";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("OictEnergetikaWorker - FetchAccessConfigurationTask", () => {
    let sandbox: SinonSandbox;
    let task: FetchAccessConfigurationTask;
    before(() => {
        const connector = CommodityContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        connector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        task = new FetchAccessConfigurationTask("test.limitAccess");
        sandbox.stub(task["accessConfigurationDataSource"], "getDataSource").callsFake(() =>
            Object.assign({
                getAll: sandbox
                    .stub()
                    .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/../data/access_configuration.json", "utf8"))),
            })
        );
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should execute update access limits", async () => {
        const repository = CommodityContainer.resolve<AccessLimitationRepository>(
            WorkerContainerToken.AccessLimitationRepository
        );

        const result = await repository["sequelizeModel"].findAll({ raw: true });
        expect(result.length).to.equal(5);
        await task.consume({ content: Buffer.from(JSON.stringify({})) } as any);
        const result2 = await repository["sequelizeModel"].findAll({ raw: true });
        expect(result2.length).to.equal(4);
    });
});
