import { OictApiHelper, OictResourceType } from "#ie/datasources/oict-energetika/helpers";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { expect } from "chai";
import sinon from "sinon";

describe("OictApiHelper", () => {
    describe("getResourcePath", () => {
        it("should return a resource path for Buildings", async () => {
            const resourcePath = OictApiHelper.getResourcePath(OictResourceType.Buildings);
            expect(resourcePath).to.equal("buildings/full");
        });

        it("should return a resource path for Consumption", async () => {
            const resourcePath = OictApiHelper.getResourcePath(OictResourceType.Consumption);
            expect(resourcePath).to.equal("data/consumption");
        });

        it("should return a resource path for ConsumptionVisapp", async () => {
            const resourcePath = OictApiHelper.getResourcePath(OictResourceType.ConsumptionVisapp);
            expect(resourcePath).to.equal("data/visapp");
        });

        it("should return a resource path for Devices", async () => {
            const resourcePath = OictApiHelper.getResourcePath(OictResourceType.Devices);
            expect(resourcePath).to.equal("devices/full");
        });
    });

    describe("getValidator", () => {
        it("should return a validator", async () => {
            const validator = OictApiHelper.getValidator(OictResourceType.Buildings);
            expect(validator).to.be.instanceOf(JSONSchemaValidator);
            expect(validator["modelName"]).to.equal("OICTEnergetikaBuildingsValidator");
        });
    });
});
