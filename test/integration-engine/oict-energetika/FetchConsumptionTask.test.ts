import { OictDataSourceFactory } from "#ie/datasources/oict-energetika/OictDataSourceFactory";
import { IFetchConsumptionInput } from "#ie/interfaces/oict-energetika/IFetchConsumptionInput";
import { FetchConsumptionTask } from "#ie/tasks/oict-energetika/FetchConsumptionTask";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("OictEnergetikaWorker - FetchConsumptionTask", () => {
    let sandbox: SinonSandbox;
    let task: FetchConsumptionTask;

    const testParamsValid: IFetchConsumptionInput = {
        dateFrom: "2022-12-30",
        dateTo: "2023-01-05",
    };

    const testParamsInvalid: IFetchConsumptionInput = {
        dateFrom: "2022-12-30",
        dateTo: "2022-01-32",
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        task = new FetchConsumptionTask("test.test");
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should execute", async () => {
        sandbox.stub(OictDataSourceFactory, "getDataSource").returns({ getAll: sandbox.stub() } as any);
        sandbox.stub(task["consumptionTransformation"], "transform").resolves(["test"]);
        const saveStub = sandbox.stub(task["consumptionRepository"], "saveData").resolves();
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsValid)) } as any);

        await expect(promise).to.be.fulfilled;
        expect(saveStub.callCount).to.equal(4);
        expect(saveStub.getCall(0).args).to.deep.equal([["test"]]);
        expect(saveStub.getCall(1).args).to.deep.equal([["test"]]);
    });

    it("should not validate (dateParams must be valid)", async () => {
        const saveStub = sandbox.stub(task["consumptionRepository"], "saveData").resolves();
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsInvalid)) } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            "[Queue test.test.fetchConsumption] Message validation failed: [dateTo must be a valid ISO 8601 date string]"
        );

        expect(saveStub.callCount).to.equal(0);
    });

    it("should reject (fetch error)", async () => {
        sandbox.stub(OictDataSourceFactory, "getDataSource").returns({ getAll: sandbox.stub().rejects() } as any);
        sandbox.stub(task["consumptionTransformation"], "transform").resolves(["test"]);
        const saveStub = sandbox.stub(task["consumptionRepository"], "saveData").resolves();
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsValid)) } as any);

        await expect(promise).to.be.rejectedWith(GeneralError, "Failed to fetch consumption data");
        expect(saveStub.callCount).to.equal(0);
    });

    describe("getDataSources", () => {
        it("should generate data sources", () => {
            config.datasources.OICTEnergetikaApiUrl = "https://test.com";
            const dataSources = Array.from(task["getDataSources"](testParamsValid));

            expect(dataSources).to.have.length(4);
            expect((dataSources[0]["protocolStrategy"] as any)["connectionSettings"].url).to.contain(
                "/data/consumption?dateFrom=2022-12-30&dateTo=2023-01-04"
            );

            expect((dataSources[1]["protocolStrategy"] as any)["connectionSettings"].url).to.contain(
                "/data/visapp?dateFrom=2022-12-30&dateTo=2023-01-04"
            );

            expect((dataSources[2]["protocolStrategy"] as any)["connectionSettings"].url).to.contain(
                "/data/consumption?dateFrom=2023-01-04&dateTo=2023-01-05"
            );

            expect((dataSources[3]["protocolStrategy"] as any)["connectionSettings"].url).to.contain(
                "/data/visapp?dateFrom=2023-01-04&dateTo=2023-01-05"
            );
        });
    });
});
