import { ConsumptionTransformation } from "#ie/transformations/oict-energetika/ConsumptionTransformation";
import { expect } from "chai";
import fs from "fs/promises";
import path from "path";

describe("ConsumptionTransformation", () => {
    const transformation = new ConsumptionTransformation();

    it("should transform consumption data", async () => {
        const data = await fs.readFile(path.resolve(__dirname, "fixture", "consumption-datasource.json"));
        const transformedData = await fs.readFile(path.resolve(__dirname, "fixture", "consumption-transformed.json"));

        const result = await transformation.transform(JSON.parse(data.toString()));
        expect(result).to.deep.equal(JSON.parse(transformedData.toString()));
    });

    it("should transform consumption visapp (Dot Controls) data", async () => {
        const data = await fs.readFile(path.resolve(__dirname, "fixture", "consumption-visapp-datasource.json"));
        const transformedData = await fs.readFile(path.resolve(__dirname, "fixture", "consumption-visapp-transformed.json"));

        const result = await transformation.transform(JSON.parse(data.toString()));
        expect(result).to.deep.equal(JSON.parse(transformedData.toString()));
    });
});
