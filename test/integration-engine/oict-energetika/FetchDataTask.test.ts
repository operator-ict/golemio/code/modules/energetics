import { IFetchDataInput } from "#ie/interfaces/oict-energetika/IFetchDataInput";
import { FetchDataTask } from "#ie/tasks/oict-energetika/FetchDataTask";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("OictEnergetikaWorker - FetchDataTask", () => {
    let sandbox: SinonSandbox;
    let task: FetchDataTask;

    const testParamsValid: IFetchDataInput = {
        targetDays: 2,
    };

    const testParamsInvalid: IFetchDataInput = {
        targetDays: 0,
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        task = new FetchDataTask("test.test");
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        const clock = sinon.useFakeTimers({ now: new Date("2023-01-05T16:32:56.635Z") });
        const msgExchangeStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsValid)) } as any);

        await expect(promise).to.be.fulfilled;
        expect(msgExchangeStub.getCall(0).args).to.deep.equal([
            "test.test",
            "fetchConsumption",
            { dateFrom: "2023-01-03", dateTo: "2023-01-05" },
        ]);

        clock.restore();
    });

    it("should not validate (targetDays must not be less than 1)", async () => {
        const msgExchangeStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsInvalid)) } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            "[Queue test.test.fetchData] Message validation failed: [targetDays must not be less than 1]"
        );

        expect(msgExchangeStub.callCount).to.equal(0);
    });
});
