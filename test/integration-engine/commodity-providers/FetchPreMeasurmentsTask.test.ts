import { CommodityContainer } from "#ie/commodity-providers/ioc/Di";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { FetchPreMeasurementDataTask } from "#ie/commodity-providers/workers/tasks/FetchPreMeasurementDataTask";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("FetchPreMeasurmentsTask", () => {
    let sandbox: SinonSandbox;
    let dataStub: SinonStub;
    const container = IntegrationEngineContainer.createChildContainer();
    const postgresConnector = IntegrationEngineContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
    beforeEach(() => {
        container.register(
            WorkerContainerToken.PreElectroApiDataSourceProvider,
            class DummyFactory {
                getMeasurements = sandbox.stub().callsFake(() => {
                    return {
                        data: ["1"],
                    };
                });
            }
        );
        container.register(
            WorkerContainerToken.PreElectroHelper,
            class DummyFactory {
                filterFilesByDate = dataStub;
            }
        );
        container.register(
            WorkerContainerToken.MeasurementRepository,
            class DummyRepository {
                bulkSave = dataStub;
            }
        );
        container.register(
            WorkerContainerToken.StaticMeterSettingsRepository,
            class DummyRepository {
                getData = dataStub;
            }
        );
        container.register(
            WorkerContainerToken.PreElectroTransformation,
            class DummyRepository {
                transformElement = dataStub;
            }
        );
        container.register(WorkerContainerToken.FetchPreMeasurementDataTask, FetchPreMeasurementDataTask);
    });

    before(async () => {
        sandbox = sinon.createSandbox();
        dataStub = sandbox.stub().callsFake(() => ["1"]);
        await postgresConnector.connect();
        CommodityContainer.registerInstance<ISimpleConfig>(
            CoreToken.SimpleConfig,
            new SimpleConfig({
                module: {
                    energetics: {
                        commodityProviders: {
                            pre_electro_ftp: {
                                url: {
                                    host: "",
                                    port: 1,
                                    user: "h",
                                    password: "c",
                                },
                            },
                        },
                    },
                },
            })
        );
    });

    afterEach(() => {
        sandbox?.restore();
        container.clearInstances();
    });

    it("should load data", async () => {
        const task = container.resolve<FetchPreMeasurementDataTask>(WorkerContainerToken.FetchPreMeasurementDataTask);

        await task["execute"]({
            dateFrom: "2024-04-01",
            dateTo: "2024-04-03",
        });

        expect(dataStub.callCount).to.be.equal(4);
    });
});
