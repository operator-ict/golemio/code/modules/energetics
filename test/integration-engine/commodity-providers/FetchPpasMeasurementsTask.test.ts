import { CommodityProvider } from "#ie/commodity-providers/datasources/helpers/CommodityProviderEnum";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { MeasurementRepository } from "#ie/commodity-providers/repositories/MeasurementRepository";
import { StaticMeterSettingsRepository } from "#ie/commodity-providers/repositories/StaticMeterSettingsRepository";
import { AveMeasurementTransformation } from "#ie/commodity-providers/transformations/AveMeasurementTransformation";
import { FetchPpasMeasurementsTask } from "#ie/commodity-providers/workers/tasks/FetchPpasMeasurementsTask";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("FetchPpasMeasurementsTask", () => {
    let sandbox: SinonSandbox;
    const container = IntegrationEngineContainer.createChildContainer();
    const postgresConnector = IntegrationEngineContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);

    beforeEach(() => {
        container.register(
            CoreToken.SimpleConfig,
            class DummyConfig {
                getValue() {
                    return "http://test.com";
                }
            }
        );
        container.register(WorkerContainerToken.CommodityProviderDataSourceFactory, class DummyFactory {});
        container.register(WorkerContainerToken.AveMeasurementTransformation, AveMeasurementTransformation);
        container.register(WorkerContainerToken.MeasurementRepository, MeasurementRepository);
        container.registerSingleton(
            WorkerContainerToken.StaticMeterSettingsRepository,
            class DummyRepository {
                getData() {
                    return [
                        {
                            MeterSerialNumber: "12077594",
                            Name: "epraoict114.consumption",
                            Addr: "/10.14.1/PF1",
                            Meter: "27ZG100Z0643059J",
                            Type: "provider_value",
                            Var: "core|core2",
                        },
                    ];
                }
            }
        );
        container.register(WorkerContainerToken.FetchPpasMeasurementsTask, FetchPpasMeasurementsTask);
        const fetchStub = sandbox.stub().resolves({
            status: 200,
            json: () =>
                Promise.resolve({
                    success: true,
                    sessionId: "test123",
                }),
        });
        sandbox.replace(globalThis, "fetch", fetchStub);
    });

    before(async () => {
        sandbox = sinon.createSandbox();

        await postgresConnector.connect();
    });

    afterEach(() => {
        sandbox?.restore();
        container.clearInstances();
    });

    it("should fetch and save measurements", async () => {
        const task = container.resolve<FetchPpasMeasurementsTask>(WorkerContainerToken.FetchPpasMeasurementsTask);
        sandbox.stub(task, "getDataFromDataSource" as any).callsFake((resType, _auth, params: any) => {
            if (resType === "Places") {
                return [
                    {
                        Id: 1,
                        DeviceSerialNumber: "12077594",
                        Eic: "27ZG100Z0643059J",
                    },
                ];
            } else if (resType === "DeviceData") {
                return [
                    {
                        StandardTime: "2024-03-16T06:00:00",
                        OperatingAmount: 35.80000000000291,
                        ConvertDifference: 0,
                    },
                ];
            }
        });

        await task.execute({
            providerType: CommodityProviderType.PpasAveV,
            dateFrom: "2024-01-01",
            dateTo: "2024-01-02",
        });

        const measurements = await task["measurementRepository"]["sequelizeModel"].findAll({
            attributes: {
                exclude: ["created_at", "updated_at"],
            },
            where: {
                source: CommodityProvider.PpasInternetAveApi,
            },
            raw: true,
        });

        expect(measurements).to.have.length(1);
        expect(measurements[0]).deep.equal({
            timestamp: new Date("2024-03-16T05:00:00.000Z"),
            value: "35.800000000002910",
            addr: "/10.14.1/PF1",
            variable: "core",
            type: "provider_value",
            meter: "27ZG100Z0643059J",
            measurement_category: "epraoict114.consumption",
            source: CommodityProvider.PpasInternetAveApi,
        });
    });

    it("should not save duplicated measurements", async () => {
        const task = container.resolve<FetchPpasMeasurementsTask>(WorkerContainerToken.FetchPpasMeasurementsTask);
        sandbox.stub(task, "getDataFromDataSource" as any).callsFake((resType, _auth, params: any) => {
            if (resType === "Places") {
                return [
                    {
                        Id: 1,
                        DeviceSerialNumber: "12077594",
                        Eic: "27ZG100Z0643059J",
                    },
                ];
            } else if (resType === "DeviceData") {
                return [
                    {
                        StandardTime: "2024-03-16T06:00:00",
                        OperatingAmount: 35.80000000000291,
                        ConvertDifference: 0,
                    },
                    {
                        StandardTime: "2024-03-16T06:00:00",
                        OperatingAmount: 35.80000000000291,
                        ConvertDifference: 0,
                    },
                ];
            }
        });

        await task.execute({
            providerType: CommodityProviderType.PpasAveV,
            dateFrom: "2024-01-01",
            dateTo: "2024-01-02",
        });

        const measurements = await task["measurementRepository"]["sequelizeModel"].findAll({
            attributes: {
                exclude: ["created_at", "updated_at"],
            },
            where: {
                source: CommodityProvider.PpasInternetAveApi,
            },
            raw: true,
        });

        expect(measurements).to.have.length(1);
    });

    it("should throw an error when measurementOptions are not found", async () => {
        const task = container.resolve<FetchPpasMeasurementsTask>(WorkerContainerToken.FetchPpasMeasurementsTask);
        container.resolve<StaticMeterSettingsRepository>(WorkerContainerToken.StaticMeterSettingsRepository).getData = () =>
            Promise.resolve(null);

        const error = await task
            .execute({
                providerType: CommodityProviderType.PpasAveV,
                dateFrom: "2024-01-01",
                dateTo: "2024-01-02",
            })
            .catch((e) => e);

        expect(error.message).to.equal("Measurement options not found");
    });

    describe("splitOptionsByVars", () => {
        it("should split options by vars", async () => {
            const task = container.resolve<FetchPpasMeasurementsTask>(WorkerContainerToken.FetchPpasMeasurementsTask);
            const options = [
                {
                    MeterSerialNumber: "12077594",
                    Name: "epraoict114.consumption",
                    Addr: "/10.14.1/PF1",
                    Meter: "27ZG100Z0643059J",
                    Type: "provider_value",
                    Var: "core|core2",
                },
                {
                    MeterSerialNumber: "7692",
                    Name: "epraoict114.consumption",
                    Addr: "/10.14.2/PF1",
                    Meter: "27ZG100Z0015383N",
                    Type: "provider_value",
                    Var: "core",
                },
            ];

            const result = task["splitOptionsByVars"](options);
            expect(result).deep.equal([
                {
                    MeterSerialNumber: "12077594",
                    Name: "epraoict114.consumption",
                    Addr: "/10.14.1/PF1",
                    Meter: "27ZG100Z0643059J",
                    Type: "provider_value",
                    Var: "core",
                },
                {
                    MeterSerialNumber: "12077594",
                    Name: "epraoict114.consumption",
                    Addr: "/10.14.1/PF1",
                    Meter: "27ZG100Z0643059J",
                    Type: "provider_value",
                    Var: "core2",
                },
                {
                    MeterSerialNumber: "7692",
                    Name: "epraoict114.consumption",
                    Addr: "/10.14.2/PF1",
                    Meter: "27ZG100Z0015383N",
                    Type: "provider_value",
                    Var: "core",
                },
            ]);
        });
    });
});
