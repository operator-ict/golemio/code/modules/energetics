import { PreElectroHelper } from "#ie/commodity-providers/datasources/pre-elektro-api/helpers/PreElectroHelper";
import { CommodityContainer } from "#ie/commodity-providers/ioc/Di";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("PreElectroHelper", () => {
    let sandbox: SinonSandbox;
    const container = IntegrationEngineContainer.createChildContainer();

    before(async () => {
        sandbox = sinon.createSandbox();
        CommodityContainer.registerInstance<ISimpleConfig>(
            CoreToken.SimpleConfig,
            new SimpleConfig({
                module: {
                    energetics: {
                        commodityProviders: {
                            pre_electro_ftp: {
                                url: {
                                    host: "",
                                    port: 1,
                                    user: "h",
                                    password: "c",
                                },
                            },
                        },
                    },
                },
            })
        );
    });

    afterEach(() => {
        sandbox?.restore();
        container.clearInstances();
    });

    it("filters out correct file names according to date", async () => {
        const helper = CommodityContainer.resolve<PreElectroHelper>(WorkerContainerToken.PreElectroHelper);

        sandbox
            .stub(helper, "listAllFiles")
            .resolves([
                { name: "2024-04-01-095500.csv" },
                { name: "2024-04-02-095500.csv" },
                { name: "2024-04-03-095500.csv" },
                { name: "2024-04-04-095500.csv" },
                { name: "2024-04-05-095500.csv" },
            ]);

        const result = await helper.filterFilesByDate("2024-04-01", "2024-04-03");

        expect(result).to.deep.equal(["2024-04-01-095500.csv", "2024-04-02-095500.csv", "2024-04-03-095500.csv"]);
    });
});
