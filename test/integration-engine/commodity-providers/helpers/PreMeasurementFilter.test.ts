import { IPreEanMeasurement } from "#ie/commodity-providers/workers/interfaces/IPreEanMeasurement";
import { expect } from "chai";
import { PreMeasurementFilter } from "#ie/commodity-providers/helper/PreMeasurementFilter";

describe("PreMeasurementFilter", () => {
    it("sums duplicates correctly", () => {
        const input: IPreEanMeasurement = {
            ean: "1234",
            T1_Wh_raw_data: [
                ["31.10.2021 02:30", 5],
                ["31.10.2021 02:45", 4],
                ["31.10.2021 02:30", 10],
            ],
        };

        const expected: IPreEanMeasurement = {
            ean: "1234",
            T1_Wh_raw_data: [
                ["31.10.2021 02:30", 15],
                ["31.10.2021 02:45", 4],
            ],
        };

        expect(PreMeasurementFilter.sumDuplicates(input)).to.deep.eq(expected);
    });
});
