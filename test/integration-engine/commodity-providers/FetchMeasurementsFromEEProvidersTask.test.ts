import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { FetchMeasurementsFromEEProvidersTask } from "#ie/commodity-providers/workers/tasks/FetchMeasurementsFromEEProvidersTask";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonFakeTimers, SinonSandbox } from "sinon";

describe("FetchMeasurementsFromEEProvidersTask", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    const container = IntegrationEngineContainer.createChildContainer();

    beforeEach(() => {
        container.register(WorkerContainerToken.FetchMeasurementsFromEEProvidersTask, FetchMeasurementsFromEEProvidersTask);
        clock = sinon.useFakeTimers({
            now: new Date("2023-10-10T12:00:00Z"),
        });
    });

    before(async () => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
        container.clearInstances();
    });

    it("should send messages to fetchPpasEEMeasurements", async () => {
        const sendStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();
        const task = container.resolve<FetchMeasurementsFromEEProvidersTask>(
            WorkerContainerToken.FetchMeasurementsFromEEProvidersTask
        );

        await task.execute({ targetDays: 2 });

        expect(sendStub.callCount).to.equal(3);
        expect(sendStub.getCall(0).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_a",
            dateFrom: "2023-10-08",
            dateTo: "2023-10-11",
        });
        expect(sendStub.getCall(1).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_c",
            dateFrom: "2023-10-08",
            dateTo: "2023-10-11",
        });
        expect(sendStub.getCall(2).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_mo",
            dateFrom: "2023-10-08",
            dateTo: "2023-10-11",
        });
    });

    it("should send messages to fetchPpasEEMeasurements with 59 days interval", async () => {
        const sendStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();
        const task = container.resolve<FetchMeasurementsFromEEProvidersTask>(
            WorkerContainerToken.FetchMeasurementsFromEEProvidersTask
        );

        await task.execute({ targetDays: 60 });

        expect(sendStub.callCount).to.equal(6);
        expect(sendStub.getCall(0).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_a",
            dateFrom: "2023-08-11",
            dateTo: "2023-09-11",
        });
        expect(sendStub.getCall(1).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_c",
            dateFrom: "2023-08-11",
            dateTo: "2023-09-11",
        });
        expect(sendStub.getCall(2).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_mo",
            dateFrom: "2023-08-11",
            dateTo: "2023-09-11",
        });
        expect(sendStub.getCall(3).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_a",
            dateFrom: "2023-09-11",
            dateTo: "2023-10-11",
        });
        expect(sendStub.getCall(4).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_c",
            dateFrom: "2023-09-11",
            dateTo: "2023-10-11",
        });
        expect(sendStub.getCall(5).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_mo",
            dateFrom: "2023-09-11",
            dateTo: "2023-10-11",
        });
    });
});
