import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { FetchPtasWebScrapedMeasurementsTask } from "#ie/commodity-providers/workers/tasks/FetchPtasWebScrapedMeasurementsTask";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { ISelectedMeters } from "#sch/datasources/interfaces/IStaticMeterSettings";

describe("FetchPtasWebScrapedMeasurementsTask", () => {
    let sandbox: SinonSandbox;
    let transformStub: SinonStub;
    let getStaticDataStub: SinonStub;
    let bulkSaveStub: SinonStub;

    const selectedMeters: ISelectedMeters = {
        "3050028": "69112600",
        "3011693": "1011693",
    };

    const staticMeasurementOptions = [
        {
            ConsumerId: "1001617",
            MeterId: "0003011693",
            Name: "epraoict101.consumption",
            Addr: "/10.1/TF1",
            Meter: "3011693",
            Type: "provider_value",
            Var: "core",
        },
    ];

    const container = IntegrationEngineContainer.createChildContainer();
    const postgresConnector = IntegrationEngineContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
    beforeEach(() => {
        container.register(
            WorkerContainerToken.PreElectroApiDataSourceProvider,
            class DummyFactory {
                getMeasurements = sandbox.stub().callsFake(() => {
                    return {
                        data: ["1"],
                    };
                });
            }
        );
        container.register(
            WorkerContainerToken.MeasurementRepository,
            // MeasurementRepository
            class DummyRepository {
                bulkSave = bulkSaveStub;
            }
        );
        container.register(
            WorkerContainerToken.StaticMeterSettingsRepository,
            // StaticMeterSettingsRepository
            class DummyRepository {
                getData = getStaticDataStub;
            }
        );
        container.register(
            WorkerContainerToken.PtasWebMeasurementTransformation,
            // PtasWebMeasurementTransformation
            class DummyTransformation {
                transformElement = transformStub;
            }
        );
        container.register(WorkerContainerToken.FetchPtasWebScrapedMeasurementsTask, FetchPtasWebScrapedMeasurementsTask);
    });

    before(async () => {
        sandbox = sinon.createSandbox();
        transformStub = sandbox.stub().callsFake(() => ["1"]);
        getStaticDataStub = sandbox
            .stub()
            .callsFake((ptas: string, option: string) =>
                option === "SelectedMeters" ? selectedMeters : staticMeasurementOptions
            );
        bulkSaveStub = sandbox.stub().resolves();

        await postgresConnector.connect();
    });

    afterEach(() => {
        sandbox?.restore();
        container.clearInstances();
    });

    it("should call all methods", async () => {
        const task = container.resolve<FetchPtasWebScrapedMeasurementsTask>(
            WorkerContainerToken.FetchPtasWebScrapedMeasurementsTask
        );
        const data = {
            placeId: "3011693",
            measurements: [
                {
                    "Datum odečtu": "25.4.2024",
                    "Čas odečtu": "0:56",
                    "Výrobní číslo": "1011693",
                    "Druh registru": "Teplo horké vody",
                    Odečet: "2 968,59",
                    "MJ odečtu": "GJ",
                    "MJ zúčt.": "GJ",
                    "OM závislé": "GJ",
                    "Typ odečtu": "",
                    "Status odečtu": "",
                },
            ],
        };
        await task["execute"](data);

        sandbox.assert.calledTwice(getStaticDataStub);
        sandbox.assert.calledOnce(transformStub);
        sandbox.assert.calledOnce(bulkSaveStub);
    });
});
