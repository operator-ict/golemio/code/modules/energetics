import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { FetchMeasurementOptionsStaticDataTask } from "#ie/commodity-providers/workers/tasks/FetchMeasurementOptionsStaticDataTask";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("FetchMeasurementOptionsStaticDataTask", () => {
    let sandbox: SinonSandbox;
    const container = IntegrationEngineContainer.createChildContainer();

    beforeEach(() => {
        container.registerSingleton(WorkerContainerToken.StaticMeterSettingsDataSourceProvider, class DummyProvider {});
        container.registerSingleton(WorkerContainerToken.StaticMeterSettingsRepository, class DummyRepository {});
        container.register(WorkerContainerToken.FetchMeasurementOptionsStaticDataTask, FetchMeasurementOptionsStaticDataTask);
        container.register(WorkerContainerToken.StaticMeterSettingsDataSourceCapitalDs, class DummyProvider {});
    });

    before(async () => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox?.restore();
        container.clearInstances();
    });

    it("should save measurement options", async () => {
        const saveDataStub = sandbox.stub().resolves();
        const task = container.resolve<FetchMeasurementOptionsStaticDataTask>(
            WorkerContainerToken.FetchMeasurementOptionsStaticDataTask
        );
        task["meterSettingsRepository"] = { saveData: saveDataStub } as any;
        task["staticDataSourceProvider"] = {
            getDataSource: () => ({
                getAll: () =>
                    Promise.resolve({
                        CemWaterOptions: { MeasurementOptions: [{}] },
                        CemElectroOptions: { MeasurementOptions: [{}] },
                        AveMOptions: { MeasurementOptions: [{}] },
                        AveVOptions: { MeasurementOptions: [{}] },
                        PreOptions: { MeasurementOptions: [{}] },
                        PtasOptions: { MeasurementOptions: [{}] },
                        PreInputOptions: { MeasurementOptions: [{}] },
                    }),
            }),
        } as any;
        task["meterSettingsRepositoryCapitalDistrict"] = {
            getDataSource: () => ({
                getAll: () =>
                    Promise.resolve({
                        "AveHl.m.Prahaptions": { MeasurementOptions: [{}] },
                        "AveHlavni.m.Prahaptions": { MeasurementOptions: [{}] },
                        "AveHlavni.mesto.Praha.64581ptions": { MeasurementOptions: [{}] },
                    }),
            }),
        } as any;

        await task["execute"]();

        expect(saveDataStub.callCount).to.equal(10);
    });

    it("should not save measurement options if none found", async () => {
        const saveDataStub = sandbox.stub().resolves();
        const task = container.resolve<FetchMeasurementOptionsStaticDataTask>(
            WorkerContainerToken.FetchMeasurementOptionsStaticDataTask
        );
        task["meterSettingsRepository"] = { saveData: saveDataStub } as any;
        task["staticDataSourceProvider"] = {
            getDataSource: () => ({
                getAll: () =>
                    Promise.resolve({
                        CemWaterOptions: { MeasurementOptions: [] },
                        CemElectroOptions: { MeasurementOptions: [] },
                        AveMOptions: { MeasurementOptions: [] },
                        AveVOptions: { MeasurementOptions: [] },
                        PreOptions: { MeasurementOptions: [] },
                        PtasOptions: { MeasurementOptions: [] },
                        PreInputOptions: { MeasurementOptions: [] },
                    }),
            }),
        } as any;
        task["meterSettingsRepositoryCapitalDistrict"] = {
            getDataSource: () => ({
                getAll: () =>
                    Promise.resolve({
                        "AveHl.m.Prahaptions": { MeasurementOptions: [{}] },
                        "AveHlavni.m.Prahaptions": { MeasurementOptions: [{}] },
                        "AveHlavni.mesto.Praha.64581ptions": { MeasurementOptions: [{}] },
                    }),
            }),
        } as any;

        await task["execute"]();

        expect(saveDataStub.callCount).to.equal(3);
    });

    it("should throw error if no measurement options found", async () => {
        const task = container.resolve<FetchMeasurementOptionsStaticDataTask>(
            WorkerContainerToken.FetchMeasurementOptionsStaticDataTask
        );
        task["meterSettingsRepository"] = { saveData: () => Promise.resolve() } as any;
        task["staticDataSourceProvider"] = {
            getDataSource: () => ({
                getAll: () => Promise.resolve({}),
            }),
        } as any;
        task["meterSettingsRepositoryCapitalDistrict"] = {
            getDataSource: () => ({
                getAll: () => Promise.resolve({}),
            }),
        } as any;

        try {
            await task["execute"]();
            expect.fail("Should throw error");
        } catch (e) {
            expect(e.message).to.equal("No measurements options found for key: CemWaterOptions");
        }
    });
});
