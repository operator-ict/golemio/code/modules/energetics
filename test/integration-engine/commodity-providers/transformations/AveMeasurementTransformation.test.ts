import { AveMeasurementTransformation } from "#ie/commodity-providers/transformations/AveMeasurementTransformation";
import { expect } from "chai";

describe("AveMeasurementTransformation", () => {
    it("should transform data (core)", () => {
        const transformation = new AveMeasurementTransformation();
        const data = [
            {
                MeterSerialNumber: "12077594",
                Name: "epraoict114.consumption",
                Addr: "/10.14.1/PF1",
                Meter: "27ZG100Z0643059J",
                Type: "provider_value",
                Var: "core",
                StandardTime: "2024-03-16T06:00:00",
                OperatingAmount: 35.80000000000291,
                ConvertDifference: 0,
            },
        ];

        const result = transformation.transformArray(data);
        expect(result).to.deep.equal([
            {
                source: "ppas_ave_api",
                measurement_category: "epraoict114.consumption",
                addr: "/10.14.1/PF1",
                meter: "27ZG100Z0643059J",
                timestamp: new Date("2024-03-16T05:00:00.000Z"),
                variable: "core",
                type: "provider_value",
                value: 35.80000000000291,
            },
        ]);
    });

    it("should transform data (core2)", () => {
        const transformation = new AveMeasurementTransformation();
        const data = [
            {
                MeterSerialNumber: "12077594",
                Name: "epraoict114.consumption",
                Addr: "/10.14.1/PF1",
                Meter: "27ZG100Z0643059J",
                Type: "provider_value",
                Var: "core2",
                StandardTime: "2024-03-16T06:00:00",
                OperatingAmount: 35.80000000000291,
                ConvertDifference: 0,
            },
        ];

        const result = transformation.transformArray(data);
        expect(result).to.deep.equal([
            {
                source: "ppas_ave_api",
                measurement_category: "epraoict114.consumption",
                addr: "/10.14.1/PF1",
                meter: "27ZG100Z0643059J",
                timestamp: new Date("2024-03-16T05:00:00.000Z"),
                variable: "core2",
                type: "provider_value",
                value: 0,
            },
        ]);
    });
});
