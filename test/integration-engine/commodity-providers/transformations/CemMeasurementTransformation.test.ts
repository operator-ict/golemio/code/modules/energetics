import { CemMeasurementTransformation } from "#ie/commodity-providers/transformations/CemMeasurementTransformation";
import { UnimonitorCemApiHelper } from "#ie/helpers";
import { expect } from "chai";

describe("CemMeasurementTransformation", () => {
    it("should transform data (high tariff)", () => {
        const transformation = new CemMeasurementTransformation();
        const data = [
            {
                MeterSerialNumber: "3706359",
                Name: "epraoict107.consumption",
                Addr: "/10.7.1/EF1",
                Meter: "859182400306528517",
                Type: "provider_value",
                Var: "core",
                timestamp: 1649771280000,
                value: 100,
                counterTypeId: UnimonitorCemApiHelper.counterType.ElHighTariff,
            },
        ];

        const result = transformation.transformArray(data);
        expect(result).to.deep.equal([
            {
                source: "cem_api",
                measurement_category: "epraoict107.consumption",
                addr: "/10.7.1/EF1",
                meter: "859182400306528517",
                timestamp: new Date(1649771280000),
                variable: "VT",
                type: "provider_value",
                value: 100,
            },
        ]);
    });

    it("should transform data (low tariff)", () => {
        const transformation = new CemMeasurementTransformation();
        const data = [
            {
                MeterSerialNumber: "3706359",
                Name: "epraoict107.consumption",
                Addr: "/10.7.1/EF1",
                Meter: "859182400306528517",
                Type: "provider_value",
                Var: "core",
                timestamp: 1649771280000,
                value: 100,
                counterTypeId: UnimonitorCemApiHelper.counterType.ElLowTariff,
            },
        ];

        const result = transformation.transformArray(data);
        expect(result).to.deep.equal([
            {
                source: "cem_api",
                measurement_category: "epraoict107.consumption",
                addr: "/10.7.1/EF1",
                meter: "859182400306528517",
                timestamp: new Date(1649771280000),
                variable: "NT",
                type: "provider_value",
                value: 100,
            },
        ]);
    });

    it("should transform data (unknown tariff)", () => {
        const transformation = new CemMeasurementTransformation();
        const data = [
            {
                MeterSerialNumber: "3706359",
                Name: "epraoict107.consumption",
                Addr: "/10.7.1/EF1",
                Meter: "859182400306528517",
                Type: "provider_value",
                Var: "core",
                timestamp: 1649771280000,
                value: 100,
                counterTypeId: 0,
            },
        ];

        const result = transformation.transformArray(data);
        expect(result).to.deep.equal([
            {
                source: "cem_api",
                measurement_category: "epraoict107.consumption",
                addr: "/10.7.1/EF1",
                meter: "859182400306528517",
                timestamp: new Date(1649771280000),
                variable: "core",
                type: "provider_value",
                value: 100,
            },
        ]);
    });
});
