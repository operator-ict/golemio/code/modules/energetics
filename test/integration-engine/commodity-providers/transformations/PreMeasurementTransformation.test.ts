import { PreMeasurementTransformation } from "#ie/commodity-providers/transformations/PreMeasurementTransformation";
import { preEanMeasurement } from "../../../input-gateway/data/preEanMeasurement";
import { expect } from "chai";
import { transformedPreMeasurements } from "../../../input-gateway/data/transformedPreMeasurements";

describe("PreMeasurementTransformation", () => {
    const transformation = new PreMeasurementTransformation();

    it("transforms data correctly", () => {
        const option = {
            Name: "epraoict15.consumption",
            Addr: "/2.15/EF1",
            Meter: "859182400306074267",
            MeterSerialNumber: "53435572",
            Var: "EFwActi",
            Type: "provider_value",
        };
        const measurementInput = preEanMeasurement;
        const actual = transformation.transformElement({ option, measurementInput });
        expect(actual).to.deep.eq(transformedPreMeasurements);
    });
});
