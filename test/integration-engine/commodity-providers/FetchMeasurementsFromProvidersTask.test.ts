import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { FetchMeasurementsFromProvidersTask } from "#ie/commodity-providers/workers/tasks/FetchMeasurementsFromProvidersTask";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonFakeTimers, SinonSandbox } from "sinon";

describe("FetchMeasurementsFromProvidersTask", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    const container = IntegrationEngineContainer.createChildContainer();

    beforeEach(() => {
        container.register(WorkerContainerToken.FetchMeasurementsFromProvidersTask, FetchMeasurementsFromProvidersTask);
        clock = sinon.useFakeTimers({
            now: new Date("2023-10-10T12:00:00Z"),
        });
    });

    before(async () => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
        container.clearInstances();
    });

    it("should send messages to fetchVeoliaMeasurements", async () => {
        const sendStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();
        const task = container.resolve<FetchMeasurementsFromProvidersTask>(
            WorkerContainerToken.FetchMeasurementsFromProvidersTask
        );

        await task.execute({ targetDays: 2 });

        expect(sendStub.callCount).to.equal(5);
        expect(sendStub.getCall(0).args[2]).to.deep.equal({
            providerType: "cem_api_veolia_water",
            dateFrom: "2023-10-08",
            dateTo: "2023-10-11",
        });

        expect(sendStub.getCall(1).args[2]).to.deep.equal({
            providerType: "cem_api_veolia_electro",
            dateFrom: "2023-10-08",
            dateTo: "2023-10-11",
        });

        expect(sendStub.getCall(2).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_v",
            dateFrom: "2023-10-08",
            dateTo: "2023-10-11",
        });

        expect(sendStub.getCall(3).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_m",
            dateFrom: "2023-10-08",
            dateTo: "2023-10-11",
        });
    });

    it("should send messages to fetchVeoliaMeasurements with 59 days interval", async () => {
        const sendStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();
        const task = container.resolve<FetchMeasurementsFromProvidersTask>(
            WorkerContainerToken.FetchMeasurementsFromProvidersTask
        );

        await task.execute({ targetDays: 60 });

        expect(sendStub.callCount).to.equal(10);
        expect(sendStub.getCall(0).args[2]).to.deep.equal({
            providerType: "cem_api_veolia_water",
            dateFrom: "2023-08-11",
            dateTo: "2023-09-11",
        });

        expect(sendStub.getCall(1).args[2]).to.deep.equal({
            providerType: "cem_api_veolia_electro",
            dateFrom: "2023-08-11",
            dateTo: "2023-09-11",
        });

        expect(sendStub.getCall(2).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_v",
            dateFrom: "2023-08-11",
            dateTo: "2023-09-11",
        });

        expect(sendStub.getCall(3).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_m",
            dateFrom: "2023-08-11",
            dateTo: "2023-09-11",
        });

        expect(sendStub.getCall(4).args[2]).to.deep.equal({
            dateFrom: "2023-08-11",
            dateTo: "2023-09-11",
        });

        expect(sendStub.getCall(5).args[2]).to.deep.equal({
            providerType: "cem_api_veolia_water",
            dateFrom: "2023-09-11",
            dateTo: "2023-10-11",
        });

        expect(sendStub.getCall(6).args[2]).to.deep.equal({
            providerType: "cem_api_veolia_electro",
            dateFrom: "2023-09-11",
            dateTo: "2023-10-11",
        });

        expect(sendStub.getCall(7).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_v",
            dateFrom: "2023-09-11",
            dateTo: "2023-10-11",
        });

        expect(sendStub.getCall(8).args[2]).to.deep.equal({
            providerType: "ppas_ave_api_m",
            dateFrom: "2023-09-11",
            dateTo: "2023-10-11",
        });
        expect(sendStub.getCall(9).args[2]).to.deep.equal({
            dateFrom: "2023-09-11",
            dateTo: "2023-10-11",
        });
    });
});
