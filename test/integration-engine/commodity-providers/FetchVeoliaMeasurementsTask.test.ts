import { CommodityProvider } from "#ie/commodity-providers/datasources/helpers/CommodityProviderEnum";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { MeasurementRepository } from "#ie/commodity-providers/repositories/MeasurementRepository";
import { StaticMeterSettingsRepository } from "#ie/commodity-providers/repositories/StaticMeterSettingsRepository";
import { CemMeasurementTransformation } from "#ie/commodity-providers/transformations/CemMeasurementTransformation";
import { FetchVeoliaMeasurementsTask } from "#ie/commodity-providers/workers/tasks/FetchVeoliaMeasurementsTask";
import { UnimonitorCemApiHelper } from "#ie/helpers";
import { CommodityProviderType } from "#sch/models/helpers/CommodityProviderTypeEnum";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("FetchVeoliaMeasurementsTask", () => {
    let sandbox: SinonSandbox;
    const container = IntegrationEngineContainer.createChildContainer();
    const postgresConnector = IntegrationEngineContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);

    beforeEach(() => {
        container.register(
            CoreToken.SimpleConfig,
            class DummyConfig {
                getValue() {
                    return "";
                }
            }
        );
        container.register(WorkerContainerToken.CommodityProviderDataSourceFactory, class DummyFactory {});
        container.register(WorkerContainerToken.CemMeasurementTransformation, CemMeasurementTransformation);
        container.register(WorkerContainerToken.MeasurementRepository, MeasurementRepository);
        container.registerSingleton(
            WorkerContainerToken.StaticMeterSettingsRepository,
            class DummyRepository {
                getData() {
                    return [
                        {
                            MeterSerialNumber: "3706359",
                            Name: "epraoict107.consumption",
                            Addr: "/10.7.1/EF1",
                            Meter: "859182400306528517",
                            Type: "provider_value",
                            Var: "core",
                        },
                    ];
                }
            }
        );
        container.register(WorkerContainerToken.FetchVeoliaMeasurementsTask, FetchVeoliaMeasurementsTask);
        const fetchStub = sandbox.stub().resolves({
            status: 200,
            headers: {
                has: () => true,
                get: () => ["cookie"],
                getSetCookie: () => ["cookie"],
            },
        });
        sandbox.replace(globalThis, "fetch", fetchStub);
    });

    before(async () => {
        sandbox = sinon.createSandbox();

        await postgresConnector.connect();
    });

    afterEach(() => {
        sandbox?.restore();
        container.clearInstances();
    });

    it("should fetch and save measurements", async () => {
        const task = container.resolve<FetchVeoliaMeasurementsTask>(WorkerContainerToken.FetchVeoliaMeasurementsTask);
        sandbox.stub(task, "getDataFromDataSource" as any).callsFake((resType, _auth, params: any) => {
            if (resType === "Devices") {
                return [
                    {
                        me_id: 1,
                        me_serial: "3706359",
                    },
                ];
            } else if (resType === "Counters") {
                return [
                    {
                        me_id: 1,
                        var_id: 69,
                        pot_id: UnimonitorCemApiHelper.counterType.ElHighTariff,
                    },
                ];
            } else if (resType === "MeasurementsByCounter") {
                if (params?.counterId === 69) {
                    return [
                        {
                            timestamp: 1649771280000,
                            value: 100,
                        },
                    ];
                }
            }
        });

        await task.execute({
            providerType: CommodityProviderType.CemApiVeoliaElectro,
            dateFrom: "2024-01-01",
            dateTo: "2024-01-02",
        });

        const measurements = await task["measurementRepository"]["sequelizeModel"].findAll({
            attributes: {
                exclude: ["created_at", "updated_at"],
            },
            where: {
                source: CommodityProvider.UnimonitorCemApi,
            },
            raw: true,
        });

        expect(measurements).to.have.length(1);
        expect(measurements[0]).deep.equal({
            timestamp: new Date(1649771280000),
            value: "100.000000000000000",
            addr: "/10.7.1/EF1",
            variable: "VT",
            type: "provider_value",
            meter: "859182400306528517",
            measurement_category: "epraoict107.consumption",
            source: "cem_api",
        });
    });

    it("should not save duplicated measurements", async () => {
        const task = container.resolve<FetchVeoliaMeasurementsTask>(WorkerContainerToken.FetchVeoliaMeasurementsTask);
        sandbox.stub(task, "getDataFromDataSource" as any).callsFake((resType, _auth, params: any) => {
            if (resType === "Devices") {
                return [
                    {
                        me_id: 1,
                        me_serial: "3706359",
                    },
                ];
            } else if (resType === "Counters") {
                return [
                    {
                        me_id: 1,
                        var_id: 69,
                        pot_id: UnimonitorCemApiHelper.counterType.ElHighTariff,
                    },
                ];
            } else if (resType === "MeasurementsByCounter") {
                if (params?.counterId === 69) {
                    return [
                        {
                            timestamp: new Date(1649771280000).getTime(),
                            value: 100,
                        },
                        {
                            timestamp: new Date(1649771280000).getTime(),
                            value: 100,
                        },
                    ];
                }
            }
        });

        await task.execute({
            providerType: CommodityProviderType.CemApiVeoliaElectro,
            dateFrom: "2024-01-01",
            dateTo: "2024-01-02",
        });

        const measurements = await task["measurementRepository"]["sequelizeModel"].findAll({
            attributes: {
                exclude: ["created_at", "updated_at"],
            },
            where: {
                source: CommodityProvider.UnimonitorCemApi,
            },
            raw: true,
        });

        expect(measurements).to.have.length(1);
    });

    it("should throw an error when measurementOptions are not found", async () => {
        const task = container.resolve<FetchVeoliaMeasurementsTask>(WorkerContainerToken.FetchVeoliaMeasurementsTask);
        container.resolve<StaticMeterSettingsRepository>(WorkerContainerToken.StaticMeterSettingsRepository).getData = () =>
            Promise.resolve(null);

        const error = await task
            .execute({
                providerType: CommodityProviderType.CemApiVeoliaElectro,
                dateFrom: "2024-01-01",
                dateTo: "2024-01-02",
            })
            .catch((e) => e);

        expect(error.message).to.equal("Measurement options not found");
    });
});
