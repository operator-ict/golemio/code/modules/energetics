import sinon, { SinonSandbox, SinonStub } from "sinon";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { WorkerContainerToken } from "#ie/commodity-providers/ioc/WorkerContainerToken";
import { ProcessPreEanMeasurementsTask } from "#ie/commodity-providers/workers/tasks/ProcessPreEanMeasurementsTask";
import { IPreEanMeasurement } from "#ie/commodity-providers/workers/interfaces/IPreEanMeasurement";

describe("ProcessPreEanMeasurementsTask", () => {
    let sandbox: SinonSandbox;
    let meterSettingsStub: SinonStub;
    let transformationStub: SinonStub;
    let repositoryStub: SinonStub;
    const container = IntegrationEngineContainer.createChildContainer();
    const postgresConnector = IntegrationEngineContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
    beforeEach(() => {
        container.register(
            WorkerContainerToken.MeasurementRepository,
            class DummyRepository {
                bulkSave = repositoryStub;
            }
        );
        container.register(
            WorkerContainerToken.StaticMeterSettingsRepository,
            class DummyRepository {
                getData = meterSettingsStub;
            }
        );
        container.register(
            WorkerContainerToken.PreMeasurementTransformation,
            class Transformation {
                transformElement = transformationStub;
            }
        );
        container.register(WorkerContainerToken.ProcessPreEanMeasurementsTask, ProcessPreEanMeasurementsTask);
    });

    before(async () => {
        sandbox = sinon.createSandbox();
        meterSettingsStub = sandbox.stub().resolves([
            {
                MeterSerialNumber: "1234",
                Name: "epraoict15.consumption",
                Addr: "/2.15/EF1",
                Meter: "859182400306074267",
                Type: "sprovider_valuetring",
                Var: "EFwActi",
            },
        ]);
        transformationStub = sandbox.stub().resolves([{}]);
        repositoryStub = sandbox.stub().resolves();
        await postgresConnector.connect();
    });

    afterEach(() => {
        sandbox?.restore();
        container.clearInstances();
    });

    it("should call all methods", async () => {
        const task = container.resolve<ProcessPreEanMeasurementsTask>(WorkerContainerToken.ProcessPreEanMeasurementsTask);
        const inputData: IPreEanMeasurement = {
            ean: "859182400306074267",
            T1_Wh_raw_data: [
                ["30.11.2024 00:15", 10],
                ["30.11.2024 00:30", 12],
            ],
        };

        await task["execute"](inputData);

        sandbox.assert.calledOnce(meterSettingsStub);
        sandbox.assert.calledOnce(transformationStub);
        sandbox.assert.calledOnce(repositoryStub);
    });
});
