# Implementační dokumentace modulu _energetics_

## Záměr

Modul slouží k ukládání dat o spotřebě energií v budovách a jednotlivých bytech a prostorách pro jejich nájemnce

## Vstupní data

### Data nám jsou posílána

-   Přes Permission Proxy na Input Gateway data si bereme z webu https://portal.ptas.cz/portal, web scraping zajišťují dataři golemia, link gitlabu
    https://gitlab.com/operator-ict/golemio/extras/importers/-/blob/development/imports/energetics/scrape_ptas.py?ref_type=heads

        -   POST (/ptas/{id}) s id odběrného místa jako parametrem => RabbitMQ send Message `savePtasMeasurements` ([Data example](../test/input-gateway/data/ptas-hot-water-data-input.csv))

#### _POST /pre_

-   PRE posílá data na Input Gateway v JSON formátu (POST na /pre).
      - Očekávaný [formát dat](../src/schema-definitions/datasources/interfaces/IPreJsonInput.ts)
      - Na IG chodí větší soubory (např. 20M). Proto se na IG rozdělí podle měřidel a vyříznou se jen data, co zpracováváme.
        Dál to postupuje na IE přes rabbit do [ProcessPreEanMeasurementsTask](../src/integration-engine/commodity-providers/workers/tasks/ProcessPreEanMeasurementsTask.ts).

#### _POST /pre/csv_

-   Endpoint pro příjem historických dat PRE v CSV formátu (POST na /pre/csv).
    - Očekávaný [formát dat](../src/schema-definitions/datasources/interfaces/IPreCsvInput.ts)
    - V IG se CSV převede na stejný formát, jako /pre používá endpoint a dál na IE postupuje stejně.
      Tj. přes rabbit do [ProcessPreEanMeasurementsTask](../src/integration-engine/commodity-providers/workers/tasks/ProcessPreEanMeasurementsTask.ts).


### Data aktivně stahujeme

**Dostupní poskytovatelé**:

-   OICT Energetika
-   Unimonitor CEM API
    -   Vrtbovský palác
    -   Veolia měření P10 - vodoměry a elektroměry
-   PPAS (Pražská plynárenská) InternetAVE API
    -   Měření plynu pro P10 (Uživatelé AveM a AveV)
    -   Měření plynu pro zbytek Prahy pro odběratele různých kategorií A,C a MO
    -   Jedná se o odběratele různých kategorií odběru („A“ „C“ „MO“), konkrétně: účet Hl. m. Praha = „C“; Hlavní m. Praha = „MO“ a HlavníměstoPraha64581_1 = „A“
-   Azure Blob Storage
    -   Mapovací JSON pro měření přímo od poskytovatelů komodit

#### _OICT Energetika_

-   zdroj dat
    -   url: https://api.oictenergetika.cz (config.datasources.OICTEnergetikaApiUrl)
    -   parametry dotazu: config.datasources.OICTEnergetikaApiHeaders
    -   API dokumentace: https://api.oictenergetika.cz/swagger/index.html
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [datasources](../src/schema-definitions/datasources)
    -   příklad vstupnich dat: [fixtures](../test/integration-engine/oict-energetika/transformations/fixture)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.energeticsoict.fetchData (data za posledních 14 dní s parametrem `{ "targetDays": 14 }`)
            -   rabin `0 30 * * * *`
            -   prod `0 30 * * * *`
-   názvy rabbitmq front
    -   dataplatform.energeticsoict.fetchData
    -   dataplatform.energeticsoict.fetchConsumption

#### _Unimonitor CEM API - Vrtbovský palác_

-   zdroj dat
    -   url: https://cemapi.unimonitor.eu/api (config.datasources.UnimonitorCemApiEnergetics.url)
    -   parametry dotazu: config.datasources.UnimonitorCemApiEnergetics
    -   API dokumentace: https://cemapi.unimonitor.eu/doc/en/index.jsp
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [EnergeticsVpalac](../src/schema-definitions/providers/EnergeticsVpalac.ts)
    -   příklad vstupnich dat: [fixtures](../test/integration-engine/data)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.energetics.fetchVpalacXHoursData (data za poslední hodinu s parametrem `{ "targetHours": 1 }`)
            -   rabin `0 0 * * * *`
            -   prod `0 0 * * * *`
        -   cron.dataplatform.energetics.fetchVpalacXDaysData (data za posledních 14 dní s parametrem `{ "targetDays": 14 }`)
            -   rabin `0 0 8 * * *`
            -   prod `0 0 8 * * *`
-   názvy rabbitmq front
    -   dataplatform.energetics.fetchVpalacXHoursData
    -   dataplatform.energetics.fetchVpalacXDaysData

#### _Unimonitor CEM API - Veolia měření P10_

-   zdroj dat
    -   url: https://cemapi.unimonitor.eu/api (module.UnimonitorCemApiEnergetics.url)
    -   parametry dotazu: module.energetics.commodityProviders.cem_api_veolia_water (vodoměry), module.energetics.commodityProviders.cem_api_veolia_electro (elektroměry)
    -   API dokumentace: https://cemapi.unimonitor.eu/doc/index.html?lang=cs
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [Devices](../src/schema-definitions/datasources/commodity-providers/CemApiDevicesJsonSchema.ts), [Counters](../src/schema-definitions/datasources/commodity-providers/CemApiCountersJsonSchema.ts), [Measurements](../src/schema-definitions/datasources/commodity-providers/CemApiMeasurementsJsonSchema.ts)
    -   příklad vstupnich dat: viz [test](../test/integration-engine/commodity-providers/FetchVeoliaMeasurementsTask.test.ts)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.energeticscommodity.fetchMeasurementsFromProviders (data za poslední dny)
            -   rabin `0 41 * * * *` (s parametrem `{ "targetDays": 14 }`)
            -   prod `0 41 * * * *` (s parametrem `{ "targetDays": 31 }`)
-   názvy rabbitmq front
    -   dataplatform.energeticscommodity.fetchMeasurementsFromProviders
    -   dataplatform.energeticscommodity.fetchVeoliaMeasurements

#### _PPAS (Pražská plynárenská) InternetAVE API_

-   zdroj dat
    -   url: https://iave.ppas.cz/website/api (module.energetics.commodityProviders.ppas_ave_api.baseUrl)
    -   API dokumentace: https://www.ave-system.com/Content/download/AveRestApiDocumentation.pdf
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [Places](../src/schema-definitions/datasources/commodity-providers/AveApiPlacesJsonSchema.ts), [DeviceData](../src/schema-definitions/datasources/commodity-providers/AveApiDeviceDataJsonSchema.ts)
    -   příklad vstupnich dat: viz [test](../test/integration-engine/commodity-providers/FetchPpasMeasurementsTask.test.ts)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.energeticscommodity.fetchMeasurementsFromProviders (data za poslední dny)
            -   rabin `0 41 * * * *` (s parametrem `{ "targetDays": 14 }`)
            -   prod `0 41 * * * *` (s parametrem `{ "targetDays": 31 }`)
-   názvy rabbitmq front
    -   dataplatform.energeticscommodity.fetchMeasurementsFromProviders
    -   dataplatform.energeticscommodity.fetchPpasMeasurements

#### _PRE (Pražská energetika)_

-   zdroj dat
    -   ftp: ftp://ftp.wa-test.oict.cz:16421 (module.energetics.pre_electro_ftp)
-   formát dat
    -   protokol: ftp
    -   datový typ: csv
    -   validační schéma: [Data](../src/schema-definitions/datasources/PreElectricConsumptionJson.ts)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.energeticscommodity.fetchMeasurementsFromProviders (data za poslední dny)
            -   rabin `0 41 * * * *` (s parametrem `{ "targetDays": 14 }`)
            -   prod `0 41 * * * *` (s parametrem `{ "targetDays": 31 }`)
-   názvy rabbitmq front
    -   dataplatform.energeticscommodity.fetchPreElectroMeasurementsTask

#### _Azure Blob Storage - mapovací JSON pro měření_

-   zdroj dat
    -   url: https://golemgolemio.blob.core.windows.net (module.energetics.commodityProviders.staticData.baseUrl)
    -   parametry dotazu: path `/golem-energetics/meterssettings.json` (module.energetics.commodityProviders.staticData.metersettings.path)
    -   API dokumentace: žádná
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [MeterSettings](../src/schema-definitions/datasources/StaticMeterSettingsJsonSchema.ts)
    -   příklad vstupnich dat: viz [test](../test/integration-engine/commodity-providers/FetchMeasurementOptionsStaticDataTask.test.ts)
-   frekvence stahování
    -   cron definice:
        -   dataplatform.energeticscommodity.fetchMeasurementOptionsStaticData (data za poslední dny)
            -   rabin `0 53 6 * * *` (s parametrem `{}`)
            -   prod `0 53 6 * * *` (s parametrem `{}`)
-   názvy rabbitmq front
    -   dataplatform.energeticscommodity.fetchMeasurementOptionsStaticData

## Zpracování dat / transformace

Všechny tabulky se nachází ve schématu `energetics`

### _CommodityWorker_

Worker se stará o stahování dat (odečty, měřidla a počítadla) přímo od poskytovatelů komodit

#### _task: FetchMeasurementOptionsStaticDataTask_

Metoda sbírá mapovací JSON pro měření - odlehčený `metersettings.json` (bez secretů) z původní implementace energetické DB https://gitlab.com/operator-ict/oddeleni-vyvoje/energetika/spotreby-energii-mestskych-budov/stahovani-dat-meridel/-/blob/main/src/OIct.EnergyConsumptionGrabber/meterssettings.json. Za obsah a správu souboru odpovídá Stanislav Krňák

-   vstupní rabbitmq fronta
    -   název: dataplatform.energeticscommodity.fetchMeasurementOptionsStaticData
    -   TTL: 59 minut
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   Azure Blob Storage (golemgolemio.blob.core.windows.net/golem-energetics/meterssettings.json)
-   transformace
    -   žádné
-   data modely
    -   [StaticMeterSettingsModel](../src/schema-definitions/models/StaticMeterSettingsModel.ts) tabulka `commodity_static_meter_settings`

#### _task: FetchMeasurementsFromProvidersTask_

Metoda pouze rozesílá zprávy do ostatních front (fetchVeoliaMeasurements). Slouží k získání dat o spotřebě energií od poskytovatelů komodit za posledních X dní

-   vstupní rabbitmq fronta
    -   název: dataplatform.energeticscommodity.fetchMeasurementsFromProviders
    -   TTL: 1 hodina
    -   parametry: `{ targetDays }`
    -   validační schéma: [DataEntrypointSchema](../src/integration-engine/commodity-providers/workers/schema/DataEntrypointSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   dataplatform.energeticscommodity.fetchVeoliaMeasurements (vodoměry s parametry `{ providerType: cem_api_veolia_water, dateFrom, dateTo }`)
    -   dataplatform.energeticscommodity.fetchVeoliaMeasurements (elektroměry s parametry `{ providerType: cem_api_veolia_electro, dateFrom, dateTo }`)
-   datové zdroje
    -   žádné
-   transformace
    -   žádné
-   data modely
    -   žádné

#### _task: FetchVeoliaMeasurementsTask_

Metoda sbírá data o spotřebách od poskytovatele komodit - Veolia P10 (vodoměry a elektroměry)

-   vstupní rabbitmq fronta
    -   název: dataplatform.energeticscommodity.fetchVeoliaMeasurements
    -   TTL: 1 hodina
    -   parametry: `{ providerType, dateFrom, dateTo }`
    -   validační schéma: [VeoliaMeasurementsSchema](../src/integration-engine/commodity-providers/workers/schema/VeoliaMeasurementsSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   Unimonitor CEM API
-   transformace
    -   [CemMeasurementTransformation](../src/integration-engine/commodity-providers/transformations/CemMeasurementTransformation.ts)
-   data modely
    -   [StaticMeterSettingsModel](../src/schema-definitions/models/StaticMeterSettingsModel.ts) tabulka `commodity_static_meter_settings`
    -   [MeasurementModel](../src/schema-definitions/models/MeasurementsModel.ts) tabulka `consumption_energy_measurements`

#### _task: FetchPpasMeasurementsTask_

Metoda sbírá data o spotřebách od poskytovatele komodit - PPAS (Pražská plynárenská) P10

-   vstupní rabbitmq fronta
    -   název: dataplatform.energeticscommodity.fetchPpasMeasurements
    -   TTL: 1 hodina
    -   parametry: `{ providerType, dateFrom, dateTo }`
    -   validační schéma: [PpasMeasurementsSchema](../src/integration-engine/commodity-providers/workers/schema/PpasMeasurementsSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   PPAS (Pražská plynárenská) InternetAVE API
-   transformace
    -   [AveMeasurementTransformation](../src/integration-engine/commodity-providers/transformations/AveMeasurementTransformation.ts)
-   data modely
    -   [StaticMeterSettingsModel](../src/schema-definitions/models/StaticMeterSettingsModel.ts) tabulka `commodity_static_meter_settings`
    -   [MeasurementModel](../src/schema-definitions/models/MeasurementsModel.ts) tabulka `consumption_energy_measurements`

#### _task: FetchPreElectroMeasurementsTask_

Metoda sbírá data o spotřebách od poskytovatele komodit - PRE (Pražská energetika)

-   vstupní rabbitmq fronta
    -   název: dataplatform.energeticscommodity.fetchPreElectroMeasurementsTask
    -   TTL: 1 hodina
    -   parametry: `{ dateFrom, dateTo }`
    -   validační schéma: [MeasurementsSchema](../src/integration-engine/commodity-providers/workers/schema/MeasurementsSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   PRE (Pražská energetika) FTP
-   transformace
    -   [PreElectroTransformation](../src/integration-engine/commodity-providers/transformations/PreElectroTransformation.ts)
-   data modely
    -   [StaticMeterSettingsModel](../src/schema-definitions/models/StaticMeterSettingsModel.ts) tabulka `commodity_static_meter_settings`
    -   [MeasurementModel](../src/schema-definitions/models/MeasurementsModel.ts) tabulka `consumption_energy_measurements`

#### _task: ProcessPreEanMeasurementsTask_

Přijímá data o spotřebách od PRE z IG (viz. nahoře)

-   vstupní rabbitmq fronta
    -   název: dataplatform.energeticscommodity.processPreEanMeasurements
    -   TTL: 1 hodina
    -   parametry: [IPreEanMeasurement](../src/integration-engine/commodity-providers/workers/interfaces/IPreEanMeasurement.ts)
    -   validuje se na IG
-   závislé fronty nejsou
-   datový zdroj je PRE IG endpoint (viz. nahoře)
-   transformace
    -   [PreMeasurementTransformation](../src/integration-engine/commodity-providers/transformations/PreMeasurementTransformation.ts)
    -   Vstupní hodnoty se stejným datem měření se sčítají. Duplicitní timestamp vzniká při přechodu na zimní čas.
-   data modely
    -   odsud si bere statická data: [StaticMeterSettingsModel](../src/schema-definitions/models/StaticMeterSettingsModel.ts) tabulka `commodity_static_meter_settings`
    -   sem ukládá: [MeasurementModel](../src/schema-definitions/models/MeasurementsModel.ts) tabulka `consumption_energy_measurements`

#### _task: FetchPtasWebScrapedMeasurementsTask_

Pomocí tohoto tasku zpracováváme a ukládáme data z input gateway z fronty savePtasMeasurements. Podle původního scraperu z C# bereme pouze teplo horké vody.
Aby nedocházelo ke konfliktům měřidel, filtrují se měřidla dle seznamu uloženém ve statických datech.

-   vstupní rabbitmq fronta
    -   název: dataplatform.energeticscommodity.savePtasMeasurements
    -   TTL: 1 hodina
    -   input: parsované csv příklad ([Data example](../test/input-gateway/data/ptas-hot-water-data-input.csv))
    -   validační schéma: [PtasWebScrapingJsonSchema](../src/schema-definitions/datasources//PtasWebScrapingJsonSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   PTAS Pražská Teplárenská
-   transformace
    -   [PtasWebMeasurementTransformation](../src/integration-engine/commodity-providers/transformations/PtasWebMeasurementTransformation.ts.ts)
-   data modely
    -   [StaticMeterSettingsModel](../src/schema-definitions/models/StaticMeterSettingsModel.ts) tabulka `commodity_static_meter_settings`
    -   [MeasurementModel](../src/schema-definitions/models/MeasurementsModel.ts) tabulka `consumption_energy_measurements`

### _OictEnergetikaWorker_

Worker se stará o stahování dat (spotřeba, seznamy budov a meřidel) z datového zdroje OICT Energetika

#### _task: FetchDataTask_

Metoda pouze rozesílá zprávy do ostatních front (fetchConsumption)

-   vstupní rabbitmq fronta
    -   název: dataplatform.energeticsoict.fetchData
    -   TTL: 29 minut
    -   parametry: `{ targetDays }`
    -   validační schéma: [FetchDataSchema](../src/integration-engine/schema/oict-energetika/FetchDataSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   dataplatform.energeticsoict.fetchConsumption
-   datové zdroje
    -   žádné
-   transformace
    -   žádné
-   data modely
    -   žádné

#### _task: FetchConsumptionTask_

Metoda sbírá data o spotřebách od různých poskytovalů API (consumption) a ze systémů Dot Controls (visapp)

-   vstupní rabbitmq fronta
    -   název: dataplatform.energeticsoict.fetchConsumption
    -   TTL: 10 hodin
    -   parametry: `{ dateFrom, dateTo }`
    -   validační schéma: [FetchDataSchema](../src/integration-engine/schema/oict-energetika/FetchDataSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   OICT Energetika (data/consumption, data/visapp)
-   transformace
    -   [ConsumptionTransformation](../src/integration-engine/transformations/oict-energetika/ConsumptionTransformation.ts)
-   data modely
    -   ConsumptionModel `consumption_energy_consumption`

#### _task: FetchAccessConfigurationTask_

Task aktualizuje lookup tabulku pro limitaci přístupu k output API.

-   vstupní rabbitmq fronta
    -   název: dataplatform.energeticsoict.fetchAccessConfiguration
    -   TTL: 10 minutes
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   blob storage
        - rabín: https://rabingolemio.blob.core.windows.net/rabin-energetics/access_configuration.json
        - golem: https://golemgolemio.blob.core.windows.net/golem-energetics/access_configuration.json
-   transformace
    -   není
-   data modely
    -   AccessLimitationModel `access_limitation`

### _EnergeticsVpalacWorker_

Worker se stará o stahování dat (odečty, měřidla a počítadla) z datového zdroje Unimonitor CEM API - Vrtbovský palác

#### _fetchXHoursData()_

Metoda sbírá aktuální data

-   vstupní rabbitmq fronta
    -   název: dataplatform.energetics.fetchVpalacXHoursData
    -   TTL: 1 hodina
    -   parametry: `{ targetHours }`
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   CEM API
-   transformace
    -   [transformations](../src/integration-engine/transformations)
-   data modely
    -   VpalacMeasurementModel `vpalac_measurement`
    -   VpalacMeasuringEquipmentModel `vpalac_measuring_equipment`
    -   VpalacMeterTypeModel `vpalac_meter_type`
    -   VpalacTypeMeasuringEquipmentModel `vpalac_type_measuring_equipment`
    -   VpalacUnitsModel `vpalac_units`

#### _fetchXDaysData()_

Metoda sbírá historická data

-   vstupní rabbitmq fronta
    -   název: dataplatform.energetics.fetchVpalacXDaysData
    -   TTL: 1 den
    -   parametry: `{ targetDays }`
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   CEM API
-   transformace
    -   [transformations](../src/integration-engine/transformations)
-   data modely
    -   VpalacMeasurementModel `vpalac_measurement`
    -   VpalacMeasuringEquipmentModel `vpalac_measuring_equipment`
    -   VpalacMeterTypeModel `vpalac_meter_type`
    -   VpalacTypeMeasuringEquipmentModel `vpalac_type_measuring_equipment`
    -   VpalacUnitsModel `vpalac_units`

## Uložení dat

-   typ databáze
    -   PSQL
-   databázové schéma
    -   OICT Energetika <br/>
        ![consumption energy diagram](./assets/consumption_energy_erd.png)
    -   Vrtbovský palác <br/>
        ![vpalac diagram](./assets/vpalac_erd.png)
    -   Poskytovatelé komodit <br/>
        ![commodity diagram](./assets/commodity_erd.jpg)
-   retence dat
    -   data si uchováváme

## Output API

### Obecné

-   OpenAPI v2 dokumentace
    -   zdrojový soubor: [openapi-output.yaml](./openapi-output.yaml)
    -   rabin: https://rabin.golemio.cz/v2/docs/openapi/
    -   golem: https://api.golemio.cz/v2/docs/openapi/
    -   přístup je řízen pomocí interního parametru `accessLimit`, který musí být vždy nastaven a omezen z permission proxy
        - při obdržení dotazu na API se stáhnou povolené organizace z tabulky `access_limit` a omezí se podle toho dotazy do databáze
        - pokud je `accessLimit` nastaven na `admin`, tak se vždy dotazuje na všechny organizace

### API

#### _/v2/energetics/buildings_

#### _/v2/energetics/buildings/:id_

-   zdrojové tabulky
    -   `consumption_energy_buildings_primary`
    -   `consumption_energy_buildings_secondary`

#### _/v2/energetics/organizations_

#### _/v2/energetics/organizations/:id_

-   zdrojové tabulky
    -   organizations
    -   organization_responsible_users
    -   organizations_buildings
    -   consumption_energy_buildings

#### _/v2/energetics/devices_

#### _/v2/energetics/devices/:id_

-   zdrojové tabulky
    -   consumption_energy_devices

#### _/v2/energetics/measurements_

#### _/v2/energetics/measurements/monthly-reading_

-   zdrojové tabulky
    -   consumption_energy_measurements
