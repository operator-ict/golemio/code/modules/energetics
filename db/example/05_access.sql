INSERT INTO organizations_buildings (organization_id,building_id) VALUES	 (1,5);


INSERT INTO access_limitation (group_name, organization_ids, created_at, updated_at) VALUES('praha-10', '{64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,85,87,88,89,91,93,94,95,96,97,84,86,90,92}', '2024-06-24 10:47:50.349', '2024-06-24 13:08:00.920');
INSERT INTO access_limitation (group_name, organization_ids, created_at, updated_at) VALUES('test', '{1}', '2024-06-24 10:47:50.349', '2024-06-24 13:08:00.920');
INSERT INTO access_limitation (group_name, organization_ids, created_at, updated_at) VALUES('test2', '{2,3}', '2024-06-24 10:47:50.349', '2024-06-24 13:08:00.920');
INSERT INTO access_limitation (group_name, organization_ids, created_at, updated_at) VALUES('testAll', '{1,2,3}', '2024-06-24 10:47:50.349', '2024-06-24 13:08:00.920');
INSERT INTO access_limitation (group_name, organization_ids, created_at, updated_at) VALUES('removed', '{1,2,3}', '2024-06-24 10:47:50.349', '2024-06-24 13:08:00.920');