INSERT INTO organization_responsible_users (id, first_name, last_name, "position", phone, mail, company, organization_id) VALUES
(1, 'Milan', 'Vorel', 'ředitel', NULL, 'milan.vorel@skolahostivar.cz', NULL, 1),
(2, 'Zdeněk', 'Kočárek', 'vedoucí technické správy', NULL, 'zdenek.kocarek@skolahostivar.cz', NULL, 1),
(3, 'Jaroslav', 'Malý', 'správa budov', NULL, 'jaroslav.maly@skolahostivar.cz', NULL, 1),
(4, 'Jaroslav', 'Mervínský', 'ředitel', NULL, 'reditel@gvp.cz', NULL, 2),
(5, 'Martin', 'Horyna', 'zástupce ředitele', NULL, 'horyna@gvp.cz', NULL, 2),
(6, 'Milan', 'Slanina', 'školník', NULL, 'slanina@gvp.cz', NULL, 2),
(7, 'Filip', 'Magram', 'ředitel', NULL, 'reditel@gmhs.cz', NULL, 3),
(8, 'Zuzana', 'Koutníková', 'ekonomka školy', NULL, 'ekonom@gmhs.cz', NULL, 3),
(9, 'Blanka', 'Blahová', 'správa budov', NULL, 'spravce@gmhs.cz', NULL, 3),
(10, 'Julius', 'Olšiak', 'referent správy budov (školník)', NULL, NULL, NULL, 3);

INSERT INTO organizations (id, "name", "label", address_street, address_house_number, address_city, address_country, address_mail, address_phone, address_web_address, category, created_by_id, grafana_url) VALUES
(1, 'SŠAI Weilova', 'SO01 SŠAI', 'Weilova', '12704', 'Praha 10 - Hostivař', 'Česká Republika', 'mailbox@skolahostivar.cz', '242 456 100', 'http://www.skolahostivar.cz', 'Školství', 64, 'http://10.200.0.73:3000/d/l8OJLI2ik/01-ss-automobilni-a-informatiky-weilova?orgId=2'),
(2, 'Gymnázium Na Vítězné pláni', 'SO02 GVP', 'Na Vítězné pláni', '1160', 'Praha 4, 140 00', 'Česká Republika', 'mail@gvp.cz', '261 109 611', 'http://www.gvp.cz/', 'Školství', 64, 'http://10.200.0.73:3000/d/e7zcnRYmk/02-gymnazium-na-vitezne-plani?orgId=2'),
(3, 'Gymnázium a Hudební škola hlavního města Prahy ', 'SO03 GYHU', 'Komenského náměstí ', '400', 'Praha 3, 130 00', 'Česká Republika', 'sekretariat@gmhs.cz', '221 434 711', 'http://www.gmhs.cz/', 'Školství', 64, 'http://10.200.0.73:3000/d/2um5VgYik/03-gymnazium-a-hudebni-skola-hlavniho-mesta-prahy?orgId=2');

INSERT INTO organizations_buildings (organization_id, building_id) VALUES
(1,	7),
(1,	38),
(1,	37),
(1,	36),
(2,	10),
(3,	11);
