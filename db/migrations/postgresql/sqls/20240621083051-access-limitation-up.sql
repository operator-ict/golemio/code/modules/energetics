CREATE TABLE access_limitation (
	group_name varchar(50) NOT NULL,
	organization_ids _int4 NOT NULL,
    created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL,
	CONSTRAINT access_limitation_pk PRIMARY KEY (group_name)
);
