CREATE TABLE consumption_energy_buildings_primary (
    id int4 NOT NULL,
	description varchar(100) , 
	building_name varchar(100) , 
	building_address_code varchar(100) , 
	building_label varchar(100) , 
	main_use varchar(100) ,
	secondary_use varchar(100) ,
    year_of_construction int2 ,
    create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT consumption_primary_energy_buildings_pkey PRIMARY KEY (id)
);
 
CREATE TABLE consumption_energy_buildings_secondary (
	id int4 NOT NULL,
	building_address_code varchar(100) ,
	building_label varchar(100) ,
	current_note varchar(100) ,
	method_of_protection varchar(100) ,
	built_up_area real , --n
	heated_bulding_volume int8 ,
	students_count int8 , --n
	employees_count int8 ,--n
	classrooms_count int4 ,--n
	beds_count int8 ,--n
	eno_id varchar(100) ,
	csu_code varchar(100) ,
	ku_code varchar(100) ,
	allotment_number varchar(100),--n
	registration_unit varchar(100) ,
	energetic_management varchar(100) ,
	opening_hours varchar(100) ,
	weekend_opening_hours varchar(100) ,
     location geometry, -- lat and long
	address_street varchar(100) ,
	address_house_number varchar(100) ,
	address_city varchar(100) ,
	address_country varchar(100) ,
	address_mail varchar(100) ,
	address_phone varchar(100) ,
	address_web_address varchar(100) ,
	penb_penbnumber int4 ,--n
	penb_issue_date timestamp,
	penb_total_building_envelope_area  real ,--n
	penb_volume_factor_of_avshape varchar(100) ,
	penb_total_energy_reference_area  real ,--n
	penb_total_provided_energy real ,--n
	penb_total_provided_energy_category varchar(50) ,
	penb_primary_non_renewable_energy real,--n
	penb_primary_non_renewable_energy_category varchar(50) ,
	penb_building_envelope real ,--n
	penb_building_envelope_category varchar(100) ,
	penb_heating real ,--n
	penb_heating_category varchar(50) ,
	penb_cooling real ,--n
	penb_cooling_category varchar(50) ,
	penb_ventilation real ,--n
	penb_ventilation_category varchar(50) ,
	penb_humidity_adjustment real ,--n
	penb_humidity_adjustment_category varchar(100) ,
	penb_warm_water real ,--n
	penb_warm_water_category varchar(50) ,
	penb_lighting real ,--n
	penb_lighting_category varchar(50) ,
	energy_audits_energy_audit  varchar(100) ,
	energy_audits_earegistration_number varchar(100),--n
	energy_audits_created_at timestamp ,
	building_envelope_side_wall_prevailing_construction varchar(100) ,
	building_envelope_side_wall_area  real ,--n
	building_envelope_side_wall_heat_insulation varchar(50) ,
	building_envelope_side_wall_year_of_adjustment varchar(50)  , --year but its text (po roce xxxx)
	building_envelope_side_wall_technical_condition varchar(50) ,
	building_envelope_filling_of_hole_construction varchar(100) ,
	building_envelope_filling_of_hole_area  real ,--n
	building_envelope_filling_of_hole_year_of_adjustment varchar(50)  ,
	building_envelope_filling_of_hole_technical_condition varchar(50) ,
	building_envelope_roof_construction varchar(50) ,
	building_envelope_roof_area real  ,--n
	building_envelope_roof_thermal_insulation varchar(50) ,
	building_envelope_roof_year_of_adjustment varchar(50)  ,
	building_envelope_roof_technical_condition varchar(50) ,
	building_envelope_floor_of_the_lowest_heated_floor_construction varchar(100) ,
	building_envelope_floor_of_the_lowest_heated_floor_area real ,--n
	floor_of_the_lowest_heated_floor_thermal_insulation varchar(50) ,
	building_envelope_floor_of_the_lowest_heated_floor_year_of_adju varchar(100) ,
	floor_of_the_lowest_heated_floor_technical_condition varchar(50) ,
	fuel_and_energy_coal boolean ,
	fuel_and_energy_gas boolean ,
	fuel_and_energy_electricity boolean ,
	fuel_and_energy_czt boolean ,
	fuel_and_energy_oze boolean ,
	fuel_and_energy_other boolean ,
	technical_equipment_heating_main_source_of_heat varchar(100) ,
	technical_equipment_heating_heat_percentage int2 ,--n
	technical_equipment_heating_secondary_source_of_heat varchar(100) ,
	technical_equipment_heating_heating_system varchar(100) ,
	technical_equipment_heating_year varchar(50) , --year but its text (po roce xxxx)
	technical_equipment_heating_technical_condition varchar(100) ,
	technical_equipment_cooling_cooling_system varchar(100) ,
	technical_equipment_cooling_cooling_area_percentage int2,
	technical_equipment_cooling_year varchar(50) ,
	technical_equipment_cooling_technical_condition varchar(100) ,
	technical_equipment_ventilation_ventilation varchar(100) ,
	technical_equipment_ventilation_year varchar(50) ,
	technical_equipment_ventilation_technical_condition varchar(100) ,
	technical_equipment_humidity_adjustment_humidity_adjustment varchar(100) ,
	technical_equipment_humidity_adjustment_year varchar(100) ,
	technical_equipment_humidity_adjustment_technical_condition varchar(100) ,
	technical_equipment_hot_water_predominant_way_of_heating_tv varchar(100) ,
	technical_equipment_hot_water_hot_water_source varchar(100) ,
	technical_equipment_hot_water_year varchar(50) ,
	technical_equipment_hot_water_technical_condition varchar(100) ,
	technical_equipment_lighting_lighting varchar(100) ,
	technical_equipment_lighting_year varchar(50) ,
	technical_equipment_lighting_technical_condition varchar(100) ,
	technical_equipment_lighting_other_technological_elements varchar(100) ,
	technical_equipment_lighting_measurement_method varchar(100) ,
	oze_energy_production_solar_energy_photovoltaic varchar(100) ,
	oze_energy_production_solar_energy_photothermal varchar(100) ,
	oze_energy_production_integrated_turbines_wind_energy varchar(100) ,
	oze_energy_production_heat_pump varchar(100) ,
	waste_and_emissions_solid_waste_production varchar(100) ,
	waste_and_emissions_tied_co2_emissions varchar(100) ,
	waste_and_emissions_sox_emissions varchar(100) ,
	waste_and_emissions_operating_co_2emissions varchar(100) ,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT consumption_secondary_energy_buildings_pkey PRIMARY KEY (id)
);