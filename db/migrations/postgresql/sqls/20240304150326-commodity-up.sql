create table commodity_static_meter_settings (
    "provider" varchar(30) not null,
    resource_type varchar(30) not null,
    "data" json not null,

     -- audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    constraint static_data_pk primary key ("provider", resource_type)
);

COMMENT ON COLUMN commodity_static_meter_settings."data" IS 'Mapovací JSONy pro měřidla podle provideru (např Veolia vodoměry)';

CREATE TABLE consumption_energy_measurements (
	"timestamp" timestamptz NOT NULL,
	value numeric(30, 15) NOT NULL,
	addr varchar(255) NOT NULL,
	var varchar(255) NOT NULL,
	"type" varchar(255) NULL,
	meter varchar(255) NOT NULL,
    "source" varchar(20) NULL,
	measurement_category varchar(50) NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT consumption_energy_measurements_pkey PRIMARY KEY ("timestamp", addr, var, meter)
);

create index consumption_energy_measurements_source_idx on consumption_energy_measurements (source);
create index consumption_energy_measurements_measurement_category_idx on consumption_energy_measurements (measurement_category);
