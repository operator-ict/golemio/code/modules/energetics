ALTER TABLE consumption_energy_measurements DROP CONSTRAINT consumption_energy_measurements_pkey;
ALTER TABLE consumption_energy_measurements ADD CONSTRAINT consumption_energy_measurements_pk PRIMARY KEY ("timestamp",addr,var);
