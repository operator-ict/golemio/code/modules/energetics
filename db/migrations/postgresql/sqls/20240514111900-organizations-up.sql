create table organizations (
    id int4 not null,
    name varchar(100) null,
    label varchar(50) null,
    address_street varchar(100) null,
    address_house_number varchar(100) null,
    address_city varchar(100) null,
    address_country varchar(100) null,
    address_mail varchar(100) null,
    address_phone varchar(50) null,
    address_web_address varchar(100) null,
    category varchar(80) null,
    created_by_id int4 null,
    grafana_url varchar(255) null,

    CONSTRAINT organizations_pk PRIMARY KEY (id)
);

create table organizations_buildings (
    organization_id int4 not null,
    building_id int4 not null,

    CONSTRAINT organizations_buildings_pk PRIMARY KEY (organization_id, building_id)
);

create table organization_responsible_users (
    id int4 not null,
    organization_id int4 not null,
    first_name varchar(100) null,
    last_name varchar(100) null,
    position varchar(100) null,
    phone varchar(50) null,
    mail varchar(50) null,
    company varchar(100) null,

    CONSTRAINT organization_responsible_users_pk PRIMARY KEY (id)
);
