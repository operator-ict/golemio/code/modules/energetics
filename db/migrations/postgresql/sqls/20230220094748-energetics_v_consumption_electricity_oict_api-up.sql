CREATE OR REPLACE VIEW 
	energetics.v_consumption_electricity_oict_api
AS
SELECT 
	date_trunc('day'::text, c.time_utc::date::timestamp with time zone) AS den,
    c.time_utc at time zone 'Europe/Prague' as time_utc,
    array_to_string((string_to_array(c.addr, '/'))[:3], '/') as addr,
    'kWh'::text AS unit,
    c.value * 1.04 AS value_num,
    row_number() OVER (PARTITION BY c.addr, (date_trunc('month'::text, c.time_utc::date::timestamp with time zone)) ORDER BY c.value DESC) AS ran
FROM 
    energetics.consumption_energy_consumption c
WHERE 
    c.var::text = 'EFwActi' 
  	and type = 'provider_value' 
  	and time_utc > '2019-01-01'
  	and addr like any (array['/2.3/EF1%', '/2.7/EF1%', '/2.8/EF1%', '/2.9/EF1%', '/2.10.1/EF1%']);
